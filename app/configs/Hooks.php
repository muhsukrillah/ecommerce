<?php
// admin menu
add_action('admin_menu', function($menu){
    $menu->add_menu('dashboard', 'Dashboard', 'fa fa-dashboard', ['href' => admin_url('index/')]);
    if(\sasak\User::$role->can('read_product')){
        $menu->add_menu('product', 'Produk', 'fa fa-bookmark', ['href' => admin_url('product/index/')]);
        $menu->add_child('all-product', 'Semua Produk', 'fa fa-bookmark', ['href' => admin_url('product/index/')], 'product');
        if(\sasak\User::$role->can('create_product')){
            $menu->add_child('new-product', 'Tambah Produk', 'fa fa-plus', ['href' => admin_url('product/add/')], 'product');
        }
        if(\sasak\User::$role->can('create_product_category')){
            $menu->add_child('product-category', 'Kategori Produk', 'fa fa-tags', ['href' => admin_url('product/category/')], 'product');
        }
        if(\sasak\User::$role->can('create_product_brand')){
            $menu->add_child('product-brand', 'Brand', 'fa fa-leaf', ['href' => admin_url('product/brand/')], 'product');
        }
    }
    if(\sasak\User::$role->can('read_order')){
        $menu->add_menu('order', 'Pesanan', 'fa fa-shopping-cart', ['href' => admin_url('order/index/')]);
        $menu->add_child('all-order', 'Semua Pesanan', 'fa fa-list', ['href' => admin_url('order/index/')], 'order');
        if(\sasak\User::$role->can('create_order')){
            $menu->add_child('new-order', 'Tambah Order', 'fa fa-plus', ['href' => admin_url('order/tambah/')], 'order');
        }
    }
    if(\sasak\User::$role->can('read_invoice')){
        $menu->add_menu('invoice', 'Tagihan', 'fa fa-tag', ['href' => admin_url('invoice/index/')]);
        $menu->add_child('all-invoice', 'Data Tagihan', 'fa fa-tag', ['href' => admin_url('invoice/index/')], 'invoice');
        $menu->add_child('invoice-payment', 'Pembayaran Tagihan', 'fa fa-check', ['href' => admin_url('invoice/payment')], 'invoice');
    }
    if(\sasak\User::$role->can('read_post')){
        $menu->add_menu('post', 'Tulisan', 'fa fa-pencil', ['href' => admin_url('post/index/')]);
        $menu->add_child('all-post', 'Semua Tulisan', 'fa fa-pencil', ['href' => admin_url('post/index/')], 'post');
        if(\sasak\User::$role->can('create_post')){
            $menu->add_child('new-post', 'Tambah Tulisan', 'fa fa-plus', ['href' => admin_url('post/tambah/')], 'post');
        }
    }
    if(\sasak\User::$role->can('read_user')){
        $menu->add_menu('user', 'Pengguna', 'fa fa-users', ['href' => admin_url('user/index/')]);
        $menu->add_child('user-index', 'Semua Pengguna', 'fa fa-users', ['href' => admin_url('user/index/')], 'user');
        if(\sasak\User::$role->can('create_user')){
            $menu->add_child('user-add', 'Tambah Pengguna', 'fa fa-plus', ['href' => admin_url('user/tambah/')], 'user');
        }
        $menu->add_child('user-access', 'Akses Pengguna', 'fa fa-key', ['href' => admin_url('user/role/')], 'user');
    }
    $menu->add('profil', 'Profil Saya', 'fa fa-user', ['href' => admin_url('profile/')]);
    $menu->add('bantuan', 'Bantuan', 'fa fa-question-circle', ['href' => admin_url('bantuan/')]);
});

/**
 * Breadcrumb
 */
add_action('breadcrumb', function($b){
    $b->_set_template('wrap_start', '<ol class="breadcrumb">');
    $b->_set_template('items', '<li><a href="{link}"><i class="{icon}"></i> {title}</a></li>');
    $b->_set_template('active_item', '<li><span>{title}</span></li>');
    $b->_set_template('separator', '');
    $b->_set_template('wrap_end', '</ol>');
});