<?php
/**
 *  Database setting
 * -------------------
 */
/**
 * database driver / library
 * untuk saat ini yang normal adalah PDO
 * sendangkan MySQLi masih dalam pengembangan
 *
 * Default              pdo
 * Sifat                Required / Wajib
 */
define("DB_DRIVER", "pdo"); // pdo || mysqli
/**
 * Database Host        nama host database..
 * --------------
 *
 * Default              localhost
 * Sifat                Required / Wajib
 */
define("DB_HOST", "localhost");
/*
 * Database User        Nama pengguna database..
 * -------------
 *
 * Default              root
 * Sifat                Required / Wajib
 */
define("DB_USER", "root");
/**
 * Database User Password sebagai kata sandi pengguna database
 * ----------------------
 *
 * Default              '' / kosong / tanpa password
 * Sifat                Required / Wajib
 */
define("DB_PASS", "");
/**
 * Database Name        Nama database
 * -------------        
 * 
 * Default              no-default
 * Sifat                Required / Wajib
 */
define("DB_NAME", "ecomerce");