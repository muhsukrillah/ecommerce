<?php
/**
 * Apps configurations
 *
 * Dibuat Oleh              Muh. Sukrillah
 * Url                      http://www.sukrillah.xyz
 * Versi                    1.0
 ************************************************************/
/************************************************************
 * define applications environment
 *
 * @conts                   ENVIRONMENT
 * @type                    string
 * @value                   developement|testing|production
 * @default                 development
 */
define("ENVIRONMENT", "development");
/**
 * define the base path / public path
 * 
 * @const               BASE_PATH
 * @type                string path
 * @value               ROOT_PATH . 'public/' [default]
 */
define("BASE_PATH", ROOT_PATH . 'public_html/');
/**
 * Asset Path           lokasi file aset-aset seperti gambar, css, js, dll..
 * ----------
 *                      * Harus di akhiri dengan tanda => /
 *
 * @const               ASSETS_PATH
 * @type                string path
 * @value               BASE_PATH . 'assets/' [default]
 */
define("ASSETS_PATH", BASE_PATH .'assets/');
/** ================================================== **/
// Enable Database
define("USE_DATABASE", TRUE);
/**
 * Security Key         sebagai scurity salt untuk password user & token
 * ------------
 *
 * Default              no-default
 * Sifat                Required / Wajib
 */
define("SECURITY_KEY", '1098(*#)(jkfds!@$#@(JKFD0;3(!@##$_(-0fdddedda93+=(=-');
/**
 * Timezone             Timezone / Sumber Waktu
 * --------
 * 
 * Default              Asia/Jakarta    => Waktu Indonesia
 * Sifat                Opsional / Tidak Wajib
 */
define("TIME_ZONE", "Asia/Jakarta");
/**
 * define logs path of app
 *
 * @const               LOGS_PATH
 * @type                string path
 * @value               APP_PATH . 'logs/' [default]
 */
define("LOG_PATH", APP_PATH . 'logs/');
/**
 * Loging Config            untuk kebutuhan log sistem
 * -------------
 * Default                  TRUE
 * Sifat                    Opsional / Tidak Wajib
 *
 * @const                   LOG_ENABLED
 * @type                    boolean
 * @value                   TRUE
 * -------------------------------------------------------- */
define("LOG_ENABLED", 0);
/**
 * ============================================================
 * Controller Setting
 * ============================================================
 */
/**
 * Site Titile          Judul Program
 * -----------
 * 
 * Default              no-default
 * Sifat                Required / Wajib
 */
define("SITE_TITLE", 'Home Mart');
/**
 * admin directory      nama folder controller admin
 * ---------------
 * 
 * Default              no-default
 * Sifat                tidak wajib / optional
 */
define("ADMIN_DIR", "admin");
/**
 * Base URL             URL induk / URL Utama
 * --------
 *                      Jika diberikan nilai, sistem akan menggunakan URL pilihan ini
 *                      Jika tidak tersedia / kosong. Sistem akan membuat secara otomatis
 *
 * Default              no-default
 * Sifat                Opsional / Tidak Wajib
 **/
//define("BASE_URL", "http://localhost/si-pegawai/");
/**
 * 
 * Default Controller   Pengontrol utama
 * ------------------
 *
 * Default              index       => app/controllers/Index.php
 * Sifat                Opsional / Tidak Wajib
 */
define("DEFAULT_CONTROLLER", "index@index");