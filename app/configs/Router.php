<?php
/**
 * Routes
 *
 * Dibuat Oleh 			: Muh Sukrillah
 * Url 					: http://www.sukrillah.xyz
 * Versi 				: 1.0
 */
namespace app;

\sasak\Router()->all('/', 'index@index');

//\sasak\Router()->all('/{any}', 'Index@view_product');

\sasak\Router()->all('/x', function(){
	return \sasak\View::load('about');
});

\sasak\Router()->all('/robots.txt', function(){
	header("Content-Type: text/plain");
	ob_start();
	echo 'User-agent:*' ."\n";
	echo 'Disallow:/admin/' ."\n";
	echo 'Dsallow:/user/' ."\n";
	echo 'Disallow:/account/login' ."\n";
	echo 'Disallow:/account/register' ."\n";
	echo 'Allow:/' ."\n";
	return ob_get_clean();
});

\sasak\Router()->get('/admin/', ['scope', 'view', function($scope, $view){
	$code = '<pre style="text-align:left">' . "\n";
	$code.= "&lt;?php\n";
	$code.= "\sasak\Router()->get('/admin/', ['scope', 'view', function(\$scope, \$view){\n";
	$code.= "\t\$scope->data = &quot;Hello, Sasak Frmework&quot;;\n";
	$code.= "\treturn \$view::load('about');\n";
	$code.= "});\n";
	$code.= "?&gt;</pre>";

	$scope->data = $code;

	return $view::load('about');
}]);

\sasak\Router()->get('/hhh', '\app\controller\Index@view_product');

\sasak\Router()->get('/warid/{any}', ['response', 'param', function($res, $params){
	$res::send($params[0] .' '. uri()->segment(1));
	//return $view::load('about');
}]);

\sasak\Router()->get('/selamat_datang', function($a = '', $b = ''){
	$nama = \sasak\Input()->get('nama');
	$segment = uri()->segment(1);
	return 'Selamat Datang <b>'. $nama .'</b>';
});

\sasak\Router()->get('/halo/test/lorem', function(){
	$i = \sasak\Input()->get('segment');
	return print_r(uri()->segment($i), true);
});

\sasak\Router()->get('/xxx/{id}', ['param', 'View', function($param, $view){
	return 'viewing user with id => '. $param[0];
	//return $view::load('about');
}]);

\sasak\Router()->get('/produk/{any}.html', ['param', 'loader', function($product_name, $loader){
	\app\Customer::auth(NULL, FALSE);

	$product = $loader->load->object('product');
	$data['product'] = $product->get_by(['product_name' => $product_name]);	
	$product_image = $loader->load->object('product_image');
	$images = $product_image->get_by(['id_product' => $data['product']['id_product']]);
	$img_featured = '';
	$img = [];
	foreach($images as $k => $v) {
        if(!is_array($v)){
            if($k == 'url'){
                $img[] = array('url' => $v);
                $img_featured = $v;
            }
            if($k == 'thumbnail_url' && !empty($v)){
                $img_featured = $v;
            }
            continue;
        }

        $img[] = array(
            'url' => $v['url'],
            'featured_url' => $v['featured_url'],
            'thumbnail_url' => $v['thumbnail_url'],
            'miniatur_url' => str_replace(['.jpg', '.png', '.gif'], ['_60x60.jpg', '_60x60.png', '_60x60.gif'], $v['url'])
        );
        if(!empty($img_featured)) continue;
        $img_featured = isset($v['featured_url']) ? $v['featured_url'] : $v['url'];
    }
    $data['product']['featured_image'] = $img_featured;
    $data['product']['images'] = $img;
    // brand
    $brand = $loader->load->object('brand');
    $brand_data = $brand->get_by(['id_brand' => $data['product']['id_brand']]);
    $data['product']['brand'] = $brand_data['brand_name'];
    // category
    $category = $loader->load->object('category');
    $category_data = $category->get_by(['id_category' => $data['product']['id_category']]);
    // breadcrumb
    $breadcrumb = $loader->load->library('breadcrumb', ['return' => TRUE]);
    $breadcrumb->set_use_separator(FALSE);
    $breadcrumb->add(['title' => 'Home', 'link' => base_url(), 'icon' => '<i class="fa fa-home"></i> ']);
    if($category_data['parent_id']){
    	//$cats[] = $category_data;
    	$cats = $category->get_parent($category_data);
    }else{
    	$cats[] = $category_data;
    }
    //$cats = array_reverse($cats);
    foreach($cats as $key => $value){
    	$breadcrumb->add(['title' => $value['category_name'], 'link' => base_url('category/'. $value['slug'] .'/')]);
    }
    $breadcrumb->add(['title' => $data['product']['product_title']]);
    //$breadcrumb->add
    $data['breadcrumb'] = $breadcrumb;
    // load title 
	$title = $loader->load->library('lib_title', ['return' => TRUE]);
	$title->set($data['product']['product_title']);
	$data['title'] =& $title;
	// assets
    $asset = $loader->load->library('Assets', ['return' => TRUE]);
	$asset->setPath(BASE_PATH)->setBaseUrl(asset_url());
	$asset
	->addCss(['url' => 'css/material-colors.css'])
	->addJs(['url' => 'js/jQuery-2.1.4.min.js', 'id' => 'jQueryJs'])
    ->addJs(['url' => 'bootstrap/js/bootstrap.min.js', 'id' => 'BootstrapJs'])
    ->addJs(['url' => 'plugins/lazy-loading/jquery.lazy.min.js'])
    ->addJs(['url' => 'plugins/accounting/accounting.min.js'])
    ->addJs(['url' => 'templates/homemart/js/front.js']);

    $data['Assets'] =& $asset;
	return $loader->load->view('product_single', $data);
}]);














\sasak\Router()->get('/user/{int}/img/{int}x{int}/{any}', function($nip, $w, $h, $file){
	ob_start();
	echo '<pre>';
	echo 'NIP : '. $nip ."\n";
	echo 'Width : '. $w ."\n";
	echo 'Height : '. $h ."\n";
	echo 'file : '. $file;
	$ret = ob_get_clean();
	return $ret;
});

\sasak\Router()->get('/regex/{any}?', function($param = ''){
	// get host name from URL
	$url = \sasak\Input()->server("HTTP_HOST") . uri()->request();
	//exit($url);
	//$url = "http://www.php.net/index.html";
	preg_match('#^(?:http://)?([^/]+)([^*]+)#i', $url, $matches1);
	$host = $matches1[1];
	// get last two segments of host name
	preg_match('/[^.]+\.[^.]+$/', $host, $matches);
	echo '<pre>';
	print_r($matches1);
	echo "domain name is: {$matches[0]}\n";
	echo "param: ". $param ."\n";
	exit;
});

\sasak\Router()->get('/tes', '\app\controller\user@tes');
\sasak\Router()->get('/user/profile', function(){
	return \sasak\View::load('user_profil');
});
\sasak\Router()->get('/ajax/{s1}/{s2}/?', function($s1, $s2){
	return '$s1 : '. $s1 .', $s2 = '. $s2;
});

\sasak\Router()->get('/ajax/tes/{int}', function($angka){
	return 'tipe variable : '. gettype($angka);
});

\sasak\Router()->post('/action/{action}', function($action){
	return 'anda sedang '. $action .'..';
});

\sasak\Router()->get('/tes/tos/{angka}/{angka}/{any}?', function($b, $c, $d = ''){
	\sasak\User::auth();
	if(class_exists('app\Model')){
		$model = new \sasak\Model;
		$query = $model->db->query("SELECT * FROM `pegawai` ORDER BY `date_add` DESC;");
		$data = $query->fetchAll(\PDO::FETCH_ASSOC);
		return '<pre>'.print_r($data, 1).'</pre>';
		return 'model found..';
	}else{

	}
	return '('. $b.' + '.$c.' * 2 / 100) = '. ($b+$c * 2 / 100) .' => '. $d;
});

\sasak\Router()->error('404', function(){
	return \sasak\View::load('404');
});

\sasak\Router()->get('/error/{any}.html', function($tes){
	$m = [];
	$s = uri()->current_url();//'/error/90.html';
	$p = preg_match_all('#^/error/([0-9a-z]+).html$#', $s, $m);
	$m = array_slice($m, 1);
	$int = preg_match('#^([0-9]+)$#', $tes);
	Error('request : '. $s .'<br/>is_int : '. $tes .' => '. $int .'<pre> '. print_r($m,1));
});