<?php
/**
 * price model
 *
 * @author              : Muh. Sukrillah
 * @url                 : http://www.sukrillah.xyz
 * @version             : 1.0
 */
namespace app\model;

class Price_Model extends \sasak\Model {
    /**
     * table name
     *
     * @var     string  table_name
     * @access  private
     */
    protected $table_name = 'price';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
}