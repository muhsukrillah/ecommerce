<?php
/***
 * Delivery model class
 *
 * @author              : Muh. Sukrillah
 * @version             : 1.0
 */
namespace app\model;

class Delivery_Model extends \sasak\Model {
    /**
     * table name
     * @var     string  table_name
     * @access  private
     */
    protected $table_name = 'delivery';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
}