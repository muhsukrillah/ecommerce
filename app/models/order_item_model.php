<?php
/**
 * Order item model class
 *
 * @author              Muh Sukrillah
 * @version             1.0
 */
namespace app\model;

class Order_Item_Model extends \sasak\Model {
    /**
     * table name
     * @var     string table_name
     * @access  private
     */
    protected $table_name = 'order_item';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }

    function get_order_item_with_product($id_order = 0){
        $sql = "SELECT `order_item`.`id_order`, `product`.`product_title`, `order_item`.`quantity`,`order_item`.`subtotal`,`order_item`.`total` FROM `product` JOIN `order_item` ON `product`.`id_product` = `order_item`.`id_product` AND `order_item`.`id_order` = ?";
        $query = $this->db->filter_query($sql, [$id_order]);
        $data = $query->fetch_all();
        return $data;
    }

    function __filter_insert($data){
        if(isset($data['date_updated'])){
            unset($data['date_updated']);
        }
        if(isset($data['date_added'])){
            unset($data['date_added']);
        }
        return $data;
    }

    function __filter_update($data){
        return $this->__filter_insert($data);
    }
}