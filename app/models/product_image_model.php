<?php
/**
 * product image model class
 *
 * @author              : Muh. Sukrillah
 * @url                 : http://www.sukrillah.xyz
 * @version             : 1.0
 */
namespace app\model;

class Product_Image_Model extends \sasak\Model {
    /**
     * table name
     * @var     string  table_name
     * @access  private
     */
    protected $table_name = 'product_image';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
}