<?php
/**
 * brand model class
 *
 * @author          : Muh. Sukrillah
 * @url             : http://www.sukrillah.xyz
 * @version         : 1.0
 */
namespace app\model;

class Brand_Model extends \sasak\Model {
    /**
     * table name
     * @var     string  table_name
     * @access  private
     */
    protected $table_name = 'brand';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
}