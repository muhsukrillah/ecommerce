<?php
/**
 * Customer model class
 *
 * @author               Muh. Sukrillah
 */
namespace app\model;

class Customer_Model extends \sasak\Model {
    /**
     * table name
     * @var     string  table_name
     * @access  private
     */
    protected $table_name = 'customer';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
}