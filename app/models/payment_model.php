<?php
/**
 * payment model class
 *
 * @author              : Muh. Sukrillah
 * @version             : 1.0
 */
namespace app\model;

class Payment_Model extends \sasak\Model {
    /**
     * table name
     * @var     string  table_name
     * @access  private
     */
    protected $table_name = 'payment_method';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
        $this->date_added = date('Y-m-d H:i:s');
        $this->date_updated = $this->date_added;
    }
}