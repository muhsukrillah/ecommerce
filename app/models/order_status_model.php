<?php
/**
 * order status object class
 *
 * @author          Muh Sukrillah
 * @version         1.0
 */
namespace app\model;

class Order_Status_Model extends \sasak\Model {
    /**
     * table name
     * @var     string  table_name
     * @access  private
     */
    protected $table_name = 'order_status';
    /**
     * construct
     */
    function __construct(){
        parent::__construct();
    }

    function __filter_insert($data){
        if(isset($data['date_updated'])){
            unset($data['date_updated']);
        }
        return $data;
    }

    function __filter_update($data){
        return $this->__filter_insert($data);
    }
}