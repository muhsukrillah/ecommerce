<?php
/**
 * product category model
 *
 * Dibuat Oleh              : Muh. Sukrillah
 * Url                      : http://www.sukrillah.xyz
 * Versi                    : 1.0
 *
 */
namespace app\model;

class Category_Model extends \sasak\Model {
    /**
     * table name
     * @var     string  table_name
     * @access  public
     */
    protected $table_name = 'category';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
        \sasak\Log::add('Class ['. __CLASS__ .'] Initialized..');
    }

    function __filter_insert($data){
        if(is_array($data) && !empty($data)){
            if(isset($data['category_name']) && !isset($data['slug'])){
                $data['slug'] = url_title($data['category_name']);
            }
            if(isset($data['slug'])){
                \sasak\Log::add('Filtering insert data', TRUE, TRUE);
                $data['slug'] = $this->filter_slug($data['slug']);
            }
        }
        return $data;
    }

    function filter_slug($slug = ''){
        $t = $this->table_name;
        $sql = "SELECT `slug` FROM `$t`  WHERE `slug` = ?";
        $query = $this->db->filter_query($sql, [$slug]);
        if($query->num_rows()){
            $i = 1;
            do{
                $alt_slug = $slug ."-$i";
                $i++;
                $query = $this->db->filter_query($sql, [$alt_slug]);
            } while ($query->num_rows());
            $slug = $alt_slug;
        }
        return $slug;
    }
}