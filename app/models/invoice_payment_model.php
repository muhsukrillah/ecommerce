<?php
/**
 * Invoice class model
 * 
 * @author              Muh Sukrillah
 * @version             1.0
 */
namespace app\model;

class Invoice_Payment_Model extends \sasak\Model {
    /**
     * table name
     * @var     string table_name
     * @access  private
     */
    protected $table_name = 'invoice_payment';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }

    function get_payment_detail($id_invoice = 0, $id_customer = 0){
        $sql = "SELECT `ip`.`id`, `ip`.`id_invoice`, `ip`.`id_customer`, CONCAT(`customer`.`first_name`,' ', `customer`.`last_name`) `customer_full_name`, `pm`.`payment_name` `payment_method`, CONCAT(`ba`.`bank_name`, '@', `ba`.`bank_owner`) `bank_account`, `ip`.`value`, `ip`.`date_added`, `ip`.`description`, `ip`.`images`, `ip`.`evaluated` FROM `invoice_payment` `ip`, `customer`, `payment_method` `pm`, `bank_account` `ba` WHERE `ip`.`id_customer` = `customer`.`id_customer` AND `ip`.`id_payment` = `pm`.`id_payment` AND `ip`.`id_bank` = `ba`.`id` AND `ip`.`id_invoice` = ? AND `customer`.`id_customer` = ?";
        $query = $this->db->filter_query($sql, [$id_invoice, $id_customer]);
        $data = $query->fetch();
        return $data;
    }
}