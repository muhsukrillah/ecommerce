<?php
/**
 * Product model class
 * @author                  : Muh. Sukrillah
 * @url                     : http://www.sukrillah.xyz
 * Versi                    : 1.0
 *
 */
 namespace app\model;

 class Product_Model extends \sasak\Model {
    /**
     * nama table
     * @var     string table_name
     * @access  public
     */
    protected $table_name = 'product';
    /**
     * constructor
     */
    function __construct($table_name = ''){
        parent::__construct();
        if(isset($table_name) && !empty($table_name)){
            $this->set_table_name($table_name);
        }
    }
    // model::search_by(['field' => '%value%'], ['order_by' => ['field' => 'asc']]);
    function search_by($data = [], $args = []){
        // order
        // model::get_all(['order_by' => ['field' => 'asc']])
        if(isset($args['order_by']) && !empty($args['order_by'])) {
            $this->db->order_by($args['order_by']);
        }
        // limit
        // model::get_all(['limit' => [0, 10]])
        if(isset($args['limit']) && !empty($args['limit'])){
            $offset = isset($args['limit'][0]) ? $args['limit'][0] : 0;
            $limit = isset($args['limit'][1]) ? $args['limit'][1] : 10;
            $this->db->limit($limit, $offset);
        }
        foreach($data as $key => $value) {
            $this->db->where($key, 'like', $value);
        }
        return $this->db->get($this->table_name)->fetch_all();
    }

    function fetch_all(){
        $args = func_get_args();
        foreach($args as $key => $value){
            // order
            // model::get_all(['order_by' => ['field' => 'asc']])
            if(isset($value['order_by']) && !empty($value['order_by'])) {
                $this->db->order_by($value['order_by']);
            }
            // limit
            // model::get_all(['limit' => [0, 10]])
            if(isset($value['limit']) && !empty($value['limit'])){
                $offset = isset($value['limit'][0]) ? $value['limit'][0] : 0;
                $limit = isset($value['limit'][1]) ? $value['limit'][1] : 10;
                $this->db->limit($limit, $offset);
            }
        }
        return $this->db->get($this->table_name)->fetch_all();
    }

    function get_model_number($id_produk = 0){
        $this->db->select('*')->from($this->table_name)->where(['id_product' => $id_produk]);
        $query = $this->db->get();
        $data = $query->fetch();
        return $data;
    }

    function generate_model_number($prefix = 'P'){
        $query = $this->db->query("SELECT COUNT(*) `total` FROM `". $this->table_name ."`");
        $data = $query->fetch();
        $total = (int) $data['total'];
        $total = $total + 1;
        if(!isset($prefix) || empty($prefix)){
            $prefix = "P";
        }
        $max_length = 10;
        $length = strlen($prefix) + strlen($total);
        
        if($length == $max_length){
            return ($prefix . $total);
        }

        $zero = str_repeat('0', ($max_length - $length));
        $model = $prefix . $zero . $total;
        return $model;
    }
    /**
     * get category
     */
    function get_category($id_produk = 0){
        $query = $this->db->filter_query("SELECT `category`.* FROM `product`, `category`, `product_category` `relation` WHERE `product`.`id_product` = `relation`.`id_product` AND `category`.`id_category` = `relation`.`id_category` AND `product`.`id_product` = ?", [$id_produk]);
        $data = $query->fetch();
        return $data;
    }
    function get_stock($id_produk = 0){
        $query = $this->db->filter_query("SELECT `stock`.* FROM `product`, `satuan`, `stock` WHERE `product`.`id_product` = `satuan`.`id_product` AND `stock`.`id_satuan` = `satuan`.`id_satuan` AND `product`.`id_product` = ? GROUP BY `satuan`.`id_product`", [$id_produk]);
        $data = $query->fetch();
        return $data;
    }
 }