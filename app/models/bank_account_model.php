<?php
/**
 * Bank account model
 *
 * @author          Muh Sukrillah
 * @version         1.0
 */
namespace app\model;

class Bank_Account_Model extends \sasak\Model {
    /**
     * table name
     * @var     string  table_name
     * @access  private
     */
    protected $table_name = 'bank_account';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }

    function __filter_insert($data){
        if(isset($data['date_added'])){
            unset($data['date_added']);
        }
        if(isset($data['date_updated'])){
            unset($data['date_updated']);
        }
        return $data;
    }

    function __filter_update($data){
        return $this->__filter_insert($data);
    }
}