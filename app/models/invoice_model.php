<?php
/**
 * Invoice class model
 * 
 * @author              Muh Sukrillah
 * @version             1.0
 */
namespace app\model;

class Invoice_Model extends \sasak\Model {
    /**
     * table name
     * @var     string table_name
     * @access  private
     */
    protected $table_name = 'invoice';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
}