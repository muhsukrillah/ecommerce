<?php
/**
 * product category model
 *
 * @author                  : Muh. Sukrillah
 * @url                     : http://www.sukrillah.xyz
 * @version                 : 1.0
 */
namespace app\model;

class Product_Category_Model extends \sasak\Model {
    /**
     * table name
     * @var     string  table name
     * @access  private
     */
    protected $table_name = 'product_category';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }

    public function fetch($id_product = 0){
        $sql = "SELECT `product`.*, `category`.`category_name`, `category`.`slug` `category_slug`, `category`.`parent_id`, `category`.`order` FROM `product`, `category`, `product_category` WHERE `product`.`id_product` = `product_category`.`id_product` AND `category`.`id_category` = `product_category`.`id_category`";
    }
}