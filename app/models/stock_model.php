<?php
/**
 * stock model class
 *
 * @author              : Muh. Sukrillah
 * @url                 : http://www.sukrillah.xyz
 * @version             : 1.0
 */
namespace app\model;

class Stock_Model extends \sasak\Model {
    /**
     * table name
     * @var     string  table_name
     * @access  private
     */
    protected $table_name = 'stock';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
}