<?php
/**
 * satuan model class
 *
 * @author                  : Muh. Sukrillah
 * @url                     : http://www.sukrillah.xyz
 * @version                 : 1.0
 */
namespace app\model;

class Satuan_Model extends \sasak\Model {
    /**
     * table name
     * 
     * @var     string  table_name
     * @access  private
     */
    protected $table_name = 'satuan';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
}