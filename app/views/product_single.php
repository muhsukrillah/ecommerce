<?php include 'header.php'; ?>
        <div class="row">
            <div class="col-md-3 left-container">
                <div class="panel panel-category">
                    <div class="panel-title"><h4 class="lead">Kategori</h4></div>
                    <div class="panel-body">
                        <div class="category-wrapper list-group text-left">
                        <?php
                        if(isset($categories) && !empty($categories)){
                            foreach($categories as $key => $value){
                        ?>
                            <a href="<?php echo base_url('cat/'. $value['slug'] .'/'); ?>" class="list-group-item"><?php echo $value['category_name']; ?></a>
                        <?php 
                            }
                        } 
                        ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-lg-9 col-sm-12 right-col pull-right">
                <div class="panel panel-product">
                    <div class="panel-heading">
                        <?php echo (isset($breadcrumb) ? $breadcrumb->get('') : ''); ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-lg-4 col-md-4 carousel-holder">
                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                    <?php 
                                    if(isset($product['images']) && !empty($product['images'])){
                                        $i = 0;
                                        $indicators = '';
                                        $items = '';
                                        foreach($product['images'] as $key => $image){
                                            $indicators .= '<li data-target="#carousel-example-generic" data-slide-to="'. $i .'" class="'. (!$i ? 'active' : '') .'"></li>';
                                            $items .= '<div class="item'. (!$i ? ' active' : '') .'">';
                                            $items .= '<img class="slide-image" src="'. $image['url'] .'" alt="">';
                                            $items .= '</div>';
                                        $i++;
                                        }
                                    }
                                    ?>
                                    <ol class="carousel-indicators">
                                    <?php echo $indicators; ?>
                                    </ol>
                                    <div class="carousel-inner"><?php echo $items; ?></div>
                                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-8 col-md-8">
                                <h2 class="headline" style="margin-top:0"><?php echo $product['product_title']; ?></h2>
                                <br>
                            </div>
                        <?php if(isset($product) && !empty($product)){ ?>
                            <div class="col-sm-12 col-lg-8 col-md-8 product-item">
                                <div class="col-md-6 col-sm-6" style="padding-left:0">
                                    <div class="ratings" style="padding:0">
                                        <p class="pull-right">5 Ulasan</p>
                                        <p class="text-left">
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                        </p>
                                    </div>
                                    <div class="stock">
                                    <strong>Stok :</strong> <?php echo $product['stock']; ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 text-right">
                                    <h3 class="clearfix text-primary price" style="margin-top:0;"><?php echo (isset($product['harga_jual']) ? format_harga($product['harga_jual']) : 'Rp 2.400'); ?></h3>
                                    <div class="clearfix">
                                        <div class="input-group">
                                            <span class="input-group-addon">Jumlah</span>
                                            <?php
                                            $pdata = json_encode(array(
                                                'id_product' => $product['id_product'],
                                                'harga' => $product['harga_jual'],
                                                'product_title' => clean($product['product_title']),
                                                'url' => base_url('produk/'. $product['product_name'] .'.html'),
                                                'image' => str_replace(['.jpg', '.png', '.gif'], ['_60x60.jpg', '_60x60.png', '_60x60.gif'], $product['images'][0]['url'])
                                            ));
                                            ?>
                                            <input type="hidden" name="data" id="pdata" value='<?php echo $pdata; ?>'>
                                            <input type="hidden" name="harga" id="harga" value="<?php echo $product['harga_jual']; ?>">
                                            <input type="number" name="qty" id="qty" value="1" class="form-control" min="1" max="<?php echo $product['stock']; ?>">
                                            <div class="input-group-btn">
                                                <button id="buyButton" type="submit" href="javascript:void(0);" class="btn btn-primary">&nbsp;<i class="fa fa-shopping-cart"></i> Beli&nbsp;</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 separator"><br></div>
                                <div class="col-sm-12 col-md-12" style="padding-left:0">
                                    <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <td><strong>Model</strong></td>
                                            <td>:</td>
                                            <td><?php echo $product['model']; ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Brand</strong></td>
                                            <td>:</td>
                                            <td><?php echo $product['brand']; ?></td>
                                        </tr>
                                    </tbody>
                                    </table>
                                    <h4><i class="fa fa-info-circle"></i> Deskripsi</h4>
                                    <div class="description"><?php echo $product['description']; ?></div>
                                </div>
                            </div>
                        <?php 
                        }
                        ?>
                        </div>
                    </div>
                </div>
                <!-- review -->
                <div class="panel panel-review">
                    <div class="panel-heading">
                        <h3 class="">Ulasan</h3>
                    </div>
                    <div class="panel-body">
                        <div class="review-contents">
                            <a name="review"></a>
                            <div class="review">
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                    <small><img src="<?php echo asset_url('img/blank-user.png'); ?>" class="img-circle" style="width:40px;height:40px;"/> <b>Muh. Sukrillah</b> pada <cite title="Source Title"><?php echo date('l, M Y'); ?></cite></small>
                                    <div class="ratings" style="margin-top:10px">
                                        <p class="text-left">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </p>
                                    </div>
                                </blockquote>
                            </div>
                        </div>
                        <div class="review-form">
                            <a name="review-form"></a>
                            <div class="form-container">
                                <h4>Berikan Ulasan Anda</h4>
                                <div class="rating">
                                    <p class="text-left">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </p>
                                </div>
                                <div class="comment">
                                    <div class="form-group">
                                        <textarea name="rating_comment" id="rating_comment" class="form-control" placeholder="Tambahkan komentar Anda.."></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-send"></i> Kirim</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end review -->
                <!-- produk terkait -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="">Produk Terkait</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="thumbnail">
                                    <img class="lazy" data-src="<?php echo $product['featured_image']; ?>" style="height:150px;" alt="">
                                    <div class="caption text-center">
                                        <h4 class="text-"><a href="<?php echo base_url('produk/'. $product['product_name']); ?>.html"><?php echo $product['product_title']; ?></a>
                                        </h4>
                                        <h3 class="text-"><?php echo (isset($product['harga_jual']) ? format_harga($product['harga_jual']) : 'Rp 2.400'); ?></h3>
                                        <p><a href="javascript:void(0);" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Beli</a></p>
                                    </div>
                                    <div class="ratings" style="margin-top:10px">
                                        <p class="pull-right">5 Ulasan</p>
                                        <p class="text-left">
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                        </p>
                                        <br>
                                    </div>
                                </div><!-- thumbnail -->
                            </div>
                            <div class="col-md-4">
                                <div class="thumbnail">
                                    <img class="lazy" data-src="<?php echo $product['featured_image']; ?>" style="height:150px;" alt="">
                                    <div class="caption text-center">
                                        <h4 class="text-"><a href="<?php echo base_url('produk/'. $product['product_name']); ?>.html"><?php echo $product['product_title']; ?></a>
                                        </h4>
                                        <h3 class="text-"><?php echo (isset($product['harga_jual']) ? format_harga($product['harga_jual']) : 'Rp 2.400'); ?></h3>
                                        <p><a href="javascript:void(0);" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Beli</a></p>
                                    </div>
                                    <div class="ratings" style="margin-top:10px">
                                        <p class="pull-right">5 Ulasan</p>
                                        <p class="text-left">
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                        </p>
                                        <br>
                                    </div>
                                </div><!-- thumbnail -->
                            </div>
                            <div class="col-md-4">
                                <div class="thumbnail">
                                    <img class="lazy" data-src="<?php echo $product['featured_image']; ?>" style="height:150px;" alt="">
                                    <div class="caption text-center">
                                        <h4 class="text-"><a href="<?php echo base_url('produk/'. $product['product_name']); ?>.html"><?php echo $product['product_title']; ?></a>
                                        </h4>
                                        <h3 class="text-"><?php echo (isset($product['harga_jual']) ? format_harga($product['harga_jual']) : 'Rp 2.400'); ?></h3>
                                        <p><a href="javascript:void(0);" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Beli</a></p>
                                    </div>
                                    <div class="ratings" style="margin-top:10px">
                                        <p class="pull-right">5 Ulasan</p>
                                        <p class="text-left">
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                        </p>
                                        <br>
                                    </div>
                                </div><!-- thumbnail -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-12">
                <div class="panel">
                    <div class="panel-heading"><h4>Lagi Promo</h4></div>
                    <div class="panel-body">
                        <ol class="list-group">
                            <li class="list-group-item"><a href="<?php echo base_url('produk/'. $product['product_name']); ?>.html"><?php echo $product['product_title']; ?></a></li>
                        </ol><!-- thumbnail -->
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading"><h4>Produk Terbaru</h4></div>
                    <div class="panel-body">
                        <ol class="list-group">
                            <li class="list-group-item"><a href="<?php echo base_url('produk/'. $product['product_name']); ?>.html"><?php echo $product['product_title']; ?></a></li>
                        </ol><!-- thumbnail -->
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading"><h4>Produk Terlaris</h4></div>
                    <div class="panel-body"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->
    <?php include 'middle_info.php'; ?>
<?php include 'footer.php'; ?>