<?php include 'header.php'; ?>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-6 pull-right">
        <div class="panel panel-login">
            <div class="panel-heading blue-grey lighten-2 white-text"><h4><i class="fa fa-lock"></i> Login</h4></div>
            <div class="panel-body">
                <div class="alert-container">
                    <div class="alert alert-warning hide">
                        <button class="close" data-dismiss="alert">&times;</button>
                        <p></p>
                    </div>
                </div>
                <form role="form" id="login-form" action="<?php echo base_url('customer/ajax/auth/login'); ?>" method="post">
                    <input type="hidden" name="next" value='<?php echo (isset($next) ? $next : ''); ?>'>
                    <div class="col-sm-12 col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" name="cusername" id="cusername" class="form-control" required placeholder="Username.." autofocus/>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                            <input type="password" name="cpassword" id="cpassword" class="form-control" required placeholder="Kata Sandi.." />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="input-group pull-right">
                                <br>
                                <a href='<?php echo base_url('customer/register'. (isset($next) ? '?next='. urlencode($next) : '')); ?>'>Belum Punya Akun?</a> &nbsp;&bull;&nbsp;
                                <a href="#">Lupa Kata Sandi?</a> &nbsp;
                                <button class="btn btn-primary" type="submit">Login <i class="fa fa-arrow-right"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="panel panel-promo">
            <div class="panel-heading"><h4>Promo</h4></div>
            <div class="panel-body">

            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>