<!DOCTYPE html>
<html lang="id_ID">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo (isset($title) ? $title->get() : 'Home'); ?></title>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto|Open+Sans'/>
    <?php echo (isset($Assets) ? $Assets->getCss() : ''); ?>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/templates/homemart/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- materialize --
    <link href="/assets/materialize/css/materialize.min.css" rel="stylesheet">
    <!-- font-wesome -->
    <link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/templates/homemart/css/shop-homepage.css'); ?>" rel="stylesheet">
    <script>/*<![CDATA[*/var base_url='<?php echo base_url(); ?>', asset_url='<?php echo asset_url();?>';/*]]>*/</script>
</head>
<body class="<?php echo (isset($bodyClass) ? $bodyClass->get() : ''); ?>">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url(); ?>"><?php echo SITE_TITLE; ?></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=" navbar-collapse" id="nav">
                <form class="navbar-form navbar-left col-md-6" role="search" action="<?php echo base_url('search'); ?>">
                    <div class="form-group">
                        <div class="input-group">
                            <input name="query" type="text" class="form-control" placeholder="Cari Produk..">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> </button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- <p class="navbar-text navbar-">Selamat Datang, Tamu!</p>-->
                <ul class="nav navbar-nav navbar-right">
                <?php if(\app\Customer::is_logedin()){ ?>
                    <li class="">
                        <a href="<?php echo base_url('customer/profile'); ?>"><strong><i class="fa fa-user"></i> <?php echo \app\Customer::$data['first_name'] .' '. \app\Customer::$data['last_name']; ?></strong></a>
                    </li>
                <?php } else { ?>
                    <li>
                        <a href="<?php echo base_url('customer/login'. (isset($next) ? '?next='. urlencode($next) : '')); ?>"><i class="fa fa-sign-in"></i> Login</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('customer/register'); ?>"><i class="fa fa-user"></i> Register</a>
                    </li>
                <?php } ?>
                    <li>
                        <a href="javascript:sCart.show();"><i class="fa fa-shopping-cart"></i> Troli Belanja <span id="sCart_counter">0</span></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- Page Content -->
    <div class="container">