<?php include 'header.php'; ?>
<div class="row">
            <div class="col-md-3 left-container">
                <?php include 'left-menu.php'; ?>
            </div>
            <div class="col-md-9"> 
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="panel panel-badge text-center">
                            <div class="panel-body">
                                <img src="<?php echo asset_url('img/blank-user.png'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-8 col-sm-6">
                        <div class="panel panel-badge text-center">
                            <div class="panel-heading blue-grey lighten-4">
                                <h3><?php echo (isset($nama) ? $nama : ''); ?></h3>
                            </div>
                            <div class="panel-body">
                                <table class="table table-responsive">
                                <tr>
                                    <td class="text-left"><strong>Email</strong></td>
                                    <td class="text-left">:</td>
                                    <td class="text-left"><?php echo (isset($data['email']) ? $data['email'] : ''); ?></td>
                                </tr>
                                <tr>
                                    <td class="text-left"><strong>Nomor HP</strong></td>
                                    <td class="text-left">:</td>
                                    <td class="text-left"><?php echo (isset($data['phone']) ? $data['phone'] : ''); ?></td>
                                </tr>
                                <tr>
                                    <td class="text-left"><strong>Alamat</strong></td>
                                    <td class="text-left">:</td>
                                    <td class="text-left"><?php echo (isset($data['address']) ? $data['address'] : ''); ?></td>
                                </tr>
                                <tr>
                                    <td class="text-left"><strong>Jenis Kelamin</strong></td>
                                    <td class="text-left">:</td>
                                    <?php
                                    if(isset($data['gender'])){
                                        $gender = ['L' => 'Laki-Laki', 'P', 'Perempuan'];
                                        $g = isset($gender[$data['gender']]) ? $gender[$data['gender']] : 'Lainnya';
                                    }
                                    ?>
                                    <td class="text-left"><?php echo (isset($data['gender']) ? $g : ''); ?></td>
                                </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="panel panel-badge text-center">
                            <div class="panel-body">
                                <img src="<?php echo asset_url('img/blank-user.png'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="panel panel-badge text-center">
                            <div class="panel-heading">
                                <h3>Item Keranjang Belanja</h3>
                            </div>
                            <div class="panel-body">
                                <h1 class="">10</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>