<?php include 'header.php'; ?>
    <div class="row">
            <div class="col-md-3 left-container">
            <?php include 'left-menu.php'; ?>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-right-content">
                            <div class="panel-heading text-center">
                                <h4><i class="fa fa-list-alt"></i> Data Tagihan Anda</h4>
                            </div>
                            <div class="panel-body">
                                <table class="table table-responsive table-striped">
                                <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Tanggal</th>
                                        <th>Jatuh Tempo</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(isset($invoice_data) && !empty($invoice_data)){
                                    foreach($invoice_data as $key => $value){
                                        $time = strtotime($value['date_added']);
                                        $time_ex = strtotime($value['date_expired']);

                                        echo '<tr>';
                                        echo '<td>#'. $value['id_invoice'] .'</td>';
                                        echo '<td>'. date('d/m/Y', $time) .'</td>';
                                        echo '<td>'. date('d/m/Y', $time_ex) .'</td>';
                                        echo '<td><span class="curency">'. format_rupiah($value['total'], '') .'</span></td>';
                                        $status = '<span class="label label-danger label-sm">Belum Dibayar</span>';
                                        if($value['status']){
                                            $status = '<span class="label label-success label-sm">Sudah Dibayar</span>';
                                        }
                                        echo '<td>'. $status .'</td>';
                                        echo '<td>';
                                        echo '<a href="'. base_url('customer/invoice?invoice_id='. $value['id_invoice']) .'" class="label label-info label-sm tooltips" data-toggle="tooltips" title="Lihat Detail Tagihan"><i class="fa fa-search"></i></a>';
                                        echo ' <a href="'. base_url('customer/confirm_payment?invoice_id='. $value['id_invoice']) .'" class="label label-success label-sm tooltips" data-toggle="tooltip" title="Konfirmasi Pembayaran"><i class="fa fa-check"></i></a>';
                                        echo '</td>';
                                        echo '</tr>';
                                    }
                                }
                                ?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- row 9 -->
            </div><!-- col-md-9 -->
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>