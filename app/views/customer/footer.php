<div class="footer-container">
    <hr>
    <!-- Footer -->
    <footer>
        <div class="row-fluid">
            <div class="col-lg-12">
                <div style="color:#ccc;">
                    <p>&copy; <b>CV. SOLIDTech</b> <?php echo date('Y'); ?> - All right reserved - Developed By <b><a href="https://www.facebook.com/muh.sukrillah" target="_blank">Muh. Sukrillah</a></b>, Powered By <b>Sasak Framework</b></p>
                    <small>Waktu proses : {elapsed_time} detik</small> <small>Penggunaan memory: {memory_usage}</small>
                </div>
                <br>
            </div>
        </div>
    </footer>
</div>
    <?php echo (isset($Assets) ? $Assets->getJs() : ''); ?>
</body>
</html>