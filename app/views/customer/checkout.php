<?php include 'header.php'; ?>
    <div class="row">
            <div class="col-md-3 left-container">
            <?php include 'left-menu.php'; ?>
            </div>
            <div class="col-md-9">
                <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-right-content">
                        <div class="panel-body">
                            <table class="table table-scart table-responseive">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Produk</th>
                                    <th>Harga</th>
                                    <th>Qty</th>
                                    <th>Total</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            </table>
                            <div class="cupon-container hide">
                                <div class="col-md-12">
                                    <h4 class="pull-right">Kode Kupon Belanja</h4>
                                </div>
                                <div class="col-md-3 pull-right">                                    
                                    <div class="input-group">
                                        <input type="text" name="cupon" id="cupon" class="form-control">
                                        <div class="input-group-btn">
                                            <button type="button" id="checkCupon" class="btn btn-primary">OK</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div><!-- row -12 -->
                <div class="row hide row-checkout">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4>Pembayaran</h4>
                                </div>
                                <div class="panel-body">
                                    <select name="payment" id="payment" class="form-control">
                                    <?php if(isset($payment) && !empty($payment)){ ?>
                                        <option value="0">Pilih</option>
                                    <?php foreach($payment as $key => $value){ ?>
                                        <option value="<?php echo $value['id_payment']; ?>"><?php echo $value['payment_name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4>Pengiriman</h4>
                                </div>
                                <div class="panel-body">
                                    <select name="delivery" id="delivery" class="form-control">
                                    <?php if(isset($delivery) && !empty($delivery)){ ?>
                                        <option value="0">Pilih</option>
                                    <?php foreach($delivery as $key => $value){ ?>
                                        <option value="<?php echo $value['id_delivery']; ?>" data-value="<?php echo $value['biaya']; ?>"><?php echo $value['delivery_name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                    </select>                            
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Nama Penerima</label>
                                        <input type="text" name="shippingName" id="shippingName" class="form-control" value='<?php echo clean(\app\Customer::$data['fullname']); ?>'>
                                    </div>
                                    <div class="form-group">
                                        <label>Nomor Telpon Penerima</label>
                                        <input type="text" name="shippingContact" id="shippingContact" class="form-control" value='<?php echo clean(\app\Customer::$data['phone']); ?>'>
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat Pengiriman</label>
                                        <textarea id="shippingAddress" class="form-control"><?php echo clean(\app\Customer::$data['address']); ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Keterangan</label>
                                        <textarea id="description" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel">
                        <div class="panel-heading"><h4>Total</h4></div>
                        <div class="panel-body">
                            <table class="table-responsive table">
                            <tbody>
                                <tr>
                                    <td><b>Biaya Kirim</b></td>
                                    <td class="text-right"><span id="biayaKirim" class="curency">0</span></td>
                                </tr>
                                <tr>
                                    <td><b>Potongan</b></td>
                                    <td class="text-right"><span id="potongan" class="curency">0</span></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><b>Total Harus Dibayar</b></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="text-right"><strong id="totalBiaya" class='curency'>0</strong></td>
                                </tr>
                            </tbody>
                            </table>
                            <div class="form-group">
                                <button class="btn btn-primary btn-lg pull-right" type="button" id="paymentButton">Bayar <i class="fa fa-arrow-right"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                </div><!-- row 9 -->
            </div><!-- col-md-9 -->
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>