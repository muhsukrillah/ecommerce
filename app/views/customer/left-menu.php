<div class="panel panel-category">
                    <div class="panel-title"><h4 class="lead">Menu</h4></div>
                    <div class="panel-body">
                        <div class="category-wrapper list-group text-left">
                            <a class="list-group-item" href="<?php echo base_url('customer/index'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                            <a class="list-group-item" href="<?php echo base_url('customer/checkout'); ?>"><i class="fa fa-shopping-cart"></i> Troli Belanja</a>                            
                            <a class="list-group-item" href="<?php echo base_url('customer/invoice'); ?>"><i class="fa fa-list-alt"></i> Tagihan</a>
                             <a class="list-group-item" href="<?php echo base_url('customer/confirm_payment'); ?>"><i class="fa fa-check"></i> Konfirmasi Pembayaran</a>
                             <a class="list-group-item" href="<?php echo base_url('customer/order'); ?>"><i class="fa fa-bookmark"></i> Pembelian</a>
                            <a class="list-group-item" href="<?php echo base_url('customer/testimoni'); ?>"><i class="fa fa-comment"></i> Kirim Testimoni</a>
                            <a class="list-group-item" href="<?php echo base_url('customer/review'); ?>"><i class="fa fa-comments"></i> Ulasan Saya</a>
                            <a class="list-group-item" href="<?php echo base_url('customer/profile'); ?>"><i class="fa fa-user"></i> Profil Saya</a>
                            <a class="list-group-item" onclick="return confirm('Apakah Anda yakin?');" href="<?php echo base_url('customer/logout'); ?>"><i class="fa fa-sign-out"></i> Logout</a>
                        </div>
                    </div>
                </div>