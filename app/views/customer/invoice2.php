<?php include 'header.php'; ?>
    <div class="row">
            <div class="col-md-3 left-container">
            <?php include 'left-menu.php'; ?>
            </div>
            <div class="col-md-9">
                <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-right-content">
                        <div class="panel-body">
                            <!-- Content Wrapper. Contains page content -->
                          <div class="content-wrapper">
                            <!-- Main content -->
                            <section class="invoice">
                              <!-- title row -->
                              <div class="row">
                                <div class="col-xs-12">
                                  <h2 class="page-header">
                                    <?php echo (isset($shop_name) ? $shop_name : 'Nama Toko'); ?>
                                    <small class="pull-right">Tanggal: <?php echo date('d/m/Y'); ?></small>
                                  </h2>
                                </div><!-- /.col -->
                              </div>
                              <!-- info row -->
                              <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Dari
                                  <address>
                                    <strong><?php echo $shop_name; ?></strong><br>
                                    <?php echo $option->get('shop_address'); ?><br>
                                    Phone: <?php echo $option->get('shop_contact_phone'); ?><br/>
                                    Email: <?php echo $option->get('shop_email'); ?>
                                  </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                  Kepada
                                  <address>
                                    <strong><?php echo \app\Customer::$data['fullname']; ?></strong><br>
                                    <?php echo \app\Customer::$data['address']; ?><br>
                                    Phone: <?php echo \app\Customer::$data['phone']; ?><br>
                                    Email: <?php echo \app\Customer::$data['email']; ?>
                                  </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                  <b>Tagihan #<?php echo (isset($invoice_data['id_invoice']) ? $invoice_data['id_invoice'] : 1); ?></b><br/>
                                  <b>Order ID:</b> <?php echo $invoice_data['id_order']; ?><br/>
                                  <b>Tanggal Dibuat:</b> <?php echo date('d/m/Y', strtotime($invoice_data['date_added'])); ?><br>
                                  <?php $jatuh_tempo = strtotime($invoice_data['date_expired']); ?>
                                  <b>Jatuh Tempo:</b> <?php echo date('d/m/Y', $jatuh_tempo); ?><br/>
                                  <b>ID Pelanggan:</b> <?php echo \app\Customer::$data['uuid']; ?>
                                </div><!-- /.col -->
                              </div><!-- /.row -->

                              <!-- Table row -->
                              <div class="row">
                                <div class="col-xs-12 table-responsive">
                                  <table class="table table-striped">
                                    <thead>
                                      <tr>
                                        <th>#No.</th>
                                        <th>Produk</th>
                                        <th>Harga</th>
                                        <th class="text-center">Qty</th>
                                        <th class="text-right">Subtotal</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if(isset($order_item_data) && !empty($order_item_data)){
                                        $i = 1;
                                        foreach($order_item_data as $key => $value){
                                            echo '<tr>';
                                            echo '<td>#'. $i .'</td>';
                                            echo '<td>'. $value['product_title'] .'</td>';
                                            echo '<td><span class="curency">'. format_rupiah($value['subtotal'], '') .'</span></td>';
                                            echo '<td class="text-center">'. $value['quantity'] .'</td>';
                                            echo '<td class="text-right"><span class="curency">'. format_rupiah($value['total'], '') .'</span></td>';
                                            echo '</tr>';

                                            $i++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                  </table>
                                </div><!-- /.col -->
                              </div><!-- /.row -->

                              <div class="row">
                                <!-- accepted payments column -->
                                <div class="col-xs-6">
                                  <p class="lead">Pembayaran Via:</p>
                                  <p><b>- <?php echo $payment_method; ?></b></p>
                                  <p class="lead">Keterangan:</p>
                                  <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;"><?php echo $order_data['description']; ?></p>
                                  <p class="lead">Status Tagihan: <?php echo ($invoice_data['status'] ? '<span class="text-success">Terbayar</span>' : '<span class="text-danger">Belum Dibayar</span>'); ?></p>
                                </div><!-- /.col -->
                                <div class="col-xs-6">
                                  <div class="table-responsive">
                                <?php
                                $subtotal = $invoice_data['total'] - $biaya_kirim;
                                ?>
                                    <table class="table">
                                      <tr>
                                        <th style="width:50%">Subtotal:</th>
                                        <td><span class="curency"><?php echo format_rupiah($subtotal, ''); ?></span></td>
                                      </tr>
                                      <tr>
                                        <th>PPN (0.00%)</th>
                                        <td><span class="curency">0</span></td>
                                      </tr>
                                      <tr>
                                        <th>Biaya Kirim:</th>
                                        <td><span class="curency"><?php echo (isset($biaya_kirim) ? format_rupiah($biaya_kirim, '') : 0); ?></span></td>
                                      </tr>
                                      <tr>
                                        <th>Total:</th>
                                        <td><span class="curency"><?php echo (isset($invoice_data['total']) ? format_rupiah($invoice_data['total'], '') : 0); ?></span></td>
                                      </tr>
                                    </table>
                                  </div>
                                </div><!-- /.col -->
                              </div><!-- /.row -->

                              <!-- this row will not appear when printing -->
                              <div class="row no-print">
                                <div class="col-xs-12">
                                  <a href="<?php echo base_url('customer/invoice_print?id='. $invoice_data['id_invoice']); ?>" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                                  <?php if(!$invoice_data['status']){ ?>
                                  <a href="<?php echo base_url('customer/confirm_payment?invoice_id='. $invoice_data['id_invoice']); ?>" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Konfirmasi Pembayaran</a>
                                  <?php } ?>
                                  <a href="<?php echo base_url('customer/invoice_print?id='. $invoice_data['id_invoice']); ?>&amp;type=pdf" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</a>
                                </div>
                              </div>
                            </section><!-- /.content -->
                            <div class="clearfix"></div>
                          </div><!-- /.content-wrapper -->
                        </div>
                    </div>
                </div><!-- row 9 -->
            </div><!-- col-md-9 -->
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>
