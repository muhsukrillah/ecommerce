<?php include 'header.php'; ?>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-6 pull-right">
        <div class="panel panel-register">
            <div class="panel-heading blue-grey lighten-3 white-text"><h4><i class="fa fa-user"></i> Register</h4></div>
            <div class="panel-body">
                <div class="alert-container">
                    <div class="alert alert-warning hide">
                        <button class="close" data-dismiss="alert">&times;</button>
                        <p>tes</p>
                    </div>
                </div>
                <form role="form" id="register_form" action="<?php echo base_url('customer/ajax/auth/register'); ?>" method="post">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="first_name">Nama Depan <span class="text-danger">*</span></label>
                            <input type="text" name="first_name" id="first_name" required class="form-control" title="Masukkan nama depan Anda di sini..">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="last_name">Nama Belakang</label>
                            <input type="text" name="last_name" id="last_name" class="form-control" title="Masukkan nama belakang Anda di sini..">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="last_name">Email <b class="text-danger">*</b></label>
                            <input type="email" name="email" id="email" class="form-control" required title="Masukkan alamat email Anda di sini..">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="last_name">Nomor HP</label>
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="08xxx" title="Masukkan nomor seluler Anda di sini..">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="address">Alamat</label>
                            <input type="text" name="address" id="address" class="form-control" title="Masukkan alamat Anda di sini..">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="gender">Jenis Kelamin <b class="text-danger">*</b></label>
                            <br>
                            <label><input type="radio" name="gender" value="L" required title="Pilih jika jenis kelamin Anda Laki-Laki"> Laki-Laki</label>
                            <label><input type="radio" name="gender" value="P" required title="Pilih jika jenis kelamin Anda Perempuan"> Perempuan</label>
                            <label><input type="radio" name="gender" value="O" required title="Pilih jika jenis Kelamin Anda lainnya.."> Lainnya</label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <label>Username <b class="text-danger">*</b></label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" name="cusername" id="cusername" class="form-control" required placeholder="Username.." />
                        </div>
                    </div>                    
                    <div class="col-sm-12 col-md-6">
                        <label for="password">Password <b class="text-danger">*</b></label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                            <input type="password" name="cpassword" id="cpassword" class="form-control" required placeholder="Password" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="agreement"></label><br>
                            <label><input type="checkbox" name="agreement" id="agreement" title="Centang jika Anda setuju" value="yes" required> Saya setuju dengan <a href="#">Syarat &amp; Ketentuan</a> yang berlaku</label>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <div class="input-group pull-right">
                                <br>
                                <input type="hidden" name="next" id="next" value='<?php echo (isset($next) ? urlencode($next) : ''); ?>'>
                                <a href="<?php echo base_url('customer/login'. (isset($next) ? '?next='. urlencode($next) : '')); ?>">Sudah Punya Akun?</a> &nbsp;
                                <button class="btn btn-primary" type="submit">Mendaftar <i class="fa fa-arrow-right"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="panel panel-login">
            <div class="panel-heading"><h4>Promo</h4></div>
            <div class="panel-body">

            </div>
        </div>
    </div>
</div>
</div>
<?php include 'footer.php'; ?>