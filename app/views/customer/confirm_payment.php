<?php include 'header.php'; ?>
    <div class="row">
            <div class="col-md-3 left-container">
            <?php include 'left-menu.php'; ?>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-right-content">
                            <div class="panel-heading text-center">
                                <h4><i class="fa fa-check"></i> Konfirmasi Pembayaran</h4>
                            </div>
                            <div class="panel-body">
                                <div class="progress progress-xs active invisible" style="height:10px;"><div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%"><span class="sr-only">10% Complete</span></div></div>
                                <form name="confirm_payment" method="post" enctype="multipart/form-data" action="<?php echo base_url('customer/ajax/confirm_payment'); ?>">
                                    <div class="row">                                
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Pilih Tagihan</label>
                                            <?php 
                                            if(isset($invoices) && !empty($invoices)){ 
                                                echo '<select name="invoice_id" id="invoice_id" class="form-control">';
                                                foreach($invoices as $key => $value){
                                                    $timer = strtotime($value['date_expired']);
                                            ?>
                                                <option value="<?php echo $value['id_invoice']; ?>"<?php echo ((isset($id_invoice) && $id_invoice == $value['id_invoice']) ? ' selected' : ''); ?>>#<?php echo $value['id_invoice'] .' - '. date('d-m-Y', $timer) . ' @ '. format_rupiah($value['total']); ?></option>
                                            <?php 
                                                }
                                                echo '</select>';
                                            }else{
                                                echo '<div class="alert alert-info">Maaf, Anda tidak memiliki tagihan yang belum dibayar..</div>';
                                            }
                                            ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <button type="button" id="sendData" class="btn btn-primary pull-right">Kirim</button>
                                            </div>
                                        </div>
                                    </div><!-- row -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Metode Pembayaran</label>
                                                <?php
                                                if(isset($payment_method) && !empty($payment_method)){
                                                    echo '<select name="payment_method" id="payment_method" class="form-control">';
                                                    echo '<option value="0">-- Pilih --</option>';
                                                    foreach($payment_method as $key => $value){
                                                ?>
                                                    <option value="<?php echo $value['id_payment']; ?>"><?php echo $value['payment_name']; ?></option>
                                                <?php 
                                                    }
                                                    echo '</select>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Pembayaran Dikirim Ke:</label>
                                                <?php 
                                                if(isset($bank_account) && !empty($bank_account)){
                                                    echo '<select name="bank_account" id="bank_account" class="form-control">';
                                                    foreach($bank_account as $key => $value){
                                                        echo '<option value="'. $value['id'] .'">'. $value['bank_name'] .' @ '.$value['bank_owner'] .'</option>';
                                                    }
                                                    echo '</select>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div><!-- row -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nominal Pembayaran</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">Rp.</span>
                                                    <input type="text" name="nominal" id="nominal" placeholder="0" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Keterangan</label>
                                                <textarea name="description" id="description" class="form-control" placeholder="Tambahkan keterangan.."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-4">
                                        <label for="file">Upload Gambar Bukti Pembayaran</label>
                                        <div class="uploader-wrap">
                                            <span class="icon"><i class="fa fa-upload"></i> Pilih File</span>
                                            <input type="file" name="file[]" id="fileUploader" multiple accept="image/*">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row images-wrapper"></div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- row 9 -->
            </div><!-- col-md-9 -->
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>