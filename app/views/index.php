<?php include 'header.php'; ?>
        <div class="row">
            <div class="col-md-3 left-container">
                <div class="panel panel-category">
                    <div class="panel-title"><h4 class="lead">Kategori</h4></div>
                    <div class="panel-body">
                        <div class="category-wrapper list-group text-left">
                        <?php
                        if(isset($categories) && !empty($categories)){
                            foreach($categories as $key => $value){
                        ?>
                            <a href="<?php echo base_url('cat/'. $value['slug'] .'/'); ?>" class="list-group-item"><?php echo $value['category_name']; ?></a>
                        <?php 
                            }
                        } 
                        ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row carousel-holder">
                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="5"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="<?php echo asset_url('brosur/1456479411_0_SLB-Unilever-240216.jpg'); ?>" style="height:300px;" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="<?php echo asset_url('brosur/1458007775_1_dc-slb.jpg'); ?>" style="height:300px;" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="<?php echo asset_url('brosur/1458028188_0_SLB-Enchanteur-150316.jpg'); ?>" style="height:300px;" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="<?php echo asset_url('brosur/1458725074_0_SLB-HEAD--SHOULDERS-160316.jpg'); ?>">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="<?php echo asset_url('brosur/1458807407_3_SLB-diaper-fair-160316.jpg'); ?>">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="<?php echo asset_url('brosur/1459214990_1_selasa-soft-drink-SLB.jpg'); ?>">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>

                </div>

                <div class="row">
                <?php if(isset($products) && !empty($products)){ ?>
                <?php
                foreach($products as $key => $product){
                ?>
                    <div class="col-sm-4 col-lg-4 col-md-4 product-item">
                        <div class="thumbnail">
                            <img class="lazy" data-src="<?php echo $product['featured_image']; ?>" style="height:150px;" alt="<?php echo $product['product_title']; ?>">
                            <div class="caption">
                                <h4 class="text-"><a href="<?php echo base_url('produk/'. $product['product_name']); ?>.html"><?php echo $product['product_title']; ?></a>
                                </h4>
                                <h3 class="text-"><?php echo (isset($product['harga_jual']) ? format_harga($product['harga_jual']) : 'Rp 2.400'); ?></h3>
                                <?php
                                $json = json_encode([
                                    'id_product' => $product['id_product'],
                                    'product_title' => clean($product['product_title']),
                                    'image' => str_replace(['_300x150.jpg', '_300x150.png', '_300x150.gif'], ['_60x60.jpg', '_60x60.png', '_60x60.gif'], $product['featured_image']),
                                    'url' => base_url('produk/'. $product['product_name']) .'.html',
                                    'harga' => (int) $product['harga_jual'],
                                    'qty' => 1,
                                    'subtotal' => (int) $product['harga_jual'],
                                    'total' => (int)$product['harga_jual']
                                ]);
                                ?>
                                <p><a href="javascript:void(0);" onclick='sCart.set(<?php echo $json; ?>);sCart.refresh();' class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Beli</a></p>
                            </div>
                            <div class="ratings" style="margin-top:10px">
                                <p class="pull-right">5 Ulasan</p>
                                <p class="text-left">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                </p>
                                <br>
                            </div>
                        </div>
                    </div>
                <?php 
                }
                }
                ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->
    <div class="container"><hr></div>
    <div class="container middle-container">
        <div class="row">
            <div class="col-md-6 text-left">
                <h3>Pembayaran</h3>
                <img src="<?php echo asset_url('img/logo-bank-footer.png'); ?>">
            </div>
            <div class="col-md-3 text-center">
                <h3>Pengiriman</h3>
                <img src="<?php echo asset_url('img/logo-jne.png'); ?>">
            </div>
            <div class="col-md-3 text-center">
                <h3>Ikuti Kami</h3>
                <ul class="inline social-media">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-12 text-left">
                <h3>Tentang HomeMart</h3>
                <p class="text-left">HomeMart adalah minimarket online di Mataram, Lombok, Indonesia yang menyediakan lebih dari 10.000 produk kebutuhan sehari-hari dengan harga kompetitif. HomeMart menjadi solusi praktis belanja bulanan di tengah padatnya aktifitas dan terbatasnya waktu.
                </p>
                <blockquote><em>Belanja online cerdas, hemat dan cepat, ya di HomeMart!</em></blockquote>
            </div>
        </div>
    </div>
    <!-- /.container -->
    <div class="footer-container">
        <hr>
        <!-- Footer -->
        <footer>
            <div class="row-fluid">
                <div class="col-lg-12">
                    <div style="color:#ccc;">
                        <p>&copy; <b>CV. SOLIDTech</b> <?php echo date('Y'); ?> - All right reserved - Powered By <b>Sasak Framework</b></p>
                        <small>Waktu proses : {elapsed_time} detik</small> <small>Penggunaan memory: {memory_usage}</small>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <?php echo (isset($Assets) ? $Assets->getJs() : ''); ?>
</body>
</html>