<?php include 'header.php'; ?>
<!-- Left side column. contains the logo and sidebar -->
<?php include 'menu.php'; ?>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-shopping-cart"></i> Ubah Produk
            <small>Ubah Data Produk</small>
          </h1>
         <?php echo (isset($breadcrumb) ? $breadcrumb->get() : ''); ?>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-sm-12">
                <?php if(isset($message)): ?>
                <div class="alert alert-<?php echo ((isset($message_type) && $message_type) ? 'success' : 'warning'); ?> alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="fa fa-info-circle"></i> Pesan</h4>
                    <?php echo $message; ?>                     
                </div>
                <?php endif; ?>
                <form id="product_form" method="POST" autocomplete="off" enctype="multipart/form-data" data-original-action="<?php echo admin_url('product/edit'); ?>" action="<?php echo admin_url('product/edit'); ?>">
                    <input type="hidden" name="id_product" id="id_product" value="<?php echo (isset($id_product) ? $id_product : ''); ?>">
                    <div class="nav-tabs-custom tabs-success">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_general" data-toggle="tab" aria-expanded="true"><i class="fa fa-info-circle"></i> Umum</a></li>
                            <li class=""><a href="#tab_harga" data-toggle="tab" aria-expanded="true"><i class="fa fa-money"></i> Harga</a></li>
                            <li class=""><a href="#tab_gambar" data-toggle="tab" aria-expanded="true"><i class="fa fa-image"></i> Gambar</a></li>
                            <li><a href="#tab_brand" data-toggle="tab" aria-expanded="false"><i class="fa fa-leaf"></i> Brand</a></li>
                            <li class="pull-right">
                                <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i> Simpan</button>
                            </li>
                            <li class="pull-right">
                                 <a href="<?php echo admin_url('product/index'); ?>">Batal</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_general">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="model">Model</label> <span class="text-danger">*</span>
                                            <div class="input-group">
                                                <input type="text" name="model" id="model" class="form-control" value="<?php echo (isset($model) ? $model : ''); ?>" required>
                                                <span class="input-group-addon" title="Merupakan sebuah kode unik untuk produk"><i class="fa fa-info-circle"></i></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="product_title">Nama Produk</label> <span class="text-danger">*</span>
                                            <div class="input-group">
                                                <input type="text" name="product_title" id="product_title" class="form-control" required value="<?php echo (isset($product_title) ? $product_title : ''); ?>">
                                                <span class="input-group-addon" title="Nama dari Produk. Informasi ini akan ditampilkan untuk pelanggan"><i class="fa fa-info-circle"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="description">Keterangan</label> <span class="" title="Informasi lebih rinci tentang produk"><i class="fa fa-info-circle"></i></span>
                                            <div class="input-grou">
                                                <textarea name="description" id="description" class="form-control" rows="5"><?php echo (isset($description) ? $description : ''); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .row -->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="status">Status</label>
                                            <div class="input-group">
                                                <select name="status" id="status" class="form-control">
                                                    <option value="0"<?php echo ((isset($status) && $status == 0) ? ' selected' : ''); ?>>Konsep</option>
                                                    <option value="1"<?php echo ((isset($status) && $status == 1) ? ' selected' : ''); ?>>Dipublikasikan</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 pull-right">
                                        <div class="form-group">
                                            <label for="product_title">Kategori</label> <span class='text-default'>(opsional)</span>
                                            <div class="input-group col-md-12">
                                                <select name="id_category" id="category" class="form-contro col-md-12">
                                                    <option value="0">Pilih</option>
                                                    <?php if(isset($category_data) && !empty($category_data)): 
                                                        foreach($category_data as $key => $value){
                                                    ?>
                                                        <option value="<?php echo $value['id_category']; ?>"<?php echo ((isset($id_category) && $value['id_category'] == $id_category) ? ' selected' : ''); ?>><?php echo $value['category_name']; ?></option>
                                                    <?php }
                                                    endif;
                                                    ?>
                                                </select>
                                                <!-- <span class="input-group-addon" title="Kategori produk. Untuk mempermudah pelanggan dalam mencari produk yang diinginkan"><i class="fa fa-info-circle"></i></span>-->
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>  
                            <div class="tab-pane" id="tab_harga">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="harga_beli">Harga Beli</label> <span class="text-default">(opsional)</span>
                                            <div class="input-group">
                                                <span class="input-group-addon">Rp.</span>
                                                <input type="text" name="harga_beli" id="harga_beli" class="form-control" placeholder="0" value="<?php echo (isset($harga_beli) ? $harga_beli : ''); ?>">
                                                <span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="harga_jual">Harga Jual</label> <strong class="text-danger">(*)</strong>
                                            <div class="input-group">
                                                <span class="input-group-addon">Rp.</span>
                                                <input type="text" name="harga_jual" id="harga_jual" class="form-control" placeholder="0" value="<?php echo (isset($harga_jual) ? $harga_jual : ''); ?>">
                                                <span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="harga_coret" style="font-style:strike;">Harga Coret</label> <span class="text-default">(opsional)</span>
                                            <div class="input-group">
                                                <span class="input-group-addon">Rp.</span>
                                                <input type="text" name="harga_coret" id="harga_coret" class="form-control" placeholder="0" value="<?php echo (isset($harga_coret) ? $harga_coret : ''); ?>">
                                                <span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">                                        
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="stok">Stok</label> <strong class="text-danger">(*)</strong>
                                                    <div class="input-group">
                                                        <input type="text" name="stock" id="stok" class="form-control"  placeholder="0" value="<?php echo (isset($stock) ? $stock : ''); ?>">
                                                        <span class="input-group-addon" title="Stok Produk / Jumlah persediaan produk"><i class="fa fa-info-circle"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- form-group -->
                                        <!--<div class="form-group">
                                            <div class="btn-grup">
                                                <button type="button" class="btn btn-sm btn-success hide" id="save_prop"><i class="fa fa-save"></i> Simpan</button>
                                                <button type="button" class="btn btn-sm btn-success" id="add_prop"><i class="fa fa-plus"></i> Tambah</button>
                                                <button type="button" class="btn btn-sm btn-default" id="reset_prop">Reset</button>
                                                <button type="button" class="btn btn-sm btn-danger" id="clear_prop"><i class="fa fa-clear"></i> Clear</button>
                                            </div>
                                        </div>-->
                                    </div>
                                </div><!-- row -->
                            </div><!-- #tab_harga -->
                            <div class="tab-pane" id="tab_gambar">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="file">Upload Gambar</label>
                                        <div class="uploader-wrap">
                                            <span class="icon"><i class="fa fa-upload"></i> Pilih File</span>
                                            <input type="file" name="file[]" id="fileUploader" multiple accept="image/*">
                                        </div>
                                        <input type="hidden" name="images" id="images" value='<?php echo (isset($images) ? clean($images) : ''); ?>'>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row images-wrapper">
                                        <?php if(isset($images_array) && !empty($images_array)){ ?>
                                        <?php $i = 0; ?>
                                        <!-- <?php print_r($images_array); ?> -->
                                        <?php foreach($images_array as $key => $img) { ?>                                        
                                            <div class="img-wrapper col-md-4 processed" style="margin-bottom:10px;">
                                                <div style="height:160px; border:1px solid #eee; text-align:center;padding-top:70px;background-image:url('<?php echo (isset($img['thumbnail_url']) ? $img['thumbnail_url'] : (isset($img['url']) ? $img['url'] : '')); ?>'); background-repeat: no-repeat; background-size: cover;">
                                                    <span class="remove bg-red" onclick="removeUploadedImage(this, <?php echo $i++; ?>);"><i class="fa fa-times"></i></span>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- #tab_gambar -->
                            <div class="tab-pane" id="tab_brand">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h4>Pilih Brand</h4>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <select name="id_brand" id="brand" class="form-control">
                                                    <option value="null">Pilih</option>
                                                <?php if(isset($brand)){ ?>
                                                <?php foreach($brand as $key => $value){ ?>
                                                    <option value="<?php echo $value['id_brand']; ?>"<?php echo ((isset($id_brand) && $id_brand == $value['id_brand']) ? ' selected' : ''); ?>><?php echo $value['brand_name']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                                </select>
                                                <span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <h4 class="text-center"><i class="fa fa-plus"></i> Tambah Brand Baru</h4>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="brand_name">Nama Brand</label>
                                                <div class="input-group">
                                                    <input type="text" id="brand_name" class="form-control brand-data">
                                                    <span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="brand_phone">Telpon</label>
                                                <div class="input-group">
                                                    <input type="text" id="brand_phone" class="form-control brand-data">
                                                    <span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="brand_email">Email</label>
                                                <div class="input-group">
                                                    <input type="text" id="brand_email" class="form-control brand-data">
                                                    <span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="description">Description</label>
                                                <textarea id="brand_description" class="form-control brand-data"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button type="button" id="save_brand" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- row -->
                            </div><!-- #tab_brand -->
                        </div>
                        <div class="tab-footer">

                        </div>
                    </div>
                </form>
            </div>
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include 'footer.php'; ?>
    <?php echo (isset($Assets) ? $Assets->getJs() : ''); ?>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--<script src="assets/dist/js/pages/dashboard.js"></script>-->
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('assets/dist/js/demo.js'); ?>"></script>
    <script>
    <?php 
    $time = date('d-m') .'-'. ((int)date('Y') - 15);
    $maxDate = (int) (strtotime($time));
    echo '/* time: '. $time .'; maxDate :'. $maxDate .' */'."\n";
    $startY = $maxDate; 
    echo 'var startDate = "'. date('Y/m/d', $startY) .'";'."\n";
    ?>
    $(function(){
        $('#tanggal_lahir').datepicker({
            autoclose:true,
            language:'id',
            format:'yyyy-mm-dd',
            endDate: startDate
        });
        $('#tanggal_lahir').inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
    });
    </script>    
  </body>
</html>