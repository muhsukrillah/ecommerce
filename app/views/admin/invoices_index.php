<?php include 'header.php'; ?>
<!-- Left side column. contains the logo and sidebar -->
<?php include 'menu.php'; ?>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-tag"></i> Tagihan
            <small>Data Tagihan</small> 
          </h1>
          <?php echo (isset($breadcrumb) ? $breadcrumb->get() : ''); ?>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-sm-12">
              <div class="nav-tabs-custom tabs-success">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_data" data-toggle="tab" aria-expanded="true"><i class="fa fa-list"></i> Data</a></li>
                    <li class=""><a href="#tab_option" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog"></i> Opsi</a></li>
                    <li class="pull-right">
                        <div class="btn-group">
                        <a href="<?php echo base_url('product/cetak'); ?>" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Cetak</a>
                        </div>
                    </li>
                </ul>
                <div class="tab-content">
                  <div class="table-wrapper tab-pane active" id="tab_data">
                    <div class="pane panel-right-content">
                            <div class="panel-bod">
                                <table class="table table-responsive table-striped">
                                <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Pelanggan</th>
                                        <th>Tanggal</th>
                                        <th>Jatuh Tempo</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(isset($invoice_data) && !empty($invoice_data)){

                                    foreach($invoice_data as $key => $value){
                                        $time = strtotime($value['date_added']);
                                        $time_ex = strtotime($value['date_expired']);

                                        echo '<tr>';
                                        echo '<td>#'. $value['id_invoice'] .'</td>';
                                        echo '<td>'. $value['customer_name'] .'</td>';
                                        echo '<td>'. date('d/m/Y', $time) .'</td>';
                                        echo '<td>'. date('d/m/Y', $time_ex) .'</td>';
                                        echo '<td><span class="curency">'. format_rupiah($value['total']) .'</span></td>';
                                        $status = '<span class="label label-danger label-sm">Belum Dibayar</span>';
                                        if($value['status']){
                                            $status = '<span class="label label-success label-sm">Sudah Dibayar</span>';
                                        }
                                        echo '<td>'. $status .'</td>';
                                        echo '<td>';
                                        echo '<a href="'. admin_url('invoice/detail?invoice_id='. $value['id_invoice']) .'" class="label label-info label-sm tooltips" data-toggle="tooltip" title="Lihat Detail Tagihan"><i class="fa fa-search"></i></a>';
                                        echo '</td>';
                                        echo '</tr>';
                                    }
                                }
                                ?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    <!-- pagination -->
                    <div class="row">
                      <div class="col-md-6">
                        <strong>Total Data:</strong> <?php echo (isset($total) ? $total : 0); ?>
                      </div>
                      <div class="col-md-6">
                        <div class="btn-group pull-right">
                        <?php echo (isset($pagination) ? $pagination : ''); ?>
                        </div>
                      </div>
                    </div>
                  </div><!-- tab-pane -->
                  <div class="tab-pane" id="tab_option">
                    <div class="row">                            
                      <form>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Jumlah Data Perhalaman</label>
                            <div class="input-group input-group-sm pull-left">                           
                              <div class="input-group-addon tooltips" data-toggle="tooltip" data-title="jumlah data perhalaman">Tampilkan jumlah data</div>
                              <input type="hidden" id="curPerPage" value='<?php echo (isset($perPage) ? $perPage : 0); ?>'>                              
                              <input type="hidden" name="page" value='<?php echo (isset($page) ? $page : 1); ?>'/>
                              <?php if(isset($query) && !empty($query)){ ?>
                                <input type="hidden" name="query" value='<?php echo $query; ?>'/>
                              <?php } ?>
                              <input type="number" min="1" name="perPage" id="perPage" size="3" class="form-control" value='<?php echo (isset($perPage) ? $perPage : 10); ?>' onblur="(this.form.perPage.value != this.form.curPerPage.value) ? this.form.submit() : false;"/>
                              <div class="input-group-btn">
                                <button type="submit" class="btn btn-sm">OK</button>
                              </div>                              
                            </div>
                          </div><!-- form-group -->
                        </div>
                      </form>
                    </div>
                  </div>
                </div><!-- tab-content -->
              </div>
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php 
    include 'footer.php'; 
    // echo assets
    echo (isset($Assets) ? $Assets->getJs() : '');
    ?>
  </body>
</html>