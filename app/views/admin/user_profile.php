<?php include 'header.php'; ?>
<!-- Left side column. contains the logo and sidebar -->
<?php include 'menu.php'; ?>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-users"></i> Profil Pegawai
            <small>Data Pegawai <?php echo (isset($pegawai) ? '['. $pegawai->nama .']' : ''); ?></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class=""><a href="<?php echo base_url('pegawai'); ?>">Pegawai</a></li>
            <li class="active">Profil <?php echo (isset($pegawai) ? '['. $pegawai->nama .']' : ''); ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-sm-12">
              <div class="box box-primary">
                <?php if((isset($found)) && isset($pegawai)){ ?>
                <div class="box-body">                  
                    <div class=".content" style="margin:10px 0px;">
                      <div class="row">
                        <div class="col-md-7">
                            <div class="col-md-5">
                                <div class="foto">
                                <?php 
                                if(isset($pegawai->foto) && !empty($pegawai->foto)){
                                  $foto = $pegawai->foto;
                                }else{
                                  $foto = asset_url('gambar/muh.sukrillah.gif');
                                }
                                ?>
                                  <img src="<?php echo $foto; ?>" width="100%"/>
                                </div>
                            </div>
                            <div class="col-md-7">
                              <div class="section">
                                <label class='text-info'>Nomor Induk Pegawai</label>
                                <fieldset>                                  
                                  <legend><?php echo $pegawai->nip; ?></legend>
                                </fieldset>
                                <label class='text-info'>Nama Lengkap</label>
                                <fieldset>
                                  <legend><?php echo $pegawai->nama; ?></legend>
                                </fieldset>
                                <label class='text-info'>Tanggal Lahir</label>
                                <fieldset>
                                  <legend>
                                    <?php echo date_id(strtotime($pegawai->tanggal_lahir)); ?> 
                                    (<?php echo $pegawai->umur; ?> Th)
                                  </legend>
                                </fieldset>
                                <label class='text-info'>Jenis Kelamin</label>
                                <fieldset>
                                  <legend><?php echo ($pegawai->jenis_kelamin == 'L' ? 'Pria' : 'Wanita'); ?></legend>
                                </fieldset>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                          <div class="section">
                            <label class='text-info'>Status Nikah</label>
                            <fieldset>                                  
                              <legend><?php echo ($pegawai->status_nikah ? 'Menikah' : 'Belum Menikah'); ?></legend>
                            </fieldset>
                            <?php if($pegawai->status_nikah){ ?>
                            <label class='text-info'>Jumlah Istri</label>
                            <fieldset>
                              <legend><?php echo $pegawai->jumlah_istri; ?></legend>
                            </fieldset>
                            <label class='text-info'>Jumlah Anak</label>
                            <fieldset>
                              <legend><?php echo $pegawai->jumlah_anak; ?></legend>
                            </fieldset>
                            <?php } ?>
                            <label class='text-info'>Gaji Pokok</label>
                            <fieldset>
                              <legend>Rp. <?php echo $pegawai->gaji('gaji_pokok'); ?> <span class="pull-right"><a href="<?php echo base_url('gaji/?nip='. $pegawai->nip); ?>" class="label label-success label-sm">Lihat Detail Gaji <i class="fa fa-external-link"></i></a></span></legend>
                            </fieldset>
                          </div>
                        </div>
                      </div><!-- /.row -->
                      <div class="row">
                        <div class="col-md-4">
                          <div class="section">
                            <label class='text-info'>Alamat</label>
                            <fieldset>                                  
                              <legend><?php echo clean($pegawai->alamat); ?></legend>
                            </fieldset>
                            <label class='text-info'>Nomor HP</label>
                            <fieldset>
                              <legend><?php echo $pegawai->hp; ?></legend>
                            </fieldset>
                            <label class='text-info'>Email</label>
                            <fieldset>
                              <legend><?php echo $pegawai->email; ?></legend>
                            </fieldset>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="section">
                            <label class='text-info'>Nomor Rekening</label>
                            <fieldset>                                  
                              <legend><?php echo $pegawai->no_rekening; ?></legend>
                            </fieldset>
                            <label class='text-info'>Nama Pemilik Rekening</label>
                            <fieldset>
                              <legend><?php echo $pegawai->nama_pemilik_rekening; ?></legend>
                            </fieldset>
                            <label class='text-info'>Nama Bank Rekening</label>
                            <fieldset>
                              <legend><?php echo $pegawai->nama_bank_rekening; ?></legend>
                            </fieldset>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="section">
                            <label class='text-info'>Jabatan</label>
                            <fieldset>                                  
                              <legend><?php echo $pegawai->nama_jabatan; ?></legend>
                            </fieldset>
                            <label class='text-info'>Nama Pemilik Rekening</label>
                            <fieldset>
                              <legend><?php echo $pegawai->nama_pemilik_rekening; ?></legend>
                            </fieldset>
                            <label class='text-info'>Nama Bank Rekening</label>
                            <fieldset>
                              <legend><?php echo $pegawai->nama_bank_rekening; ?></legend>
                            </fieldset>
                          </div>
                        </div>
                      </div><!-- /.row -->
                      <div class="row">
                        <div class="col-md-12"><h3>Data Login Akun</h3></div>
                        <div class="col-md-12">
                          <div class="section col-md-4">
                            <label class='text-info'>Nama Pengguna</label>
                            <fieldset>                                  
                              <legend><?php echo (isset($pegawai->username) ? $pegawai->username : 'Belum Ada'); ?></legend>
                            </fieldset>
                          </div>
                          <div class="section col-md-4">
                            <label class='text-info'>Kata Sandi</label>
                            <fieldset>
                              <legend>*****</legend>
                            </fieldset>
                          </div>
                          <div class="section col-md-4">
                            <label class='text-info'>Level</label>
                            <fieldset>
                              <legend><?php echo get_user_level((isset($pegawai->level) ? $pegawai->level : 4)); ?></legend>
                            </fieldset>
                          </div>
                        </div>
                      </div>
                    </div><!-- /.content -->                  
                </div>
                <div class="box-footer">
                  <div class="pull-left">
                    <strong>Tanggal Terdaftar : </strong> <?php echo date_id(strtotime($pegawai->date_add)); ?>
                  </div>
                  <div class="pull-right">
                    <a class="btn btn-flat btn-default btn-sm" href="<?php echo base_url('pegawai/edit?nip='. $pegawai->nip); ?>"><i class="fa fa-edit"></i> Ubah Data</a>
                  </div>
                </div>
                <?php }else{ ?>
                  <div class="box-body">
                    <div class="alert alert-danger">Data tidak tersedia..</div>
                  </div>
                <?php } ?>
            </div><!-- /.box -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include 'footer.php'; ?>
<!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('assets/js/jQuery-2.1.4.min.js'); ?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <!--
    <script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
    <!-- FastClick --
    <script src="<?php echo base_url('assets/plugins/fastclick/fastclick.min.js'); ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('assets/dist/js/app.min.js'); ?>"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--<script src="assets/dist/js/pages/dashboard.js"></script>-->
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('assets/dist/js/demo.js'); ?>"></script>
  </body>
</html>