<?php include 'header.php'; ?>
<!-- Left side column. contains the logo and sidebar -->
<?php include 'menu.php'; ?>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-tags"></i> Kategori Produk
            <small>Data kategori produk</small>
          </h1>
         <?php echo (isset($breadcrumb) ? $breadcrumb->get() : ''); ?>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-sm-12">
                <?php if(isset($message)): ?>
                <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="fa fa-info-circle"></i> Pesan</h4>
                    <?php echo $message; ?>                     
                </div>
                <?php endif; ?>
                <form id="product_form" method="POST" action="<?php echo (isset($form_action) ? $form_action : admin_url('product/category/new')); ?>" autocomplete="off">
                    <div class="nav-tabs-custom tabs-success">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="fa fa-tags"></i> Kategori</a></li>
                            <li class="pull-right">
                                <button class="btn btn-primary btn-sm" type="submit" name="save"><i class="fa fa-save"></i> Simpan</button>
                            </li>
                            <li class="pull-right">
                                 <a id="cancel" href="<?php echo admin_url('product/category/'); ?>">Batal</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                        <?php
                                        if(isset($cdata) && isset($cdata['id_category'])){
                                        ?>
                                        <input type="hidden" name="id_category" value="<?php echo $cdata['id_category']; ?>">
                                        <?php
                                        }
                                        ?>
                                            <label for="category_name">Nama Kategori <b class='text-danger'>*</b></label>
                                            <div class="input-grou">
                                                <input type="text" name="category_name" id="category_name" class="form-control" required value='<?php echo (isset($cdata['category_name']) ? $cdata['category_name'] : ''); ?>' placeholder="Masukkan nama kategori di sini..">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="order">Order</label> <span class="text-default">(opsional)</span>
                                            <div class="input-group">
                                                <input type="text" name="order" id="order" value="<?php echo ((isset($cdata) && isset($cdata['order'])) ? $cdata['order'] : 0); ?>" class="form-control">
                                                <span class="input-group-addon" title="Urutan kategori.."><i class="fa fa-info-circle"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="description">Keterangan</label> <span class='text-default'>(opsional)</span>
                                            <div class="input-grou">
                                                <textarea name="description" id="description" class="form-control"><?php echo ((isset($cdata) && isset($cdata['description'])) ? clean($cdata['description']) : ''); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="product_title">Induk Kategori</label> <span class='text-default'>(opsional)</span>
                                            <div class="input-grou">
                                                <select name="parent_category" id="parent_category" class="form-control">
                                                    <option value="0">Pilih</option>
                                                    <?php if(isset($category_data) && !empty($category_data)): 
                                                        foreach($category_data as $key => $value){
                                                    ?>
                                                        <option value="<?php echo $value['id_category']; ?>"<?php echo ((isset($cdata) && isset($cdata['parent_id']) && $cdata['parent_id'] == $value['id_category']) ? ' selected' : ''); ?>><?php echo $value['category_name']; ?></option>
                                                    <?php }
                                                    endif;
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .row -->
                            </div>
                        </div>
                    </div>
                </form>
            </div><!-- col-sm-12 -->
            <div class='col-md-12'>
                <div class="box box-primary">
                    <div class="box-body">
                        <table class="table table-responrive table-striped">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Kategori</th>
                                <th>Keterangan</th>
                                <th>Order</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(isset($category_data) && !empty($category_data)){
                            $i = 1;
                            foreach($category_data as $key => $value){
                                // buat row baru
                                echo '<tr>';
                                    // nomor
                                    echo '<td>'. ($i) .'.</td>';
                                    // nama kategori
                                    echo '<td>'. $value['category_name'] .'</td>';
                                    // deskripsi
                                    echo '<td>'. $value['description'] .'</td>';
                                    // order
                                    echo '<td>'. $value['order'] .'</td>';
                                    // aksi
                                    echo '<td>';
                                    echo '<div class="btn-group pull-right">';
                                    // aksi info
                                    //echo '<a href="'. admin_url('product/category/detail?id_category='. $value['id_category']) .'" class="btn btn-info btn-xs"><i class="fa fa-info"></i> detail</a> ';
                                    // aksi edit
                                    echo '<a href="'. admin_url('product/category/?id_category='. $value['id_category']) .'" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i> edit</a> ';
                                    // aksi hapus
                                    echo '<a href="'. admin_url('product/category/delete?id_category='. $value['id_category']) .'" class="btn btn-danger btn-xs" onclick="return confirm(\'Apakah anda yakin?\');"><i class="fa fa-trash-o"></i> hapus</a>';
                                    echo '</div>';
                                    echo '</td>';
                                echo '</tr>';
                                $i++;
                            }
                        }
                        ?>
                        </tbody>
                        </table>
                    </div><!-- box-body -->
                </div><!-- box -->
            </div><!-- col-md-12 -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include 'footer.php'; ?>
    <?php echo (isset($Assets) ? $Assets->getJs() : ''); ?>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--<script src="assets/dist/js/pages/dashboard.js"></script>-->
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('assets/dist/js/demo.js'); ?>"></script>
    <script>
    <?php 
    $time = date('d-m') .'-'. ((int)date('Y') - 15);
    $maxDate = (int) (strtotime($time));
    echo '/* time: '. $time .'; maxDate :'. $maxDate .' */'."\n";
    $startY = $maxDate; 
    echo 'var startDate = "'. date('Y/m/d', $startY) .'";'."\n";
    ?>
    $(function(){
        $('#tanggal_lahir').datepicker({
            autoclose:true,
            language:'id',
            format:'yyyy-mm-dd',
            endDate: startDate
        });
        $('#tanggal_lahir').inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
    });
    </script>    
  </body>
</html>