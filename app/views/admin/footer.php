<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 31/03/2015
 * Time: 22:55
 */ 
?>
  <footer class="main-footer hide-print">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>&copy; Copyright: <a href="http://www.sukrillah.xyz">Muh. Sukrillah</a>.</strong> All rights reserved. <strong>Waktu proses:</strong> {elapsed_time} detik, <strong>Penggunaan Memory:</strong> {memory_usage}
  </footer>
</div><!-- ./wrapper -->