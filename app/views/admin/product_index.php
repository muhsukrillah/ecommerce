<?php include 'header.php'; ?>
<!-- Left side column. contains the logo and sidebar -->
<?php include 'menu.php'; ?>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-bookmark"></i> Produk
            <small>Data Produk</small> 
            <a href="<?php echo admin_url('product/add/'); ?>" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i> Tambah Produk Baru</a>
          </h1>
          <?php echo (isset($breadcrumb) ? $breadcrumb->get() : ''); ?>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-sm-12">
              <div class="nav-tabs-custom tabs-success">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_data" data-toggle="tab" aria-expanded="true"><i class="fa fa-list"></i> Data</a></li>
                    <li class=""><a href="#tab_option" data-toggle="tab" aria-expanded="true"><i class="fa fa-cog"></i> Opsi</a></li>
                    <li class="pull-right" style="width:25%;">
                      <div class="search-container">
                        <form onsubmit="return (!this.query.value.length ? false : true);">
                          <div class="form-grou">
                            <div class="input-group">
                              <input type="text" id="query" name="query" placeholder="Cari Produk.." class="form-control" value='<?php echo (isset($query) ? clean($query) : ''); ?>'>
                              <div class="input-group-btn">
                                 <button class="btn"><i class="fa fa-search"></i></button>
                              </div>                      
                            </div>
                          </div>
                        </form>
                      </div>
                    </li>
                    <li class="pull-right">
                      <?php if(\sasak\User::$role->can('create_pegawai')){ ?>
                        <div class="btn-group">
                        <a href="<?php echo base_url('product/cetak'); ?>" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Cetak</a>
                        </div>
                      <?php } ?>
                    </li>
                </ul>
                <div class="tab-content">
                  <div class="table-wrapper tab-pane active" id="tab_data">
                    <table class="table table- table-responsive table-striped responsive-table">
                      <thead>
                        <tr class="bg-">
                          <th><a href="<?php echo uri()->compare(['orderby'=>'date_added.'. ((isset($order) && isset($order[1]) && $order[1] == 'asc') ? 'desc' : 'asc')]); ?>">No. <?php echo (isset($order) ? order_status($order[0], 'date_added', $order[1]) : ''); ?></a></th>
                          <th><a href="<?php echo uri()->compare(['orderby'=>'model.'. ((isset($order) && isset($order[1]) && $order[1] == 'asc') ? 'desc' : 'asc')]); ?>">Model <?php echo (isset($order) ? order_status($order[0], 'model', $order[1]) : ''); ?></a></th>
                          <th><a href="<?php echo uri()->compare(['orderby'=>'product_title.'. ((isset($order) && isset($order[1]) && $order[1] == 'asc') ? 'desc' : 'asc')]); ?>">Nama <?php echo (isset($order) ? order_status($order[0], 'product_title', $order[1]) : ''); ?></a></th>
                          <th><a href="<?php echo uri()->compare(['orderby'=>'stock_akhir.'. ((isset($order) && isset($order[1]) && $order[1] == 'asc') ? 'desc' : 'asc')]); ?>">Stok <?php echo (isset($order) ? order_status($order[0], 'stock_akhir', $order[1]) : ''); ?></a></th>
                          <th class="text-right">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php 
                      $i = 1;
                      // cek apakah ada data
                      if(isset($data_produk) && !empty($data_produk)){
                        // buat daftar / list produk
                        foreach($data_produk as $key => $data){                          
                      ?>
                          <tr>
                            <td><?php echo ($i++); ?>.</td>
                            <td><?php echo $data['model']; ?></td>
                            <td><a href="<?php echo admin_url('product/detail?id_product='. $data['id_product']); ?>"><?php echo $data['product_title']; ?></a></td>
                            <td><?php echo $data['stock_akhir']; ?></td>
                            <td class="right-align align-right text-right">
                              <a href="<?php echo admin_Url('product/detail?id_product='. $data['id_product']); ?>" data-toggle="tooltip" class="tooltips label label-info" title="Lihat Data Lengkap"><i class="fa fa-eye"></i></a> 
                              <?php if(isset($allow_edit) && $allow_edit) { ?>
                              <a href="<?php echo admin_url('product/edit?id_product='. $data['id_product']); ?>" class="text-warning tooltips label label-warning" data-toggle="tooltip" title="Ubah Data"><i class="fa fa-edit"></i></a> 
                              <?php } ?>
                              <?php if(isset($allow_delete) && $allow_delete) { ?>
                              <a onclick='return confirm("Apakah Anda yakin ingin menghapus data ini?");' href="<?php echo base_url('product/delete?id_product='. $data['id_product']); ?>" class="text-danger tooltips label label-danger" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash-o"></i></a>
                              <?php } ?>
                            </td>
                          </tr>
                      <?php
                          
                        }
                      }
                      ?>
                      </tbody>
                    </table>
                    <!-- pagination -->
                    <div class="row">
                      <div class="col-md-6">
                        <strong>Total Data:</strong> <?php echo ($i-1); ?> dari <?php echo (isset($total) ? $total : 0); ?>
                      </div>
                      <div class="col-md-6">
                        <div class="btn-group pull-right">
                        <?php echo (isset($pagination) ? $pagination : ''); ?>
                        </div>
                      </div>
                    </div>
                  </div><!-- tab-pane -->
                  <div class="tab-pane" id="tab_option">
                    <div class="row">                            
                      <form>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Jumlah Data Perhalaman</label>
                            <div class="input-group input-group-sm pull-left">                           
                              <div class="input-group-addon tooltips" data-toggle="tooltip" data-title="jumlah data perhalaman">Tampilkan jumlah data</div>
                              <input type="hidden" id="curPerPage" value='<?php echo (isset($perPage) ? $perPage : 0); ?>'>                              
                              <input type="hidden" name="page" value='<?php echo (isset($page) ? $page : 1); ?>'/>
                              <?php if(isset($query) && !empty($query)){ ?>
                                <input type="hidden" name="query" value='<?php echo $query; ?>'/>
                              <?php } ?>
                              <input type="number" min="1" name="perPage" id="perPage" size="3" class="form-control" value='<?php echo (isset($perPage) ? $perPage : 10); ?>' onblur="(this.form.perPage.value != this.form.curPerPage.value) ? this.form.submit() : false;"/>
                              <div class="input-group-btn">
                                <button type="submit" class="btn btn-sm">OK</button>
                              </div>                              
                            </div>
                          </div><!-- form-group -->
                        </div>
                      </form>
                    </div>
                  </div>
                </div><!-- tab-content -->
              </div>
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include 'footer.php'; ?>
    <?php /*
<!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('assets/js/jQuery-2.1.4.min.js'); ?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <!--
    <script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
    <!-- FastClick -
    <script src="<?php echo base_url('assets/plugins/fastclick/fastclick.min.js'); ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('assets/dist/js/app.min.js'); ?>"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--<script src="assets/dist/js/pages/dashboard.js"></script>-->
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('assets/dist/js/demo.js'); ?>"></script>
    */
    echo (isset($Assets) ? $Assets->getJs() : '');
    ?>
  </body>
</html>