<?php include 'header.php'; ?>
<!-- Left side column. contains the logo and sidebar -->
<?php include 'menu.php'; ?>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-tag"></i> Tagihan
            <small>Data Tagihan</small> 
          </h1>
          <?php echo (isset($breadcrumb) ? $breadcrumb->get() : ''); ?>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-sm-12">
              <div class="nav-tabs-custom tabs-success">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_data" data-toggle="tab" aria-expanded="true"><i class="fa fa-list"></i> Data</a></li>
                    <li class="pull-right">
                        <div class="btn-group">
                        <a href="<?php echo base_url('product/cetak'); ?>" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Cetak</a>
                        </div>
                    </li>
                </ul>
                <div class="tab-content">
                  <div class="table-wrapper tab-pane active" id="tab_data">
                    <div class="pane panel-right-content">
                            <div class="panel-bod">
                                <table class="table table-responsive table-striped">
                                <caption><h3>Data Tagihan</h3>
                                <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Pelanggan</th>
                                        <th>ID Order</th>
                                        <th>Tanggal</th>
                                        <th>Jatuh Tempo</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>#<?php echo $invoice_data['id_invoice']; ?></td>
                                    <td><?php echo $invoice_data['customer_name']; ?></td>
                                    <td><?php echo $invoice_data['id_order']; ?></td>
                                    <td><?php echo date_to_date_id($invoice_data['date_added']); ?></td>
                                    <td><?php echo date_to_date_id($invoice_data['date_expired']); ?></td>
                                    <td><label class="label label-info"><?php echo format_rupiah($invoice_data['total']); ?></label></td>
                                    <td><?php 
                                    if($invoice_data['status']){
                                      echo '<span class="label label-success">Sudah Dibayar</span>';
                                    }else{
                                      echo '<span class="label label-danger">Belum Dibayar</span>';
                                    }
                                    ?></td>
                                    <td></td>
                                  </tr>
                                </tbody>
                                </table>
                                <?php
                                $images = [];
                                if(isset($invoice_payment) && !empty($invoice_payment)){
                                  extract($invoice_payment);
                                ?>
                                <div class="clear clearfix"></div>
                                 <table class="table table-responsive table-striped">
                                 <caption><h3>Data Pembayaran</h3></caption>
                                 <body>                                  
                                  <tr>
                                    <th>Pelanggan</th>
                                    <td>:</td>
                                    <td><?php echo (isset($customer_full_name) ? $customer_full_name : ''); ?></td>
                                  </tr>
                                  <tr>
                                    <th>Tanggal</th>
                                    <td>:</td>
                                    <td><?php echo (isset($date_added) ? date_to_date_id($date_added) : ''); ?></td>
                                  </tr>
                                  <tr>
                                    <th>Jumlah Pembayaran</th>
                                    <td>:</td>
                                    <td><label class="label label-info"><?php echo format_rupiah($invoice_payment['value']); ?></label></td>
                                  </tr>
                                  <tr>
                                    <th>Metode Pembayaran</th>
                                    <td>:</td>
                                    <td><?php echo $invoice_payment['payment_method']; ?></td>
                                  </tr>
                                  <tr>
                                    <th>Rekening Tujuan</th>
                                    <td>:</td>
                                    <td><?php echo $invoice_payment['bank_account']; ?></td>
                                  </tr>
                                    <th>Keterangan</th>
                                    <td>:</td>
                                    <td><?php echo $invoice_payment['description']; ?></td>
                                  </tr>
                                 </thead>
                                 <tbody>
                                  <?php if(isset($invoice_payment['evaluated']) && !$invoice_payment['evaluated']){ ?>
                                  <tr>
                                    <td colspan="3" class="text-right">
                                      <input type="hidden" id="payment_id" value="<?php echo $invoice_payment['id']; ?>">
                                      <input type="hidden" id="invoice_id" value='<?php echo $invoice_payment['id_invoice']; ?>'>
                                      <button type="button" class="btn btn-success" id="approvePayment"><i class="fa fa-check"></i> Tanggapi Pembayaran</button>
                                    </td>
                                  </tr>
                                  <?php } ?>
                                <?php
                                  if(isset($invoice_payment['images']) && !empty($invoice_payment['images'])){
                                      $images = @unserialize($invoice_payment['images']);
                                    }
                                 ?>
                                 </tbody>
                                 </table>
                                 <?php }else{
                                  echo '<div class="clear clearfix"></div>';
                                  echo '<div class="text-danger">Belum ada pembayaran..</div>';
                                  } ?>
                                 <div class="clear clearfix"></div>
                                 <?php
                                 if(isset($images) && !empty($images)){
                                  echo '<div class="row">';
                                  foreach($images as $key => $value){
                                    echo '<div class="col-md-4">';
                                    echo '<img src="'. $value['url'] .'" style="width:100%;max-width:100%;">';
                                    echo '</div>';
                                  }
                                  echo '</div>';
                                 }
                                 ?>
                            </div>
                        </div>
                  </div><!-- tab-pane -->
                  <div class="tab-pane" id="tab_option">
                    <div class="row">                            
                      <form>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Jumlah Data Perhalaman</label>
                            <div class="input-group input-group-sm pull-left">                           
                              <div class="input-group-addon tooltips" data-toggle="tooltip" data-title="jumlah data perhalaman">Tampilkan jumlah data</div>
                              <input type="hidden" id="curPerPage" value='<?php echo (isset($perPage) ? $perPage : 0); ?>'>                              
                              <input type="hidden" name="page" value='<?php echo (isset($page) ? $page : 1); ?>'/>
                              <?php if(isset($query) && !empty($query)){ ?>
                                <input type="hidden" name="query" value='<?php echo $query; ?>'/>
                              <?php } ?>
                              <input type="number" min="1" name="perPage" id="perPage" size="3" class="form-control" value='<?php echo (isset($perPage) ? $perPage : 10); ?>' onblur="(this.form.perPage.value != this.form.curPerPage.value) ? this.form.submit() : false;"/>
                              <div class="input-group-btn">
                                <button type="submit" class="btn btn-sm">OK</button>
                              </div>                              
                            </div>
                          </div><!-- form-group -->
                        </div>
                      </form>
                    </div>
                  </div>
                </div><!-- tab-content -->
              </div>
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php 
    include 'footer.php'; 
    // echo assets
    echo (isset($Assets) ? $Assets->getJs() : '');
    ?>
  </body>
</html>