    <aside class="main-sidebar hide-print">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <?php if(!empty(\sasak\User::$data->avatar)){ ?>
              <img src="<?php echo \sasak\User::$data->avatar; ?>" class="img-circle" alt="User Image">
              <?php }else{ ?>
              <img src="<?php echo asset_url('img/blank-user.png'); ?>" class="img-circle" alt="User-Image">
              <?php } ?>
            </div>
            <div class="pull-left info">
              <p><?php echo \sasak\User::$data->display_name ?></p>
              <a href="#!"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>          
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Menu</li>
            <?php echo (isset($menu) ? $menu->get_menu(['class'=>'sidebar-menu'], '', FALSE) : ''); ?>
          </ul>

        </section>
        <!-- /.sidebar -->
      </aside>