<?php include 'header.php'; ?>
<!-- Left side column. contains the logo and sidebar -->
<?php include 'menu.php'; ?>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-leaf"></i> Merk
            <small>Data Merk</small>
          </h1>
         <?php echo (isset($breadcrumb) ? $breadcrumb->get() : ''); ?>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-sm-12">
                <?php if(isset($message)): ?>
                <div class="alert alert-<?php echo ((isset($message_type) && $message_type) ? 'success' : 'warning'); ?> alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="fa fa-info-circle"></i> Pesan</h4>
                    <?php echo $message; ?>                     
                </div>
                <?php endif; ?>
                <form id="product_form" method="POST" autocomplete="off">
                    <div class="nav-tabs-custom tabs-success">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_brand" data-toggle="tab" aria-expanded="false"><i class="fa fa-leaf"></i> Brand</a></li>
                            <li class="pull-right">
                                <button class="btn btn-primary btn-sm" id="save_brand"><i class="fa fa-save"></i> Simpan</button>
                            </li>
                            <li class="pull-right">
                                 <a href="javascript:void(0);" id="cancel">Batal</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_brand">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <input type="hidden" id="id_brand" value="">
                                                <label for="brand_name">Nama Brand</label>
                                                <div class="input-group">
                                                    <input type="text" id="brand_name" class="form-control brand-data">
                                                    <span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="brand_phone">Telpon</label>
                                                <div class="input-group">
                                                    <input type="text" id="brand_phone" class="form-control brand-data">
                                                    <span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="brand_email">Email</label>
                                                <div class="input-group">
                                                    <input type="text" id="brand_email" class="form-control brand-data">
                                                    <span class="input-group-addon"><i class="fa fa-info-circle"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="description">Description</label>
                                                <textarea id="brand_description" class="form-control brand-data"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <table class="table table-striped table-brand">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Brand</th>
                                <th>Telpon</th>
                                <th>Email</th>
                                <th>Deskripsi</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(isset($brand)){ $i = 1; ?>
                        <?php foreach($brand as $key => $value){ ?>
                            <tr data-id="<?php echo $value['id_brand']; ?>" data-json='<?php echo json_encode($value); ?>'>
                                <td><?php echo $i++; ?>.</td>
                                <td><?php echo $value['brand_name']; ?></td>
                                <td><?php echo $value['brand_phone']; ?></td>
                                <td><?php echo $value['brand_email']; ?></td>
                                <td><?php echo $value['description']; ?></td>
                                <td class="text-right"><a href="javascript:void(0);" class="action edit label label-info"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0);" class="action delete label label-danger"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                        <?php } ?>
                        <?php } ?>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include 'footer.php'; ?>
    <?php echo (isset($Assets) ? $Assets->getJs() : ''); ?>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--<script src="assets/dist/js/pages/dashboard.js"></script>-->
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('assets/dist/js/demo.js'); ?>"></script>
    <script>
    <?php 
    $time = date('d-m') .'-'. ((int)date('Y') - 15);
    $maxDate = (int) (strtotime($time));
    echo '/* time: '. $time .'; maxDate :'. $maxDate .' */'."\n";
    $startY = $maxDate; 
    echo 'var startDate = "'. date('Y/m/d', $startY) .'";'."\n";
    ?>
    $(function(){
        $('#tanggal_lahir').datepicker({
            autoclose:true,
            language:'id',
            format:'yyyy-mm-dd',
            endDate: startDate
        });
        $('#tanggal_lahir').inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
    });
    </script>    
  </body>
</html>