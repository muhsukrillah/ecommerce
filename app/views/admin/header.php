<!DOCTYPE html>
<html ng-app="sasak">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo (isset($title) ? $title->get() : 'Dashboard'); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo asset_url('bootstrap/css/bootstrap.min.css'); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo asset_url('plugins/font-awesome/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo asset_url('plugins/select2/select2.min.css'); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo asset_url('dist/css/AdminLTE.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo asset_url('css/print.css'); ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css'); ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/flat/blue.css'); ?>">
    <!-- Morris chart --
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/morris/morris.css'); ?>">
    <!-- jvectormap --
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css'); ?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datepicker/datepicker3.css'); ?>">
    <!-- Daterange picker --
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/daterangepicker/daterangepicker-bs3.css'); ?>">
    <!-- bootstrap wysihtml5 - text editor 
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>">
    -->
    <?php echo (isset($Assets) ? $Assets->getCss() : ''); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
    /*<![CDATA[*/
    var base_url = '<?php echo base_url(); ?>', admin_url = '<?php echo admin_url(); ?>', token='<?php echo \sasak\session()->get_token(); ?>';
    /*]]>*/
    </script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini <?php echo (isset($bodyClass) ? $bodyClass : ''); ?>">
    <div class="wrapper">

      <header class="main-header hide-print">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>HM</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Home Mart</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top hide-print" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <?php if(!empty(\sasak\User::$data->avatar)){ ?>
                  <img src="<?php echo \sasak\User::$data->avatar; ?>" class="user-image" alt="User Image">
                  <?php }else{ ?>
                  <img src="<?php echo asset_url('img/blank-user.png'); ?>" class="user-image" alt="User-Image">
                  <?php } ?>
                  <span class="hidden-xs"><?php echo \sasak\User::$data->display_name; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <?php if(!empty(\sasak\User::$data->avatar)){ ?>
                  <img src="<?php echo \sasak\User::$data->avatar; ?>" class="user-image" alt="User Image">
                  <?php }else{ ?>
                  <img src="<?php echo asset_url('img/blank-user.png'); ?>" class="user-image" alt="User-Image">
                  <?php } ?>
                    <p>
                      <?php echo \sasak\User::$data->display_name; ?> - <?php echo \sasak\User::$role->get_role_title(); ?>
                      <!--<small>Member since Nov. 2012</small>-->
                    </p>
                  </li>
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo \sasak\User::get_profile_url(); ?>" class="btn btn-default btn-flat">Profil</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo \sasak\User::get_logout_url(); ?>" class="btn btn-default btn-flat">Keluar</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button 
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>-->
            </ul>
          </div>
        </nav>
      </header>