<div class="container"><hr></div>
    <div class="container middle-container">
        <div class="row">
            <div class="col-md-6 text-left">
                <h3>Pembayaran</h3>
                <img src="<?php echo asset_url('img/logo-bank-footer.png'); ?>">
            </div>
            <div class="col-md-3 text-center">
                <h3>Pengiriman</h3>
                <img src="<?php echo asset_url('img/logo-jne.png'); ?>">
            </div>
            <div class="col-md-3 text-center">
                <h3>Ikuti Kami</h3>
                <ul class="inline social-media">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-12 text-left">
                <h3>Tentang HomeMart</h3>
                <p class="text-left">HomeMart adalah minimarket online di Mataram, Lombok, Indonesia yang menyediakan lebih dari 10.000 produk kebutuhan sehari-hari dengan harga kompetitif. HomeMart menjadi solusi praktis belanja bulanan di tengah padatnya aktifitas dan terbatasnya waktu.
                </p>
                <blockquote><em>Belanja online cerdas, hemat dan cepat, ya di HomeMart!</em></blockquote>
            </div>
        </div>
    </div>
    <!-- /.container -->