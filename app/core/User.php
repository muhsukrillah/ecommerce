<?php
/**
 * Initial class of system User Class \sasak\User;
 *
 * Dibuat Oleh          : Muh. Sukrillah
 * Url                  : http://www.sukrillah.xyz
 * Versi                : 1.0
 */
namespace app;

class User extends \sasak\User {

    function __construct(){
        parent::__construct();
    }
}