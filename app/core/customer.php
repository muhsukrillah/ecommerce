<?php
/**
 * User Class
 *
 * Dibuat Oleh      : Muh Sukrillah
 * Url              : http://www.sukrillah.xyz
 * Versi            : 1.0
 */
namespace app;

class Customer {
    /**
     * user instance
     *
     * @var         $instance
     * @access      private
     * @type        object
     */
    private static $instance;
    /**
     * user login status
     *
     * @var         $is_loggedin
     * @access      private
     * @type        boolean
     */
    private static $is_loggedin = FALSE;
    /**
     * data instead
     *
     * @var         $data
     * @access      public
     * @type        object
     */
    public static $data;
    /**
     * user class constructor
     *
     * @access      public
     * @param       null
     * @return      null
     */
    function __construct(){
        self::$instance =& $this;
    }

    public static function auth_check(){
        self::$is_loggedin = FALSE;

        $loader = new \sasak\Loader();
        $session =& \sasak\Session::instance();

        $userdata = $session->extract_session();
        if(empty($userdata)){
            return;
        }
        $username = isset($userdata['username']) ? $userdata['username'] : 'unknown';
        $password = isset($userdata['password']) ? $userdata['password'] : '';
        $timestamp = isset($userdata['timestamp']) ? $userdata['timestamp'] : time();

        $customer = $loader->object('customer');
        if(filter_var($username, FILTER_VALIDATE_EMAIL)){
            $data = $customer->get_by(['email' => $username]);
        }else{
            $data = $customer->get_by(['username' => $username]);
        }
        if(isset($data['id_customer'])){
            if(\sasak\Hash::verify($data['password'] . $timestamp, $data['token'])){
                self::$is_loggedin = TRUE;
                $fullname = '';
                if(isset($data['first_name'])){
                    $fullname .= $data['first_name'];
                }
                if(isset($data['last_name'])){
                    $fullname .= ' '. $data['last_name'];
                }
                $data['fullname'] = $fullname;
                self::$data =& $data;
                unset($data);
                return;
            }else{
                //\sasak\Log::add('[Customer AutoLogin] token missmatch.', TRUE, TRUE);
            }
        }else{
            //\sasak\Log::add('[Customer AutoLogin] Customer Data not found with username => '. $username, TRUE, TRUE);
        }
    }
    /**
     * get the customer data
     * 
     * @access  public
     * @param   string meta
     * @return  mixed|boolean
     */
    public static function get_data($meta = ''){
        if(self::$is_loggedin){
            if(isset(self::$data[$meta])){
                return self::$data[$meta];
            }else{
                return FALSE;
            }
        }
        return FALSE;
    }
    /**
     * helper to check the customer was login or not
     *
     * @access  public
     * @param   null
     * @return  bool
     */
    public static function is_logedin(){
        return self::$is_loggedin;
    }
    /**
     * helper to check the user has login and check all session data is valid
     *
     * @access  public
     * @param   string  redirect url
     * @param   bool    auto redirect   default is : true
     * @return  null
     */
    public static function auth($redirect_to = '', $auto_redirect = TRUE){
        // check user credintials
        self::auth_check();
        // check 
        if(!self::is_logedin() && $auto_redirect){
            if(empty($redirect_to)){
                $redirect_to = uri()->current_url();
            }
            $session =& \sasak\Session::instance();
            $session->set_userdata([]);
            redirect(base_url('customer/login') . (!empty($redirect_to) ? '?next='. urlencode($redirect_to) : ''));
        }
    }
    /**
     * helper to get the object instance
     * 
     * @access  public
     * @param   null
     * @return  object instance
     */
    public static function instance(){
        return self::$instance;
    }
}
/*
 * Berkas           : customer.php
 * Lokasi           : /app/core/customer.php
 *
 * Dibuat Oleh      : Muh Sukrillah
 * Url              : http://www.sukrillah.xyz
 */