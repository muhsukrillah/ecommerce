<?php 
function fbLikeCount($appid,$appsecret){
    //Construct a Facebook URL
    $json_url ='https://graph.facebook.com/'.$appid.'?access_token='.$appsecret;
    $json = file_get_contents($json_url);
    $json_output = json_decode($json);

    //Extract the likes count from the JSON object
    if($json_output->likes){
        return $likes = $json_output->likes;
    }else{
        return 0;
    }
}