<?php 
namespace app\controller; 
/**
 * User Controller
 *
 * Dibuat Oleh 			: Muh Sukrillah
 * Url 					: http://www.sukrillah.xyz
 * Versi 				: 1.0
 */
//use app\User;
use system\Controller;
use system\Log;

class User extends Controller {

	function __construct(){
		parent::__construct();
		// cek apakah user sudah login
		\system\User::auth();
		$this->admin_menu->set_active_menu('profil');
	}

	private function ___debug(){
		$ud = $this->session->get_session_data();
		$token = $ud['token'];
		$valid = $this->session->validate_token($token);
		$ud['vtoken'] = $token;
		$ud['token_valid'] = ($valid) ? 'TRUE' : 'FALSE';
		echo '<pre>';
		print_r($ud);
		exit;
	}

	function index(){
		error_reporting(E_ALL);
		//$this->data['title'] =& $this->title;
		//$this->breadcrumb->add('Dashboard', '#', 'fa fa-home');
		//$this->___debug();
		error_reporting(E_ALL);
		$nip = \system\User::$data->nip;
		
		// load pegawai model
		$this->load->model(['pegawai','user']);
		$this->load->object('pegawai');
		$this->load->object('user');
		$pegawai_model = new \app\model\pegawai_model;
		$pegawai = new \system\object_model\pegawai($nip);		
		$pegawai->__set_data($nip, 'nip');

		if(isset($pegawai->nip)){
			$this->data['found'] = TRUE;
			$this->data['pegawai'] =& $pegawai;
			$this->title->add('Profil Saya');
		}else{
			$this->data['found'] = FALSE;
			$this->title->add('Error');
		}
		return $this->load->view('user_profil', $this->data);
		//return $this->load->view('profil_user');
	}

	function edit(){
		error_reporting(E_ALL);
		/*if(!\app\User::$role->can('update_profil')){
			Error('Maaf, Anda tidak diijinkan untuk mengakses halaman ini..');
		}*/

		$this->load->model(['pegawai', 'jabatan', 'user','gaji']);

		$this->load->object('user');

		$nip = \system\User::$data->pegawai->nip;

		$data = $this->input->post();

		if(isset($data) && !empty($data)){

			$nip = (int) (isset($data['last_nip']) ? $data['last_nip'] : 0);
			unset($data['last_nip']);

			$pegawai = $this->pegawai_model->get_by(['nip' => $nip]);

			if(!isset($pegawai['id_user'])){
				die('Data Anda tidak benar!');
			}
			// load user_model
			$user_model = new \system\model\user_model;
			$userdata = new \system\object_model\user($user_model);
			$userdata->__set_data($pegawai['id_user']);
			if(isset($userdata->id_user)){
				$pegawai['username'] 	= $userdata->username;
				$pegawai['level']		= $userdata->level;
				$pegawai['password'] 	= $userdata->password;
			}
			if($pegawai['tanggal_lahir'] == '0000-00-00'){
				$pegawai['tanggal_lahir'] = '';
			}
			//echo '<pre>';
			//print_r($pegawai);
			//echo '</pre>';
			//echo '<br/>';

			$login_data = [];
			foreach($data as $key => $value){
				$key = (string) $key;
				if(in_array($key, ['username','password'])){
					if($key == 'password' && empty($value)){
						unset($data[$key]);
						continue;
					}
					if($key == 'password'){
						if($pegawai[$key] == md5($value)){
							unset($data[$key]);
						}else{
							$login_data[$key] = $value;
						}
						continue;
					}
					if($key == 'username'){
						if($pegawai[$key] == $value){
							unset($data[$key]);
						}else{
							$login_data[$key] = $value;
						}
						continue;
					}

				}
				
				if(!isset($pegawai[$key])){					
					//echo $key .' is not found.. <br/>';
					unset($data[$key]);
					continue;
				}				
				if($data[$key] == $pegawai[$key]){					
					unset($data[$key]);
					//echo '['. $key .'] => it will be ignored..<br/>';
				}
			}
			/*print_r($data);
			print_r($login_data);
			exit;*/
			// foto
			if(isset($_FILES['foto']) && !empty($_FILES['foto'])){
				$this->load->library(['Uploader'], [['namespace'=>'uploader']]);
				$this->uploader->initialize([
					'is_image' 		=> TRUE,
					'upload_path' 	=> ASSETS_PATH .'gambar/',
					'new_name' 		=> $nip,
					'field_name' 	=> 'foto'
				]);
				$upload = $this->uploader->upload();
				if($upload){
					$upload_data = $this->uploader->get_data();
					$foto = 'gambar/'. $upload_data['new_name'];
					$data['foto'] = $foto;
					Log::add('Gambar berhasil diunggah..');
				}else{
					Log::add('Gambar gagal diunggah..');
					Log::add('Uploader : '. $this->uploader->get_message());
				}
			}
			// end foto
			$simpan = $this->pegawai_model->update($data, $nip);
			if($simpan){
				header("location:". base_url('user'));
				exit;
			}
		}
		// buat judul halaman
		$this->title->add('Ubah Profil');
		
		// data pegawai
		$pegawai = $this->pegawai_model->get_by(['nip' => $nip]);
		$this->load->model('user');
		$user_model = new \system\model\user_model;
		// data akun
		$userdata = new \system\object_model\user($user_model);
		$userdata->__set_data((int) $pegawai['id_user']);
		// set data pegawaa[username], pegawai[password], pegawai[level]
		if(isset($userdata->id_user)){
			$pegawai['id_user'] = $userdata->id_user;
			$pegawai['username'] = $userdata->username;
			$pegawai['level'] = $userdata->level;
		}
		// data jabatan
		//if(\app\User::$role->can('update_pegawai')){
		//$this->load->model('jabatan');
		$jabatan = $this->jabatan_model->getAll();
		$jabatan_ar = [];
		foreach($jabatan as $key => $value){
			$jabatan_ar[$value['id_jabatan']] = $value['nama_jabatan'];
		}
		$this->data['jabatan'] = $jabatan_ar;
		if(is_array($pegawai)){
			if($pegawai['tanggal_lahir'] == '0000-00-00'){
				$pegawai['tanggal_lahir'] = '';
			}
			$this->data = array_merge($this->data, $pegawai);
			$this->data['found'] = TRUE;
		}else{
			$pesan = 'Data pegawai dengan nip ['. $nip .'] tidak tersedia..';
			$pesan .= '<a href="'. base_url('pegawai') .'">Kembali</a>';
			$this->data['pesan'] = $pesan;
			$this->data['jenis_pesan'] = 'error';
			$this->data['found'] = FALSE;
		}
		return $this->load->view('user_edit', $this->data);
	}

	private function tos($func){
		return $func('Halo, ');
	}

	function tes($func){

		return $this->tos(function($text = ''){
			return $text .'nama saya muh. sukrillah';
		});
	}

	function logout(){
		$this->session->truncate();
		redirect(login_url());
	}
}