<?php 
/**
 * 
 * Login Class
 *
 * Dibuat Oleh 			: muh. sukrillah
 * Url					: http://www.sukrillah.xyz
 * Versi 				: 1.0
 *
 **/
namespace app\controller;

use \sasak\Controller;
class Login extends Controller {

 	function __construct(){
 		parent::__construct();
 		$this->load->library('lib_title', ['namespace'=>'title']);

 		$this->data['title'] =& $this->title;
 	}

 	private function ___debug(){
 		$ud = $this->session->get_session_data();

 		echo '<pre>';
 		print_r($ud);
 		exit;
 	}

 	function index(){
 		error_reporting(E_ALL);
 		$this->title->add('Login');
 		$data = $this->input->post();
 		
 		//$this->___debug(); 		
		
 		if(is_array($data) && !empty($data)){
 			//exit(print_r($data,1));
 			$this->__do_login($data);
 		}

 		$next = $this->input->get('next');
 		$this->data['next_url'] = $next;
 		return $this->load->view('account/login', $this->data);
 	}

 	private function __do_login($data = []){ 		
 		$username = isset($data['username']) ? $data['username'] : '';
 		$password = isset($data['password']) ? $data['password'] : '';
 		$remember = isset($data['remember']) ? $data['remember'] : FALSE;

 		$this->data = array_merge($this->data, $data);

 		if(empty($username)){
 			$this->data['error'] = 'Maaf, masukkan nama pengguna Anda dulu..';
 			return;
 		}
 		if(empty($password)){
 			$this->data['error'] = 'Maaf, masukkan kata sandi Anda..';
 			return;
 		}
 		$next_url = isset($data['next']) ? $data['next'] : '';

 		//$this->load->model('user');
 		//$user = new \sasak\object_model\user($username);
        $user = $this->load->object('user', $username);
 		if(isset($user->id_user)){
 			if(md5($password) == $user->password){
 				$login_time = date('d-m-Y H:i:s');
 				$timestamp = strtotime($login_time);
 				$username = $user->username;
 				$password = $user->password;
 				$this->session->set_remember($remember);
 				$this->session->encrypt_session(compact('username','password','timestamp'));
                if($user->level == 1){
                    $redirect_to = base_url('admin/');
                }else{
                    $redirect_to = base_url('user/');
                }
 				if(!empty($next_url)){
 					$redirect_to = $next_url;
 				}
                \sasak\Log::add($username .' berhasil login..');
                //$this->input->set_cookie('ms_session', 'a:2:{s:8:"userdata";s:117:"a:3:{s:8:"username";s:5:"warid";s:8:"password";s:32:"d9b1d7db4cd6e70935368a1efb10e377";s:9:"timestamp";i:1454028118;}";s:5:"token";s:56:"OWUyOWU4MTkyYzdhOGRjMzE4MjA2YzlhMDE4OTA2YTU5ZjBmMGYyYQ==";}');
                //$cookie = $this->input->cookie('ms_session');
                //\app\Log::add('Cookie data : '. $cookie);
                //Error($username .' berhasil login..');
 				redirect($redirect_to);
 				exit;
 			}else{
 				$this->data['error'] = "Kata Sandi Salah..";
 				$this->data['username'] = $username;
 			}
 		}else{
 			$this->data['error'] = "Nama Pengguna salah..";
 		}
 	} 	
 }