<?php
/**
 * Account Logout Controller
 *
 * Dibuat Oleh              : Muh. Sukrillah
 * Url                      : http://www.sukrillah.xyz
 * Versi                    : 1.0
 */
namespace app\controller;

use \sasak\Controller;
use \sasak\User;

class Logout extends Controller {

    function __construct(){
        parent::__construct();
        if(User::is_logedin()){
            $this->session->truncate();
        }
        header("location: ". login_url());
        exit;
    }

    function index(){

    }
}