<?php
/**
 * Auth Register Controller
 * Dibuat Oleh              : Muh. Sukrillah
 * Url                      : http://www.sukrillah.xyz
 * Versi                    : 1.0
 */
namespace app\controller;

use \sasak\Controller;

class Register extends Controller {

    function __construct(){
        parent::__construct();
    }

    function index(){
        $this->title->add('Daftar');
        
        return $this->load->view('account/register');
    }
}