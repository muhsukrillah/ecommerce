<?php
/**
 * Gaji Controller
 *
 * Dibuat Oleh 			: Muh Sukrillah
 * Url 					: http://www.sukrillah.xyz
 * Versi 				: 1.0
 */
namespace app\controller;
use app\Controller;
use app\User;
use app\Log;

class Gaji extends Controller{

	function __construct(){
		parent::__construct();

		User::auth();

		if(!User::$role->can('create_gaji')){
			Error('Maaf, Anda tidak diijinkan untuk mengakses halaman ini..');
		}
		// aktifkan menu gaji
		$this->admin_menu->set_active_menu('gaji');
	}

	public function tes(){
		error_reporting(E_ALL);
		 require APP_PATH . 'Hash.php';
		 $hash = '$2y$10$gGAyzaJSQo8PMWiAN7ffwOsPuLmhG5zmobgIp9o8I5cwYOtt7yMnS';

		 $valid = \app\Hash::verify('1232', $hash);
		 return ($valid ? 'valid' : 'not valid');
	}

	function ajax($action = ''){
		error_reporting(E_ALL);
		if($this->input->is_ajax_request()){
			//error_reporting(E_ALL);
			$this->__on_action('hapus_bonus', function($data){
				$this->load->model('gaji');
				$id_gaji = (int) $data['id_gaji'];
				$id_bonus = (int) $data['id_bonus'];
				$res = [];
				$delete = $this->gaji_model->delete_bonus($id_gaji, $id_bonus);
				if($delete){
					$res['success'] =  TRUE;
				}else{
					$res['success'] = FALSE;
				}
				header('Content-type:text/json');
				echo json_encode($res);
				exit;
			});
			$this->__on_action('edit_bonus', function($data){
				error_reporting(E_ALL);
				$this->load->model('gaji');
				$id_gaji = (int) $data['id_gaji'];
				$id_bonus = (int) $data['id_bonus'];
				$jumlah = (int) $data['nominal'];
				$keterangan = $data['keterangan'];
				$token = $data['token'];
				if(!User::$role->can('update_bonus')){
					header('Content-type:text/json');
					echo json_encode(['success'=> false, 'message' => 'Anda tidak mempunyai hak untuk melakukan aksi ini!']);
					return;
				}
				if(!$this->session->validate_token($token)){
					header('Content-type:text/json');
					echo json_encode([
						'success' => false,
						'message' => 'Anda tidak diijinkan untuk melakukan ini..'
					]);
				}
				$res = [];
				$updated = $this->gaji_model->update_bonus($id_gaji, $id_bonus, compact('jumlah', 'keterangan'));
				if($updated){
					$res['success'] = true;
					$res['message'] = 'Data berhasil diubah..';
					$res['nominal'] = $jumlah;
					$res['jumlah'] = number_format($jumlah, 0, '.', '.');
					$res['keterangan'] = $keterangan;
				}else{
					$res['success'] = false;
					$res['message'] = 'Data gagal diubah..';
				}
				header('Content-type:text/json');
				echo json_encode($res);
				exit;
			});
			$this->__on_action('hapus_potongan', function($data){
				$this->load->model('gaji');
				$id_potongan = (int) $data['id_potongan'];
				$id_gaji = (int) $data['id_gaji'];
				$token = $data['token'];
				if(!User::$role->can('delete_potongan')){
					header('Content-type: text/json');
					echo json_encode(['success' => false, 'message' => 'Maaf, Anda tidak memiliki hak untuk melakukan ini..']);
					exit;
				}
				if(!$this->session->validate_token($token)){
					header('Content-type: text/json');
					echo json_encode(['success' => false, 'message' => 'Maaf, Anda tidak diijinkan untuk melakukan ini..']);
					exit;
				}
				$res = [];
				$delete = $this->gaji_model->delete_potongan($id_potongan, $id_gaji);
				if($delete){
					$res['success'] = true;
					$res['message'] = 'Data berhasil dihapus..';
				}else{
					$res['success'] = false;
					$res['message'] = 'Data gagal dihapus..';
				}
				header('Content-type: text/json');
				echo json_encode($res);
				exit;
			});
		}else{;
			\app\Response::set_status(404, 'Page Not Found');
			return view('404');
		}
	}

	function index(){
		if(!User::$role->can('create_gaji')){
			Error('Maaf, Anda tidak diijinkan untuk mengakses halaman ini..');
		}
		// buat judul
		$this->title->add('Gaji Pegawai');
		// admin menu
		$this->admin_menu->set_active_child_menu('gaji');
		// gaji pokok handler
		$this->__gaji_pokok_handler();
		// tambah bonus handler
		$this->__tambah_bonus_handler();
		// potongan handler
		$this->__tambah_potongan_handler();
		// load pegawai model
		$this->load->model('pegawai');
		$data_pegawai = $this->pegawai_model->get_all(['nama' => 'asc'], 'unlimited');
		$this->data['data_pegawai'] = $data_pegawai;

		// get nip
		$nip = $this->input->get('nip');
		// load pegawai object
		
		if(!empty($nip)){
			$pegawai = $this->load->object('pegawai', TRUE, $nip);//& $this->pegawai;//new \app\object_model\pegawai($nip);
			if(isset($pegawai->nip)){
				$this->title->add($pegawai->nama);
				$this->data['nip'] = $nip;
				$this->data['pegawai'] =& $pegawai;
			}
		}

		// buat tampilan
		return $this->load->view('pegawai_gaji', $this->data);
	}

	function bonus_tetap(){
		if(!User::$role->can('create_gaji')){
			Error('Maaf, Anda tidak diijinkan untuk mengakses halaman ini..');
		}

		$this->title->add('Bonus Tetap');

		$this->load->model('gaji');

		$id_bonus = $this->input->get('edit');
		$hapus = $this->input->get('hapus');

		if($id_bonus){
			$bonus_data = $this->gaji_model->get_bonus_tetap_by_id($id_bonus);
			if($hapus){
				if(!User::$role->can('delete_gaji')){
					Error('Maaf, Anda tidak diijinkan untuk melakukan ini..');
				}
				if(!empty($bonus_data) && isset($bonus_data['id_bonus'])){
					$del = $this->gaji_model->delete_bonus_tetap($id_bonus);
					if($del){
						$ref = $this->input->server('HTTP_REFERER');
						header('location:'. $ref);
						exit;
					}else{
						$this->data['pesan'] = 'Data gagal dihapus..';
					}
				}
			}
			$this->data['bonus_data'] = $bonus_data;
			$this->data['action'] = 'edit';
		}else{
			$this->data['bonus'] = 'add';
		}

		$this->__on_action('add', function($data){

			$jumlah = (int) $data['jumlah'];
			$keterangan = clean((isset($data['keterangan']) ? $data['keterangan'] : 'Lain-Lain'));

			$save = $this->gaji_model->add_bonus_tetap($jumlah, $keterangan);
			if($save){
				header("location:". base_url('gaji/bonus_tetap'));
				exit;
			}else{
				$this->data['pesan'] = 'Data gagal disimpan..';
			}
		});

		$this->__on_action('edit', function($data){
			if(!User::$role->can('update_gaji')){
				Error('Maaf, Anda tidak diijinkan untuk melakukan ini..');
			}
			$jumlah = (int) $data['jumlah'];
			$keterangan = clean((isset($data['keterangan']) ? $data['keterangan'] : 'Lain-Lain'));
			$id_bonus = (int) $data['id_bonus'];

			$save = $this->gaji_model->update_bonus_tetap($id_bonus, $jumlah, $keterangan);
			if($save){
				header("location:". base_url('gaji/bonus_tetap'));
				exit;
			}else{
				$this->data['pesan'] = 'Data gagal disimpan..';
			}
		});

		$bonus_tetap = $this->gaji_model->get_bonus_tetap();
		$this->data['bonus_tetap'] = $bonus_tetap;

		$this->admin_menu->set_active_child_menu('bonus-tetap');

		return $this->load->view('pegawai_bonus_tetap', $this->data);
	}

	private function __on_action($action = '', $handler = null){
		$data = $this->input->post();
		if(isset($data['action']) && $data['action'] == $action){
			if(is_callable($handler)){
				call_user_func_array($handler, [$data]);
			}
		}
	}

	private function __gaji_pokok_handler(){
		$this->__on_action('gaji', function($data){
			$nip = clean($data['nip']);
			$gaji_pokok = (int) $data['gaji_pokok'];
			$tunjangan_istri = (int) $data['tunjangan_istri'];
			$tunjangan_anak = (int) $data['tunjangan_anak'];
			$id_gaji = (int) $data['id_gaji'];

			if(empty($nip)){
				$this->data['pesan'] = 'Data tidak lengkap.. [nip]';
				return;
			}
			$pegawai = $this->load->object('pegawai', TRUE, $nip);
			if(!isset($pegawai->nip)){
				$this->data['pesan'] = 'Data pegawai dengan nip ['. $nip .'] tidak tersedia..';
				return;
			}
			// aksi tambah
			if(!$id_gaji){
				// cek role
				if(!User::$role->can('create_gaji')){
					$this->data['pesan'] = 'Maaf, Anda tidak diijinkan untuk melakukan ['. User::$role->can('create_gaji', 'desc') .']';
					return;
				}
				$this->load->model('gaji');
				$gaji = compact('nip', 'gaji_pokok', 'tunjangan_anak', 'tunjangan_istri');
				$save = $this->gaji_model->add($gaji);
				if($save){
					$this->data['pesan'] = 'Data gaji berhasil ditambhkan..';
					$this->data['pesan_sukses'] = TRUE;
					return;
				}else{
					$this->data['pesan'] = 'Data gaji gagal ditambahkan..';
					return;
				}
			}
			// aksi edit
			if($id_gaji){
				// cek role
				if(!User::$role->can('update_gaji')){
					$this->data['pesan'] = 'Maaf, Anda tidak diijinkan untuk melakukan ['. User::$role->can('update_gaji', 'desc') .']';
					return;
				}
				$this->load->model('gaji');
				$gaji = compact('gaji_pokok', 'tunjangan_anak', 'tunjangan_istri');
				$update = $this->gaji_model->update($gaji, ['nip' => $nip]);
				if($update){
					$this->data['pesan'] = 'Data gaji berhasil diperbarui..';
					$this->data['pesan_sukses'] = TRUE;
					return;
				}else{
					$this->data['pesan'] = 'Data gaji gagal diperbarui..';
				}
			}
		});
	}

	private function __tambah_potongan_handler(){
		error_reporting(E_ALL);
		if(!User::$role->can('create_potongan')){
			$this->data['pesan'] = 'Maaf, Anda tidak diijinkan untuk melakukan ['. User::$role->can('create_potongan', 'desc') .']';
			return;
		}
		$post = $this->input->post();
		if(isset($post['action']) && $post['action'] == 'tambah_potongan'){
			if(!isset($post['token']) || !$this->session->validate_token($post['token'])){
				$this->data['pesan'] = 'Anda tidak diijinkan untuk melakukan aksi ini..';
				\app\Log::add($this->data['pesan']);
				return;
			}
			$this->load->model('gaji');
			$id_gaji = (int) $post['id_gaji'];
			$jumlah = (int) $post['jumlah'];
			$keterangan = clean($post['keterangan']);
			$data = compact('jumlah', 'keterangan');

			$simpan = $this->gaji_model->add_potongan($id_gaji, $data);
			if($simpan){
				$this->data['pesan'] = 'Data potongan gaji berhasil disimpn..';
				$this->data['pesan_sukses'] = TRUE;
				Log::add('Potongan untuk id_gaji ['. $id_gaji .'] berhasil ditambahkan..');
			}else{
				$this->data['pesan'] = 'Data potongan gaji gagal disimpan..';
				Log::add('Potongan untuk id_gaji ['. $id_gaji .'] gagal disimpan..');
			}
			unset($_POST);
		}
	}

	private function __tambah_bonus_handler(){
		error_reporting(E_ALL);
		if(!User::$role->can('create_bonus')){
			$this->data['pesan'] = 'Maaf, Anda tidak diijinkan untuk menambah bonus..';
			return;
		}
		$post = $this->input->post();
		if(isset($post['action']) && $post['action'] == 'tambah_bonus'){
			if(!isset($post['token']) || !$this->session->validate_token($post['token'])){
				$this->data['pesan'] = 'Anda tidak diijinkan untuk melakukan aksi ini..';
				\app\Log::add($this->data['pesan']);
				return;
			}
			$this->load->model('gaji');
			$id_gaji = (int) $post['id_gaji'];
			$jumlah = (int) $post['jumlah'];
			$keterangan = clean($post['keterangan']);
			$data = compact('jumlah', 'keterangan');
			if(!$id_gaji || $id_gaji == 0){
				$this->data['pesan'] = 'Maaf, pegawai belum memiliki data gaji pokok..';
				return;
			}
			$simpan = $this->gaji_model->add_bonus($id_gaji, $data);
			if($simpan){
				Log::add('Bonus untuk id_gaji ['. $id_gaji .'] berhasil ditambahkan..');
			}else{
				Log::add('Bonus untuk id_gaji ['. $id_gaji .'] gagal disimpan..');
			}
			unset($_POST);
		}
	}

	public function cetak_slip_gaji($nip = NULL){
		if(!isset($nip) || empty($nip)){
			Error('Maaf, data pegawai tidak benar..');
		}

		$pegawai = $this->load->object('pegawai', TRUE, $nip);
		if(!isset($pegawai->nip)){
			Error('Maaf, data pegawai tidak tersedia..');
		}

		$get = $this->input->get();
		if(isset($get['auto_print'])){
			$this->data['auto_print'] = TRUE;
		}
		
		$this->data['pegawai'] =& $pegawai;

		$this->title->set_root('Slip Gaji '. $pegawai->nama);

		return $this->load->view('slip_gaji');
	}
}