<?php
/**
 * Admin Product Controller
 *
 * Dibuat Oleh              : Muh. Sukrillah
 * Url                      : http://www.sukrillah.xyz
 * Versi                    : 1.0
 */
namespace app\controller;

use \sasak\Controller;
use \sasak\User;

class Product extends Controller {

    function __construct(){
        parent::__construct();
        // User must login first
        User::auth();
        // Assets class
        $this->load->library('Assets', ['namespace' => 'Assets', 'param' => ['path' => BASE_PATH, 'base_url', base_url()]]);
        $this->Assets->setPath(BASE_PATH)->setBaseUrl(asset_url());
        $this->data['Assets'] =& $this->Assets;
        // run menu
        $menu =& $this->menu;
        do_action('admin_menu', $menu);
        $this->data['menu'] =& $this->menu;
        $breadcrumb =& $this->breadcrumb;
        do_action('breadcrumb', $breadcrumb);
        //$this->data['breadcrumb']
        // set active menu
        $this->menu->set_active_menu('product');

    }

    private function __breadcrumb(){
        $this->breadcrumb->add(
            ['title' => 'Dashboard', 'icon' => 'fa fa-dashboard', 'link' => admin_url('index/')]
        )->add('Produk', admin_url('product/index/'), 'fa fa-shopping-cart');
        $data = func_get_args();
        if(isset($data) && is_array($data) && !empty($data)){
            foreach($data as $bread){
                $this->breadcrumb->add($bread);
            }
        }
        return $this;
    }

    private function __js(){
        error_reporting(E_ALL);
        $data = func_get_args();
        $this->Assets
        ->addJs(['url' => 'js/jQuery-2.1.4.min.js', 'id' => 'jQueryJs']) // http://local/assets/js/jQuery 
        ->addJs(['url' => 'bootstrap/js/bootstrap.min.js', 'id' => 'BootstrapJs'])
        ->addJs(['url' => 'dist/js/app.min.js'])
        ->addJs(['url' => 'plugins/datepicker/bootstrap-datepicker.js'])
        ->addJs(['url' => 'plugins/datepicker/locales/bootstrap-datepicker.id.js'])
        ->addJs(['url' => 'plugins/input-mask/jquery.inputmask.js'])
        ->addJs(['url' => 'plugins/input-mask/jquery.inputmask.date.extensions.js'])
        ->addJs(['url' => 'dist/js/demo.js']);

        if(!empty($data)){
            if(is_object($data[0])){
                $param[] =& $this->Assets;
                @call_user_func_array($data[0], $param);
                return $this;
            }
            foreach($data as $js){
                $this->Assets->addJs($js);
            }
        }
        return $this;
    }
    /**
     * product index page controller
     */
    function index(){
        if(!User::$role->can('read_product')){
            Error('Maaf, Anda tidak diijinkan untuk mengakses halaman ini..');
        }
        $this->title->add('Produk');

        $this->__breadcrumb();

        $this->__js();
        //$this->menu->set_active_menu('dashboard');
        $this->menu->set_active_child_menu('all-product');
        // get object model
        $product = $this->load->object('product');
        // search query
        $query = $this->input->get('query');
        if($query){
            $data = $product->search(['product_title' => '%'. $query .'%']);
            $this->data['query'] = $query;
            $total = count($data);
        }else{
            // get total data
            $total = $product->count_all();    
        }        
        // paging
        $orderby = $this->input->get('orderby');
        if(!empty($orderby)){
            $order_ex = explode('.', $orderby);   
            if($order_ex[0] == 'nama'){
                $order_ex[0] = 'product_title';
            }  
            $order = [$order_ex[0] => $order_ex[1]];
            $this->data['order'] = $order_ex;
        }else{
            $order = NULL;
        }

        $page = (int) $this->input->get('page');
        $perPage = (int) $this->input->get('perPage');
        $perPageDef = 10;
        // load pagination library
        $this->load->library('pagination');
        $path = uri()->compare(['page' => '%d']);
        $pagination = new \sasak\lib\Pagination($total, $page);
        $pagination->appearance = array(
                'nav_prev'          => '<a href="%s" class="prev btn btn-primary"><span>prev</span></a>',
                'nav_number_link'   => '<a href="%s" class="btn btn-primary"><span>%d</span></a>',
                'nav_number'        => '<a href="#!" class="active btn btn-default"><span>%d</span></a>',
                'nav_more'          => '<a href="#!" class="more btn btn-default focus"><span>...</span></a>',
                'nav_next'          => '<a href="%s" class="next btn btn-primary"><span>next</span></a>'
        );
        $pagination->perPage = ($perPage > 0) ? $perPage : $perPageDef;
        $pagination->path = $path;
        $pagination->setStart($page);
        $pagination->setCount($total);

        $limit = $pagination->getLimit();
        $offset = $pagination->getLimitOffset();
        $this->data['pagination'] = $pagination->paginate();
        $this->data['page'] = ($page == 0) ? 1 : $page;
        $this->data['perPage'] = ($perPage > 0) ? $perPage : $perPageDef;
        // get data
        if($query){
            $data = $product->search(['product_title' => '%'. $query .'%'], ['order_by' => (isset($order) ? $order : ['date_added' => 'ASC']), 'limit' => [$limit, $offset]]);
        }else{
            $data = $product->get_all([
                'order_by' => (isset($order) ? $order : ['date_added' => 'ASC']),
                'limit' => [$limit,$offset]
            ]);
        }

        $this->data['total'] = $total;
        $this->data['data_produk'] = $data;
        // edit access
        $this->data['allow_edit'] = FALSE;
        if(User::$role->can('update_product')){
            $this->data['allow_edit'] = TRUE;
        }
        // delete access
        $this->data['allow_delete'] = FALSE;
        if(User::$role->can('delete_product')) {
            $this->data['allow_delete'] = TRUE;
        }
        return $this->load->view('admin/product_index');
    }
    /**
     * product image uploader
     */
    function ajax_upload_image(){
        error_reporting(E_ALL);
        if(!User::$role->can('create_product')){
            return json_encode([
                'success' => FALSE,
                'message' => 'Maaf, Anda tidak diijinkan untuk melakukan aksi ini..'
            ]);
        }
        $this->load->library('uploader');
        
        $files = $_FILES;
        $dir = 'catalogs/'. date('Y') .'/'. date('m') .'/';
        $this->uploader->initialize([
            'is_image'      => TRUE,
            'upload_path'   => ASSETS_PATH . $dir,
            'new_name'      => time(),
            'multiple'      => TRUE,
            'field_name'    => 'file'
        ]);
        $upload = $this->uploader->upload();
        if($upload){
            // auto save images
            $auto_save = FALSE;
            if($id_product = $this->input->post('id_product')){
                $product_image = $this->load->object('product_image');
                $auto_save = TRUE;
            }
            $upload_data = $this->uploader->get_data();
            foreach($upload_data as $key => $value){
                $upload_data[$key]['url'] = asset_url($dir . $value['new_name']);
                // create new resized images
                $orig_path = $dir . $value['new_name'];
                $path = $value['full_path'];
                $ext = $value['extention'];
                $miniatur_name = $value['raw_name'] . '_60x60'. $ext;
                $thumbnail_name = $value['raw_name'] .'_300x150'. $ext;
                $featured_name = $value['raw_name'] .'_800x300'. $ext;
                $newdir = dirname($path) .'/';
                $miniatur = $newdir . $miniatur_name;
                $thumbnail = $newdir . $thumbnail_name;
                $featured = $newdir . $featured_name;
                // create miniatur
                \sasak\lib\Thumbnail::output($path, $miniatur, array(
                    'width' => 60,
                    'height' => 60,
                    'method' => THUMBNAIL_METHOD_SCALE_MAX
                ));
                // create thumbnail
                \sasak\lib\Thumbnail::output($path, $thumbnail, array(
                    'width' => 300,
                    'height' => 150,
                    'method' => THUMBNAIL_METHOD_SCALE_MIN
                ));
                // create featured
                \sasak\lib\Thumbnail::output($path, $featured, array(
                    'width' => 800,
                    'height' => 300,
                    'method' => THUMBNAIL_METHOD_SCALE_MIN
                ));
                $upload_data[$key]['miniatur_url'] = asset_url($dir . $miniatur_name);
                $upload_data[$key]['miniatur_path'] = $dir . $miniatur_name;
                $upload_data[$key]['thumbnail_url'] = asset_url($dir . $thumbnail_name);
                $upload_data[$key]['thumbnail_path']= $dir . $thumbnail_name;
                $upload_data[$key]['featured_url']  = asset_url($dir . $featured_name);
                $upload_data[$key]['featured_path'] = $dir . $featured_name;

                // save
                if($auto_save){
                    $product_image->id_product = $id_product;
                    $product_image->url = $upload_data[$key]['url'];
                    $product_image->path = $orig_path;
                    $product_image->file_name = $value['new_name'];
                    $product_image->title = $this->input->post('product_title');
                    $product_image->alt = $this->input->post('product_title');
                    $product_image->type = $value['mime_type'];
                    $product_image->thumbnail_url = $upload_data[$key]['thumbnail_url'];
                    $product_image->thumbnail_path = $upload_data[$key]['thumbnail_path'];
                    $product_image->featured_url = $upload_data[$key]['featured_url'];
                    $product_image->featured_path = $upload_data[$key]['featured_path'];
                    // save image
                    $product_image->save();
                }
                
            }
            \sasak\Log::add('Gambar berhasil diunggah..');

            ob_start();
            @header("Content-type: text/json");
            return json_encode($upload_data);
        }else{
            \sasak\Log::add('Gambar gagal diunggah..');
            \sasak\Log::add('Uploader : '. $this->uploader->get_message());
            ob_start();
            @header("Content-type: text/json");
            return json_encode([
                'status' => 'error',
                'message' => $this->uploader->get_message(),
                'data' => $files,
            ]);
        }
    }
    /**
     * ajax delete product image
     */
    function ajax_delete_image(){
        $post = $this->input->post();
        $response = [];
        if(isset($post['id'])){
            $ids = [];
            if(!is_array($post['id'])){
                $id = (int) $post['id'];
                if($id){
                    $ids[] = $id;
                }
            }
            if(!empty($ids)){
                $product_image = $this->load->object('product_image');
                $deleted = 0;
                $deleted_id = [];
                foreach($ids as $_id){
                    $product_image->id = $_id;
                    $data = $product_image->get_by(['id' => $_id]);
                    if($product_image->delete()) {
                        if(file_exists(ASSETS_PATH . $data['path'])){
                            unlink(ASSETS_PATH . $data['path']);
                        }else{
                            \sasak\Log::add('data gagal dihapus => '. ASSETS_PATH . $data['path'], TRUE, TRUE);
                        }
                        if(file_exists(ASSETS_PATH . $data['thumbnail_path'])){
                            unlink(ASSETS_PATH . $data['thumbnail_path']);
                        }else{
                            \sasak\Log::add('data gagal dihapus => '. ASSETS_PATH . $data['thumbnail_path'], TRUE, TRUE);
                        }
                        if(file_exists(ASSETS_PATH . $data['featured_path'])){
                            unlink(ASSETS_PATH . $data['featured_path']);
                        }else{
                            \sasak\Log::add('data gagal dihapus => '. ASSETS_PATH . $data['featured_path'], TRUE, TRUE);
                        }
                        $miniatur = str_replace('.', '_60x60.', ASSETS_PATH . $data['path']);
                        if(file_exists($miniatur)){
                            unlink($miniatur);
                        }else{
                            \sasak\Log::add('data gagal dihapus => '. $miniatur, TRUE, TRUE);
                        }
                        $deleted++;
                        $deleted_id[] = $_id;
                    }
                }
                if($deleted){
                    $response['success'] = TRUE;
                    $response['message'] = 'Data berhasil dihapus..';
                    $response['data'] = $deleted_id;
                }else{
                    $response['success'] = FALSE;
                    $response['message'] = 'Data gagal dishapus..';
                }
            }else{
                $response['success'] = FALSE;
                $response['message'] = 'Data tidak valid..';
            }
        }else{
            $response['success'] = FALSE;
            $response['message'] = 'Data tidak ada..';
        }
        header("Content-type: text/json");
        return json_encode($response);
    }
    /**
     * New Product
     *
     * /product/add/
     *
     * @access  public
     * @param   optional    string param
     * @return  view
     */
    function add($param = ''){
        error_reporting(E_ALL);
        // hak akses
        if(!User::$role->can('create_product')){
            Error('Maaf, Anda tidak diijinkan untuk mengakses halaman ini..');
        }
        // post
        $post = \sasak\Input()->post();
        if(!empty($post)){
            return $this->___save($post);
        }
        $this->title->add('Produk');
        $this->title->add('Tambah Produk');
        $this->__breadcrumb(
            ['title' => 'Produk Baru', 'icon' => 'fa fa-plus', 'link' => admin_url('product/add/')]
        );
        $this->menu->set_active_child_menu('new-product');
        $this->__js(function($assets){
            $assets->addJs(
                ['url'  => 'admin/js/main.js'], 
                ['url'  => 'plugins/jquery-form/jquery.form.min.js', 'id' => 'jquery_form'],
                ['url'  => 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'],
                ['url'  => 'plugins/select2/select2.min.js'],
                ['url'  => 'admin/js/product.js'],
                ['url'  => 'admin/js/product_add.js']
            );
            $assets->addCss(
                ['url'  => 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'],
                ['url'  => 'admin/css/product_add.css']
            );
        });

        // category
        $category = $this->load->object('category');
        $category_data = $category->get_all([
            'order_by' => ['order']
        ]);
        foreach($category_data as $key => $value){
            if(!$value['parent_id']) continue;
            $category_data[$key]['category_name'] = $this->__fetch_parent($value['parent_id'], $value['category_name']);
        }

        $this->data['category_data'] = $category_data;
        // brand
        $brand = $this->load->object('brand');
        $brand_data = $brand->get_all();
        $this->data['brand'] = $brand_data;
        // product object
        $product = $this->load->object('product');
        $this->data['model_number'] = $product->generate_model_number();
        // message
        $message = \sasak\Session::get_flash_data('message');
        if($message){
            $this->data['message'] = $message;
            $this->data['message_type'] = $this->input->get('message_type');
        }
        return $this->load->view('admin/product_add');
    }
    /**
     * save product
     * 
     * @method  ___save
     * @access  private
     * @param   null
     * @return  null
     */
    private function ___save($post){
        // id user
        $id_user = \sasak\User::$data->id_user;
        $product = $this->load->object('product');

        $product->model = $post['model'];
        $product->product_title = $post['product_title'];
        $product->product_name = url_title($post['product_title']);
        $product->description = $post['description'];
        $product->harga_beli =(int) (isset($post['harga_beli']) ? $post['harga_beli'] : 0);
        $product->harga_jual = (int) (isset($post['harga_jual']) ? $post['harga_jual'] : 0);
        $product->harga_coret = (int) (isset($post['harga_coret']) ? $post['harga_coret'] : 0);
        $product->stock_awal = (int) (isset($post['stok']) ? $post['stok'] : 0);
        $product->stock_akhir = $product->stock_awal;
        $product->status = (int) $post['status'];
        $product->id_user = $id_user;
        $product->id_brand = (int) (isset($post['brand']) ? $post['brand'] : 0);
        $product->id_category = (int) (isset($post['id_category']) ? $post['id_category'] : 0);

        if($product_id = $product->save()){
            // product category
            /**
            $product_category = $this->load->object('product_category');
            $product_category->id_category = (int) $post['category'];
            $product_category->id_product = $product_id;*
            //save 
            $product_category->save();*/
            // product images
            if(isset($post['images']) && !empty($post['images'])){
                $images = json_decode($post['images']);
                $product_image = $this->load->object('product_image');
                foreach($images as $img){                    
                    $product_image->id_product = $product_id;
                    $product_image->url = $img->url;
                    $product_image->path = $img->full_path;
                    $product_image->file_name = $img->raw_name;
                    $product_image->title = $post['product_title'];
                    $product_image->alt = $post['product_title'];
                    $product_image->type = $img->mime_type;
                    $product_image->thumbnail_url = $img->thumbnail_url;
                    $product_image->thumbnail_path = $img->thumbnail_path;
                    $product_image->featured_url = $img->featured_url;
                    $product_image->featured_path = $img->featured_path;
                    // save
                    $product_image->save();
                }
            }

            \sasak\Session::set_flash_data('message', 'Data berhasil disimpan..');
            redirect(admin_url('product/add?message_type=1'));
        }else{
            \sasak\Session::set_flash_data('message', 'Data gagal disimpan..');
            redirect(admin_url('product/add?message_type=0'));
        }
    }
    /**
     * edit product method
     */
    function edit(){
        // post
        $post = \sasak\Input()->post();
        if(!empty($post)){
            $this->___update($post);
        }
        // title
        $this->title->set('Edit Produk');
        // breadcrumb
        $this->__breadcrumb(
            ['title' => 'Ubah Produk', 'icon' => 'fa fa-pencil', 'link' => admin_url('product/index')]
        );
       // $this->breadcrumb->add(['title' => 'Produk Baru', 'icon' => 'fa fa-plus', 'link' => admin_url('product/add/')]);

        // assets
        $this->__js(function($assets){
            $assets->addJs(['url' => 'plugins/select2/select2.min.js']);
            $assets->addJs(['url' => 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js']);
            $assets->addCss(['url' => 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css']);
            $assets->addJs(['url'  => 'plugins/jquery-form/jquery.form.min.js', 'id' => 'jquery_form']);
            $assets->addJs(['url' => 'admin/js/main.js']);
            $assets->addJs(['url' => 'admin/js/product.js']);
            $assets->addCss(['url' => 'admin/css/product_add.css']);
            $assets->addJs(['url' => 'admin/js/product_add.js']);
        });
        // category
        $category = $this->load->object('category');
        $category_data = $category->get_all([
            'order_by' => ['order']
        ]);
        foreach($category_data as $key => $value) {
            if(!$value['parent_id']) continue;
            $category_data[$key]['category_name'] = $this->__fetch_parent($value['parent_id'], $value['category_name']);
        }
        $this->data['category_data'] = $category_data;
        // get product id
        $product_id = $this->input->get('id_product');
        // load image object
        $image = $this->load->object('product_image');
        $images = $image->get_by(['id_product' => (int) $product_id], [], TRUE);
        if(sizeof($images) == 1){
            $this->data['images_array'] = $images;
            $this->data['images'] = json_encode([$this->data['images_array']]);
        }else{
            $this->data['images_array'] = $images;
            $this->data['images'] = json_encode($images);
        }
        // brand
        $brand = $this->load->object('brand');
        $brand_data = $brand->get_all();
        $this->data['brand'] = $brand_data;
        // load product object
        $product = $this->load->object('product');
        // get product data
        $data = $product->get_by(['id_product' => $product_id]);
        $this->data = array_merge($this->data, $data);
        // message
        $message = \sasak\Session::get_flash_data('message');
        if($message){
            $this->data['message'] = $message;
            $message_type = $this->input->get('message_type');
            if($message_type){
                $this->data['message_type'] = $message_type;
            }   
        }
        return $this->load->view('admin/product_edit');
    }
    /**
     * update product
     */
    private function ___update($post = []){
        $product = $this->load->object('product');
        // get the product id
        $id_product = (int) (isset($post['id_product']) ? $post['id_product'] : 0);
        // get all data with product id
        $product_data = $product->get_by(['id_product' => $id_product]);
        // set object product id
        $product->id_product = $id_product;
        $updated = 0;
        foreach($post as $key => $value){
            if($key == 'id_product'){
                continue;
            }
            if(!in_array($key, ['id_product', 'product_title', 'harga_jual', 'harga_beli', 'harga_coret', 'brand', 'images', 'id_category', 'stock', 'id_brand', 'status', 'description'])){
                unset($post[$key]);
                continue;
            }
            if(in_array($key, ['images'])){
                continue;
            }
            if($post[$key] != $product_data[$key]){
                $product->$key = $value;
                $updated++;
            }
        }
        unset($product->date_added);
        if(!$updated){
            \sasak\Session::set_flash_data('message', 'Tidak ada data yang dirubah..');
            redirect(admin_url('product/edit?id_product='. $id_product));
        }
        // product images
        if(isset($post['images'])){
            $images = json_decode($post['images']);
        }
        // echo '<pre>';
        //print_r($product);
        //exit;
        if($product->update()){
            \sasak\Session::set_flash_data('message', 'Data berhasil diubah..');
            redirect(admin_url('product/edit?id_product='. $id_product .'&message_type=1'));
        }else{
            \sasak\Session::set_flash_data('message', 'Data gagal diubah');
            redirect(admin_url('product/edit?id_product='. $id_product));
        }
        exit;
    }
    /**
     * product category
     *
     */
    function category($method = 'index'){
        if(!User::$role->can('create_product_category')){
            Error("Maaf, Anda tidak diijinkan untuk mengakses halaman ini..");
        }
        // set title
        $this->title->set('Kategori Produk');
        // set breadcrum
        $this->__breadcrumb(['title' => 'Kategori', 'icon'=>'fa fa-tags', 'link' => admin_url('product/category')]);
        // menu
        $this->menu->set_active_child_menu('product-category');
        // javascript

        $this->__js(function($assets){
            $assets->addJs(['url' => 'admin/js/main.js']);
        });
        switch($method){
            case 'edit':
                if(!User::$role->can('update_product_category')){
                    \sasak\Session::set_flash_data('message', 'Maaf, Anda tidak diijinkan untuk mengubah');
                    redirect(admin_url('product/category'));
                }
                $post = \sasak\Input()->post();
                $id_category = isset($post['id_category']) ? $post['id_category'] : 0;
                $category_name = isset($post['category_name']) ? $post['category_name'] : '';
                $description = isset($post['description']) ? $post['description'] : '';
                $parent_id = (int) (isset($post['parent_category']) ? $post['parent_category'] : 0);
                $order = (int) (isset($post['order']) ? $post['order'] : 0);
                if(! $id_category){
                    \sasak\Session::set_flash_data('message', 'Data tidak tersedia..');
                    redirect(admin_url('product/category'));
                }
                $category = $this->load->object('category');
                $category->id_category = $id_category;
                if(!empty($category_name)){
                    $category->category_name = $category_name;
                }
                if(!empty($description)){
                    $category->description = $description;
                }
                if($parent_id){
                    $category->parent_id = (int) $parent_id;
                }else{
                    $category->parent_id = 0;
                }
                if(empty($order)){
                    $order = 0;
                }
                $category->order = $order;
                if($category->update()){
                    \sasak\Session::set_flash_data('message', 'Data berhasil diubah..');
                }else{
                    \sasak\Session::set_flash_data('message', 'Data gagal diubah..');
                }
                redirect(admin_url('product/category'));
            break;
            case 'delete':
                if(!User::$role->can('delete_category_product')){
                    \sasak\Session::set_flash_data('message', 'Maaf, Anda tidak diijinkan untuk menghapus data kategori produk..');
                    redirect(admin_url('product/category'));
                }
                $id_category = (int) \sasak\Input()->get('id_category');
                if(!$id_category){
                    \sasak\Session::set_flash_data('message', 'Data tidak ditemukan..');
                    redirect(admin_url('product/category'));
                }
                $category = $this->load->object('category');
                $category->id_category = $id_category;
                if($category->delete()){
                    \sasak\Session::set_flash_data('message', 'Data berhasil dihapus..');
                }else{
                    \sasak\Session::set_flash_data('message', 'Data gagal dihapus..');
                }
                redirect(admin_url('product/category'));
            break;
            case 'new':
                $post = \sasak\Input()->post();
                $category_name = isset($post['category_name']) ? $post['category_name'] : '';
                $parent_id = isset($post['parent_category']) ? $post['parent_category'] : 0;
                $description = isset($post['description']) ? $post['description'] : '';
                $order = isset($post['order']) ? $post['order'] : 0;
                // category name required
                // display error message when it's empty
                if(empty($category_name)){
                    \sasak\Session::set_flash_data('message', 'Nama kategori tidak boleh kosong..');
                    redirect(admin_url('product/category'));
                }

                $category = $this->load->object('category');
                $category->category_name = $category_name;
                $category->slug = str_replace([' ', '&'], '-', strtolower($category_name));
                $category->parent_id = (int) $parent_id;
                $category->description = $description;
                $category->order = $order;
                if($category->save()){
                    \sasak\Session::set_flash_data('message', 'Data berhasil disimpan..');
                    redirect(admin_url('product/category'));
                }else{
                    \sasak\Session::set_flash_data('message', 'Data gagal disimpan..');
                    redirect(admin_url('product/category'));
                }
            break;
            default:
                $message = \sasak\Session::get_flash_data('message');
                if($message){
                    $this->data['message'] = $message;
                }
                $id_category = \sasak\Input()->get('id_category');
                if($id_category){
                    $cat = new \app\object_model\Category;
                    $cats = $cat->get_by(['id_category' => $id_category]);
                    $this->data['cdata'] = $cats;
                    $this->data['form_action'] = admin_url('product/category/edit');
                }
                $category = $this->load->object('category');
                $category_data = $category->get_all([
                    'order_by' => ['order' => 'ASC']
                ]);
                foreach($category_data as $key => $value){
                    if(!$value['parent_id']) {
                        continue;
                    }

                    $category_data[$key]['category_name'] = $this->__fetch_parent($value['parent_id'], $value['category_name']);
                }

                $this->data['category_data'] = $category_data;
                return $this->load->view('admin/product_category');
        }
    }
    /**
     * fungsi untuk mengambail data parent category produk
     * 
     * @access  private
     * @param   array   data
     * @return  array   data
     */
    private function __fetch_parent($parent_id = 0, $category_name){
        if(!$parent_id){
            return $category_name;
        }
        $category = $this->load->object('category');
        $cats = $category->get_by(['id_category' => $parent_id]);
        if(!empty($cats)){
            if($cats['parent_id']){
                $category_name = $cats['category_name'] .' &raquo; '. $category_name;
                return $this->__fetch_parent($cats['parent_id'], $category_name);
            }else{
                $category_name = $cats['category_name'] .' &raquo; '. $category_name;
                return $category_name;
            }
        }else{
            return $category_name;
        }
    }
    /**
     * brand
     */
    function brand($method = ''){
        switch($method){
            case 'add':
                $post = $this->input->post();
                $brand_name = isset($post['brand_name']) ? $post['brand_name'] : '';
                $brand_phone = isset($post['brand_phone']) ? $post['brand_phone'] : '';
                $brand_email = isset($post['brand_email']) ? $post['brand_email'] : '';
                $description = isset($post['description']) ? $post['description'] : '';
                if(empty($brand_name)){
                    return json_encode([
                        'success' => FALSE,
                        'message' => 'Nama brand tidak boleh kosong..'
                    ]);
                }
                $brand = $this->load->object('brand');
                $brand->brand_name = $brand_name;
                $brand->brand_phone = $brand_phone;
                $brand->brand_email = $brand_email;
                $brand->description = $description;
                if($id_brand = $brand->save()){
                    return json_encode([
                        'success' => TRUE,
                        'message' => 'Data berhasil disimpan..',
                        'data' => [
                            'id_brand' => $id_brand,
                            'brand_name' => $brand_name,
                            'brand_phone' => $brand_phone,
                            'brand_email' => $brand_email,
                            'description' => $description
                        ]
                    ]);
                }else{
                    return json_encode([
                        'success' => FALSE,
                        'message' => 'Data gagal disimpan..'
                    ]);
                }
            break;
            case 'edit':
                $post = $this->input->post();
                $id_brand = isset($post['id_brand']) ? $post['id_brand'] : 0;
                if(!$id_brand){
                    return json_encode([
                        'success' => FALSE,
                        'message' => 'Maaf, data tidak valid..'
                    ]);
                }

                $brand_name = isset($post['brand_name']) ? $post['brand_name'] : '';
                $brand_phone = isset($post['brand_phone']) ? $post['brand_phone'] : '';
                $brand_email = isset($post['brand_email']) ? $post['brand_email'] : '';
                $description = isset($post['description']) ? $post['description'] : '';
                if(empty($brand_name)){
                    return json_encode([
                        'success' => FALSE,
                        'message' => 'Nama brand tidak boleh kosong..'
                    ]);
                }
                $brand = $this->load->object('brand');
                $brand->id_brand = $id_brand;
                $brand->brand_name = $brand_name;
                $brand->brand_phone = $brand_phone;
                $brand->brand_email = $brand_email;
                $brand->description = $description;
                if($brand->update()){
                    return json_encode([
                        'success' => TRUE,
                        'message' => 'Data berhasil diperbarui..',
                        'data' => [
                            'id_brand' => $id_brand,
                            'brand_name' => $brand_name,
                            'brand_phone' => $brand_phone,
                            'brand_email' => $brand_email,
                            'description' => $description
                        ]
                    ]);
                }else{
                    return json_encode([
                        'success' => FALSE,
                        'message' => 'Data gagal diperbarui..'
                    ]);
                }
            break;
            case 'delete':
                $post = $this->input->post();
                $id_brand = (int) (isset($post['id_brand']) ? $post['id_brand'] : 0);
                if(!$id_brand){
                    return json_encode([
                        'success' => FALSE,
                        'message' => 'Maaf, data brand tidak ditemukan..'
                    ]);
                }
                $brand = $this->load->object('brand');
                $brand->id_brand = $id_brand;
                if($brand->delete()){
                    return json_encode([
                        'success' => TRUE,
                        'message' => 'Data berhasil dihapus..',
                        'data' => ['id_brand' => $id_brand]
                    ]);
                }else{
                    return json_encode([
                        'success' => FALSE,
                        'message' => 'Data gagal disimpan..'
                    ]);
                }
            break;
            default:
                $this->title->add('Brand Produk');
                $this->__breadcrumb(
                    ['title' => 'Brand Produk', 'icon' => 'fa fa-leaf', 'link' => admin_url('product/brand')]
                );
                $this->menu->set_active_child_menu('product-brand');
                $this->__js(function($assets){
                    $assets->addJs(['url' => 'plugins/select2/select2.min.js']);
                    $assets->addJs(['url' => 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js']);
                    $assets->addCss(['url' => 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css']);
                    //$assets->addJs(['url' => 'admin/js/product.js']);
                    $assets->addCss(['url' => 'admin/css/product_add.css']);
                    $assets->addJs(['url' => 'admin/js/product_brand.js']);
                });
                $brand = $this->load->object('brand');
                $data_brand = $brand->get_all([
                    'limit' => [0, 5],
                    'order_by' => ['date_added' => 'DESC']
                ]);
                $this->data['brand'] = $data_brand;
                return $this->load->view('admin/product_brand');
        }
    }
}