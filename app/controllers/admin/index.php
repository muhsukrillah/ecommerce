<?php
/**
 * Admin index Controller
 * Dibuat oleh              : Muh. Sukrillah
 * Url                      : http://www.sukrillah.xyz
 * Versi                    : 1.0
 */
namespace app\controller;

use \sasak\Controller;
use \sasak\User;

class Index extends Controller {

    function __construct(){
        parent::__construct();
        // user must login
        User::auth();

        // run menu
        $menu =& $this->menu;
        do_action('admin_menu', $menu);
        $this->menu();
        $this->__js();
    }

    private function __js(){
        error_reporting(E_ALL);
        $data = func_get_args();
         $this->load->library('Assets', ['namespace' => 'Assets', 'param' => ['path' => BASE_PATH, 'base_url', base_url()]]);
        $this->Assets->setPath(BASE_PATH)->setBaseUrl(asset_url());

        $this->Assets
        ->addJs(['url' => 'js/jQuery-2.1.4.min.js', 'id' => 'jQueryJs']) // http://local/assets/js/jQuery 
        ->addJs(['url' => 'bootstrap/js/bootstrap.min.js', 'id' => 'BootstrapJs'])
        ->addJs(['url' => 'dist/js/app.min.js'])
        ->addJs(['url' => 'plugins/datepicker/bootstrap-datepicker.js'])
        ->addJs(['url' => 'plugins/datepicker/locales/bootstrap-datepicker.id.js'])
        ->addJs(['url' => 'plugins/input-mask/jquery.inputmask.js'])
        ->addJs(['url' => 'plugins/input-mask/jquery.inputmask.date.extensions.js'])
        ->addJs(['url' => 'dist/js/demo.js']);

        if(!empty($data)){
            if(is_object($data[0])){
                $param[] =& $this->Assets;
                @call_user_func_array($data[0], $param);
                return $this;
            }
            foreach($data as $js){
                $this->Assets->addJs($js);
            }
        }
        $this->data['Assets'] =& $this->Assets;
        return $this;
    }

    function index(){
        $this->menu->set_active_menu('dashboard');

        return $this->load->view('admin/dashboard');
    }

    function menu(){
        $this->data['menu'] =& $this->menu;
    }
}
