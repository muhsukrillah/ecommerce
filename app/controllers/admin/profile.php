<?php
/**
 * Profile Controller
 *
 * Dibuat Oleh              : Muh. Sukrillah
 * Url                      : http://www.sukrillah.xyz
 * Versi                    : 1.0
 */
namespace app\controller;

use \sasak\Controller;
use \sasak\User;

class Profile extends Controller {

    function __construct(){
        parent::__construct();
    }

    function index(){
        return 'your profile..';
    }
}