<?php
/**
 * Invoice Controller Class
 *
 * @author              Muh Sukrillah
 * @version             1.0
 */
namespace app\controller;
use \sasak\User;

class Invoice extends \sasak\Controller {
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();

        // User must login first
        User::auth();
        // Assets class
        $this->load->library('Assets', ['namespace' => 'Assets', 'param' => ['path' => BASE_PATH, 'base_url', base_url()]]);
        $this->Assets->setPath(BASE_PATH)->setBaseUrl(asset_url());
        $this->data['Assets'] =& $this->Assets;
        // run menu
        $menu =& $this->menu;
        do_action('admin_menu', $menu);
        $this->data['menu'] =& $this->menu;
        $breadcrumb =& $this->breadcrumb;
        do_action('breadcrumb', $breadcrumb);
        //$this->data['breadcrumb']
        // set active menu
        $this->menu->set_active_menu('invoice');
    }

    private function __js(){
        error_reporting(E_ALL);
        $data = func_get_args();
        $this->Assets
        ->addJs(['url' => 'js/jQuery-2.1.4.min.js', 'id' => 'jQueryJs']) // http://local/assets/js/jQuery 
        ->addJs(['url' => 'bootstrap/js/bootstrap.min.js', 'id' => 'BootstrapJs'])
        ->addJs(['url' => 'dist/js/app.min.js'])
        ->addJs(['url' => 'plugins/datepicker/bootstrap-datepicker.js'])
        ->addJs(['url' => 'plugins/datepicker/locales/bootstrap-datepicker.id.js'])
        ->addJs(['url' => 'plugins/input-mask/jquery.inputmask.js'])
        ->addJs(['url' => 'plugins/input-mask/jquery.inputmask.date.extensions.js'])
        ->addJs(['url' => 'dist/js/demo.js']);

        if(!empty($data)){
            if(is_object($data[0])){
                $param[] =& $this->Assets;
                @call_user_func_array($data[0], $param);
                return $this;
            }
            foreach($data as $js){
                $this->Assets->addJs($js);
            }
        }
        return $this;
    }

    function index(){
        if(!User::$role->can('read_invoice')){
            Error('Maaf, Anda tidak diijinkan untuk mengakses halaman ini..');
        }
        $this->title->set('Data Tagihan');
        $this->__js();

        $this->menu->set_active_child_menu('all-invoice');
        // load invoices object
        $invoice = $this->load->object('invoice');
        $all_invoices = $invoice->get_all();
        // load customer
        $customer = $this->load->object('customer');
        foreach($all_invoices as $key => $value){
            $cust = $customer->get_by(['id_customer' => $value['id_customer']]);
            $customer_name = $cust['first_name'] .' '. $cust['last_name'];
            $all_invoices[$key]['customer_name'] = $customer_name;
        }
        $this->data['invoice_data'] = $all_invoices;
        $this->data['total'] = count($all_invoices);

        return $this->load->view('admin/invoices_index');  
    }

    function ajax_approve_payment(){
        $token = $this->input->post('token');
        if(!\sasak\session()->validate_token($token)){
            return $this->___ajax_response([
                'success' => FALSE,
                'message' => 'Maaf, Anda tidak diijinkan untuk melakukan aksi ini..'
            ]);
        }
        $ref = $this->input->post('ref');
        if(!in_array($ref, ['invoice_detail'])){
            return $this->___ajax_response([
                'success' => FALSE,
                'message' => 'Maaf, Anda tidak diijinkan untuk melakukan aksi ini..'
            ]);
        }
        $payment_id = (int) $this->input->post('id_payment');
        $invoice_id = (int) $this->input->post('id_invoice');
        if(!$payment_id || !$invoice_id){
            return $this->___ajax_response([
                'success' => FALSE,
                'message' => 'Maaf, data tidak valid..'
            ]);
        }
        // load invoice payment object model
        $ip = $this->load->object('invoice_payment');
        $ip->id = $payment_id;
        $ip->evaluated = 1;
        if($ip->update(['id_invoice' => $invoice_id])){
            return $this->___ajax_response([
                'success' => TRUE,
                'message' => 'Data berhasil diperbarui..'
            ]);
        }else{
            return $this->___ajax_response([
                'success' => FALSE,
                'message' => 'Maaf, data gagal diperbarui. Sepertinya terjadi kesalahan pada sistem.'
            ]);
        }
    }

    function detail(){
        if(!User::$role->can('read_invoice')){
            Error('Maaf, Anda tidak diijinkan untuk mengakses halaman ini..');
        }
        $this->title->set('Data Tagihan');
        $this->__js(
            ['url' => 'admin/js/invoice.js']
        );

        $this->load->library('body_class');
        $this->body_class->add('invoice_detail');

        $this->data['bodyClass'] = $this->body_class->get();

        $this->menu->set_active_child_menu('all-invoice');
        // invoice id
        $invoice_id = (int) $this->input->get('invoice_id');
        // load invoices object
        $invoice = $this->load->object('invoice');
        $all_invoices = $invoice->get_by([
            'id_invoice' => $invoice_id
        ]);
        $id_customer = (int) $all_invoices['id_customer'];
        // load customer object model
        $customer = $this->load->object('customer');
        $cust = $customer->get_by(['id_customer' => $id_customer]);
        $customer_name = $cust['first_name'] .' '. $cust['last_name'];
        $all_invoices['customer_name'] = $customer_name;
        $this->data['invoice_data'] = $all_invoices;
        $this->data['total'] = count($all_invoices);
        // load invoice payment object model
        $ip = $this->load->object('invoice_payment');
        //$invoice_payment = $ip->get_by(['id_invoice' => $invoice_id, 'id_customer' => $all_invoices['id_customer']]);
        $this->data['invoice_payment'] = $ip->get_payment_detail($invoice_id, $id_customer);

        return $this->load->view('admin/invoice_detail');
    }

    function ___ajax_response($res = []){
        if(!is_array($res)){
            $res = [];
        }
        ob_start();
        @header("Content-type: text/json");
        return json_encode($res);
    }
}