<?php 
/**
 * author 		: muh. sukrillah
 * author url 	: http://www.sukrillah.xyz
 * 
 **/
namespace app\controller;

use app\Controller;
use app\User;
use app\Log;

class Pegawai extends Controller {
	public $data;

	function __construct(){
		parent::__construct();

		User::auth();

		$this->load->library('lib_title', ['namespace'=>'title']);
		$this->title->add('Pegawai');
		$this->data['title'] =& $this->title;
		// admin menu
		$this->admin_menu->set_active_menu('pegawai');
		//$this->__menu();
	}

	private function __menu(){
		$this->admin_menu->add_menu('pegawai', 'Pegawai', 'fa fa-user', ['href' => base_url('pegawai/index')]);
	}

	function __desctruct(){
		unset($this->data);
	}

	function index(){
		//error_reporting(E_ALL);
		if(!User::$role->can('read_pegawai')){
			Error('Maaf, Anda tidak diijinkan untuk mengakses halaman ini..');
		}
		// admin menu
		$this->admin_menu->set_active_child_menu('index-pegawai');
		$this->load->model('pegawai');
		$orderby = $this->input->get('orderby');
		if(!empty($orderby)){
			$order_ex = explode('.', $orderby);		
			$order = [$order_ex[0] => $order_ex[1]];
			$this->data['order'] = $order_ex;
		}else{
			$order = NULL;
		}

		$page = (int) $this->input->get('page');
		$perPage = (int) $this->input->get('perPage');
		$perPageDef = 10;
		// pagination
		$this->load->library('pagination');
		$path = uri()->compare(['page' => '%d']);//base_url('pegawai/index?page=%d');
		
		$total = $this->pegawai_model->count_all();
		$pagination = new \app\lib\Pagination($total, $page);
		$pagination->appearance = array(
				'nav_prev' 			=> '<a href="%s" class="prev btn btn-primary"><span>prev</span></a>',
				'nav_number_link' 	=> '<a href="%s" class="btn btn-primary"><span>%d</span></a>',
				'nav_number' 		=> '<a href="#!" class="active btn btn-default"><span>%d</span></a>',
				'nav_more' 			=> '<a href="#!" class="more btn btn-default focus"><span>...</span></a>',
				'nav_next' 			=> '<a href="%s" class="next btn btn-primary"><span>next</span></a>'
		);
		$pagination->perPage = ($perPage > 0) ? $perPage : $perPageDef;
		$pagination->path = $path;
		$pagination->setStart($page);
		$pagination->setCount($total);

		$limit = $pagination->getLimit();
		$offset = $pagination->getLimitOffset();
		$this->data['pagination'] = $pagination->paginate();
		$this->data['page'] = ($page == 0) ? 1 : $page;
		$this->data['perPage'] = ($perPage > 0) ? $perPage : $perPageDef;
		// get all data
		$pegawai = $this->pegawai_model->get_all($order, [$limit,$offset]);
		$this->data['data_pegawai'] = $pegawai;
		
		$this->data['subtotal'] = $this->data['perPage'];
		$this->data['total'] = $this->pegawai_model->count_all();

		return $this->load->view('pegawai', $this->data);
	}

	function cetak(){
		if(!User::$role->can('read_pegawai')){
			Error('Maaf, Anda tidak diijinkan untuk mengakses halaman ini..');
		}
		// admin menu
		$this->admin_menu->set_active_child_menu('index-pegawai');
		$this->load->model('pegawai');
		// get all data
		$pegawai = $this->pegawai_model->get_all([], 'unlimited');
		$this->data['data_pegawai'] = $pegawai;
		
		return $this->load->view('pegawai_cetak', $this->data);
	}

	function ajax(){
		error_reporting(E_ALL);
		echo uri()->current_url();
		echo '<br>';
		echo $this->input->get('tes');
		echo '<br/>';
		echo uri()->segment(0);
		echo '<br>';
		echo array_to_url(['perpage'=>1,'page'=>2]);

		return;
		$this->session->set_userdata('username', 'Muh Sukrillah');

		$session = $this->session->userdata();
		echo '<pre>';
		print_r($session);
		echo '</pre>';
		exit;
	}

	function looping($jumlah = 0){
		for($i = 1, $j = $jumlah; $i <= $j; $i++){
			echo '<li>'. $i .'</li>';
		}
	}

	function perkalian($nilai1 = 0, $nilai2 = 0){
		$total = $nilai1 * $nilai2 / 2;
		$tampil = $nilai1 .' x '. $nilai2 .' / 2 = '. $total;
		return $tampil;
	}

	function nama($nama = 'tidak dikenal'){
		return 'halo nama saya ' . $nama;
	}

	function tes(){
		$last_nip = '1601021';
		$last_nip_id = (int) (substr($last_nip, 6));
		echo $last_nip_id + 1;
	}

	function tambah(){
		if(!User::$role->can('create_pegawai')){
			Error('Maaf, Anda tidak diijinkan untuk mengakses halaman ini..');
		}
		// admin menu
		$this->admin_menu->set_active_child_menu('tambah-pegawai');

		$this->title->add('Tambah Baru');
		$data = $this->input->post();
		if(isset($data) && !empty($data)){			
			$this->___save($data);
		}

		$this->load->model(['jabatan','pegawai']);
		$jabatan_a = $this->jabatan_model->getAll();
		$jabatan = [];
		foreach($jabatan_a as $key => $value){
			$jabatan[$value['id_jabatan']] = $value['nama_jabatan'];
		}
		$this->data['jabatan'] = $jabatan;
		$this->data['new_nip'] = date('ymd') . $this->pegawai_model->get_new_nip();
		return $this->load->view('pegawai_add', $this->data);
	}

	function delete(){
		if(!User::$role->can('delete_pegawai')){
			Error("Maaf, Anda tidak diijinkan untuk mengakses halaman ini..");
		}

		$nip = $this->input->get('nip');

		$this->load->model('pegawai');
		$hapus = $this->pegawai_model->delete($nip);
		if($hapus){
			echo 'Data berhasil dihapus.. <a href="'. base_url('pegawai') .'">Lihat Data</a>';
		}else{
			echo $this->pegawai_model->last_query();
			echo 'Data gagal dihapus.. <a href="'. base_url('pegawai') .'">Kembali</a>';
		}
	}

	function edit(){		
		error_reporting(E_ALL);
		if(!User::$role->can('update_pegawai')){
			Error('Maaf, Anda tidak diijinkan untuk mengakses halaman ini..');
		}
		// admin menu
		$this->admin_menu->set_active_child_menu('index-pegawai');

		$this->load->model(['pegawai', 'jabatan', 'user','gaji']);
		//$this->load->model('jabatan');
		//$this->load->model('user');

		$this->load->object('user');			

		$data = $this->input->post();

		if(isset($data) && !empty($data)){

			$nip = (int) (isset($data['last_nip']) ? $data['last_nip'] : 0);
			unset($data['last_nip']);

			$pegawai = $this->pegawai_model->get_by(['nip' => $nip]);

			if(!isset($pegawai['id_user'])){
				die('Data pegawai tidak benar!');
			}
			// load user_model
			$user_model = new \app\model\user_model;
			$userdata = new \app\object_model\user($user_model);
			$userdata->__set_data($pegawai['id_user']);
			if(isset($userdata->id_user)){
				$pegawai['username'] 	= $userdata->username;
				$pegawai['level']		= $userdata->level;
				$pegawai['password'] 	= $userdata->password;
			}

			$data['id_jabatan'] = (int) $data['jabatan'];
			unset($data['jabatan']);
			//echo '<pre>';
			//print_r($pegawai);
			//echo '</pre>';
			//echo '<br/>';
			
			foreach($data as $key => $value){
				if(in_array((string)$key, ['username','password','level'])){
					continue;
				}
				if(in_array((string)$key, ['gaji_pokok', 'tunj_istri', 'tunj_anak'])){
					continue;
				}
				$key = (string) $key;
				if(!isset($pegawai[$key])){					
					//echo $key .' is not found.. <br/>';
					unset($data[$key]);
					continue;
				}				
				if($data[$key] == $pegawai[$key]){					
					unset($data[$key]);
					//echo $key .' not changed.. it will be ignored..<br/>';
				}
			}
			//print_r($data);
			//return;
			return $this->___update($data, $nip);

			//$this->load->view('edit_pegawai', $this->data);
			//return;
		}
		// buat judul halaman
		$this->title->add('Ubah Data');
		// get nip parameter
		$nip = $this->input->get('nip');
		
		// data pegawai
		$pegawai = $this->pegawai_model->get_by(['nip' => $nip]);
		$this->load->model('user');
		$user_model = new \app\model\user_model;
		// data akun
		$userdata = new \app\object_model\user($user_model);
		$userdata->__set_data((int) $pegawai['id_user']);
		// set data pegawaa[username], pegawai[password], pegawai[level]
		if(isset($userdata->id_user)){
			$pegawai['id_user'] = $userdata->id_user;
			$pegawai['username'] = $userdata->username;
			$pegawai['level'] = $userdata->level;
		}
		// gaji
		$gaji = $this->gaji_model->get_by(['nip'=>$nip]);
		if(is_array($gaji) && !empty($gaji)){
			unset($gaji['nip'], $gaji['id_gaji']);
			foreach($gaji as $key => $value){
				$pegawai[$key] = $value;
			}
		}
		// data jabatan
		$jabatan = $this->jabatan_model->getAll();
		$jabatan_ar = [];
		foreach($jabatan as $key => $value){
			$jabatan_ar[$value['id_jabatan']] = $value['nama_jabatan'];
		}
		$this->data['jabatan'] = $jabatan_ar;
		if(is_array($pegawai)){
			if($pegawai['tanggal_lahir'] == '0000-00-00'){
				$pegawai['tanggal_lahir'] = '';
			}
			$this->data = array_merge($this->data, $pegawai);
			$this->data['found'] = TRUE;
		}else{
			$pesan = 'Data pegawai dengan nip ['. $nip .'] tidak tersedia..';
			$pesan .= '<a href="'. base_url('pegawai') .'">Kembali</a>';
			$this->data['pesan'] = $pesan;
			$this->data['jenis_pesan'] = 'error';
			$this->data['found'] = FALSE;
		}
		return $this->load->view('pegawai_edit', $this->data);
	}

	function detail(){
		error_reporting(E_ALL);
		$nip = $this->input->get('nip');
		// admin menu
		$this->admin_menu->set_active_child_menu('index-pegawai');
		// load pegawai
		$this->load->object('pegawai');
		
		$pegawai = new \app\object_model\pegawai();
		$pegawai->__set_data($nip, 'nip');

		if(isset($pegawai->nip)){
			$this->data['found'] = TRUE;
			
			$this->title->add($pegawai->nama);
			$this->data['pegawai'] =& $pegawai;
		}else{
			$this->data['found'] = FALSE;
			$this->title->add('Error');
		}
		return $this->load->view('pegawai_detail', $this->data);
	}

	function cari(){
		// admin menu
		$this->admin_menu->set_active_child_menu('index-pegawai');

		$query = $this->input->get('query');
		if(empty($query)){
			return $this->index();
		}
		$page = (int) $this->input->get('page');

		$this->load->model('pegawai');
		$this->load->library('pagination');

		$orderby = $this->input->get('orderby');
		if(!empty($orderby)){
			$order_ex = explode('.', $orderby);		
			$order = [$order_ex[0] => $order_ex[1]];
			$this->data['order'] = $order_ex;
		}else{
			$order = NULL;
		}

		$path = base_url('pegawai/cari?query='. $query .'&page=%d');
		
		$total = $this->pegawai_model->total_cari($query);
		$pagination = new \app\lib\Pagination($total, $page);
		$pagination->appearance = array(
				'nav_prev' 			=> '<a href="%s" class="prev btn btn-primary"><span>prev</span></a>',
				'nav_number_link' 	=> '<a href="%s" class="btn btn-primary"><span>%d</span></a>',
				'nav_number' 		=> '<a href="javascript:;" class="active btn btn-default"><span>%d</span></a>',
				'nav_more' 			=> '<a href="javascript:;" class="more btn btn-default focus"><span>...</span></a>',
				'nav_next' 			=> '<a href="%s" class="next btn btn-primary"><span>next</span></a>'
		);
		$pagination->perPage = 15;
		$pagination->path = $path;
		$pagination->setStart($page);
		$pagination->setCount($total);

		$limit = $pagination->getLimit();
		$offset = $pagination->getLimitOffset();

		$pegawai = $this->pegawai_model->cari($query, $order, [$limit, $offset], $subtotal);
		$this->data['data_pegawai'] = $pegawai;
		
		$this->data['subtotal'] = $subtotal;
		$this->data['total'] = $total;
		$this->data['query'] = $query;	

		$this->data['pagination'] = $pagination->paginate();

		return $this->load->view('pegawai', $this->data);
	}

	private function ___update($data = array(), $nip = 0){
		error_reporting(E_ALL);
		// data gaji
		$gaji = [];
		if(isset($data['gaji_pokok']) && !empty($data['gaji_pokok'])){
			$gaji['gaji_pokok'] = (int) $data['gaji_pokok'];
			unset($data['gaji_pokok']);
		}
		if(isset($data['tunj_istri']) && !empty($data['tunj_istri'])){
			$gaji['tunjangan_istri'] = (int) $data['tunj_istri'];
			unset($data['tunj_istri']);
		}
		if(isset($data['tunj_anak']) && !empty($data['tunj_anak'])){
			$gaji['tunjangan_anak'] = (int) $data['tunj_anak'];
			unset($data['tunj_anak']);
		}
		// end data gaji
		// foto
		if(isset($_FILES['foto']) && !empty($_FILES['foto'])){
			$this->load->library(['Uploader'], [['namespace'=>'uploader']]);
			$this->uploader->initialize([
				'is_image' 		=> TRUE,
				'upload_path' 	=> ASSETS_PATH .'gambar/',
				'new_name' 		=> $nip,
				'field_name' 	=> 'foto'
			]);
			$upload = $this->uploader->upload();
			if($upload){
				$upload_data = $this->uploader->get_data();
				$foto = 'gambar/'. $upload_data['new_name'];
				$data['foto'] = $foto;
				Log::add('Gambar berhasil diunggah..');
			}else{
				Log::add('Gambar gagal diunggah..');
				Log::add('Uploader : '. $this->uploader->get_message());
			}
		}
		// end foto
		$simpan = $this->pegawai_model->update($data, $nip);

		if($simpan){
			$link = '<a href="'. base_url('pegawai/detail?nip='. $nip) .'">lihat data</a>';
			$this->data['pesan'] = 'Data berhail diedit.. '. $link;
			$this->data['jenis_pesan'] = 'sukses';
		}else{
			$this->data['pesan'] = 'Data gagal di siedit..';
			$this->data['pesan'] = 'gagal';
		}
		unset($data); 
		// data pegawai
		$pegawai = $this->pegawai_model->get_by(['nip' => $nip]);
		$this->load->model(['user','gaji']);
		$data_gaji = $this->gaji_model->get($nip);
		
		if(!isset($data_gaji['id_gaji'])){
			if(!empty($gaji)){
				$gaji['nip'] = $nip;
				$add = $this->gaji_model->add($gaji);
				if($add){
					Log::add('Gaji berhasil di tambahkan..');
				}else{
					Log::add('Gaji gagal di tambahkan');
				}
			}
		}else{
			if(!empty($gaji)){
				$ug = $this->gaji_model->update($gaji, ['nip' => $nip]);
				if($ug){
					Log::add('Gaji berhasil diperbarui..'. print_r($gaji, 1));
				}else{
					Log::add('Gaji gagal diperbarui..'. print_r($gaji,1));
				}
			}
		}
		$user_model = new \app\model\user_model;
		// data akun
		$userdata = new \app\object_model\user($user_model);
		$userdata->__set_data((int) $pegawai['id_user']);
		// set data pegawaa[username], pegawai[password], pegawai[level]
		if(isset($userdata->id_user)){
			$pegawai['id_user'] = $userdata->id_user;
			$pegawai['username'] = $userdata->username;
			$pegawai['level'] = $userdata->level;
		}
		$this->data = array_merge($this->data, $pegawai);
		// data jabatan
		$jabatan = $this->jabatan_model->getAll();
		$jabatan_ar = [];
		foreach($jabatan as $key => $value){
			$jabatan_ar[$value['id_jabatan']] = $value['nama_jabatan'];
		}
		$this->data['jabatan'] = $jabatan_ar;		
		$this->data['found'] = TRUE;
		return $this->load->view('pegawai_edit', $this->data);
	}

	private function ___save($data){
		error_reporting(E_ALL);
		$this->load->model('pegawai');	
		
		/*$simpan = $this->pegawai_model->save([
			'nip' 		=> $_POST['nip'],
			'nama'		=> $_POST['nama'],
			'id_user'	=? $,'id_jabatan','jenis_kelamin','status_nikah','status_aktif','jumlah_anak','jumlah_istri','alamat','hp','email','no_ktp','no_rekening','foto'
		]);*/
		$data['id_jabatan'] = (int) $data['jabatan'];
		unset($data['jabatan']);
		// Data Gaji	
		$gaji = [];
		if(isset($data['gaji_pokok']) && !empty($data['gaji_pokok'])){
			$gaji['gaji_pokok'] = (int) $data['gaji_pokok'];
		}
		if(isset($data['tunj_istri']) && !empty($data['tunj_istri'])){
			$gaji['tunjangan_istri'] = (int) $data['tunj_istri'];
		}
		if(isset($data['tunj_anak']) && !empty($data['tunj_anak'])){
			$gaji['tunjangan_anak'] = (int) $data['tunj_anak'];
		}

		// End Gaji
		// amankan data
		$data = clean($data);

		$simpan = $this->pegawai_model->save($data);
		if($simpan){
			$this->load->model('gaji');
			$gaji['nip'] = $data['nip'];
			$simpan_gaji = $this->gaji_model->add($gaji);
			if(!$simpan_gaji){
				Log::add('Data gaji untuk pegawai ['. $data['nip'] .'] gagal disimpan..');
			}
			$link = '<a href="'. base_url('pegawai') .'">lihat data</a>';
			$this->data['pesan'] = 'Data berhail disimpan.. '. $link;
			$this->data['jenis_pesan'] = 'sukses';
			Log::add($this->data['pesan']);
		}else{
			$this->data['last_query'] = $this->pegawai_model->last_query();
			$this->data['pesan'] = 'Data gagal di simpan..';
			$this->data['jenis_pesan'] = 'gagal';
			Log::add('Data pegawai gagal disimpan.. ');
			Log::add($this->data['last_query']);
		}
		unset($_POST);
		unset($data);
	}
}