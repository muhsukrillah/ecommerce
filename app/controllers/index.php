<?php
namespace app\controller;

use \sasak\Controller;
use \sasak\User;

class Index extends Controller {
	var $data = array();

	function __construct(){
		parent::__construct();
        \app\Customer::auth(NULL, FALSE);
	}

	private function __assets(){
		$this->load->library('Assets');
		$this->Assets->setPath(BASE_PATH)->setBaseUrl(asset_url());
		$this->Assets
		->addJs(['url' => 'js/jQuery-2.1.4.min.js', 'id' => 'jQueryJs'])
        ->addJs(['url' => 'bootstrap/js/bootstrap.min.js', 'id' => 'BootstrapJs'])
        ->addJs(['url' => 'plugins/accounting/accounting.min.js'])
        ->addJs(['url' => 'plugins/lazy-loading/jquery.lazy.min.js'])
        ->addJs(['url' => 'templates/homemart/js/jquery.helpers.js'])
        ->addJs(['url' => 'templates/homemart/js/sCart.js'])
        ->addJs(['url' => 'templates/homemart/js/front.js']);
		
        /*$this->Assets->addJs([
            'url'   => 'js/angular.min.js', 
            'id'    => 'angularJs'
        ], [
            'url'   => 'js/angular-resource.min.js',
            'id'    => 'angularResource'
        ], [
            'url'   => 'js/angular-route.js',
            'id'    => 'angularRoute'
        ]);
        $this->Assets->addJs([
            'url'   => 'js/front.home.js',
            'id'    => 'frontJs'
        ]);*/

		$this->data['Assets'] =& $this->Assets;
	}

	function index(){
		error_reporting(E_ALL);
		$this->load->library('lib_title', [
			'namespace' => 'title'			
		]);
		$this->title->add('Home Mart');
		// assets
		$this->__assets();

        // load category
        $category = $this->load->object('category');
        $categories = $category->get_all([
            'where' => ['parent_id' => 0],
            'order_by' => ['order']
        ]);
        $this->data['categories'] = $categories;
        // load product
        $product = $this->load->object('product');
        $image = $this->load->object('product_image');
        $data = $product->get_all([
            'where' => ['status' => 1],
            'order_by' => ['date_added' => 'DESC'],
            'limit' => [0, 6]
        ]);

        foreach($data as $key => $value){
            // get image
            $images = $image->get_by(['id_product' => $value['id_product']]);
            $img_featured = '';
            foreach($images as $k => $v) {
                if(!is_array($v)){
                    if($k == 'url'){
                        $img[] = array('url' => $v);
                        $img_featured = $v;
                    }
                    if($k == 'thumbnail_url' && !empty($v)){
                        $img_featured = $v;
                    }
                    continue;
                }

                $img[] = array(
                    'url' => $v['url']
                );
                if(!empty($img_featured)) continue;
                $img_featured = isset($v['thumbnail_url']) ? $v['thumbnail_url'] : $v['url'];
            }
            $data[$key]['featured_image'] = $img_featured;
            $data[$key]['images'] = $img;
        }
        $this->data['products'] = $data;
        $scope = new \sasak\Scope();
        $scope->data = 'Tes';
		return $this->load->view('index', $this->data);
	}

    function view_product($product_name = ''){
        $product = $this->load->object('product');
        $data = $product->get_by(['product_name' => $product_name]);
        print_r($data);
        exit;
    }

	function user(){
		$this->load->model('user');
		$users = $this->user_model->get_all();
		print_r($users);
	}

	function tes(){
		$args = func_get_args();
		if(!empty($args)){
			print_r($args);
		}
		echo 'tes<br>';
		echo $this->uri->segment(1) .'<br>';
		echo $this->input->get('lorem') .'<br>';
		echo $this->input->get('dolor');
	}
}