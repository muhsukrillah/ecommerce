<?php
/**
 * Customer account controller
 */
namespace app\controller;

class Customer extends \sasak\Controller {

    function __construct(){
        parent::__construct();
    }

    function index(){
        \app\Customer::auth();

        error_reporting(E_ALL);

        $asset = $this->load->library('Assets', ['return' => TRUE]);
        $asset->setPath(BASE_PATH)->setBaseUrl(asset_url());
        $asset
        ->addCss(['url' => 'css/material-colors.css'])
        ->addJs(['url' => 'js/jQuery-2.1.4.min.js', 'id' => 'jQueryJs'])
        ->addJs(['url' => 'bootstrap/js/bootstrap.min.js', 'id' => 'BootstrapJs'])
        ->addJs(['url' => 'plugins/lazy-loading/jquery.lazy.min.js'])
        ->addJs(['url' => 'plugins/accounting/accounting.min.js'])
        ->addJs(['url' => 'plugins/ui-block/jquery.blockUI.js'])
        ->addJs(['url' => 'templates/homemart/js/jquery.helpers.js'])
        ->addJs(['url' => 'templates/homemart/js/sCart.js'])
        ->addJs(['url' => 'templates/homemart/js/front.js'])
        ->addJs(['url' => 'templates/homemart/js/checkout.js']);

        $this->data['Assets'] = $asset;

        return $this->load->view('customer/dashboard');
    }

    function profile(){
        \app\Customer::auth();
        
        $customer_name = \app\Customer::$data['first_name'] .' '. \app\Customer::$data['last_name'];
        $this->data['nama'] = $customer_name;

        $this->title->set($customer_name);

        $asset = $this->load->library('Assets', ['return' => TRUE]);
        $asset->setPath(BASE_PATH)->setBaseUrl(asset_url());
        $asset
        ->addCss(['url' => 'css/material-colors.css'])
        ->addJs(['url' => 'js/jQuery-2.1.4.min.js', 'id' => 'jQueryJs'])
        ->addJs(['url' => 'bootstrap/js/bootstrap.min.js', 'id' => 'BootstrapJs'])
        ->addJs(['url' => 'plugins/accounting/accounting.min.js'])
        ->addJs(['url' => 'plugins/lazy-loading/jquery.lazy.min.js'])
        ->addJs(['url' => 'plugins/ui-block/jquery.blockUI.js'])
        ->addJs(['url' => 'templates/homemart/js/jquery.helpers.js'])
        ->addJs(['url' => 'templates/homemart/js/sCart.js'])
        ->addJs(['url' => 'templates/homemart/js/front.js'])
        ->addJs(['url' => 'js/login.js']);

        $this->data['Assets'] = $asset;

        $this->data['data'] = \app\Customer::$data;

        return $this->load->view('customer/profile');
    }

    function ajax_auth($method = ''){
        if(\app\Customer::is_logedin()){
            return $this->___ajax_message([
                'success' => TRUE,
                'next' => base_url('customer/index/')
            ]);
        }else{
            switch($method){
                case 'login':
                    return $this->___auth_login();
                break;
                case 'register':
                    return $this->___auth_register();
                break;
            }
        }
    }

    private function ___ajax_message($message = []){
        header("Content-type: text/json");
        return json_encode($message);
    }

    private function ___auth_login(){
        error_reporting(E_ALL);
        $post = $this->input->post();

        $username = (isset($post['cusername']) ? $post['cusername'] : '');
        $password = (isset($post['cpassword']) ? $post['cpassword'] : '');
        $remember = (isset($post['remember']) ? $post['remember'] : FALSE);
        $next_url = (isset($post['next']) ? $post['next'] : '');

        $cdata = ['username' => $username];
        
        if(filter_var($username, FILTER_VALIDATE_EMAIL)){
            $cdata = ['email' => $username];
        }
        // load customer object model
        $customer = $this->load->object('customer');
        // get customer data
        $data = $customer->get_by($cdata);
        // check data
        if(isset($data['id_customer'])){
            // verify customer password
            \sasak\Log::add('[Login]: '. $data['password'], TRUE, TRUE);
            if(\sasak\Hash::verify($password, $data['password'])) {
                $login_time = date('d-m-Y H:i:s');
                $timestamp = strtotime($login_time);
                $username = $data['username'];
                \sasak\Log::add('[Login] : password => '. $data['password'], TRUE, TRUE);
                \sasak\Log::add('[Login] : timestamp => '. $timestamp, TRUE, TRUE);
                $password = \sasak\Hash::make($data['password'] . $timestamp);
                $customer->id_customer = $data['id_customer'];
                $customer->token = $password;
                \sasak\Log::add('[Login] : token => '. $password, TRUE, TRUE);
                $customer->update();
                $this->session->set_remember(TRUE);
                $this->session->encrypt_session(compact('username','password','timestamp'), FALSE);
                
                $redirect_to = base_url('customer/index/');

                if(!empty($next_url)){
                    $redirect_to = $next_url;
                }
                \sasak\Log::add('[Customer Login] : '. $username .' berhasil login..');
                
                return $this->___ajax_message([
                    'next' => $redirect_to,
                    'success' => TRUE,
                    'message' => 'Anda berhasil login..'
                ]);
            }else{
                return $this->___ajax_message([
                    'success' => FALSE,
                    'message' => "Kata Sandi Salah.."
                ]);
            }
        }else{
            return $this->___ajax_message(['success' => FALSE, 'message' => 'Nama Pengguna Anda salah..']);
        }
    }

    private function ___auth_register(){
        $post = $this->input->post();

        // filter agreement
        if(!isset($post['agreement']) || empty($post['agreement']) || $post['agreement'] != 'yes'){
            return $this->___ajax_message(['success' => FALSE, 'message' => 'Maaf, Anda harus setuju dengan syarat dan ketentuan yang berlaku..']);
        }
        // filter username
        if(!isset($post['cusername']) || empty($post['cusername'])){
            return $this->___ajax_message(['success' => FALSE, 'message' => 'Username tidak boleh kosong..']);
        }
        // filter password
        if(!isset($post['cpassword']) || empty($post['cpassword'])){
            return $this->___ajax_message(['success' => FALSE, 'message' => 'Password tidak boleh kosong..']);
        }
        // filter first name
        if(!isset($post['first_name']) || empty($post['first_name'])){
            return $this->___ajax_message(['success' => FALSE, 'message' => 'Nama depan tidak boleh kosong..']);
        }
        // filter email
        if(!isset($post['email']) || empty($post['email'])){
            return $this->___ajax_message(['success' => FALSE, 'message' => 'Email tidak boleh kosong..']);
        }
        if(!filter_var($post['email'], FILTER_VALIDATE_EMAIL)){
            return $this->___ajax_message(['success' => FALSE, 'message' => 'Email Anda tidak benar..']);
        }

        //$post['has_password_count'] = strlen($post['hash_password']);
        //$post['password_match'] = (\sasak\Hash::verify('123', $post['hash_password'])) ? 'TRUE' : 'FALSE';

        $customer = $this->load->object('customer');
        $check = $customer->get_by(['username' => $post['cusername']]);
        if(isset($check['id_customer'])){
            return $this->___ajax_message(['success' => FALSE, 'message' => 'Maaf, username yang Anda inputkan sudah terdaftar. Silahkan gunakan yang lain..']);
        }
        $check = $customer->get_by(['email' => $post['email']]);
        if(isset($check['id_customer'])){
            return $this->___ajax_message(['success' => FALSE, 'message' => 'Maaf, email yang Anda inputkan sudah terdaftar oleh pelanggan yang lain..']);
        }
        // generate uniq ids
        $uuid = \sasak\Hash::uniqid();
        // generate hashed password
        $hash_password = \sasak\Hash::make($post['cpassword']);

        $customer->uuid = $uuid;
        $customer->password = $hash_password;
        $customer->username = $post['cusername'];
        $customer->first_name = $post['first_name'];
        if(isset($post['last_name']) && !empty($post['last_name'])){
            $customer->last_name = $post['last_name'];
        }
        $customer->email = $post['email'];
        if(isset($post['phone']) && !empty($post['phone'])){
            $customer->phone = $post['phone'];
        }
        if(isset($post['gender']) && !empty($post['gender'])){
            $customer->gender = $post['gender'];
        }
        $customer->address = $post['address'];
        $customer->date_added = date('Y-m-d H:i:s');
        $customer->date_updated = $customer->date_added;

        $id_customer = $customer->save();
        if($id_customer){
            // success
            return $this->___ajax_message([
                'success' => TRUE,
                'message' => 'Terima kasih, Anda sudah terdaftar. Selanjutnya silahkan Anda periksa email Anda untuk konfirmasi email..'
            ]);
        }else{
            // failed
            return $this->___ajax_message([
                'success' => FALSE,
                'message' => 'Maaf, sepertinya terjadi kesalahan pada sistem. Akun Anda belum terdaftar..'
            ]);
        }
    }

    function ajax_cupon_check(){
        $code = clean($this->input->get('code'));
        $belanja = (int) $this->input->get('total_belanja');
        $value = 0;
        if($code){
            $cupon = $this->load->object('cupon');
            $data = $cupon->get_by(['code' => $code]);
            if(isset($data['value'])){
                $now = date('Y-m-d');
                if($now <= $data['date_expired']){
                    $value = (int) $data['value'];
                }
                if($belanja < $data['minimal']){
                    $value = 0;
                }
            }
        }
        return $this->___ajax_message(['code' => $code, 'value' => $value]);
    }

    function ajax_confirm_checkout(){
        // login check
        \app\Customer::auth(NULL, FALSE);
        if(!\app\Customer::is_logedin()){
            $this->___ajax_message([
                'success' => FALSE,
                'message' => 'Maaf, Anda harus login terlebih dahulu',
                'redirect' => base_url('customer/login')
            ]);
        }
        $post = $this->input->post();
        $id_customer = \app\Customer::$data['id_customer'];
        $id_payment = (int) (isset($post['id_payment']) ? $post['id_payment'] : 1);
        $id_delivery = (int) (isset($post['id_delivery']) ? $post['id_delivery'] : 0);
        // delivery
        $delivery = $this->load->object('delivery');
        $delivery_data = $delivery->get_by(['id_delivery' => $id_delivery]);
        $delivery_biaya = 0;
        if(isset($delivery_data['biaya'])){
            $delivery_biaya = (int) $delivery_data['biaya'];
        }
        // cupon
        $code_cupon = isset($post['code_cupon']) ? clean($post['code_cupon']) : '';
        $cupon = $this->load->object('cupon');
        $cupon_data = $cupon->get_by(['code' => $code_cupon]);
        $id_cupon = (int) $cupon_data['id_cupon'];
        $cupon_value = 0;
        if(isset($cupon_data['value'])){
            $now = date('Y-m-d');
            if($now <= $cupon_data['date_expired']){
                $cupon_value = (int) $cupon_data['value'];
            }
        }
        // description
        $description = isset($post['description']) ? clean($post['description']) : '';
        $shippingName = isset($post['shippingName']) ? clean($post['shippingName']) : \app\Customer::$data['fullname'];
        $shippingContact = isset($post['shippingContact']) ? clean($post['shippingContact']) : \app\Customer::get_data('phone');
        $shippingAddress = isset($post['shippingAddress']) ? clean($post['shippingAddress']) : \app\Customer::get_data('address');
        // load order object
        $order = $this->load->object('order');
        $order->id_customer = $id_customer;
        $order->id_payment = (int) $id_payment;
        $order->id_delivery = (int) $id_delivery;
        $order->id_cupon = (int) $id_cupon;
        $order->description = $description;
        $order->shipping_name = $shippingName;
        $order->shipping_contact = $shippingContact;
        $order->shipping_address = $shippingAddress;
        $order->date_added = date('Y-m-d');
        if($id_order = $order->save()){
            // load cart
            $cart = $this->load->object('cart');
            $cart_data = $cart->get_by(['id_customer' => $id_customer], [], TRUE);
            // load [order item] object
            $order_item = $this->load->object('order_item');
            $total = 0;
            $total_cart = count($cart_data);
            $saved_cart = 0;
            foreach($cart_data as $key => $value){
                $order_item->id_order = (int) $id_order;
                $order_item->id_product = (int) $value['id_product'];
                $order_item->quantity = (int) $value['quantity'];
                $order_item->subtotal = (int) $value['subtotal'];
                $order_item->total = (int) $value['total'];
                if($id_order_item = $order_item->save()){
                    $total += $order_item->total;
                    $cart->id = (int) $value['id'];
                    $cart->delete();
                    $saved_cart++;
                }                
            }
            if($total_cart == $saved_cart){
                // load order status object
                $os = $this->load->object('order_status');
                $os->id_user = 0; // sistem
                $os->id_order = $id_order;
                $os->status = 0;
                $os->description = 'Menunggu pembayaran..';
                $os->date_added = date('Y-m-d H:i:s');
                $os->save();
                // load invoice object
                $invoice = $this->load->object('invoice');
                $invoice->id_customer = $id_customer;
                $invoice->id_order = $id_order;
                // calculate total
                if($total < $cupon_data['minimal']){
                    $cupon_value = 0;
                }
                $totalInvoice = ($total + $delivery_biaya) - $cupon_value;
                $invoice->total = $totalInvoice;
                $invoice->date_added = date('Y-m-d');
                $invoice->date_expired = date('Y-m-d', mktime(0,0,0,date('m'), date('d') + 5, date('Y')));
                if($id_invoice = $invoice->save()){
                    \sasak\Log::add('Invoice added with id ['. $id_invoice .'] ..', TRUE, TRUE);
                    return $this->___ajax_message([
                        'success' => TRUE,
                        'message' => 'Proses selesai..',
                        'next' => base_url('customer/invoice')
                    ]);
                }else{
                    \sasak\Log::add('Invoice not saved.. :( ', TRUE, TRUE);
                    return $this->___ajax_message([
                        'success' => FALSE,
                        'message' => 'Maaf, proses belum selesai sepenuhnya. Sepertinya terjadi kesalahan pada sistem. Silahkan mencoba kembali atau hubungi pihak admin..'
                    ]);
                }
            }else{
                return $this->___ajax_message([
                    'success' => FALSE,
                    'message' => 'Maaf, proses belum selesai sepenuhnya. Sepertinya terjadi kesalahan pada sistem. Silahkan mencoba kembali atau hubungi pihak admin..'
                ]);
            }
        }
    }

    function ajax_checkout(){
        error_reporting(E_ALL);
        \app\Customer::auth(NULL, FALSE);
        if(\app\Customer::is_logedin()){
            $post = $this->input->post();
            if(!isset($post['data'])){
                $post['data'] = [];
                return $this->___ajax_message([
                    'next' => base_url('customer/checkout')
                ]);
            }
            $data = @json_decode($post['data']);
            $ref = isset($post['ref']) ? $post['ref'] : '';
            // load cart object
            $id_customer = \app\Customer::$data['id_customer'];
            $cart = $this->load->object('cart');
            if($ref == 'checkout' && empty($data)){
                $customer_cart = $cart->get_by(['id_customer' => $id_customer]);
                // load product object
                if(!empty($customer_cart)){
                    $product = $this->load->object('product');
                    $product_image = $this->load->object('product_image');
                    if(count($customer_cart) == 1){
                        $customer_cart[] = $customer_cart;
                    }
                    $obdata = [];
                    foreach($customer_cart as $key => $value){
                        $ob = new \stdClass();
                        $ob->id = (int) $value['id'];
                        $ob->id_customer = (int) $id_customer;
                        $ob->id_product = (int) $value['id_product'];
                        $ob->qty = (int) $value['quantity'];
                        $ob->subtotal = (int) $value['subtotal'];
                        $ob->total = (int) $value['total'];
                        $product_data = $product->get_by(['id_product' => $value['id_product']]);
                        $ob->harga = (int) $product_data['harga_jual'];
                        $ob->product_title = clean($product_data['product_title']);
                        $ob->url = base_url('produk/'. $product_data['product_name']) .'.html';
                        $image = $product_image->get_by(['id_product' => $ob->id_product], [
                            'limit' => [1]
                        ]);
                        \sasak\Log::add(print_r($image, 1));
                        if(isset($image['id'])){
                            $ob->image = str_replace(['.jpg', '.png', '.gif'], ['_60x60.jpg', '_60x60.png', '_60x60.gif'], $image['url']);
                        }else{
                            $ob->image = asset_url('img/transparent.gif');
                        }
                        $obdata[] = $ob;
                        unset($ob);
                    }
                }
                return $this->___ajax_message([
                    'success' => TRUE,
                    'data' => (isset($obdata) ? $obdata : [])
                ]);
            }
            $saved = [];
            foreach($data as $key => $ob){

                if(!isset($ob->id_product)) continue;

                $where = [
                    'id_product' => $ob->id_product,
                    'id_customer' => $id_customer
                ];
                if(isset($ob->id)){
                    $where['id'] = $ob->id;
                }                
                $get = $cart->get_by($where);

                $update = FALSE;

                if(isset($get['id'])){
                    if($get['quantity'] !== $ob->qty){
                        $update = TRUE; 
                        \sasak\Log::add($get['quantity'] .' <> '. $ob->qty .'; do update..', TRUE, TRUE);
                    }else{
                        \sasak\Log::add($get['quantity'] .' == '. $ob->qty .'; do not updated..', TRUE, TRUE);
                    }
                }else{
                    $update = FALSE;
                    \sasak\Log::add('cart data not found in database. it will inserted..', TRUE, TRUE);
                }

                if($update){
                    $cart->id = $get['id'];
                    $cart->quantity = $ob->qty;
                    $cart->total = $ob->total;
                    $updated = $cart->update();
                    if($updated){
                        $ob->id = $get['id'];
                        $saved[] = $ob;
                        \sasak\Log::add('Data berhasil diubah..', TRUE, TRUE);
                        continue;
                    }else{
                        \sasak\Log::add('Data gagal diubah..', TRUE, TRUE);
                    }
                }else{
                    if(isset($get['id'])) {
                        // do not insert again. it was saved before..
                        \sasak\Log::add('do not insert again. it was saved before..', TRUE, TRUE);
                        $saved[] = $ob;
                        continue;
                    }
                    \sasak\Log::add('inserting new product['. $ob->id_product .'] into cart..', TRUE, TRUE);
                    $cart->id_product = (int) $ob->id_product;
                    $cart->id_customer = (int) $id_customer;
                    $cart->quantity = (int) $ob->qty;
                    $cart->subtotal = (int) $ob->subtotal;
                    $cart->total = (int) $ob->total;
                    $save = $cart->save();
                    if($save){
                        \sasak\Log::add('new cart data saved with id ['. $save .'] :)', TRUE, TRUE);
                        $ob->id = $save;
                        $saved[] = $ob;
                    }else{
                        \sasak\Log::add('data not saved.. :(', TRUE, TRUE);
                    }
                }
            }
            $success = FALSE;
            if(count($saved)){
                $success = TRUE;
            }
            $message = [
                'success' => $success,
                'data' => $saved
            ];
            if($ref != 'checkout'){
                $message['next'] = base_url('customer/checkout');
            }
            return $this->___ajax_message($message);
        }else{
            $next = urlencode(base_url('customer/checkout'));
            return $this->___ajax_message([
                'next' => base_url('customer/login?next='. $next)
            ]);
        }
    }

    function ajax_confirm_payment(){
        error_reporting(E_ALL);
        \app\Customer::auth(NULL, FALSE);

        $post = $this->input->post();
        if(!\app\Customer::is_logedin()){
            return $this->___ajax_message([
                'success' => FALSE,
                'message' => 'Pelanggan tidak dikenal. Silahkan login dulu..',
                'redirect' => base_url('customer/login')
            ]);
        }
        if(!isset($post['invoice_id']) || empty($post['invoice_id'])){
            return $this->___ajax_message([
                'success' => FALSE,
                'message' => 'Data invoice tidak valid'
            ]);
        }
        if(!isset($post['payment_method']) || empty($post['payment_method'])){
            return $this->___ajax_message([
                'success' => FALSE,
                'message' => 'Data metode pembayaran tidak valid..'
            ]);
        }
        if(!isset($post['bank_account']) || empty($post['bank_account'])){
            return $this->___ajax_message([
                'success' => FALSE,
                'message' => 'Data bank tidak valid..'
            ]);
        }
        if(!isset($post['nominal']) || empty($post['nominal'])){
            return $this->___ajax_message([
                'success' => FALSE,
                'message' => 'Data jumlah pembayaran tidak tersedia..'
            ]);
        }

        // preparing data
        $id_invoice = (int) $post['invoice_id'];
        $id_customer = (int) \app\Customer::$data['id_customer'];
        $payment_method = (int) $post['payment_method'];
        $bank_account = (int) $post['bank_account'];
        $nominal = (int) $post['nominal'];
        $description = isset($post['description']) ? clean($post['description']) : '';

        // load uploader library
        $this->load->library('uploader');
        // preparing upload
        $dir = 'images/bukti_pembayaran/'. date('Y') .'/'. date('m') .'/';
        $this->uploader->initialize([
            'is_image'      => TRUE,
            'upload_path'   => ASSETS_PATH . $dir,
            'new_name'      => 'payment_'. $id_invoice .'_'. $id_customer .'_'. time(),
            'multiple'      => TRUE,
            'field_name'    => 'file'
        ]);

        $upload = $this->uploader->upload();
        $images = [];
        if($upload){
            $upload_data = $this->uploader->get_data();
            foreach($upload_data as $key => $value){
                $upload_data[$key]['url'] = asset_url($dir . $value['new_name']);
                $images[$key]['url'] = asset_url($dir . $value['new_name']);
                $images[$key]['path'] = $value['full_path'];                
            }
            \sasak\Log::add('Gambar berhasil diunggah..');
        }else{
            \sasak\Log::add('Gambar gagal diunggah..');
            \sasak\Log::add('Uploader : '. $this->uploader->get_message());
        }
        if(count($images)){
            $imgs = serialize($images);
        }else{
            $imgs = '{a:0}';
        }

        // load invoice_payment object
        $ip = $this->load->object('invoice_payment');
        $ip->id_invoice = $id_invoice;
        $ip->id_customer = $id_customer;
        $ip->id_payment = $payment_method;
        $ip->id_bank = $bank_account;
        $ip->value = $nominal;
        $ip->description = $description;
        $ip->date_added = date('Y-m-d');
        $ip->images = $imgs;
        if($id = $ip->save()){
            // load order status 
            return $this->___ajax_message([
                'success' => TRUE,
                'message' => 'Terima kasih, data berhasil disimpan. Silahkan tunggu konfirmasi dari admin..'
            ]);
        }else{
            return $this->___ajax_message([
                'success' => FALSE,
                'message' => 'Data gagal disimpan. Sepertinya terjadi kesalahan pada sistem kami. Silahkan mencoba kembali atau hubungi pihak admin. Terima kasih'
            ]);
        }
    }

    function confirm_payment(){
        \app\Customer::auth();

        $id_invoice = $this->input->get('invoice_id');

        $this->title->set('Konfirmasi Pembayaran');

        $bodyClass = $this->load->library('body_class', ['return' => TRUE]);
        $bodyClass->add('invoice');
        $this->data['bodyClass'] = $bodyClass;

        $asset = $this->load->library('Assets', ['return' => TRUE]);
        $asset->setPath(BASE_PATH)->setBaseUrl(asset_url());
        $asset
        ->addCss(['url' => 'css/material-colors.css'])
        ->addJs(['url' => 'js/jQuery-2.1.4.min.js', 'id' => 'jQueryJs'])
        ->addJs(['url' => 'bootstrap/js/bootstrap.min.js', 'id' => 'BootstrapJs'])
        ->addJs(['url' => 'plugins/accounting/accounting.min.js'])
        ->addJs(['url' => 'plugins/jquery-form/jquery.form.min.js'])
        ->addJs(['url' => 'templates/homemart/js/jquery.helpers.js'])
        ->addJs(['url' => 'templates/homemart/js/sCart.js'])
        ->addJs(['url' => 'templates/homemart/js/confirm_payment.js']);
        $this->data['Assets'] = $asset;

        $this->data['id_invoice'] = (int) $id_invoice;

        $id_customer = \app\Customer::$data['id_customer'];
        // invoice
        $invoice = $this->load->object('invoice');
        $all_invoices = $invoice->get_by(['id_customer' => $id_customer, 'status' => 0], [], TRUE);
        $this->data['invoices'] = $all_invoices;
        // payment method
        $payment = $this->load->object('payment');
        $payment_method = $payment->get_all();
        $this->data['payment_method'] = $payment_method;
        // bank account
        $bank = $this->load->object('bank_account');
        $bank_account = $bank->get_all();
        $this->data['bank_account'] = $bank_account;
        // load view
        return $this->load->view('customer/confirm_payment');
    }

    function invoice(){
        \app\Customer::auth();

        $id_invoice = (int) $this->input->get('invoice_id');
        if($id_invoice){
            return $this->__view_invoice($id_invoice);
        }

        $this->title->set('Data Tagihan');
        $bodyClass = $this->load->library('body_class', ['return' => TRUE]);
        $bodyClass->add('invoice');
        $this->data['bodyClass'] = $bodyClass;

        $asset = $this->load->library('Assets', ['return' => TRUE]);
        $asset->setPath(BASE_PATH)->setBaseUrl(asset_url());
        $asset
        ->addCss(['url' => 'css/material-colors.css'])
        ->addJs(['url' => 'js/jQuery-2.1.4.min.js', 'id' => 'jQueryJs'])
        ->addJs(['url' => 'bootstrap/js/bootstrap.min.js', 'id' => 'BootstrapJs'])
        ->addJs(['url' => 'plugins/accounting/accounting.min.js'])
        ->addJs(['url' => 'templates/homemart/js/jquery.helpers.js'])
        ->addJs(['url' => 'templates/homemart/js/sCart.js']);
        $this->data['Assets'] = $asset;

        $id_customer = \app\Customer::$data['id_customer'];
        $invoice = $this->load->object('invoice');
        $invoice_data = $invoice->get_by(['id_customer' => $id_customer], [], TRUE);
        $this->data['invoice_data'] = $invoice_data;
        return $this->load->view('customer/invoices');
    }

    function __view_invoice($id_invoice = 0){
        
        $bodyClass = $this->load->library('body_class', ['return' => TRUE]);
        $bodyClass->add('invoice');
        $this->data['bodyClass'] = $bodyClass;

        $this->title->set('Tagihan #'. $id_invoice);

        $asset = $this->load->library('Assets', ['return' => TRUE]);
        $asset->setPath(BASE_PATH)->setBaseUrl(asset_url());
        $asset
        ->addCss(['url' => 'css/material-colors.css'])
        ->addJs(['url' => 'js/jQuery-2.1.4.min.js', 'id' => 'jQueryJs'])
        ->addJs(['url' => 'bootstrap/js/bootstrap.min.js', 'id' => 'BootstrapJs'])
        ->addJs(['url' => 'plugins/accounting/accounting.min.js'])
        ->addJs(['url' => 'templates/homemart/js/jquery.helpers.js'])
        ->addJs(['url' => 'templates/homemart/js/sCart.js']);
        //->addJs(['url' => 'templates/homemart/js/checkout.js']);

        $this->data['Assets'] = $asset;

        if($this->input->get('next')){
            $this->data['next'] = $this->input->get('next');
        }
        // invoice
        $id_customer = (int) \app\Customer::$data['id_customer'];
        $invoice = $this->load->object('invoice');
        $invoice_data = $invoice->get_by(['id_invoice' => $id_invoice, 'id_customer' => $id_customer, 'status' => 0]);
        // order
        if(isset($invoice_data['id_order'])){
            $this->data['invoice_data'] = $invoice_data;

            $id_order = (int) $invoice_data['id_order'];
            $order = $this->load->object('order');
            $order_data = $order->get_by(['id_order' => $id_order]);
            $this->data['order_data'] = $order_data;
            // payment
            $payment = $this->load->object('payment');
            $peyment_data = $payment->get_by(['id_payment' => $order_data['id_payment']]);
            $this->data['payment_method'] = $peyment_data['payment_name'];
            // delivery
            $delivery = $this->load->object('delivery');
            $delivery_data = $delivery->get_by(['id_delivery' => $order_data['id_delivery']]);
            $biaya_kirim = (int) $delivery_data['biaya'];
            $this->data['biaya_kirim'] = $biaya_kirim;
            // order item
            $order_item = $this->load->object('order_item');
            $order_item_data = $order_item->get_order_item_with_product($id_order);
            $this->data['order_item_data'] = $order_item_data;
        }
        // option 
        $option = $this->load->object('options');
        $this->data['shop_name'] = $option->get('shop_name');
        $this->data['option'] = $option;

        return $this->load->view('customer/invoice2');
    }

    function checkout(){
        \app\Customer::auth();

        $this->title->set('Checkout');
        $bodyClass = $this->load->library('body_class', ['return' => TRUE]);
        $bodyClass->add('checkout');
        $this->data['bodyClass'] = $bodyClass;

        $asset = $this->load->library('Assets', ['return' => TRUE]);
        $asset->setPath(BASE_PATH)->setBaseUrl(asset_url());
        $asset
        ->addCss(['url' => 'css/material-colors.css'])
        ->addJs(['url' => 'js/jQuery-2.1.4.min.js', 'id' => 'jQueryJs'])
        ->addJs(['url' => 'bootstrap/js/bootstrap.min.js', 'id' => 'BootstrapJs'])
        ->addJs(['url' => 'plugins/lazy-loading/jquery.lazy.min.js'])
        ->addJs(['url' => 'plugins/accounting/accounting.min.js'])
        ->addJs(['url' => 'plugins/ui-block/jquery.blockUI.js'])
        ->addJs(['url' => 'templates/homemart/js/jquery.helpers.js'])
        ->addJs(['url' => 'templates/homemart/js/sCart.js'])
        ->addJs(['url' => 'templates/homemart/js/front.js'])
        ->addJs(['url' => 'templates/homemart/js/checkout.js']);

        $this->data['Assets'] = $asset;

        if($this->input->get('next')){
            $this->data['next'] = $this->input->get('next');
        }
        // load payment object
        $payment = $this->load->object('payment');
        $payment_data = $payment->get_all();
        $this->data['payment'] = $payment_data;
        // load delivery object model
        $delivery = $this->load->object('delivery');
        $delivery_data = $delivery->get_all();
        $this->data['delivery'] = $delivery_data;

        return $this->load->view('customer/checkout');
    }

    function login(){
        $this->title->set('Customer Login');
        $asset = $this->load->library('Assets', ['return' => TRUE]);
        $asset->setPath(BASE_PATH)->setBaseUrl(asset_url());
        $asset
        ->addCss(['url' => 'css/material-colors.css'])
        ->addJs(['url' => 'js/jQuery-2.1.4.min.js', 'id' => 'jQueryJs'])
        ->addJs(['url' => 'bootstrap/js/bootstrap.min.js', 'id' => 'BootstrapJs'])
        ->addJs(['url' => 'plugins/lazy-loading/jquery.lazy.min.js'])
        ->addJs(['url' => 'plugins/ui-block/jquery.blockUI.js'])
        ->addJs(['url' => 'templates/homemart/js/jquery.helpers.js'])
        ->addJs(['url' => 'templates/homemart/js/sCart.js'])
        ->addJs(['url' => 'templates/homemart/js/front.js'])
        ->addJs(['url' => 'js/login.js']);

        $this->data['Assets'] = $asset;

        if($this->input->get('next')){
            $this->data['next'] = $this->input->get('next');
        }
        return $this->load->view('customer/login');
    }

    function register($method = ''){
        $this->title->set('Register');

        $asset = $this->load->library('Assets', ['return' => TRUE]);
        $asset->setPath(BASE_PATH)->setBaseUrl(asset_url());
        $asset
        ->addCss(['url' => 'css/material-colors.css'])
        ->addJs(['url' => 'js/jQuery-2.1.4.min.js', 'id' => 'jQueryJs'])
        ->addJs(['url' => 'bootstrap/js/bootstrap.min.js', 'id' => 'BootstrapJs'])
        ->addJs(['url' => 'plugins/lazy-loading/jquery.lazy.min.js'])
        ->addJs(['url' => 'plugins/ui-block/jquery.blockUI.js'])
        ->addJs(['url' => 'templates/homemart/js/jquery.helpers.js'])
        ->addJs(['url' => 'templates/homemart/js/sCart.js'])
        ->addJs(['url' => 'templates/homemart/js/front.js'])
        ->addJs(['url' => 'js/register.js']);

        $this->data['Assets'] = $asset;

        if($this->input->get('next')){
            $this->data['next'] = $this->input->get('next');
        }

        return $this->load->view('customer/register');
    }

    function logout(){
        \app\Customer::auth(NULL, FALSE);

        if(\app\Customer::is_logedin()){
            $this->session->truncate();
            redirect(base_url('customer/login'));
        }else{
            redirect(base_url('customer/index/?logedout=false'));
        }        
    }
}