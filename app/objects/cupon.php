<?php
/**
 * Cupon object model
 *
 * @author          Muh Sukrillah
 * @version         1.0
 */
namespace app\object_model;

class Cupon extends \sasak\Bokah {
    /**
     * table name
     * @var     string  _table_name
     * @access  private
     */
    protected $_table_name = 'cupon';
    /**
     * primary key
     * @var     string  _primary_key
     * @access  private
     */
    protected $_primary_key = 'id_cupon';
    /**
     * model name
     * @var     string  _model_name
     * @access  private
     */
    protected $_model_name = '\app\model\Cupon_Model';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
    /**
     * filter update
     */
    function __filter_update($data){
        if(isset($data['date_updated'])){
            unset($data['date_updated']);
        }
        return $data;
    }
    /**
     * filter insert
     */
    function __filter_insert($data){
        return $this->__filter_update($data);
    }
}