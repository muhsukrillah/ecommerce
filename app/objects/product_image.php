<?php
/**
 * product image object model
 *
 * @author              : Muh. Sukrillah
 * @url                 : http://www.sukrillah.xyz
 * @version             : 1.0
 */
namespace app\object_model;

class Product_Image extends \sasak\Bokah {
    /**
     * table name
     * @var     string  _table_name
     * @access  private
     */
    protected $_table_name = 'product_image';
    /**
     * primary key
     * @var     string  _primary_key
     * @access  private
     */
    protected $_primary_key = 'id';
    /**
     * model name
     * @var     string _model_name
     * @access  private
     */
    protected $_model_name = 'app\model\Product_Image_Model';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
}