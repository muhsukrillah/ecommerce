<?php
/**
 * Product category object model
 * 
 * @author                      : Muh. Surillah
 * @url                         : http://www.sukrillah.xyz
 * @version                     : 1.0
 **/
namespace app\object_model;

class Product_Category extends \sasak\Bokah {
    /**
     * model name
     *
     * @var     string  _table_name
     * @access  private
     */
    protected $_table_name = 'product_category';
    /**
     * model name
     * 
     * @var     string _model_name
     * @access  private
     */
    protected $_model_name = 'app\model\Product_Category_Model';
    /**
     * primary key
     *
     * @var     string _primary_key
     * @access  private
     */
    protected $_primary_key = 'id';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
}