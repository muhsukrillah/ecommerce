<?php
/**
 * Customer object model
 *
 * @author              Muh. Sukrillah
 */
namespace app\object_model;

class Customer extends \sasak\Bokah {
    /**
     * table name
     * @var     string  _table_name
     * @access  private
     */
    protected $_table_name = 'customer';
    /**
     * primary key
     * @var     string  _primary_key
     * @access  private
     */
    protected $_primary_key = 'id_customer';
    /**
     * mdoel name
     * @var     string  _model_name
     * @access  private
     */
    protected $_model_name = '\app\model\Customer_Model';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
}