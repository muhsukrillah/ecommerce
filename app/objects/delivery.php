<?php
/**
 * Delivery object model
 *
 * @author              : Muh. Sukrillah
 * @version             : 1.0
 */
namespace app\object_model;

class Delivery extends \sasak\Bokah {
    /**
     * table name
     * @var     string  _table_name
     * @access  private
     */
    protected $_table_name = 'delivery';
    /**
     * primary key
     * @var     string  _primary_key
     * @access  private
     */
    protected $_primary_key = 'id_delivery';
    /**
     * model name
     * @var     string  _model_name
     * @access  private
     */
    protected $_model_name = '\app\model\Delivery_Model';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
}