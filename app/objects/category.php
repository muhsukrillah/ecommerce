<?php
/**
 * category object model
 *
 * Dibuat Oleh              : Muh. Sukrillah
 * Url                      : http://www.sukrillah.xyz
 * Versi                    : 1.0
 */
namespace app\object_model;

class Category extends \sasak\Bokah {
    /**
     * table name
     * @var     string $_table_name
     * @access  private
     */
    protected $_table_name = 'category';
    /**
     * model name
     * @var     string model_name
     * @access  private
     */
    protected $_model_name = 'app\model\Category_Model';
    /**
     * primary key
     * @var     string  primary_key
     * @access  private
     */
    protected $_primary_key = 'id_category';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
        $this->date_added = date('Y-m-d H:i:s');
        $this->date_updated = $this->date_added;
        \sasak\Log::add('Class ['. __CLASS__ .'] Initialized..');
    }

    function get_parent($cdata = [], $first_run = TRUE, &$cat = []){
        if($first_run){
            $cats[] = $cdata;
            $this->get_parent($cdata, FALSE, $cats);
            if(count($cats) > 1){
                $cats = array_reverse($cats);
            }
            return $cats;
        }else{
            if(!isset($cdata['parent_id']) || !$cdata['parent_id']){
                return FALSE;
            }
            $c = $this->get_by(['id_category' => $cdata['parent_id']]);
            if($c['id_category']){
                array_push($cat, $c);
                if($c['parent_id']){
                    $this->get_parent($c, FALSE, $cat);
                }
            }
        }
    }

    function fetch_parent($cdata = [], &$data = []){
        if(!$cdata['parent_id']){
            return array($cdata);
        }
        $cat = $this->get_by(['id_category' => $cdata['parent_id']]);
        if($cat['parent_id']){
            $this->fetch_parent($cat['parent_id'], $data);
        }else{
            array_push($data, $cat);
        }
    }
}
