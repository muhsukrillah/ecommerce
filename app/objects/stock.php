<?php
/**
 * stock object model
 *
 * @author              : Muh. Sukrillah
 * @url                 : http://www.sukrillah.xyz
 * @version             : 1.0
 */
namespace app\object_model;

class Stock extends \sasak\Bokah {
    /**
     * model name
     * @var         string  _model_name
     * @access      private
     */
    protected $_model_name = 'app\model\Stock_Model';
    /**
     * table name
     * @var         string  _table_name
     * @access      private
     */
    protected $_table_name = 'stock';
    /**
     * primary key
     * @var         string  _primary_key
     * @access      private
     */
    protected $_primary_key = 'id_stock';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
        /**
         * public var
         * @var         string date_added
         * @access      public
         */
        $this->date_added = date('Y-m-d H:i:s');
        $this->date_updated = $this->date_added;
    }
}