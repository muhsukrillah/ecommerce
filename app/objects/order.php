<?php
/**
 * Order object model
 *
 * @author              Muh Sukrillah
 * @version             1.0
 */
namespace app\object_model;

class Order extends \sasak\Bokah {
    /**
     * table name
     * @var     string _table_name
     * @access  private
     */
    protected $_table_name = 'order';
    /**
     * primary key
     * @var     string _primary_key
     * @access  private
     */
    protected $_primary_key = 'id_order';
    /**
     * model name
     * @var     string _model_name
     * @access  private
     */
    protected $_model_name = '\app\model\Order_Model';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }

    function __filter_insert($data){
        if(isset($data['date_updated'])){
            unset($data['date_updated']);
        }
        return $data;
    }

    function __filter_update($data){
        return $this->__filter_insert($data);
    }
}