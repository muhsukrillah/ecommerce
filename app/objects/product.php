<?php
/**
 * Product Object Class
 *
 * Dibuat Oleh              : Muh. Sukrillah
 * Url                      : http://www.sukrillah.xyz
 * Versi                    : 1.0
 */
namespace app\object_model;

class Product extends \sasak\Bokah {
    /**
     * @var     string table_name
     * @access  private
     */
    protected $_table_name = 'product';
    /**
     * @var     string model_name
     * @access  private
     */
    protected $_model_name = 'app\model\Product_Model';
    /**
     * primary key
     * @var     int   primary_key
     * @access  private
     */
    protected $_primary_key = 'id_product';

    function __construct(){
        parent::__construct();
        /**
         * date added
         */
        $this->date_added = date('Y-m-d H:i:s');
        $this->date_updated = $this->date_added;
    }
    /**
     * fungsi ini digunakan untuk membuat dan mengambil model produk baru secara otomatis
     *
     * @access  public
     * @param   optional    string  prefix
     * @return  string  model
     */
    function generate_model_number($prefix = NULL){
        return $this->__model->generate_model_number($prefix);
    }
    /**
     * search
     * @access  public
     * @param   array   needle
     * @param   array   options
     * @return  array   results
     */
    function search(){
        $args = func_get_args();
        $obj =& $this->__model;
        return @call_user_method_array('search_by', $obj, $args);
    }
    /**
     * get_all
     *
    function get_all(){
        $args = func_get_args();
        if(!empty($args)){
            /*foreach($args as $key => $value){
                if(isset($value['order_by']) && !empty($value['order_by'])) {
                    if(in_array('stock', array_keys($value['order_by']))){
                        $obj =& $this->__model;
                        return @call_user_method_array('fetch_all', $obj, $args);
                    }
                }
            }*
            $obj =& $this->__model;
            $data = @call_user_method_array('get_all', $obj, $args);
        }else{
            $data = $this->__model->get_all();
        }
        return $data;
    }*/
}