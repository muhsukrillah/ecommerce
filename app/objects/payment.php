<?php
/**
 * Payment object model 
 * 
 * @author              : Muh. Sukrillah
 * @version             : 1.0
 */
namespace app\object_model;

class Payment extends \sasak\Bokah {
    /**
     * table name
     * @var     string  _table_name
     * @access  private
     */
    protected $_table_name = 'payment_method';
    /**
     * primary key
     * @var     string  _primary_key
     * @access  private
     */
    protected $_primary_key = 'id_payment';
    /**
     * model name
     * @var     string  _model_name
     * @access  private
     */
    protected $_model_name = '\app\model\Payment_Model';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }
}