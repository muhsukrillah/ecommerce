<?php
/**
 * Brand object model
 * 
 * @author          : Muh. Sukrillah
 * @url             : http://www.sukrillah.xyz
 * @version         : 1.0
 */
namespace app\object_model;

class Brand extends \sasak\Bokah {
    /**
     * table name
     * @var         string _table_name
     * @access      private
     * @required    required
     */
    protected $_table_name = 'brand';
    /**
     * primar key
     * @var         string _primary_key
     * @access      private
     * @required    requried
     */
    protected $_primary_key = 'id_brand';
    /**
     * model name
     * @var         string _modle_name
     * @access      private
     * @required    required
     */
    protected $_model_name = 'app\model\Brand_Model';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();

        $this->date_added = date('Y-m-d H:i:s');
        $this->date_updated = $this->date_added;
    }
}