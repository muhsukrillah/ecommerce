<?php
/**
 * Price object model
 *
 * @author                  : Muh. Sukrillah
 * @url                     : http://www.sukrillah.xyz
 * @version                 : 1.0
 */
namespace app\object_model;

class Price extends \sasak\Bokah {
    /**
     * table name
     * 
     * @var     string _table_name
     * @access  private
     */
    protected $_table_name = 'price';
    /**
     * primary key
     * 
     * @var     string  _primary_key
     * @access  private
     */
    protected $_primary_key = 'id_price';
    /**
     * model name
     *
     * @var     string  _model_name
     * @access  private
     */
    protected $_model_name = 'app\model\Price_Model';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
         /**
         * public var date_added
         * @var     string  date_added
         * @access  public
         */
        $this->date_added = date('Y-m-d H:i:s');
        $this->date_updated = $this->date_added;
    }
}