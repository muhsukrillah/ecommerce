<?php
/**
 * bank account object model
 *
 * @author          Muh Sukrillah
 * @version         1.0
 */
namespace app\object_model;

class Bank_Account extends \sasak\Bokah {
    /**
     * table name
     * @var     string _table_name
     * @access  private
     */
    protected $_table_name = 'bank_account';
    /**
     * primary kay
     * @var     string _primary_key
     * @access  private
     */
    protected $_primary_key = 'id';
    /**
     * model name
     * @var     string  _model_name
     * @access  private
     */
    protected $_model_name = '\app\model\Bank_Account_Model';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }

    function __filter_insert($data){
        if(isset($data['date_added'])){
            unset($data['date_added']);
        }
        if(isset($data['date_updated'])){
            unset($data['date_updated']);
        }
        return $data;
    }

    function __filter_update($data){
        return $this->__filter_insert($data);
    }
}