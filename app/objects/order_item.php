<?php
/**
 * Order item object model
 * 
 * @author          Muh Sukrillah
 * @version         1.0
 */
namespace app\object_model;

class Order_Item extends \sasak\Bokah {
    /**
     * table name
     * @var     string _table_name
     * @access  private
     */
    protected $_table_name = 'order_item';
    /**
     * primary key
     * @var     string  _primary_key
     * @access  private
     */
    protected $_primary_key = 'id';
    /**
     * model name
     * @var     string _model_name
     * @access  private
     */
    protected $_model_name = '\app\model\Order_Item_Model';
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();
    }

    function get_order_item_with_product($id_order = 0){
        return $this->__model->get_order_item_with_product($id_order);
    }

    function __filter_insert($data){
        if(isset($data['date_added'])){
            unset($data['date_added']);
        }
        if(isset($data['date_updated'])){
            unset($data['date_updated']);
        }
        return $data;
    }
}