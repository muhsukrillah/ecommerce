<?php
/**
 * Options object model
 *
 * @author          Muh Sukrillah
 * @version         1.0
 */
namespace app\object_model;

class Options extends \sasak\Bokah {
    /**
     * table name
     * @var     string _table_name
     * @access  private
     */
    protected $_table_name = 'options';
    /**
     * primary key
     * @var     string  _primary_key
     * @access  private
     */
    protected $_primary_key = 'id_option';
    /**
     * model name
     * @var     string _model_name
     * @access  private
     */
    protected $_model_name = '\app\model\Option_Model';
    /**
     * all options data
     * @var     array   _data
     * @access  private
     */
    private $_data = array();
    /**
     * constructor
     */
    function __construct(){
        parent::__construct();

        $this->__get_all();
    }

    private function __get_all(){
        $data = $this->get_all();
        foreach($data as $key => $value){
            $on = $value['option_name'];
            $ov = $value['option_value'];
            $this->_data[$on] = $ov;
        }
    }

    function get($option_name = NULL){
        if(isset($this->_data[$option_name])){
            return $this->_data[$option_name];
        }
        return FALSE;
    }
}