(function(w, $){
    $.fn.button = function(t){
        var $this = $(this);
        if(t == 'reset'){
            if($this.data('original-text')){
                var text = $this.data('original-text');
                $this.text(text).removeAttr('data-original-text');
            }
        }else{
            $this.attr('data-original-text', $this.text());
            $this.text(t);
        }
        return this;
    };
    $.fn.disable = function () {
        this.each(function () {
            var $this = $(this);
            $this.attr('disabled', 'disabled');
        });
        return this;
    };
    $.fn.enable = function () {
        this.each(function () {
            var $this = $(this);
            $this.removeAttr('disabled');
        });
        return this;
    };
    // price handler
    w.price_handler = function (price) {
        var p = accounting.formatMoney(price, '', 0, '.', ',');
        return p;
    };
})(window, jQuery);