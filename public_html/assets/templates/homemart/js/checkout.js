var refresh_cart = function() {
    var scart_data = JSON.stringify(sCart.get());
    //console.log(scart_data);
    setTimeout(function(){
        $.ajax({
            url: base_url + 'customer/ajax/checkout',
            data: {'data': scart_data, 'ref':'checkout'},
            type:'post',
            dataType: 'json',
            beforeSend: function(){
                console.log('sending request to save local cart data..');
                $('.table-scart tbody').append('<td colspan="6" class="text-center"><img src="'+ asset_url +'img/loading.gif"></td>');
            },
            success: function(res){
                console.log(res);
                if(res.success){
                    for(var a in res.data){
                        sCart.set(res.data[a], true);
                    }

                    sCart.fetchTo('.table-scart');
                    return;
                }          
                if(res.next){
                    document.location = res.next;
                }      
            },
            complete: function(){
                var total = sCart.hitungTotal();
                if(!total){
                    $('#paymentButton').disable();
                }else{
                    $('.row-checkout, .cupon-container').removeClass('hide');
                    $('#paymentButton').enable();
                }
            },
            error: function(e){
                console.log(e.responseText);
            }
        });
    }, 500);
};
// call firstly
refresh_cart();
sCart.refresh_callback = refresh_cart;
$(function(){
    // total biaya
    $('#totalBiaya').text(price_handler(sCart.totalBelanja()));
    // delivery check
    $('#delivery').on('change', function(){
        var val = parseInt($(this).find('option:selected').data('value')), value = price_handler(val);
        if(!val){
            $('#totalBiaya').text(price_handler(sCart.totalBelanja()));
            $('#biayaKirim').text(0);
            return false;
        }
        sCart.biayaKirim = val;
        sCart.hitungTotal();
    });
    // cupon
    $('#checkCupon').on('click', function(e){
        var $this = $(this), cupon = $('#cupon'), cuponVal = cupon.val();
        if(!$.trim(cuponVal).length){
            cupon.parent().addClass('has-error');
            cupon.focus();
            return false;
        }
        $.ajax({
            url: base_url + 'customer/ajax/cupon_check',
            data: {'code': cuponVal, 'total_belanja': sCart.totalBelanja()},
            dataType: 'json',
            beforeSend: function(){
                cupon.parent().removeClass('has-error').attr('readonly', 'readonly');
                $this.disable();
            },
            success: function(res){
                sCart.potongan = res.value || 0;
                sCart.hitungTotal();
            },
            complete: function(){
                cupon.removeAttr('readonly');
                $this.enable();
            },
            error: function(e){
                console.log(e.responseText);
            }
        })
    });
    // payment
    $('#paymentButton').on('click', function(e){
        var id_payment = $('#payment').val(),
        id_delivery = $('#delivery').val(),
        code_cupon = $('#cupon').val(),
        shippingName = $('#shippingName').val(),
        shippingContact = $('#shippingContact').val(),
        shippingAddress = $('#shippingAddress').val(),
        description = $("#description").val();

        var data = {
            id_payment: id_payment,
            id_delivery: id_delivery,
            code_cupon: code_cupon,
            shippingName: shippingName,
            shippingContact: shippingContact,
            shippingAddress: shippingAddress,
            description: description
        };
        $.ajax({
            url: base_url + 'customer/ajax/confirm_checkout',
            data: data,
            dataType: 'json',
            type: 'post',
            beforeSend: function(){
                $('body').block({
                    message: '<img src="'+ asset_url +'img/loading.gif"> Tunggu..',
                    css:{
                        background:'#fff',
                        border:'none',
                        'z-index':'1031', 
                        'padding':'20px 0px',
                        'width':'15%',
                        'border-radius':'6px 6px',
                        'color':'#aaa',
                        'text-align':'center'
                    }, 
                    overlayCSS: {
                        background:'#aaa',
                        'z-index':'1030'
                    }
                }).addClass('lock');
            },
            success: function(res){
                if(res.success && res.next){
                    sCart.clear(true);
                    document.location = res.next;
                }else{
                    if(res.message){
                        alert(res.message);
                        if(res.redirect){
                            document.location = res.redirect;
                        }
                    }else{
                        alert('Maaf, sepertinya terjadi kesalahan pada sistem.. Silahkan mencoba kembali atau hubungi pihak admin..');
                    }
                }
            },
            complete: function(){
                $('body').unblock().removeClass('lock');
            },
            error: function(e){
                alert('Maaf, sepertinya terjadi kesalahan pada sistem. Silahkan coba kembali atau hubungi pihak admin');
                console.log(e.responseText);
            }
        })
    });
});