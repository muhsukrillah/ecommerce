/**
 * product image uploader
 */
 (function(w,$){
    $('#sendData').on('click', function(e){
        var $this = $(this), frm = $this.closest('form');

        frm.attr('onsubmit', 'return false;');
        frm.attr('action', base_url +'customer/ajax/confirm_payment');
        frm.ajaxSubmit({
            dataType : 'json',
            uploadProgress : function (e, position, total, percentComplete) {
                $('.progress').removeClass('invisible').find('.progress-bar').css({
                    'width' : percentComplete + '%'
                }).attr('aria-valuenow', percentComplete);
            },
            success : function(res) {
                if(res.success){
                    $('.panel-right-content .panel-body').html('<div class="alert alert-info">'+ (res.message || 'Terima kasih, data data berhasil dikirim..') + '</div>');
                }else{
                    alert(res.message || 'Maaf, data gagal dikirim. Sepertinya terjadi kesalahan pada sistem kami. Silahkan mencoba kembali atau hubungi pihak admin..');
                }
                console.log(res);
            },
            complete:function(){
                $('.progress').addClass('invisible').find('.progress-bar').css({'width': '0%'}).attr('aria-valuenow', 0);
            },
            error: function(e){
                console.log(e.responseText);
            }
        });
        frm.submit();
    });
    /**
     * image removed callback
     */
    w.removeUploadedImage = function(el, i){
        console.log(el);
        console.log(i);
        var $this = $(el), images = $('input#images').val(), ij = JSON.parse(images) || [];
        console.log(ij);
        if(ij[i]){
            console.log(ij[i]);
            var a = [], 
            del = {id:0}, 
            success = false, 
            id_product = parseInt($('#id_product').val()),
            delete_image = function(e){
                var is = JSON.stringify(e);
                $('#images').val(is);
                $this.closest('.img-wrapper').fadeOut(function(){
                    $(this).remove();
                });
            };
            for(var $i in ij){
                if(i == $i){
                    console.log($i +' will deleted..');
                    ij[$i].id = parseInt(ij[$i].id);
                    del = ij[$i];
                    continue;
                }
                a.push(ij[$i]);
            }
            if(del.id && id_product){
                if(confirm('Apakah Anda yakin ?')){
                    $.ajax({
                        url: admin_url + 'product/ajax/delete_image',
                        data: del,
                        dataType: 'json',
                        type: 'post',
                        success: function(res){
                            if(res.success){
                                delete_image(a);
                                alert(res.message);
                            }
                        }
                    });
                }
            }else{
                delete_image(a);
            }
        }else{
            console.log('ij[i] not found..');
        }
    }
 })(window, jQuery);