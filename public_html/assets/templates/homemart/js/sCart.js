var sCart = function($){
    var get_data = function(){
        var data = localStorage.getItem('sCart') || '[]';
        if(data === 'undefined'){
            data = '[]';
        }
        return JSON.parse(data);
    }, store = function(d){
        var nd = JSON.stringify(d);
        localStorage.setItem('sCart', nd);
    }, set_data = function(d, a){
        var data = get_data(), nd;
        if(typeof d == 'object'){
            if(is_exists(d)){
                if(typeof a == 'boolean' && a == true){
                    console.log('replacing..'+ a);
                    var cd = replace(d);
                }else{
                    console.log('appending .. '+ a);
                    var cd = append(d);
                }
                data = update(cd);
            }else{
                data.push(d);
            }
        }
        store(data);
    }, append = function(d){
        var cd = search(d, function(od, d){
            od.qty = parseInt(od.qty) + parseInt(d.qty);
            od.total = parseInt(od.total) + parseInt(d.total);
            return od;
        });
        return cd;
    }, replace = function(d){
        var cd = search(d, function(od, nd){
            od = nd;
            return od;
        });
        return cd;
    }, clear = function(){
        localStorage.removeItem('sCart');
    }, totalItem = function(){
        var data = get_data();
        return data.length;
    }, totalBelanja = function(){
        var total = 0, data = get_data();
        for(var i in data){
            total += data[i].total;
        }
        return total;
    }, update = function(d, cb){
        var data = get_data();
        data = search(d, function(od, d, cd, i){
            if(typeof cb == 'function'){
                cd[i] = cb(od, d);
            }else{
                cd[i] = d;
            }
            return cd;
        });
        return data;
    }, is_exists = function(d){
        return search(d);
    }, search = function(d, cb){
        var data = get_data();
        for(var i in data){
            if(data[i].id_product == d.id_product){
                if(cb){
                    return cb(data[i], d, data, i);
                }else{
                    return true;
                }
            }
        }
    }, delete_by_index = function(i, cb){
        var data = get_data(), nd = [];
        for(var a in data){
            if(a == i) continue;
            nd.push(data[a]);
        }
        var ndt = JSON.stringify(nd);
        localStorage.setItem('sCart', ndt);
        if(typeof cb == 'function'){
            return cb(i);
        }
    }, init = function(){
        var s = '<div class="scart">'+
        '<div class="modal" id="modal-scart">'+
        '<div class="modal-dialog modal-lg">'+
        '<div class="modal-content">'+
          '<div class="modal-header">'+
            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>'+
            '<h4 class="modal-title"><i class="fa fa-shopping-cart"></i> Barang Belanjaan Anda</h4>'+
          '</div>'+
          '<div class="modal-body">'+
            '<table class="table table-scart table-responseive">'+
            '<thead>'+
                '<tr>'+
                    '<th>No.</th>'+
                    '<th>Produk</th>'+
                    '<th>Harga</th>'+
                    '<th>Qty</th>'+
                    '<th>Total</th>'+
                    '<th>&nbsp;</th>'+
                '</tr>'+
            '</thead>'+
            '<tbody>'+
            '</tbody>'+
            '</table>'+
          '</div>'+
          '<div class="modal-footer">'+
            '<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>'+
            '<button type="button" class="btn btn-danger" id="btnClear" onclick="sCart.clear();"><i class="fa fa-ban"></i> Kosongkan</button>'+
            '<button type="button" class="btn btn-primary" id="btnCheckout" onclick="sCart.checkout();">Bayar <i class="fa fa-arrow-right"></i></button>'+
          '</div>'+
        '</div><!-- /.modal-content -->'+
        '</div><!-- /.modal-dialog -->'+
        '</div>'+
        '</div>';
        $('body').prepend(s);
    }, change_qty = function(i){

    };
    return {
        init: function(){
            init();
        },
        clear: function(c){
            if(!c && !confirm('Apakah Anda yakin?')){
                return false;
            }
            clear();
            this.refresh();
        },
        delete_by_index: function(i, cb){
            return delete_by_index(i, cb);
        },
        delete: function(i, el){
            if(!confirm('Apakah Anda yakin?')){
                return false;
            }
            var $this = $(el).closest('tr');
            this.delete_by_index(i, function(){
                $this.fadeOut(function(){
                    $this.remove();
                });
                sCart.refresh();
            });
        },
        set: function(data, cb){
            if(typeof cb == 'boolean'){
                set_data(data, cb);
            }else{
                set_data(data);
            }
            if(typeof cb == 'function'){
                cb(this.get());
            }
            clearTimeout(timer);
            var timer = setTimeout(function(){
                sCart.refresh();
            }, 500);
        },
        update: function(i, qty, rc){
            if(!qty) return false;
            var data = get_data();
            for(var a in data){
                if(a != i) continue;
                var nh = data[a].harga * qty;
                data[a].qty = qty;
                data[a].total = nh;
            }
            store(data);
            this.refresh(undefined, rc);
        },
        replace: function(d){
            replace(d);
        },
        get: function(){
            return get_data();
        },
        total: function(){
            return totalItem();
        },
        totalBelanja: function(){
            return totalBelanja();
        }, 
        fetchTo: function(el){
            var data = sCart.get(), a = 1, tr, 
            t = el || '.table-scart',
            table = $(el).find('tbody');
            table.empty();
            for(var i in data){
                tr += '<tr'+(data[i].id ? ' data-id="'+ data[i].id +'"' : '')+' data-index="'+ i +'" data-harga="'+ (data[i].harga) +'">'+
                '<td valign="middle">'+ (a++) +'.</td>'+
                '<td><a href="'+ (data[i].url || '#') +'" target="_blank"><img style="width:40px;height:40px;" src="'+ data[i].image +'"> '+ data[i].product_title + '</a></td>'+
                '<td><span class="curency">'+ price_handler(data[i].harga) +'</span></td>'+
                '<td class="col-sm-1"><input class="form-control input-sm" type="number" min="1" value="'+ data[i].qty +'" onchange="return sCart.update('+ i +', this.value, true);"></td>'+
                '<td><span class="curency">'+ price_handler(data[i].total) +'</span></td>'+
                '<td><a href="javascript:void(0);" onclick="sCart.delete('+ i +', this);" class="label label-sm label-danger"><i class="fa fa-trash-o"></i></a></td>'+
                '</tr>';
            }
            if(data.length){
                tr += '<td colspan="4">'+ (a - 1) +'</td><td><b class="curency">'+ price_handler(sCart.totalBelanja()) +'</b></td><td><b>Total</b></td>';
                $('#btnClear').enable();
                $('#btnCheckout').enable();
            }else{
                tr += '<td colspan="6" class="text-center">Tidak ada data..</td>';
                $('#btnClear').disable();
                $('#btnCheckout').disable();
            }
            table.append(tr);
            this.hitungTotal();
        },
        show: function(){
            var modal = $('#modal-scart');
            if(!modal.length){
                this.init();
                modal = $('#modal-scart');
            }
            modal.on('show.bs.modal', function(){
                sCart.fetchTo('.table-scart');
            });
            modal.modal('show');
        },
        refresh: function(el, run_callback) {
            var data = sCart.get(), a = 1, tr, 
            t = el || '.table-scart', 
            table = $(t).find('tbody');
            table.empty();
            for(var i in data){
                tr += '<tr'+(data[i].id ? ' data-id="'+ data[i].id +'"' : '')+' data-index="'+ i +'" data-harga="'+ (data[i].harga) +'">'+
                '<td valign="middle">'+ (a++) +'.</td>'+
                '<td><a href="'+ (data[i].url || '#') +'" target="_blank"><img style="width:40px;height:40px;" src="'+ data[i].image +'"> '+ data[i].product_title + '</a></td>'+
                '<td><span class="curency">'+ price_handler(data[i].harga) +'</span></td>'+
                '<td class="col-sm-1"><input class="form-control input-sm" type="number" min="1" value="'+ data[i].qty +'" onchange="return sCart.update('+ i +', this.value, true);"></td>'+
                '<td><span class="curency">'+ price_handler(data[i].total) +'</span></td>'+
                '<td><a href="javascript:void(0);" onclick="sCart.delete('+ i +', this);" class="label label-sm label-danger"><i class="fa fa-trash-o"></i></a></td>'+
                '</tr>';
            }
            if(data.length){
                tr += '<td colspan="4">'+ (a - 1) +'</td><td><b class="curency">'+ price_handler(sCart.totalBelanja()) +'</b></td><td><b>Total</b></td>';
                $('#btnClear').enable();
                $('#btnCheckout').enable();
            }else{
                tr += '<td colspan="6" class="text-center">Tidak ada data..</td>';
                $('#btnClear').disable();
                $("#btnCheckout").disable();
            }
            table.append(tr);

            $('#sCart_counter').text(data.length);

            if(sCart.refresh_callback && typeof sCart.refresh_callback == 'function' && run_callback){
                clearTimeout(timer);
                var timer = setTimeout(function(){
                    console.log('refresh callback..');
                    sCart.refresh_callback();
                }, 300);
            }
            this.hitungTotal();
        },
        checkout: function(el){
            var $btn = $(el), data = JSON.stringify(sCart.get());
            if($('body').hasClass('checkout')){
                $('#modal-scart').modal('hide');
                return false;
            }
            $.ajax({
                url: base_url + 'customer/ajax/checkout', 
                data:{data: data},
                dataType: 'json',
                type: 'post',
                beforeSend: function(){
                    $btn.disable().html('<i class="fa fa-spin fa-spinner"></i> Tunggu..');
                },
                success: function(res){
                    console.log(res);
                    if(res.next){
                        document.location = res.next;
                    }else{
                        alert(res.message || 'Maaf, sepertinya terjadi kesalhan sistem..');
                    }
                },
                complete: function(){
                    $btn.enable().html('Bayar <i class="fa fa-arrow-right"></i>');
                },
                error: function(e){
                    console.log(e.responseText);
                }
            });
        },
        hitungTotal: function(){
            var biayaKirim = this.biayaKirim || 0,
            potongan = this.potongan || 0,
            totalBelanja = this.totalBelanja(),
            total = 0;

            total = (biayaKirim + totalBelanja) - potongan;
            $('#totalBiaya').text(price_handler(total));
            $('#potongan').text(price_handler(potongan));
            $('#biayaKirim').text(price_handler(biayaKirim));
            return total;
        }
    };
}(jQuery);