(function(w, $){
    $('#sCart_counter').text(sCart.total());

    $('.lazy').lazy();

    $('.panel-product').on('change', '#qty', function(){
        if(!parseInt($(this).val())){
            $(this).val(1);
            return false;
        }
        var $this = $(this), val = $this.val(), harga = $("#harga").val(), price = val * harga;
        $('.price').text('Rp '+ price_handler(price));
    }).on('click', '#buyButton', function(){
        var $this = $(this), 
        $pdata = $("#pdata").val(), 
        $qty = $('#qty').val(), 
        $harga = $("#harga").val(), 
        $total = $qty * $harga;
        var cdata = JSON.parse($pdata);
        cdata.qty = parseInt($qty);
        cdata.subtotal = parseInt($harga);
        cdata.total = parseInt($total);

        sCart.set(cdata, function(d){
            $('#sCart_counter').text(d.length);
            $('#qty').val('1').trigger('change');
        });
    });
})(window, jQuery);