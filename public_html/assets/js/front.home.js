var base_url = '/';
angular.module('home', ['ngRoute'])
.controller('home', function($scope){
    console.log('Dashboard Controller..');
})
.controller('MainController', function($scope, $route, $routeParams, $location) {
     $scope.$route = $route;
     $scope.$location = $location;
     $scope.$routeParams = $routeParams;
 })

 .controller('BookController', function($scope, $routeParams) {
     $scope.name = "BookController";
     $scope.params = $routeParams;
     $scope.base_url = base_Url;
     $scope.template_url = '/assets/templates/homemart/';
 })

 .controller('ChapterController', function($scope, $routeParams) {
     $scope.name = "ChapterController";
     $scope.params = $routeParams;
 })

 .controller('HomeController', function($scope, $routeParams) {
    $scope.name = 'HomeController';
    $scope.params = $routeParams;
 })

 .controller('AboutController', function($scope, $routeParams) {
    $scope.name = 'AboutController';
    $scope.params = $routeParams;
    console.log($scope.name);
 })

.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when('/Book/:bookId', {
        templateUrl: 'book.html',
        controller: 'BookController',
        resolve: {
            // I will cause a 1 second delay
            delay: function($q, $timeout) {
                var delay = $q.defer();
                $timeout(delay.resolve, 1000);
                return delay.promise;
            }
        }
    })
    .when('/about', {
        'controller': 'AboutController',
        'templateUrl': '/assets/templates/homemart/about.html'
    })
    .when('/', {
        templateUrl: '/assets/templates/homemart/front.html',
        controller: 'ChapterController'
    })
    .otherwise({

    });
    // configure html5 to get links working on jsfiddle
    $locationProvider.html5Mode({
        enabled:true,
        requireBase: false
    });
});