$(function(){
    "use strict";
    var _message = function(msg){
        if($('.alert').length){
            $('.alert').removeClass('hide').find('p').html(msg);
        }else{
            $('.alert-container')
                .html('<div class="alert alert-warning"><button class="close" data-dismiss="alert">&times;</button><p>'+ msg +'</p></div>');
        }
        setTimeout(function(){
            $('.alert').addClass('hide');
        }, 10000);
    }, _success = function(msg){
        $('.alert-container')
        .html('<div class="alert alert-info"><button class="close" data-dismiss="alert">&times;</button><p>'+ msg +'</p></div>');
    };
    $('#login-form').on('submit', function(e){
        e.preventDefault();
        var $this = $(this), data = $this.serialize(), act = $this.attr('action') || base_url + 'customer/ajax/auth/login';
        $.ajax({
            url: act,
            data: data,
            dataType: 'json',
            type:'post',
            beforeSend: function(){
                 $('.panel-register').block({
                    message: '<img src="'+ asset_url +'img/loading.gif">',
                    css:{
                        background:'none',
                        border:'none'
                    }, 
                    overlayCSS: {
                        background:'#eee'
                    }
                });
            },
            success: function(res){
                if(res.success){
                    document.location = res.next || base_url + 'customer/index/';
                }else{
                    _message(res.message);
                }
            },
            complete: function(){
                $this.trigger('reset');
            },
            error: function(e){
                console.log(e.responseText);
                _message('Maaf, Sepertinya terjadi kesalahan dengan sistem. Silakan mencoba kembali..');
            }
        })
    })
});