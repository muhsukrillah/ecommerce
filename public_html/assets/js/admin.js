var sasak = angular.module('sasak', ['ngRoute']);
var base_url = 'http://localhost:8000/lombok-developer.web.id/public_html/lomdev_admin/index/';

sasak.controller('Dashboard', function($scope){
    $scope.counter = {
        pegawai: 44,
        pegawai_absen: 10
    };
    console.log('Dashboard Controller..');
});

sasak.controller('Test', function(){
    console.log('Test');
});

sasak.config(function($routeProvider){
    $routeProvider
    .when('/', {
        controller: 'Dashboard as ds'
    })
    .when('/test', {controller: 'Test'})
    .otherwise({redirectTo: '/'});
});
/*
sasak.factory('SasakService', ['$resource', function($resource){
    return $resource(base_url);
}]);
*/

