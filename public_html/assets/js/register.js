$(function($){
    "use strict";
    var _message = function(msg){
        if($('.alert').length){
            $('.alert').removeClass('hide').find('p').html(msg);
        }else{
            $('.alert-container')
                .html('<div class="alert alert-warning"><button class="close" data-dismiss="alert">&times;</button><p>'+ msg +'</p></div>');
        }
        setTimeout(function(){
            $('.alert').addClass('hide');
        }, 5000);
    }, _success = function(msg){
        $('.alert-container')
        .html('<div class="alert alert-info"><button class="close" data-dismiss="alert">&times;</button><p>'+ msg +'</p></div>');
    };

    $('#register_form').on('submit', function(e){
        e.preventDefault();

        if(!$.trim($('#first_name').val()).length){
            _message('Nama tidak boleh kosong..');
            $('#first_name').parent().addClass('has-error');
            $('#first_name').focus();
            return false;
        }
        if(!(/^([a-z ]+)$/i).test($('#first_name').val())){
            _message('Nama tidak benar..');
            $('#first_name').parent().addClass('has-error');
            $('#first_name').focus();
            return false;
        }
        if(!$.trim($('#email').val()).length){
            _message('Email tidak boleh kosong..');
            $('#email').parent().addClass('has-error');
            $('#email').focus();
            return false;
        }
        if(!(/^([a-z0-9\.\_]+)\@([a-z]+){2,}\.([a-z]+){2,}$/i).test($('#email').val())){
            _message('Email tidak benar..');
            $('#email').parent().addClass('has-error');
            $('#email').focus();
            return false;
        }
        if($.trim($('#phone').val()).length){
            if(!(/^(\+?[0-9]+)$/i).test($('#phone').val())){
                _message('Nomor HP tidak benar..');
                $('#phone').parent().addClass('has-error');
                $('#phone').focus();
                return false;
            }
        }

        var $this = $(this), submit = $this.find('button[type="submit"]'), data = $this.serialize();
        $.ajax({
            url: $this.attr('action'),
            data: data,
            dataType: 'json',
            type:'post',
            beforeSend: function(){
                submit.disable()
                $('.panel-register').block({
                    message: '<img src="'+ asset_url +'img/loading.gif">',
                    css:{
                        background:'none',
                        border:'none'
                    }, 
                    overlayCSS: {
                        background:'#eee'
                    }
                });
            },
            success: function(res){
                console.log(res);
                if(res.success){
                    _success(res.message);
                    $this.trigger('reset').hide();
                }
            },
            complete: function(){
                $('.panel-register').unblock();
                submit.enable();
            },
            error: function(e){
                console.log(e.responseText);
            }
        })
        console.log('Sending request..');
    });
});