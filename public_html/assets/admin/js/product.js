$(function(){
    $('#description').wysihtml5();
    $('#category').select2({tags:true});
    // brand
    var get_brand_data = function(callback){
        var brand_name = $('#brand_name'), 
        brand_phone = $('#brand_phone'),
        brand_email = $('#brand_email'),
        description = $('#brand_description'),
        data = {
            brand_name: brand_name.val(),
            brand_phone: brand_phone.val(),
            brand_email: brand_email.val(),
            description: description.val()
        };
        if(typeof callback == 'function'){
            return callback([brand_name,brand_phone,brand_email,description],data);
        }else{
            return data;
        }
    };
    $('#save_brand').on('click', function(){
        var $this = $(this), brand = $('#brand'), data = get_brand_data();
        if(!$('#brand_name').val().length){
            $('#brand_name').parent().addClass('has-error');
            $('#brand_name').focus();
            return false;
        }else{
            $('#brand_name').parent().removeClass('has-error');
        }

        $.ajax({
            type:'post',
            url: admin_url + 'product/brand/add', 
            dataType:'json',
            data: data, 
            success: function(res){
                console.log(res);
                if(res.success){
                    brand.append('<option value="'+ (res.data.id_brand || 0) +'">'+ (res.data.brand_name || 'undefined') + '</option>');
                    brand.val(res.data.id_brand);
                }else{
                    alert(res.message);
                }
            },
            complete: function(){
                get_brand_data(function(el){
                    for(var i in el){
                        el[i].val('');
                    }
                });
            }
        });
    });
});