(function(w, $){
    "use strict";
    function get_props(){
        return {
            harga_beli: $('#harga_beli').val(),
            harga_jual: $('#harga_jual').val(),
            harga_coret: $('#harga_coret').val(),
            satuan: $('#satuan').val(),
            quantity: $('#quantity').val(),
            stok: $('#stok').val()
        };
    }

    function get_tr(i){
        var tb = $('table.table-satuan'), tra = (i ? tb.find('tr[data-id="'+ i +'"]') : tb.find('tr.sample')), tr = [];
        if(tra.length && !i){
            tr = tra;
            tr.removeClass('hide').removeClass('sample');
            tra.remove();
        }else if(tra.length == 1 && !i){
            tra = tb.find('tr:first');
            tr = tra.clone();
        }else if(!i){
            tra = tb.find('tr:last');
            tr = tra.clone();
        }else{
            tr = tra;
        }
        tr.find('td').empty();
        return tr;
    }

    function set_tr(d, i){
        var tr = get_tr(i), act = $('<a href="javascript:void(0);" class="label label-xs label-info edit"><i class="fa fa-pencil"></i></a>' + "\n" + '<a href="javascript:void(0);" class="label label-xs label-danger delete"><i class="fa fa-trash-o"></i></a>'), id = i || Math.random().toString().substr(2), dj = JSON.stringify(d);

        tr.find('td').eq(0).text(d.satuan);
        tr.find('td').eq(1).text(d.quantity);
        tr.find('td').eq(2).text(d.harga_beli);
        tr.find('td').eq(3).text(d.harga_coret);
        tr.find('td').eq(4).text(d.harga_jual);
        tr.find('td').eq(5).text(d.stok);
        tr.find('td').eq(6).html(act);
        if(!i){
            tr.attr('data-id', id);
            $('table.table-satuan tbody').append(tr);
            $('#prop_data').append('<input data-id="'+ id +'" type="hidden" name="prop[]" value=\''+ dj +'\' />');
        }else{
            tr.removeClass('edited').show();
            $('input[data-id="'+ id +'"]').val(dj);
        }
        do_act(tr);
        d = null;
        return tr;
    }

    function clear_tr(){
        var tb = $('table.table-satuan tbody');
        if(!tb.find('tr').length){
            var tr = tb.find('tr:first').clone();
        }else{
            var tr = $('<tr class="sample hide"><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td></tr>');
        }
        tb.html(tr);
        $('#prop_data').empty();
        reset();
    }

    function reset(){
        $('#harga_beli').val('');
        $('#harga_jual').val('');
        $('#harga_coret').val('');
        $('#satuan').val('');
        $('#quantity').val(1);
        $('#stok').val('');
        if($('#add_prop').is(':hidden')){
            $('#add_prop').removeClass('hide');
            $('#save_prop').addClass('hide');
            $('table.table-satuan tbody').find('tr.edited').show();
        }
        $('#harga_beli').focus();
    }

    function do_act(el){
        el.on('click', '.edit', function(){
            var tr = $(this).closest('tr'), id = tr.data('id'), input = $('input[data-id="'+ id +'"]'), inval = input.val(), data = JSON.parse(inval);
            $('#harga_beli').val(data.harga_beli);
            $('#harga_jual').val(data.harga_jual);
            $('#harga_coret').val(data.harga_coret);
            $('#satuan').val(data.satuan);
            $('#quantity').val(data.quantity);
            $('#stok').val(data.stok);
            $('#add_prop').addClass('hide');
            $('#save_prop').removeClass('hide');
            tr.addClass('edited').hide();
            $('#harga_beli').focus();
        });
    }

    $('#add_prop').on('click', function(e){
        var prop = get_props();
        if(!prop.harga_jual.length){
            $('#harga_jual').focus();
            return false;
        }
        if(!prop.satuan.length){
            $('#satuan').focus();
            return false;
        }
        if(!prop.quantity.length){
            $('#quantity').focus();
            return false;
        }
        if(!prop.stok.length){
            $('#stok').focus();
            return false;
        }
        
        set_tr(prop);
        reset();
        $('#harga_beli').focus();
    });
    $('#save_prop').on('click', function(){
        var tr = $('table.table-satuan tbody').find('tr.edited'), id = tr.data('id'), input = $('input[data-id="'+ id +'"]'), prop = get_props();
        set_tr(prop, id);
        reset();
    });
    $('#clear_prop').on('click', clear_tr);
    $('#reset_prop').on('click', reset);
})(window, jQuery);
/**
 * product image uploader
 */
 (function(w,$){
    $('#fileUploader').on('change', function(e){
        var $this = $(this), files = $this[0].files, imgwrapper = '', iw = $('.img-wrapper').length || 1;
        for(var i=iw, j=files.length + iw;i<j;i++){
            imgwrapper += '<div class="col-md-4 img-wrapper" style="margin-bottom:10px;"><div  style="height:160px; border:1px solid #eee; text-align:center;padding-top:70px"><div class="progress progress-sm active"><div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"><span class="sr-only">20% Complete</span></div></div></div></div>';
            if((i % 3) == 0){
                //imgwrapper += '</div><div class="row" style="margin-top:10px;">';
            }
        }
        imgwrapper += '</div>';
        $('.images-wrapper').append(imgwrapper);
        var frm = $(this).closest('form'), act = frm.attr('action'), lact = frm.attr('last-action') || false;
        if(!lact){
            frm.attr('last-action', act);
        }
        frm.attr('onsubmit', 'return false;');
        frm.attr('action', admin_url +'product/ajax/upload_image');
        frm.ajaxSubmit({
            dataType : 'json',
            uploadProgress : function (e, position, total, percentComplete) {
                $('.img-wrapper .progress').find('.progress-bar').css({
                    'width' : percentComplete + '%'
                }).attr('aria-valuenow', percentComplete);
            },
            success : function(res) {
                console.log(res);
                if($('#images').val().length){
                    var img = $('#images').val();
                    var images = JSON.parse(img) || [];
                }else{
                    var images = [];
                }
                var i = 0;
                for(var $i in res){
                    //var img = new Image();
                    //img.src = res[$i].url;
                    $('.img-wrapper:not(.processed)').each(function(e, v){
                        $(this).find('div').html('<span class="remove bg-red" onclick="removeUploadedImage(this, '+ e +');"><i class="fa fa-times"></i></span>').css({
                            'background-image':'url("'+ (res[e].thumbnail_url || res[e].url) +'")',
                            'background-repeat':'no-repeat',
                            'background-size':'cover'
                        })
                        $(this).addClass('processed');
                    });
                    i++;
                    images.push(res[$i]);
                }
                var image = JSON.stringify(images);
                $('#images').val(image);
            },
            complete:function(){
                $('#fileUploader').val('');

                frm.removeAttr('action');
                frm.removeAttr('onsubmit');
            },
            error: function(e){
                console.log(e.responseText);
            }
        });
        frm.submit();
    });
    /**
     * image removed callback
     */
    w.removeUploadedImage = function(el, i){
        console.log(el);
        console.log(i);
        var $this = $(el), images = $('input#images').val(), ij = JSON.parse(images) || [];
        console.log(ij);
        if(ij[i]){
            console.log(ij[i]);
            var a = [], 
            del = {id:0}, 
            success = false, 
            id_product = parseInt($('#id_product').val()),
            delete_image = function(e){
                var is = JSON.stringify(e);
                $('#images').val(is);
                $this.closest('.img-wrapper').fadeOut(function(){
                    $(this).remove();
                });
            };
            for(var $i in ij){
                if(i == $i){
                    console.log($i +' will deleted..');
                    ij[$i].id = parseInt(ij[$i].id);
                    del = ij[$i];
                    continue;
                }
                a.push(ij[$i]);
            }
            if(del.id && id_product){
                if(confirm('Apakah Anda yakin ?')){
                    $.ajax({
                        url: admin_url + 'product/ajax/delete_image',
                        data: del,
                        dataType: 'json',
                        type: 'post',
                        success: function(res){
                            if(res.success){
                                delete_image(a);
                                alert(res.message);
                            }
                        }
                    });
                }
            }else{
                delete_image(a);
            }
        }else{
            console.log('ij[i] not found..');
        }
    }
 })(window, jQuery);