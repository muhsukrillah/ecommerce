/**
  * @author ComFreek <http://stackoverflow.com/users/603003/comfreek>
  * @link http://stackoverflow.com/a/16069817/603003
  * @license MIT 2013-2015 ComFreek
  * @license[dual licensed] CC BY-SA 3.0 2013-2015 ComFreek
  * You MUST retain this license header!
  */

(function (exports) {
    function valOrFunction(val, ctx, args) {
        if (typeof val == "function") {
            return val.apply(ctx, args);
        } else {
            return val;
        }
    }

    function InvalidInputHelper(input, options) {
        input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

        function changeOrInput() {
            if (input.value == "") {
                input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
            } else {
                input.setCustomValidity("");
            }
        }

        function invalid() {
            if (input.value == "") {
                input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
            } else {
               input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
            }
        }

        input.addEventListener("change", changeOrInput);
        input.addEventListener("input", changeOrInput);
        input.addEventListener("invalid", invalid);
    }

    function getAllElementsByAttribute(attribute, context){
      var matchingElements = [];
      var allElements = (context || document).getElementsByTagName('*');
      for (var i = 0, n = allElements.length; i < n; i++){
        if (allElements[i].getAttribute(attribute) !== null){
          // Element exists with attribute. Add to array.
          matchingElements.push(allElements[i]);
        }
      }
      return matchingElements;
    }
    exports.InvalidInputHelper = InvalidInputHelper;
    exports.getElementsByAttribute = getAllElementsByAttribute;

    var els = exports.getElementsByAttribute('data-custom-invalid');
    for(var i in els){
        var defaultText = els[i].getAttribute('data-invalid') || "Ruas ini tidak boleh kosong",
        emptyText = els[i].getAttribute('date-empty') || 'Ruas ini harus diisi..',
        invalidText = function(input){
            return 'Data ['+ input.value +'] tidak benar..';
        };
        exports.InvalidInputHelper(els[i], {defaultText: defaultText, emptyText: emptyText, invalidText: invalidText});
    }
})(window);

$(function(){
    var alert = $('.alert');
    if(alert.length){
        setTimeout(function(){
            alert.fadeOut(function(){
                alert.remove();
            });
        }, 10000);
    }
});