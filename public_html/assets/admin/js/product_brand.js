$(function(){
    $('#description').wysihtml5();
    $('#category').select2({tags:true});
    // brand
    var get_brand_data = function(callback){
        var id_brand = $('#id_brand'),
        brand_name = $('#brand_name'), 
        brand_phone = $('#brand_phone'),
        brand_email = $('#brand_email'),
        description = $('#brand_description'),
        data = {
            id_brand: (id_brand.val().length ? id_brand.val() : 0),
            brand_name: brand_name.val(),
            brand_phone: brand_phone.val(),
            brand_email: brand_email.val(),
            description: description.val()
        };
        if(typeof callback == 'function'){
            return callback([id_brand, brand_name,brand_phone,brand_email,description],data);
        }else{
            return data;
        }
    }, new_data = function(data, i){
        var json = JSON.stringify(data), total_data = $('.table-brand tbody tr').length;
        var tr = '<tr data-id="'+ data.id_brand +'" data-json=\''+ json +'\'>'+
        '<td>'+ (i || total_data++) +'</td>'+
        '<td>'+ (data.brand_name || '') +'</td>'+
        '<td>'+ (data.brand_phone || '') +'</td>'+
        '<td>'+ (data.brand_email || '') +'</td>'+
        '<td>'+ (data.description || '') +'</td>'+
        '<td class="text-right">'+
            '<a href="javascript:void(0);" class="action edit label label-info"><i class="fa fa-pencil"></i></a>'+
            ' <a href="javascript:void(0);" class="action delete label label-danger"><i class="fa fa-trash-o"></i></a>'+
        '</td>'+
        '</tr>';
        return tr;
    };
    $('.table-brand tbody').on('click', 'tr .edit', function(){
        var $this = $(this), tr = $this.parents('tr'), data = tr.data('json');
        $('#id_brand').val(data.id_brand);
        $('#brand_name').val(data.brand_name);
        $('#brand_phone').val(data.brand_phone);
        $('#brand_email').val(data.brand_email);
        $('#brand_description').val(data.description);
        $('#save_brand').attr('data-action', 'edit');
        tr.hide();
    }).on('click', 'tr .delete', function(){
        if(confirm('Apakah Anda yakin?')){
            var $this = $(this), id = $this.closest('tr').data('id');
            $.ajax({
                url: admin_url + 'product/brand/delete',
                type:'post',
                data:{id_brand: id},
                dataType: 'json',
                beforeSend: function(){
                    $this.find('.fa').removeAttr('class').addClass('fa fa-spin fa-spinner');
                },
                success: function(res){
                    if(res.success){
                        $this.closest('tr').fadeOut(function(){
                            $(this).remove();
                            alert(res.message);
                        });
                    }
                },
                error: function(e){
                    console.log(e.responseText);
                }
            })
        }else{
            alert('batal');
        }
    });
    $('#cancel').on('click', function(){
        get_brand_data(function(el, data){
            for(var i in el){
                el[i].val('');
            }
            $('.table-brand tbody').find('tr[data-id="'+ data.id_brand +'"]').show();
        });
        $('#save_brand').attr('data-action', 'new');
        $('#brand_name').focus();
    });
    $('#save_brand').on('click', function(){
        var $this = $(this), 
        table = $('.table-brand tbody'), 
        data = get_brand_data(), 
        url = 'product/brand/add',
        mode = $this.data('action') || 'new';

        if(!$('#brand_name').val().length){
            $('#brand_name').parent().addClass('has-error');
            $('#brand_name').focus();
            return false;
        }else{
            $('#brand_name').parent().removeClass('has-error');
        }

        if(mode == 'edit'){
            url = 'product/brand/edit';
        }

        $.ajax({
            type:'post',
            url: admin_url + url, 
            dataType:'json',
            data: data, 
            cache:false,
            beforeSend: function(){
                $this.find('fa').removeClass('fa-save').addClass('fa-spin').addClass('fa-spinner');
                $this.attr('disabled', 'disabled');
                get_brand_data(function(el){
                    for(var i in el){
                        el[i].attr('readonly', 'readonly');
                    }
                })
            },
            complete: function(){
                $this.find('fa').removeAttr('class').attr('class', 'fa fa-save');
                $this.removeAttr('disabled');
                get_brand_data(function(el){
                    for(var i in el){
                        el[i].val('').removeAttr('readonly');
                    }
                });
                $('#brand_name').focus();
            },
            success: function(res){
                console.log(res);
                if(res.success){
                    if(mode == 'new'){
                        table.append(new_data(res.data));
                    }else{
                        var t = table.find('tr[data-id="'+ res.data.id_brand +'"]'),
                        no = t.find('td:first').text();
                        t.replaceWith(new_data(res.data, no));
                    }
                    alert(res.message);
                }else{
                    alert(res.message);
                }
            }
        });
    });
});