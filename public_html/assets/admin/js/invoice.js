var Invoice = function(w, $){

    return {
        page:{
            invoice_detail: function(){
                $('#approvePayment').on('click', function(){
                    var $this = $(this);
                    $.ajax({
                        url: admin_url + 'invoice/ajax/approve_payment',
                        data:{
                            id_payment: $('#payment_id').val(),
                            id_invoice: $('#invoice_id').val(),
                            ref: 'invoice_detail',
                            token: token
                        },
                        dataType: 'json',
                        type: 'post',
                        success: function(res){
                            console.log(res);
                            if(res.success){
                                $this.html('<i class="fa fa-check"></i> Berhasil ditanggapi..').attr('disabled', 'disabled');
                            }else{
                                alert(res.message || 'Maaf, proses gagal dilakukan. Sepertinya terjadi kesalahan sistem. ');
                            }
                        },
                        error: function(e){
                            console.log(e.responseText);
                            alert('Maaf, proses gagal dilakukan. Sepertinya terjadi kesalahan sistem. ');
                        }
                    });
                });
            }
        },
        init: function(){
            var b = $('body'), $this = this;
            b.prop('class').split(' ').forEach(function(e){
                if($this.page[e]){
                    $this.page[e].call(this);
                }
            });
        }
    };
}(window,jQuery);
Invoice.init();