/*
SQLyog Ultimate v11.11 (32 bit)
MySQL - 5.5.5-10.0.17-MariaDB : Database - ecomerce
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ecomerce` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ecomerce`;

/*Table structure for table `brand` */

DROP TABLE IF EXISTS `brand`;

CREATE TABLE `brand` (
  `id_brand` int(3) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(50) NOT NULL,
  `brand_phone` varchar(15) NOT NULL,
  `brand_email` varchar(60) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  PRIMARY KEY (`id_brand`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `brand` */

/*Table structure for table `cart` */

DROP TABLE IF EXISTS `cart`;

CREATE TABLE `cart` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_customer` int(4) NOT NULL,
  `id_product` int(3) NOT NULL,
  `id_satuan` int(3) NOT NULL,
  `quantity` int(4) NOT NULL,
  `subtotal` int(7) NOT NULL DEFAULT '0',
  `total` int(7) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `cart` */

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id_category` int(4) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(25) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `order` int(2) NOT NULL DEFAULT '0',
  `parent_id` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `category` */

insert  into `category`(`id_category`,`category_name`,`slug`,`description`,`order`,`parent_id`,`date_added`,`date_updated`) values (1,'Makanan','makanan','kategori makanan',0,0,'0000-00-00 00:00:00','2016-03-28 09:48:26'),(2,'Minuman','minuman','kategori minuman',1,0,'2016-03-27 23:14:28','2016-03-27 23:14:28'),(3,'Minuman Ringan','minuman-ringan','kategori minuman ringan',1,2,'2016-03-27 23:14:49','2016-03-28 22:27:04'),(4,'Makanan Ringan','makanan-ringan','Kategori makanan ringan',0,1,'2016-03-28 08:39:21','2016-03-28 08:39:21'),(5,'Mie Instan','mie-instan','kategori produk mie instan',0,1,'2016-03-28 08:40:49','2016-03-28 08:40:49'),(6,'Bahan Kue','bahan-kue','kategori produk bahan kue',0,1,'2016-03-28 08:45:42','2016-03-28 09:48:32'),(7,'Obat-Obatan','obat-obatan','kategori obat-obatan',2,0,'2016-03-28 09:53:44','2016-03-28 09:53:54'),(8,'Obat Dewasa','obat-dewasa','Obat-obatan untuk orang dewasa',2,7,'2016-03-28 09:54:27','2016-03-28 09:54:27'),(9,'Obat Anak','obat-anak','obat-obatan untuk anak-anak',2,7,'2016-03-28 09:54:45','2016-03-28 09:54:45'),(10,'Obat Serangga','obat-serangga','Obat-obatan untuk serangga',2,7,'2016-03-28 09:55:14','2016-03-28 09:55:14'),(11,'Kopi','kopi','kategori produk minuman kopi',1,2,'2016-03-28 22:25:46','2016-03-28 22:27:13'),(12,'Air Mineral & Bersoda','air-mineral---bersoda','Produk kategori air mineral dan bersoda',1,3,'2016-03-29 14:18:27','2016-03-29 14:18:27');

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id_customer` int(3) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `token` varchar(50) DEFAULT NULL,
  `display_name` varchar(10) NOT NULL,
  `first_name` varchar(10) NOT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `gender` char(1) NOT NULL,
  `address` text,
  `phone` varchar(15) NOT NULL,
  `identity_number` varchar(30) DEFAULT NULL COMMENT 'nomor ktp,sim,paspor',
  `identity_name` varchar(30) DEFAULT NULL COMMENT 'ktp,sim',
  `bank_account_id` varchar(30) DEFAULT NULL,
  `bank_account_name` varchar(30) DEFAULT NULL,
  `bank_name` varchar(40) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT 'active/inactive',
  `date_added` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `customer` */

/*Table structure for table `order` */

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `id_order` int(4) NOT NULL AUTO_INCREMENT,
  `id_customer` int(3) NOT NULL,
  `id_user` int(3) NOT NULL,
  `date_added` date NOT NULL,
  `date_expired` date NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `order` */

/*Table structure for table `order_item` */

DROP TABLE IF EXISTS `order_item`;

CREATE TABLE `order_item` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_order` int(5) NOT NULL,
  `id_product` int(7) NOT NULL,
  `id_satuan` int(3) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtotal` int(8) NOT NULL,
  `total` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `order_item` */

/*Table structure for table `order_status` */

DROP TABLE IF EXISTS `order_status`;

CREATE TABLE `order_status` (
  `id_status` int(4) NOT NULL AUTO_INCREMENT,
  `id_user` int(3) NOT NULL,
  `id_order` int(4) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT 'canceled,pending,paid,delivered,received',
  `description` text,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `order_status` */

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id_product` int(5) NOT NULL AUTO_INCREMENT,
  `model` varchar(10) NOT NULL,
  `product_name` varchar(30) NOT NULL,
  `product_title` varchar(60) NOT NULL,
  `description` text NOT NULL,
  `harga_beli` int(7) NOT NULL DEFAULT '0',
  `harga_jual` int(7) NOT NULL DEFAULT '0',
  `harga_coret` int(7) NOT NULL DEFAULT '0',
  `id_brand` int(3) NOT NULL DEFAULT '0',
  `id_user` int(3) NOT NULL,
  `stock` int(3) DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0=draft,1=published',
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `product` */

insert  into `product`(`id_product`,`model`,`product_name`,`product_title`,`description`,`harga_beli`,`harga_jual`,`harga_coret`,`id_brand`,`id_user`,`stock`,`date_added`,`date_updated`,`status`) values (13,'P000000011','cleo-gelas-250ml-x-48-pcs','Cleo Gelas 250ml x 48 Pcs','Cleo adalah air mineral kemasan siap minum yang diproses secara higienis untuk membantu memenuhi kebutuhan hidrasi anda sehari-hari. Airnya segar dan jernih. Dikemas praktis agar mudah dibawa dan dinikmati kapanpun, dimanapun. Hadir dengan kemasan cup 250ml.',20000,25900,0,0,1,10,'2016-03-29 13:24:56','2016-03-29 13:24:56',1),(14,'P000000002','aqua-botol-1500ml-x-12-pcs','Aqua Botol 1500ml x 12 Pcs','<div><br></div><div><p>Aqua adalah air mineral dalam kemasan. Aqua merupakan pelopor air minum dalam kemasan sejak 1973 dan populer di kalangan masyarakat. Berasal dari sumber mata air terpilih dan terlindung sehingga menjamin segala kebaikan alam dari sumbernya tetap terjaga dalam setiap tetesnya. Diproses dengan standar higienis tingkat tinggi.<br><br>Setiap tetes Aqua memiliki segala kebaikan dan keseimbangan mineral alami. Produk ini bisa didapatkan di sukamart.com - Bebas antri dan macet, belanja jadi lebih mudah dan cepat.<br><br>Jangan lupa tulis ulasan untuk produk ini. Ceritakan pengalaman Anda mengkonsumsi Aqua serta pengalaman Anda berbelanja di Sukamart.</p><p><b>Berat Kotor :</b>&nbsp;18471 g</p><p><b>Dimensi Produk :&nbsp;</b>34.00cm x 35.00cm x 50.00cm</p></div>',45000,52800,0,0,1,10,'2016-03-29 13:53:59','2016-03-29 13:53:59',1),(15,'P000000003','aqua-botol-330ml-x-24-pcs','Aqua Botol 330ml x 24 Pcs','<span>Air minum dalam kemasan, air mineral dari mata air Babakan Pari, Gunung Salak.Diproduksi oleh : PT Aqua Golden Mississippi.BPOM RI MD 249110003875<br><br><b>Berat Kotor :</b>&nbsp;</span>8564 g<span><br><br><b>Dimensi :</b>&nbsp;</span>26.00cm x 20.00cm x 39.00cm<br>',24000,39000,0,0,1,15,'2016-03-29 14:24:28','2016-03-29 14:24:28',1),(16,'P000000004','aauq-click-n-go-750ml','Aauq Click \'n Go 750ml','<span>Air minum dalam kemasan, air mineral dari mata air Babakan Pari, Gunung Salak.Diproduksi oleh : PT Aqua Golden Mississippi.BPOM RI MD 249110003875<br><br><b>Berat Kotor :</b>&nbsp;</span><span>884 g<br></span><span><br><b>Dimensi :</b>&nbsp;</span>7.00cm x 25.00cm x 7.00cm',3200,4500,0,0,1,50,'2016-03-29 18:04:14','2016-03-29 18:04:14',1);

/*Table structure for table `product_category` */

DROP TABLE IF EXISTS `product_category`;

CREATE TABLE `product_category` (
  `relation_id` int(4) NOT NULL AUTO_INCREMENT,
  `id_category` int(4) NOT NULL,
  `id_product` int(7) NOT NULL,
  PRIMARY KEY (`relation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `product_category` */

insert  into `product_category`(`relation_id`,`id_category`,`id_product`) values (1,3,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,5,7),(8,5,8),(9,10,9),(10,10,10),(11,3,11),(12,11,12),(13,3,13),(14,4,14),(15,12,15),(16,12,16);

/*Table structure for table `product_image` */

DROP TABLE IF EXISTS `product_image`;

CREATE TABLE `product_image` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `id_product` int(5) NOT NULL,
  `url` varchar(100) NOT NULL,
  `path` varchar(100) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `alt` varchar(100) NOT NULL,
  `type` varchar(15) NOT NULL,
  `thumbnail_url` varchar(100) NOT NULL,
  `thumbnail_path` varchar(100) NOT NULL,
  `featured_url` varchar(100) NOT NULL,
  `featured_path` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `product_image` */

insert  into `product_image`(`id`,`id_product`,`url`,`path`,`file_name`,`title`,`alt`,`type`,`thumbnail_url`,`thumbnail_path`,`featured_url`,`featured_path`) values (15,13,'http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_1.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459232554_1.jpg','1459232554_1.jpg','Cleo Gelas 250ml x 48 Pcs','Cleo Gelas 250ml x 48 Pcs','image/jpeg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_1_300x150.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_1_300x150.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_1_800x300.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_1_800x300.jpg'),(16,13,'http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_2.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459232554_2.jpg','1459232554_2.jpg','Cleo Gelas 250ml x 48 Pcs','Cleo Gelas 250ml x 48 Pcs','image/jpeg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_2_300x150.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_2_300x150.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_2_800x300.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_2_800x300.jpg'),(17,13,'http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_3.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459232554_3.jpg','1459232554_3.jpg','Cleo Gelas 250ml x 48 Pcs','Cleo Gelas 250ml x 48 Pcs','image/jpeg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_3_300x150.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_3_300x150.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_3_800x300.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459232554_3_800x300.jpg'),(18,14,'http://ecommerce.dev:8000/assets/catalogs/2016/03/1459234437_1.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459234437_1.jpg','1459234437_1','Aqua Botol 1500ml x 12 Pcs','Aqua Botol 1500ml x 12 Pcs','image/jpeg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459234437_1_300x150.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459234437_1_300x150.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459234437_1_800x300.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459234437_1_800x300.jpg'),(19,14,'http://ecommerce.dev:8000/assets/catalogs/2016/03/1459234437_2.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459234437_2.jpg','1459234437_2','Aqua Botol 1500ml x 12 Pcs','Aqua Botol 1500ml x 12 Pcs','image/jpeg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459234437_2_300x150.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459234437_2_300x150.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459234437_2_800x300.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459234437_2_800x300.jpg'),(20,15,'http://ecommerce.dev:8000/assets/catalogs/2016/03/1459236260_1.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459236260_1.jpg','1459236260_1','Aqua Botol 330ml x 24 Pcs','Aqua Botol 330ml x 24 Pcs','image/jpeg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459236260_1_300x150.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459236260_1_300x150.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459236260_1_800x300.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459236260_1_800x300.jpg'),(21,15,'http://ecommerce.dev:8000/assets/catalogs/2016/03/1459236260_2.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459236260_2.jpg','1459236260_2','Aqua Botol 330ml x 24 Pcs','Aqua Botol 330ml x 24 Pcs','image/jpeg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459236260_2_300x150.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459236260_2_300x150.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459236260_2_800x300.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459236260_2_800x300.jpg'),(22,16,'http://ecommerce.dev:8000/assets/catalogs/2016/03/1459249350_1.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459249350_1.jpg','1459249350_1','Aauq Click \'n Go 750ml','Aauq Click \'n Go 750ml','image/jpeg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459249350_1_300x150.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459249350_1_300x150.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459249350_1_800x300.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459249350_1_800x300.jpg'),(23,16,'http://ecommerce.dev:8000/assets/catalogs/2016/03/1459249350_2.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459249350_2.jpg','1459249350_2','Aauq Click \'n Go 750ml','Aauq Click \'n Go 750ml','image/jpeg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459249350_2_300x150.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459249350_2_300x150.jpg','http://ecommerce.dev:8000/assets/catalogs/2016/03/1459249350_2_800x300.jpg','C:/xampp/htdocs/ecommerce/public_html/assets/catalogs/2016/03/1459249350_2_800x300.jpg');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(40) NOT NULL,
  `display_name` varchar(15) NOT NULL,
  `first_name` varchar(15) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `level` int(2) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `date_added` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`username`,`password`,`email`,`display_name`,`first_name`,`last_name`,`avatar`,`token`,`level`,`status`,`date_added`,`date_updated`) values (1,'muh.sukrillah','202cb962ac59075b964b07152d234b70','muh.sukrillah@gmail.com','Riel','Muh','Sukrillah',NULL,'21098321908iujlfds09',1,1,'2016-03-15 10:06:23','2016-03-15 10:06:25');

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id_role` bigint(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `role_title` varchar(50) NOT NULL,
  KEY `id_role` (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `user_role` */

insert  into `user_role`(`id_role`,`role_name`,`role_title`) values (1,'administrator','Administrator'),(2,'hrd_manager','HRD Manager'),(3,'administrasi','Administrasi'),(4,'pegawai','Pegawai'),(5,'customer','Pelanggan');

/*Table structure for table `user_role_capabilities` */

DROP TABLE IF EXISTS `user_role_capabilities`;

CREATE TABLE `user_role_capabilities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_role` bigint(20) NOT NULL,
  `capability` varchar(50) NOT NULL,
  `value` int(1) NOT NULL,
  `description` varchar(60) NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

/*Data for the table `user_role_capabilities` */

insert  into `user_role_capabilities`(`id`,`id_role`,`capability`,`value`,`description`) values (1,1,'read_pegawai',0,'Lihat Data Pegawai'),(2,1,'create_pegawai',1,'Tambah Data Pegawai'),(3,1,'update_pegawai',1,'Ubah Data Pegawai'),(4,1,'delete_pegawai',1,'Hapus Data Pegawai'),(5,1,'read_gaji',1,'Lihat Data Gaji'),(6,1,'create_gaji',1,'Tambah Data Gaji'),(7,1,'update_gaji',1,'Ubah Data Gaji'),(8,1,'delete_gaji',1,'Hapus Data Gaji'),(9,1,'create_bonus',1,'Tambah Bonus'),(10,1,'update_bonus',1,'Ubah Bonus'),(11,1,'delete_bonus',1,'Hapus Bonus'),(12,1,'read_bonus',1,'Lihat Bonus'),(13,1,'create_potongan',1,'Tambah Potongan'),(14,1,'update_potongan',1,'Ubah Potongan'),(15,1,'delete_potongan',1,'Hapus Potongan'),(16,1,'read_potongan',1,'Lihat Potongan'),(17,1,'create_user',1,'Tambah Pengguna'),(18,1,'read_user',1,'Lihat Data User'),(19,1,'update_user',1,'Ubh Data User'),(20,1,'delete_user',1,'Hapus Data User'),(21,1,'read_product',1,'Lihat Data Produk'),(22,1,'create_product',1,'Tambah Data Produk'),(23,1,'update_product',1,'Ubah Data Produk'),(24,1,'delete_product',1,'Hapus Produk'),(25,1,'create_order',1,'Tambah Order'),(26,1,'read_order',1,'Lihat data Order'),(27,1,'update_order',1,'Ubah data Order'),(28,1,'delete_order',1,'Hapus Data Order'),(29,1,'read_invoice',1,'Lihat Tagihan'),(30,1,'update_invoice',1,'Ubah Tagihan'),(31,1,'read_post',1,'Lihat Tulisan'),(32,1,'create_post',1,'Tambah Tulisan'),(33,1,'update_post',1,'Ubah Tulisan'),(34,1,'delete_post',1,'Hapus Tulisan'),(35,1,'create_product_category',1,'Tambah Kategori Produk'),(36,1,'update_product_category',1,'Ubah Kategori Produk'),(37,1,'delete_product_category',1,'Hapus Kategori Produk');

/*Table structure for table `user_sessions` */

DROP TABLE IF EXISTS `user_sessions`;

CREATE TABLE `user_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_data` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `remember_token` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_sessions` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
