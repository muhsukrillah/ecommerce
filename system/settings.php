<?php
/**
 * Sistem Configurations 
 * ---------------------
 * Dibuat Oleh              Muh. Sukrillah
 * Url                      http://www.sukrillah.xyz
 * Versi                    1.0
 * -------------------------------------------------------------------------------*/
define("BASE_PATH", ROOT_PATH);
define("APP_PATH", ROOT_PATH .'app/');
define("LIBRARIES_PATH", ROOT_PATH .'lib/');
define("CONTROLLER_PATH", ROOT_PATH .'controller/');
define("HELPERS_PATH", ROOT_PATH .'helpers/');
define("VIEW_PATH", ROOT_PATH . 'view/');
define("OBJECT_PATH", ROOT_PATH . 'object/');
define("ROUTER_PATH", ROOT_PATH . 'routes/');
define("MODELS_PATH", ROOT_PATH ."models/");
/**
 * Loging Config            untuk kebutuhan log sistem
 * -------------
 * Default                  TRUE
 * Sifat                    Opsional / Tidak Wajib
 * --------------------------------------------------------*/
define("LOG", TRUE);
/**
 * Log Path                 lokasi berkas log tersimpan..
 * --------
 * Default                  ROOT_PATH .'log/'
 * Sifat                    Wajib jika Log => TRUE
 * ---------------------------------------------------------*/
define("LOG_PATH", ROOT_PATH .'log/');
/*
echo '<pre>';
print_r($_SERVER);

exit();
*/