<?php
/**
 * Helpers 
 * 
 * Dibuat Oleh 		: Muh Sukrillah
 * Url 				: http://www.sukrillah.xyz
 * Versi 			: 1.0
 */
/**
 * Redirect Helper
 */
function redirect($url = ''){
	ob_start();
	ob_clean();
	if(empty($url)){
		$url = base_url();
	}
	header("Location: ". $url);
	exit;
}
/**
 * URL helpers
 */
function &uri(){
	$url =& \sasak\URI::instance();
	return $url;
}
/*
 * General Helpers
 */
function base_url($data = ''){
	return uri()->base_url($data);//BASE_URL . ltrim($data, '/');
}

function asset_url($data = ''){
	return base_url('assets/'. ltrim($data, '/'));
}
/**
 * login url
 */
function login_url($data = ''){
	return base_url('account/login/' . ltrim($data, '/'));
}
/**
 * logout url
 */
function logout_url($data = ''){
    return base_url('account/logout/'. ltrim($data, '/'));
}
/**
 * admin url
 */
function admin_url($data = ''){
    return base_url('admin/' . ltrim($data, '/'));
}
/**
 * user url
 */
function user_url($data = ''){
    return base_url('user/' . ltrim($data, '/'));
}
/**
 * Array to url
 */
function array_to_url($data = []){
	return uri()->array_to_url($data);
}
/**
 * Hooks
 */
function add_action($point_name = '', $action = '', $priority = 10, $accepted_args = 1){
    \sasak\Actions()->_add_action($point_name, $action, $priority, $accepted_args);
}
function do_action($point_name = ''){
    $args = func_get_args();
    $hooks =& \sasak\Actions_Hooks::instance();
    @call_user_method_array('_do_action', $hooks, $args);
    //\sasak\Actions_Hooks::_do_action($point_name);
}
/**
 * session
 */
function generate_token(){
    //$session =& session();

}
/**
 * 
 * order status
 */
function order_status($order_field = '', $field_name = '', $order = 'asc'){
	$status = '';
	if($order_field == $field_name){
		$status .= '<i class="fa';
		if($order == 'asc'){
			$status .= ' fa-sort-desc"';
		}else{
			$status .= ' fa-sort-asc"';
		}
		$status .= '></i>';
	}
	return $status;
}
function date_to_date_id($date = '', $display_day = false, $format = ''){
    $timestamp = strtotime($date);
    return date_id($timestamp, $display_day, $format);
}
/*
 * Tanggal Indonesia
 * 
 */
function date_id($timestamp = 0, $display_day = false, $format = ''){
	if(!$timestamp) $timestamp = time();
	if($display_day){
		if(empty($format)){
			$format = 'l, d M Y';
		}
		$date = date($format, $timestamp);
	}else{
		if(empty($format)){
			$format = 'd M Y';
		}
		$date = date($format, $timestamp);
	}
	$hari = array(
		'Monday' => 'Senin',
		'Tuesday' => 'Selasa',
		'Wednesday' => 'Rabu', 
		'Thursday' => 'Kamis', 
		'Friday' => 'Jum\'at',
		'Saturday' => 'Sabtu',
		'Sunday' => 'Minggu'
	);
	$bulan = array(
		'Jan' => 'Januari', 
		'Feb' => 'Februari', 
		'Mar' => 'Maret', 
		'Apr' => 'April', 
		'May' => 'Mei', 
		'Jun' => 'Juni', 
		'Jul' => 'Juli', 
		'Aug' => 'Agustus', 
		'Sep' => 'September', 
		'Oct' => 'Oktober', 
		'Nov' => 'November', 
		'Dec' => 'Desember'
	);
	foreach($hari as $key => $value){
		$date = str_replace($key, $value, $date);
	}
	foreach($bulan as $key => $value){
		$date = str_replace($key, $value, $date);
	}
	return $date;
}
/**
 * format harga rupiah
 */
function format_harga($angka = 0, $prefix = 'Rp ') {
    $harga = '';
    if(isset($prefix) && !empty($prefix)){
        $harga .= $prefix;
    }
    $harga .= number_format($angka, 0, 3, '.');
    return $harga;
}

function format_rupiah($angka = 0, $prefix = 'Rp. '){
    return format_harga($angka, $prefix);
}
/**
 * FUngsi terbilang
 */
function terbilang($angka = 0, $zero = false) {
	if($angka == 0 && $zero){
		return 'nol';
	}
    $angka = (float)$angka;
    $bilangan = array(
            '',
            'satu',
            'dua',
            'tiga',
            'empat',
            'lima',
            'enam',
            'tujuh',
            'delapan',
            'sembilan',
            'sepuluh',
            'sebelas'
    );

    if ($angka < 12) {
        return $bilangan[$angka];
    } else if ($angka < 20) {
        return $bilangan[$angka - 10] . ' belas';
    } else if ($angka < 100) {
        $hasil_bagi = (int)($angka / 10);
        $hasil_mod = $angka % 10;
        return trim(sprintf('%s puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
    } else if ($angka < 200) {
        return sprintf('seratus %s', terbilang($angka - 100));
    } else if ($angka < 1000) {
        $hasil_bagi = (int)($angka / 100);
        $hasil_mod = $angka % 100;
        return trim(sprintf('%s ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));
    } else if ($angka < 2000) {
        return trim(sprintf('seribu %s', terbilang($angka - 1000)));
    } else if ($angka < 1000000) {
        $hasil_bagi = (int)($angka / 1000); // karena hasilnya bisa ratusan jadi langsung digunakan rekursif
        $hasil_mod = $angka % 1000;
        return sprintf('%s ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));
    } else if ($angka < 1000000000) {

        // hasil bagi bisa satuan, belasan, ratusan jadi langsung kita gunakan rekursif
        $hasil_bagi = (int)($angka / 1000000);
        $hasil_mod = $angka % 1000000;
        return trim(sprintf('%s juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else if ($angka < 1000000000000) {
        // bilangan 'milyaran'
        $hasil_bagi = (int)($angka / 1000000000);
        $hasil_mod = fmod($angka, 1000000000);
        return trim(sprintf('%s milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else if ($angka < 1000000000000000) { 
        // bilangan 'triliun'                           
        $hasil_bagi = $angka / 1000000000000;                           
        $hasil_mod = fmod($angka, 1000000000000);                           
        return trim(sprintf('%s triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod))); 
    } else {
        return 'Format angka melebihi batas';                        
    }                   

}
/**
 * Fungsi terbilang v2
 * main process
 * <process>
 * <?php
 * $nilai = 1010; // as input
 * $terbilang = angka_terbilang($nilai) // as output
 * echo $terbilang
 */
function angka_terbilang($nilai){
    //... declaration
    $angka = ['', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan'];
    $satuan = [1 => 'puluh', 2 => 'ratus', 3 => 'ribut', 6 => 'juta', 9=>'milyar', 12=>'triliun'];
    $power = [0, 1, 2, 3, 6, 9, 12];
    // define function
    $baca = function($nilai) use($angka, $satuan, $power, &$baca){
        if($nilai < 10) return $angka[$nilai];
        for($i=1; $i < count($power); $i++){
            $div = pow(10, $power[$i]);
            $div1 = pow(10, $power[$i-1]);
            if($nilai < $div){
                // call recursively
                return $baca(floor($nilai / $div1)). ' '. $satuan [$power[$i-1]] .' '. $baca($nilai%$div1);
            }
        }
    };
    $str = $baca($nilai);
    // modifikasi angka belasan dan penyebutan se
    $str = preg_replace('/satu puluh (\w+/i', '\1 belas', $str);
    $str = preg_replace('/satu (ribu|ratus|puluh|belas)/', 'se\1', $str);
    // keluarkan output
    return $str;
}
/**
 * Get User Level
 */
function get_user_level($level_id = 0){
	$levels = ['4' => 'Pegawai','3'=> 'Administrasi', '2'=>'HRD Manajer', '1'=>'Administrator'];
	if(isset($levels[$level_id])){
		return $levels[$level_id];
	}
	return 'Tidak diketahui';
}
/**
 * fungsi untuk mengamankan data string
 *
 */
function clean($input = ''){
	if(is_array($input)){
		$output = array();
		foreach($input as $key => $value){
			$output[$key] = clean($value);
		}
	}else{
		$output = (string) $input;
		if(get_magic_quotes_gpc()){
			$output = stripslashes($output);
		}else{
			$output = htmlentities($output, ENT_QUOTES, 'UTF-8');
		}
	}
	return $output;
}
/**
 * Error function
 */
function Error($message = ''){
	$message = '<h3 style="color:#ee1811;">'. $message .'</h3>';
	die($message);
}
/**
 * Default Error 404
 * @access  public
 * @param   null
 * @return  null
 */
function error_404($response = NULL){
    $res = '[Error 404] Halaman Tidak Tersedia..';
    if(!isset($response) && !empty($response)){
        $res = $response;
    }
    \sasak\Response::send($res, [
        'status_code' => 404, 
        'status_text' => 'Page Not Found'
    ]);
}
/**
 * Run Command Prompt
 */
function execInBackground($cmd = NULL) { 
    if(!isset($cmd)) return 0;
    $proc = NULL;
    if (substr(php_uname(), 0, 7) == "Windows"){ 
        $proc = popen("start /B ". $cmd, "r"); 
        pclose($proc); 
    } 
    else { 
        $proc = exec($cmd . " > /dev/null &");   
    } 
    return $proc;
}
/**
 * url title
 */
function url_title($str, $separator = '-', $lowercase = TRUE) {
    if ($separator == 'dash') 
    {
        $separator = '-';
    }
    else if ($separator == 'underscore')
    {
        $separator = '_';
    }
    
    $q_separator = preg_quote($separator);

    $trans = array(
        '&.+?;'                 => '',
        '[^a-z0-9 _-]'          => '',
        '\s+'                   => $separator,
        '('.$q_separator.')+'   => $separator
    );

    $str = strip_tags($str);

    foreach ($trans as $key => $val)
    {
        $str = preg_replace("#".$key."#i", $val, $str);
    }

    if ($lowercase === TRUE)
    {
        $str = strtolower($str);
    }

    return trim($str, $separator);
}