/*
SQLyog Ultimate v11.11 (32 bit)
MySQL - 5.5.5-10.0.17-MariaDB : Database - simpeg
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`simpeg` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `simpeg`;

/*Table structure for table `absensi` */

DROP TABLE IF EXISTS `absensi`;

CREATE TABLE `absensi` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nip` varchar(9) DEFAULT NULL,
  `jam_masuk` datetime DEFAULT NULL,
  `jam_pulang` datetime DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `absensi` */

/*Table structure for table `bonus` */

DROP TABLE IF EXISTS `bonus`;

CREATE TABLE `bonus` (
  `id_bonus` int(4) NOT NULL AUTO_INCREMENT,
  `id_gaji` int(7) NOT NULL,
  `jumlah` int(8) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `tgl` datetime NOT NULL,
  PRIMARY KEY (`id_bonus`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `bonus` */

insert  into `bonus`(`id_bonus`,`id_gaji`,`jumlah`,`keterangan`,`tgl`) values (5,4,195000,'Uang Transport','2016-01-23 12:44:17'),(6,4,210000,'Uang Pulsa','2016-01-23 12:45:12'),(7,4,100000,'Bonus Lembur','2016-01-23 12:56:22'),(8,6,500000,'Bonus Komisi','2016-01-24 09:18:42'),(9,5,500000,'Bonus Komisi','2016-01-24 09:18:51'),(10,10,200000,'Uang Transport','2016-01-24 10:35:08'),(13,11,150000,'Uang Pulsa','2016-01-24 10:41:34'),(15,1,150000,'Uang Transport','2016-01-24 11:31:03'),(16,10,100000,'Bonus Lembur','2016-02-04 11:01:59'),(17,3,180000,'Bonus Lembur','2016-02-04 11:27:20'),(20,8,100000,'Bonus Lembur','2016-02-04 01:42:17');

/*Table structure for table `bonus_tetap` */

DROP TABLE IF EXISTS `bonus_tetap`;

CREATE TABLE `bonus_tetap` (
  `id_bonus` int(11) NOT NULL AUTO_INCREMENT,
  `jumlah` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_bonus`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `bonus_tetap` */

insert  into `bonus_tetap`(`id_bonus`,`jumlah`,`keterangan`) values (1,200000,'Uang Makan'),(17,25000,'Asuransi BPJS');

/*Table structure for table `gaji` */

DROP TABLE IF EXISTS `gaji`;

CREATE TABLE `gaji` (
  `id_gaji` int(7) NOT NULL AUTO_INCREMENT,
  `nip` varchar(9) NOT NULL,
  `gaji_pokok` int(8) NOT NULL,
  `tunjangan_anak` int(7) NOT NULL,
  `tunjangan_istri` int(7) NOT NULL,
  PRIMARY KEY (`id_gaji`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `gaji` */

insert  into `gaji`(`id_gaji`,`nip`,`gaji_pokok`,`tunjangan_anak`,`tunjangan_istri`) values (1,'1512301',750000,0,0),(2,'1512251',1200000,300000,500000),(3,'1512131',600000,0,0),(4,'1601011',860000,0,0),(5,'1512133',1000000,0,0),(6,'1601021',1500000,0,0),(7,'1601022',1000000,0,0),(8,'1512132',1000000,300000,0),(10,'1512142',1000000,300000,400000),(11,'1512135',1000000,0,0);

/*Table structure for table `gajian` */

DROP TABLE IF EXISTS `gajian`;

CREATE TABLE `gajian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` int(7) NOT NULL,
  `periode` date NOT NULL,
  `tgl` datetime NOT NULL,
  `total` int(8) NOT NULL,
  `potongan` int(8) NOT NULL,
  `bersih` int(8) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT 'terima|belum terima',
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `gajian` */

/*Table structure for table `izin` */

DROP TABLE IF EXISTS `izin`;

CREATE TABLE `izin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(9) DEFAULT NULL,
  `tgl_mulai` datetime DEFAULT NULL,
  `tgl_akhir` datetime DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `izin` */

/*Table structure for table `jabatan` */

DROP TABLE IF EXISTS `jabatan`;

CREATE TABLE `jabatan` (
  `id_jabatan` int(2) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(30) NOT NULL,
  PRIMARY KEY (`id_jabatan`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `jabatan` */

insert  into `jabatan`(`id_jabatan`,`nama_jabatan`) values (1,'Direktur'),(2,'Manajer Keuangan'),(3,'Manajer Pemasaran'),(4,'Marketing'),(5,'Administrasi'),(6,'Web Developer'),(7,'Programmer'),(8,'Teknisi'),(9,'Petugas Keamanan'),(10,'Petugas Kebersihan');

/*Table structure for table `option` */

DROP TABLE IF EXISTS `option`;

CREATE TABLE `option` (
  `id_option` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id_option`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `option` */

/*Table structure for table `pegawai` */

DROP TABLE IF EXISTS `pegawai`;

CREATE TABLE `pegawai` (
  `nip` varchar(9) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `id_user` int(5) NOT NULL,
  `id_jabatan` int(1) NOT NULL,
  `jenis_kelamin` char(1) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `status_nikah` tinyint(1) NOT NULL DEFAULT '0',
  `status_aktif` varchar(10) NOT NULL DEFAULT 'aktif',
  `jumlah_anak` int(2) DEFAULT '0',
  `jumlah_istri` int(1) DEFAULT '0',
  `alamat` text,
  `hp` varchar(15) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `no_ktp` varchar(30) DEFAULT NULL,
  `no_rekening` varchar(20) DEFAULT NULL,
  `nama_pemilik_rekening` varchar(50) NOT NULL,
  `nama_bank_rekening` varchar(50) NOT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pegawai` */

insert  into `pegawai`(`nip`,`nama`,`id_user`,`id_jabatan`,`jenis_kelamin`,`tanggal_lahir`,`status_nikah`,`status_aktif`,`jumlah_anak`,`jumlah_istri`,`alamat`,`hp`,`email`,`no_ktp`,`no_rekening`,`nama_pemilik_rekening`,`nama_bank_rekening`,`foto`,`date_add`,`date_modified`) values ('1512131','M Hablil Warid',17,7,'L','1994-05-16',0,'aktif',0,0,'sayang sayang, lombok barat &#39;','081907644213','m.hablilwarid@gmail.com','12100114459','100029389','M Hablil Warid','Bank Mandiri','gambar/1512131.jpg','2015-12-01 15:15:15','2016-01-29 02:37:36'),('1512132','Muhibbudin',6,4,'L','1988-07-13',1,'aktif',2,0,'mamben, lombok timur','087765249554','muhip.trulan@yahoo.com','1998128392938','100439192','Muhibbudin','Bank Mandiri','gambar/1512132.jpg','2015-12-02 13:13:13','2016-01-24 15:30:52'),('1512133','Agustina Nurfatmi',7,5,'P','0000-00-00',0,'aktif',0,0,'tanggak, gerunung, praya, lombok tengah','+6287865277508','agustina08@gmail.com','99911223874848','993882736','Agustina Nurfatmi','Bank Central Asia','gambar/1512133.jpg','2015-12-03 11:50:50','2016-01-02 05:36:40'),('1512134','Muliadi',8,8,'L','0000-00-00',0,'aktif',0,0,'lingsar, lombok barat','085129954413','muliadi@gmail.com','199232093293232','100232323223','','',NULL,'2015-12-03 12:01:00','2015-12-14 06:49:14'),('1512135','Linda Anggraini',9,5,'P','1994-10-24',0,'aktif',0,0,'cakra negara','08111928839','lindaang94@gmail.com','1994992882819','10203999299229','Linda Anggraini','Bank Central Asia','gambar/1512135.jpg','2015-12-04 12:01:00','2016-01-07 14:45:22'),('1512136','M Syukran',3,3,'L','0000-00-00',1,'aktif',2,1,'nuse, ganti, kec. paraya timur, lombok tengah.','087864331223','m.sukran@gmail.com','333221143','33929831001','','',NULL,'2015-12-13 15:30:08','2015-12-13 16:18:11'),('1512137','zian alfiansyah',4,2,'L','0000-00-00',0,'aktif',0,0,'ganti praya timur','93887409829','zian339@gmail.com','1119839844738','1193840983928','','',NULL,'2015-12-13 15:39:17','2015-12-13 15:39:17'),('1512141','M. Aidil Saputra',2,8,'L','0000-00-00',0,'aktif',0,0,'ganti, praya timur, lombok tengah','087883939391','aidilsaputra@gmail.com','11199388374','109283','','',NULL,'2015-12-14 09:01:02','2015-12-14 09:01:02'),('1512142','Harfandi',10,8,'L','1990-06-13',1,'aktif',1,1,'Pemepek','087865334717','harfandi@gmail.com','1990192839302','19938092839','Harfandi','Bank Rakyat Indonesia',NULL,'2015-12-14 10:32:02','2015-12-14 10:32:02'),('1512143','Puspita',11,4,'P','1992-06-24',0,'aktif',0,0,'Lombok TImur','081907166555','puput@gmail.com','199206240983928','100938928','Puspita','Bank Negara Indonesia',NULL,'2015-12-14 10:43:14','2015-12-14 10:43:14'),('1512201','Niluh Sukraeni',12,5,'P','1995-04-24',0,'aktif',0,0,'narmada','087864112748','niluh.sukraeni@gmail.com','919910039729390','10092800374','Niluh Sukraeni','Bank Mandiri',NULL,'2015-12-20 15:18:08','2015-12-20 15:18:08'),('1512251','Royni Lim',13,1,'L','2000-12-14',1,'aktif',3,1,'selagalas, mataram','081907955391','lim.royni@gmail.com','10928930982','9019823198','Royni Lim','Bank Central Asia',NULL,'2015-12-25 22:40:08','2015-12-31 00:55:17'),('1512301','Nurul Wahyuni Putri',14,5,'P','1992-07-29',0,'aktif',0,0,'sumbawa besar','087865444192','nurulwahyuni@gmail.com','1992098397847892','9839098293','Nurul Wahyuni Putri','Bank Rakyat Indonesia',NULL,'2015-12-30 21:47:38','2015-12-31 00:22:21'),('1601011','Sony Arya',15,1,'L','1994-07-14',0,'aktif',0,0,'sumbawa besar','087864291098','sony123@gmail.com','19943980982939','109839098298','Sony Arya','Bank Rakyat Indonesia','gambar/1601011.jpg','2016-01-01 19:09:36','2016-01-01 22:02:43'),('1601021','Muh Sukrillah',16,6,'L','1992-01-15',0,'aktif',0,0,'Nuse, Desa Ganti, Kec. Praya Timur, Kab. Lombok Tengah','087864381364','muh.sukrillah@gmail.com','199283901893793','7260000540','Muh Sukrillah','Bank Muamalat Indonesia','gambar/1601021.jpg','2016-01-02 15:28:05','2016-01-30 10:53:25');

/*Table structure for table `potongan` */

DROP TABLE IF EXISTS `potongan`;

CREATE TABLE `potongan` (
  `id_potongan` int(4) NOT NULL AUTO_INCREMENT,
  `id_gaji` int(7) NOT NULL,
  `jumlah` int(8) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `tgl` datetime DEFAULT NULL,
  PRIMARY KEY (`id_potongan`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `potongan` */

insert  into `potongan`(`id_potongan`,`id_gaji`,`jumlah`,`keterangan`,`tgl`) values (1,10,60000,'Potongan Telat','2016-01-24 11:37:09'),(2,10,100000,'Potongan Cicilan Hutang','2016-01-24 11:54:50'),(3,6,50000,'Potongan Absensi','2016-01-24 12:11:25'),(4,1,25000,'Potongan Absensi','2016-01-24 01:26:44'),(5,8,25000,'Potongan Absensi','2016-01-24 04:42:41'),(6,10,25000,'Potongan Telat','2016-02-04 11:02:40'),(8,3,50000,'Potongan Telat','2016-02-04 01:28:09');

/*Table structure for table `tunjangan_lain` */

DROP TABLE IF EXISTS `tunjangan_lain`;

CREATE TABLE `tunjangan_lain` (
  `id_tunjangan` int(4) NOT NULL AUTO_INCREMENT,
  `id_gaji` int(7) NOT NULL,
  `jumlah_tunjangan` int(8) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `tgl` datetime DEFAULT NULL,
  PRIMARY KEY (`id_tunjangan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tunjangan_lain` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` int(1) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`username`,`password`,`level`) values (2,'aidil.saputra','202cb962ac59075b964b07152d234b70',4),(3,'sukran','827ccb0eea8a706c4c34a16891f84e7b',4),(4,'zian','b0baee9d279d34fa1dfd71aadb908c3f',2),(6,'muhip','827ccb0eea8a706c4c34a16891f84e7b',4),(7,'agustina','827ccb0eea8a706c4c34a16891f84e7b',4),(8,'muliadi','827ccb0eea8a706c4c34a16891f84e7b',4),(9,'lindabeng2','827ccb0eea8a706c4c34a16891f84e7b',4),(10,'harfandi','827ccb0eea8a706c4c34a16891f84e7b',4),(11,'puput','202cb962ac59075b964b07152d234b70',4),(12,'niluh.sukraeni','827ccb0eea8a706c4c34a16891f84e7b',4),(13,'royni.lim','202cb962ac59075b964b07152d234b70',1),(14,'nurul','202cb962ac59075b964b07152d234b70',4),(15,'sony123','202cb962ac59075b964b07152d234b70',4),(16,'muh.sukrillah','202cb962ac59075b964b07152d234b70',1),(17,'warid','202cb962ac59075b964b07152d234b70',4);

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id_role` bigint(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `role_title` varchar(50) NOT NULL,
  KEY `id_role` (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `user_role` */

insert  into `user_role`(`id_role`,`role_name`,`role_title`) values (1,'administrator','Administrator'),(2,'hrd_manager','HRD Manager'),(3,'administrasi','Administrasi'),(4,'pegawai','Pegawai');

/*Table structure for table `user_role_capabilities` */

DROP TABLE IF EXISTS `user_role_capabilities`;

CREATE TABLE `user_role_capabilities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_role` bigint(20) NOT NULL,
  `capability` varchar(50) NOT NULL,
  `value` int(1) NOT NULL,
  `description` varchar(60) NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `user_role_capabilities` */

insert  into `user_role_capabilities`(`id`,`id_role`,`capability`,`value`,`description`) values (1,1,'read_pegawai',1,'Lihat Data Pegawai'),(2,1,'create_pegawai',1,'Tambah Data Pegawai'),(3,1,'update_pegawai',1,'Ubah Data Pegawai'),(4,1,'delete_pegawai',1,'Hapus Data Pegawai'),(5,1,'read_gaji',1,'Lihat Data Gaji'),(6,1,'create_gaji',1,'Tambah Data Gaji'),(7,1,'update_gaji',1,'Ubah Data Gaji'),(8,1,'delete_gaji',1,'Hapus Data Gaji'),(9,1,'create_bonus',1,'Tambah Bonus'),(10,1,'update_bonus',1,'Ubah Bonus'),(11,1,'delete_bonus',1,'Hapus Bonus'),(12,1,'read_bonus',1,'Lihat Bonus'),(13,1,'create_potongan',1,'Tambah Potongan'),(14,1,'update_potongan',1,'Ubah Potongan'),(15,1,'delete_potongan',1,'Hapus Potongan'),(16,1,'read_potongan',1,'Lihat Potongan');

/*Table structure for table `user_sessions` */

DROP TABLE IF EXISTS `user_sessions`;

CREATE TABLE `user_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_data` text NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user_sessions` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
