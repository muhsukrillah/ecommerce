<?php
/**
 * Dynamic body class library 
 *
 * @package 		Sasak Framework
 * @copyright		Sasak Framework
 * @author 			Muh Sukrillah
 * @since 			1.0
 */
namespace sasak\lib;

class Body_Class {
	/** 
	 * class name
	 * @var 	array 	_class_name
	 * @access 	private
	 */
	private $_class_name = array();
	/**
	 * removed class names
	 * @var 	array 	_removed_class
	 * @access 	private
	 */
	private $_removed_class = array();
	/**
	 * constructor
	 */
	function __construct(){
		$this->_reset();
	}
	/**
	 * reset all class
	 * @access 		public
	 * @param 		null
	 * @return 		object instance
	 */
	public function _reset(){
		$this->_class_name = array();
		$this->_removed_class = array();
		return $this;
	}
	/**
	 * add class name
	 * @access 		public
	 * @param 		string class_name
	 * @return 		object instance
	 */
	public function add_class($class_name = NULL){
		if (!isset($class_name) || empty($class_name)) return;
		
		$this->_class_name[] = $class_name;
		return $this;
	}
	// alias
	public function add($class_name = NULL){
		return $this->add_class($class_name);
	}
	/**
	 * remove the class name
	 * @access 		public
	 * @param 		string class_name
	 * @return 		object instance
	 */
	public final function remove_class($class_name = NULL){
		if(!isset($class_name) || empty($class_name)) return $this;

		$this->_removed_class[] = $class_name;
		return $this;
	}
	/**
	 * check if it was removed
	 *
	 * @access 		private
	 * @param 		string class_name
	 * @return 		boolean
	 */
	private function _is_removed_class($class_name = NULL){
		if (!isset($class_name)) {
			return $this;
		}
		return in_array($class_name, $this->_removed_class);
	}
	/**
	 * get the class names
	 *
	 * @access 	public
	 * @param 	bool 	echo 	auto echo / echo the class name output
	 * @return 	string 	
	 */
	public function get($echo = false){
		$class = $this->_class_name;
		foreach($class as $key => $value){
			if($this->_is_removed_class($value)){
				unset($class[$key]);
				continue;
			}
		}
		$output = implode(' ', $class);
		if($echo){
			echo $output;
		}else{
			return $output;
		}
	}
	/**
	 * show the class names
	 * @access 	public
	 * @param 	null
	 * @return 	string
	 */
	public function show(){
		$this->get(true);
	}
}