<?php
/**
 * @description		Title Library
 * Dibuat Oleh 		Muh Sukrillah
 * Url 				http://www.sukrillah.xyz
 * Versi			1.0
 */
namespace sasak\lib;

class lib_title {
	private $title = array();
	private $separator = '&laquo;';
	
	function __construct(){
		$this->_reset();
		$title = 'Sistem Informasi';
		if(defined('SITE_TITLE')){
			$title = SITE_TITLE;
		}
		$this->add_root($title);
	}
	
	function _reset(){
		$this->title = array();
		return $this;
	}
	
	function set_separator($sep = '&laquo;'){
		$this->separator = $sep;
		return $this;
	}
	
	function set($current = '', $root = '', $separator = '&laquo;'){
		if(!empty($separator)){
			$this->set_separator($separator);
		}
		
		if(!empty($root)){
			$this->add_root($root);
		}
		
		if(!empty($current)){
			$this->add($current);
		}
		return $this;
	}
	
	function add(){
		$args = func_get_args();
		$args = array_reverse($args);
		foreach($args as $key => $title){
			$title = trim($title);
			$title = str_replace('\n', "", $title);
			$this->title[] = $title;
		}
		return $this;
	}
	
	function add_root($title = ''){
		$this->title[0] = $title;
		return $this;
	}
	
	function set_root($title = ''){
		return $this->add_root($title);
	}
	
	function get_root(){
		$i = count($this->title) - 1;
		return $this->title[$i];
	}
	
	function get(){
		$this->title = array_reverse($this->title);
		$sep = ' '. $this->separator .' ';
		$a = implode($sep, $this->title);
		return $a;
	}
	
	function show(){
		echo $this->get();
	}
}