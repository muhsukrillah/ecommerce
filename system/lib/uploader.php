<?php
/**
 * Uploader Class
 * Untuk menangani upload file
 *
 * Dibuat Oleh 				: Muh Sukrillah
 * Url 						: http://www.sukrillah.xyz
 * Versi 					: 1.0
 */
namespace sasak\lib;
use \sasak\Lang;

class Uploader {
	/**
	 * data konfigurasi
	 * @var 	_config
	 * @type 	array
	 * @access 	private
	 */
	private $_config = [
		'upload_path'		=> '', // lokasi upload
		'new_name' 			=> '', // nama baru untuk file
		'multiple'			=> FALSE,
		'max_upload' 		=> 10, // 10 file
		'max_size'			=> 10, // 10 MB
		'is_image'			=> FALSE,
		'allowed_ext'		=> 'jpg,png,gif' // ekstensi file yang diijinkan

	];
	/** CONFIG **/
	/**
	 * is multiple upload
	 * @var 		_is_multiple
	 * @type 		boolean
	 * @access 		private
	 * @default 	FALSE
	 */
	private $_is_multiple = FALSE;
	/**
	 * Data source / data file yang akan diupload
	 * @var 		_source
	 * @type 		array
	 * @access 		private
	 * @default 	an empty array []
	 */
	private $_source = [];
	/**
	 * Upload data
	 * @var 		_data
	 * @type 		array
	 * @access 		private
	 * @default  	an empty array []
	 */
	private $_data = [];
	/**
	 * Untuk menampung pesan
	 * @var 		_message
	 * @type 		string
	 * @access 		private
	 * @default 	an empty string ''
	 */
	private $_message = '';
	/** ---------------------------------- **/
	/**
	 * Konstruktor
	 * 
	 * @param 		array 	$config => konfigurasi
	 * @access 		public
	 * @return 		null
	 *
	 */
	function __construct($config = []){
		if(is_array($config) && !empty($config)){
			$this->initialize($config);
		}
	}
	/**
	 * initialize configuration
	 *
	 * @access 		public
	 * @param 		array 	$config
	 * @return 		object
	 */
	public function initialize($config = []){
		// multiple upload
		if(isset($config['multiple']) && $config['multiple'] === TRUE){
			// jika multiple upload
			$this->_is_multiple = TRUE;
		}
		if(isset($config['field_name']) && !empty($config['field_name'])){
			$this->set_field_name($config['field_name']);
		}
		// set konfigurasi
		$this->_config = $config;
		return $this;
	}
	/**
	 * untuk mengetahui apakah proses upload lebih dari 1
	 *
	 * @access 		private
	 * @param 		null
	 * @return 		boolean
	 */
	private function is_multiple(){
		return $this->_is_multiple;
	}
	/**
	 * untuk memberikan source data dari array
	 *
	 * @access 		public
	 * @param 		array
	 * @return 		object
	 */
	public function set_source($source = []){
		$this->_source = $source;
		return $this;
	}
	/**
	 * untuk memberikan source dari global data array $_FILES
	 *
	 * @access 		public
	 * @param 		string (array name)
	 * @return 		object
	 */
	public function set_field_name($name = 'file'){
		if(empty($name)) return $this;
		if(!isset($_FILES[$name])){
			Error("Lib\Uploader : Sumber berkas tidak tersedia..");
		}
		$source = [];
		if($this->is_multiple()){
			foreach($_FILES[$name] as $key => $value) {
				if(!in_array($key, ['tmp_name','name','size','type', 'error'])) {
					Error("[Lib\Uploader][Multiple Upload] : Data berkas tidak valid..  ". print_r(array_keys($_FILES[$name]), 1));
				}
			}
			for($i = 0, $j = count($_FILES[$name]['name']); $i<$j; $i++){
				$source[$i] = [
					'name' => $_FILES[$name]['name'][$i],
					'tmp_name' => $_FILES[$name]['tmp_name'][$i],
					'size' => $_FILES[$name]['size'][$i],
					'type' => $_FILES[$name]['type'][$i],
					'error' => $_FILES[$name]['error'][$i]
				];
			}
		}else{
			if(!isset($_FILES[$name]['tmp_name']) && !isset($_FILES[$name]['name']) && !isset($_FILES[$name]['size']) && !isset($_FILES[$name]['type'])){
				Error("Lib\Uploader : Data berkas tidak valid..");
			}
			$source = $_FILES[$name];
		}
		return $this->set_source($source);
	}
	/**
	 * untuk memberikan pesan
	 *
	 * @access 		private
	 * @param 		string 	$message => pesan
	 * @return 		object
	 */
	private function set_message($message = ''){
		$this->_message = $message;
		return $this;
	}
	/**
	 * Untuk mengambil pesan 
	 *
	 * @access 		public
	 * @param 		null
	 * @return 		string
	 */
	public function get_message(){
		return $this->_message;
	}
	/**
	 * untuk mengambil data berkas yang sudah diunggah
	 *
	 * @access 		public
	 * @param 		null
	 * @return 		array
	 */
	public function get_data(){
		return $this->_data;
	}
	/**
	 * Untuk melakukan proses upload
	 *
	 * @access 		public
	 * @param 		array 	$args => opsi
	 * @return 		boolean
	 */
	public function upload(){
		// ekstarak variable $args
		// untuk mengambil opsi-opsi
		if(is_array($this->_config) && !empty($this->_config)){
			extract($this->_config);
		}
		// cek apakah lokasi upload sudah diset
		if(!isset($upload_path) || empty($upload_path)){
			$this->set_message('Lokasi upload belum diset.. [upload_path]');
			return FALSE;
		}

		$upload_path = rtrim($upload_path, '/') .'/';

		if(isset($new_name) && !empty($new_name)){
			$new_name = str_replace(' ', '_', $new_name);
		}
		if(!preg_match('#^([a-zA-Z0-9\.\-\_]+)$#', $new_name)){
			$this->set_message('Nama baru file tidak valid.. [new_name]');
			return FALSE;
		}

		$uploaded = 0;
		$success = FALSE;
		$data = [];
		if($this->is_multiple()){
			$i = 1;
			foreach($this->_source as $key => $value){		
				// tmp_name / berkas
				$tmp_name = $value['tmp_name'];
				// new name / nama baru
				$orig_name = $value['name'];
				// extensi file
				$ex_a = explode('.', $orig_name);
				$extention = end($ex_a);

				if(!isset($new_name) || empty($new_name)){
					$_new_name = $orig_name;
				}else{
					$_new_name = $new_name .'_'. $i .'.'. $extention;
				}
				$new_source = $upload_path . strtolower($_new_name);
				if(!is_dir(dirname($new_source))){
					@mkdir(dirname($new_source), 0777, TRUE);
				}
				// nama file baru
				$data['new_name'] = $_new_name;
				// nama original
				$data['orig_name'] = $orig_name;
				// ekstensi
				$data['extention'] = '.'. $extention;
				// raw name
				$data['raw_name'] = str_replace($data['extention'], '', $_new_name);
				// full path
				$data['full_path'] = $new_source;
				// mime_type
				$data['mime_type'] = $value['type'];	
				$upload = $this->do_upload($tmp_name, $new_source);
				if($upload){
					$uploaded++;
					$this->_data[] = $data;
				}
				$i++;
			}
			$success = $uploaded;
		}else{
			// tmp_name / berkas
			$tmp_name = $value['tmp_name'];
			// new name / nama baru
			$orig_name = $this->_source['name'];
			// extensi file
			$ex_a = explode('.', $orig_name);
			$extention = end($ex_a);

			if(!isset($new_name) || empty($new_name)){
				$new_name = $orig_name;
			}else{
				$new_name = $new_name .'.'. $extention;
			}
			$new_source = $upload_path . strtolower($new_name);
			if(!is_dir(dirname($new_source))){
				@mkdir(dirname($new_source), 0777, TRUE);
			}
			// nama file baru
			$data['new_name'] = $new_name;
			// nama original
			$data['orig_name'] = $orig_name;
			// ekstensi
			$data['extention'] = '.'. $extention;
			// raw name
			$data['raw_name'] = str_replace($data['extention'], '', $new_name);
			// full path
			$data['full_path'] = $new_source;
			// lakukan proses upload file
			$upload = $this->do_upload($tmp_name, $new_source);
			if($upload){
				$success = TRUE;
				$uploaded = 1;
				$this->_data = $data;
			}
		}
		return $success;
	}
	/**
	 * proses upload helper
	 * 
	 * @access 		private
	 * @param 		string 	$tmp_name 
	 * @param 		string 	$new_source
	 * @return 		boolean
	 */
	private function do_upload($tmp_name = '', $new_source = ''){
		if(empty($tmp_name)) return FALSE;
		if(!is_string($tmp_name)) return FALSE;

		$upload = move_uploaded_file($tmp_name, $new_source);
		if(!$upload){
			$this->set_message("Berkas gagal diunggah.. [upload]");
			return FALSE;
		}
		$this->set_message('Berkas berhasil diunggah.. [upload]');
		return TRUE;
	}
}