<?php
/**
 * Breadcrumb
 *
 * Dibuat Oleh 		: Muh Sukrillah
 * Url 				: http://www.sukrillah.xyz
 * Versi 			: 1.0
 */
namespace sasak\lib;

class Breadcrumb {
	private $_trail;
	private $_template;
	private $_breadcrumb;
	private $_removed_trail;
	private $_use_separator = TRUE;
	var $separator;
	
	function __construct(){
		$this->reset();
		$this->template();
		
		$this->_breadcrumb = '';
	}

	function set_use_separator($use = TRUE){
		$this->_use_separator = $use;
	}
	
	function reset(){
		$this->_trail = array();
		$this->_removed_trail = array();
		$this->_use_separator = TRUE;
	}
	
	function template(){
		$this->_template = array(
			'wrap_start'	=> '<ul class="page-breadcrumb breadcrumb">',
			'items' 		=> '<li><a href="{link}" title="{title}">{icon}{title}</a></li>',
			'separator'		=> '<li class="separator">{separator}</li>',
			'active_item'	=> '<li><span class="title">{icon}{title}</span></li>',
			'wrap_end' 		=> '</ul>'
		);
	}
	
	function _set_template($template_args = '', $item_value = ''){
		if(isset($this->_template[$template_args])){
			$this->_template[$template_args] = $item_value;
			//return str_replace($item_key, $item_value, $this->_template[$template_args]);
		}
	}
	
	function get_template($template_args = ''){
		if(empty($template_args)) return;
		
		if(isset($this->_template[$template_args]))
			return $this->_template[$template_args];
		
		return;
	}
	
	function set_separator($separator = ''){
		if(empty($separator))
			$separator = '&raquo;';
			
		$this->separator = $separator;
	}
	
	function add($title = "", $link = "", $icon_id = ""){
		if(is_array($title) && !empty($title)){
			$this->_trail[] = $title;
		}else{
			$this->_trail[sizeof($this->_trail)] = array(
				"title" => $title, 
				"link" => $link, 
				"icon" => $icon_id
			);
		}
		return $this;
	}
	
	function remove($index = NULL){
		if(is_null($index)) return;
		
		if(isset($this->_trail[$index]))
			unset($this->_trail[$index]);
		
		return $this;
	}
	
	private function _has_removed($index = NULL){
		if(is_null($index)) return;
		
		if(in_array($index, $this->_removed_trail)){
			//unset($this->_trail[$index]);
			return TRUE;
		}else{		
			return TRUE;
		}
	}
	
	function generate( $separator = '&raquo;' ){
		$this->set_separator($separator);
		
		$this->_breadcrumb = $this->get_template('wrap_start');
		for($i = 0, $j = sizeof($this->_trail); $i < $j; $i++){			
			if(($i+1) < $j){
				if(preg_match('#\{icon\}#', $this->_template['items'])){
					$this->_breadcrumb .= preg_replace('#\{icon\}#', $this->_trail[$i]['icon'], $this->_template['items']);
				}
				if($this->_use_separator && preg_match('#\{separator\}#', $this->_template['separator'])){
					$this->_breadcrumb .= preg_replace('#\{separator\}#', $this->separator, $this->_template['separator']);
				}
			}else{
				//if(preg_match('#\{icon\}#', $this->_template['active_item'])){
					$this->_breadcrumb .= preg_replace('#\{icon\}#', $this->_trail[$i]['icon'], $this->_template['active_item']);
				//}
			}
			//if(preg_match('#\{title\}#', $this->_breadcrumb)){
				$this->_breadcrumb = preg_replace('#\{title\}#', $this->_trail[$i]['title'], $this->_breadcrumb);
			//}
			//if(preg_match('#\{link\}#', $this->_breadcrumb)){
				$this->_breadcrumb = preg_replace('#\{link\}#', $this->_trail[$i]['link'], $this->_breadcrumb);
			//}						
		}
		$this->_breadcrumb .= $this->get_template('wrap_end');
		return $this->_breadcrumb;
	}

	function get($separator = ''){
		return $this->generate($separator);
	}
	
	function show( $separator = '' ){
		echo $this->generate( $separator );
	}
}

/*
<ul class="page-breadcrumb breadcrumb">
	<li>
		<i class="fa fa-home"></i>
		<a href="http://localhost/sasak-media.com/admin.html" title="home">home</a>
		<i class=""></i>
	</li>
	<li>
		<i class="fa fa-tes"></i>
		<a href="http://localhost/sasak-media.com/admin//index/tes.html" title="tes">tes</a>
		<i class=""></i>
	<li>
</ul>
*/
