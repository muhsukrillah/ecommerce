<?php
/*
 * File 		:	functions.php
 * Lokasi File	: 	/lib/functions.php
 * Pembuat		:	RielMelfost / Muh. Sukrillah
 * URL Pembuat	: 	http://www.rielmelfost.net
 * Versi		:	1.0
 * Lisensi		: 	Open Source
 */
 
function koneksi_db($host="localhost", $user="root", $pass="", $db="db_barang"){
	$koneksi = mysql_connect($host, $user, $pass);
	if(! $koneksi)
		die('<h1>Tidak dapat terkoneksi dengan database..</h1>');
	else
		mysql_select_db($db);
}

function tampil($halaman = ""){
	$file = $halaman . ".php";
	$dir = "page/";
	if(! file_exists($dir . $file)){
		echo '<h1 class="error404">Ups, Halaman tidak ditemukan..</h1>';
	}else{
		include_once($dir . $file);
	}
}

function processor($file = ""){
	if(empty($file))
		return;
		
	$processor_file = $file . ".php";
	$dir = "proses/";
	if( file_exists($dir . $processor_file) ){
		include_once($dir . $processor_file);
	}
}

function tgl_sekarang($format = "l, d M Y"){
	$hari = array(
		"Sunday" => "Ahad",
		"Monday" => "Senin",
		"Tuesday" => "Selasa",
		"Wednesday" => "Rabu",
		"Thursday" => "Kamis",
		"Friday" => "Jum'at",
		"Saturday" => "Sabtu"
	);
	
	$bln = array(
		"Jan" => "Januari",
		"Feb" => "Februari",
		"Mar" => "Maret",
		"Apr" => "April",
		"May" => "Mei",
		"Jun" => "Juni",
		"Jul" => "Jul",
		"Aug" => "Agustus",
		"Sep" => "September",
		"Oct" => "Oktober",
		"Nov" => "November",
		"Des" => "Desember"
	);
	
	$tgl = date($format, time());
	
	foreach($hari as $key => $value){
		$tgl= str_replace($key, $value, $tgl);
	}
	
	foreach($bln as $key => $value){
		$tgl= str_replace($key, $value, $tgl);
	}
	return $tgl;
}
/*
 * Fungsi untuk menampilkan pesan pada form
 */
function pesan(){
	global $pesan;
	if(isset($pesan)){
		return '<div class="'.$pesan['type'].'">'.$pesan['text'].'</div>';
	}
}
/*
 * Fungsi untuk membuat pesan error
 */
function error($pesan_text = ""){
global $pesan;
	if(empty($pesan_text)) return;
	
	$pesan['type'] = "error";
	$pesan['text'] = $pesan_text;
}
/*
 * Fungsi untuk membuat pesan sukses
 */
function sukses($pesan_text = ""){
global $pesan;
	if(empty($pesan_text)) return;
	
	$pesan['type'] = "sukses";
	$pesan['text'] = $pesan_text;
}
/*
 * Fungsi untuk menghancurkan variable
 */
function reset_var($var = ""){
	if(empty($var)) return;
	
	if(is_array($var)){
		foreach($var as $var_name){
			if( isset($_POST[$var_name]))
				unset($_POST[$var_name]);
		}
	}else{
		if( isset($_POST[$var]))
			unset($_POST[$var]);
	}
}
/* 
 * Fungsi untk mencari ekstensi file
 */
function get_ekstensi($str = ''){
	if(empty($str))
		return;
	if(strpos($str, ".")){
		$ex = explode(".", $str);
		$j = count($ex);
		$ekstensi = $ex[$j - 1];
		return $ekstensi;
	}
}

function cek_login(){
session_start();

if(! isset($_SESSION['user_data']) ){
	@header("location: login.php");
	exit;
}

}

function amankan_data($data = "", $new_func = ""){
	$func_name = array(
		'mysql_real_escape_string'
	);
	
	if(!empty($new_func) && function_exists($new_func)){
		array_push($func_name, $new_func);
	}
	
	foreach($func_name as $function){
		$data .= $function($data);
	}
	return $data;
}
?>