<?php
/**
 * Cpatcha library
 * Dibuat oleh 
 * @author              Muh. Sukrillah
 * @url                 http://www.sukrillah.xyz
 * @version             1.0
 */
namespace sasak\lib;

class Captcha {
    /**
     * initialized state
     * @var     bool    _initialized
     * @access  private
     * @default bool false
     */
    private $_initialized = FALSE;
    /**
     * session name
     * @var     string  _session_name
     * @access  private
     * @default 'sasak_capthcha'
     */
    private $_session_name = 'sasak_capthcha';
    /**
     * captcha label
     * @var     string  _label
     * @access  private
     * @default null
     */
    private $_label;
    /**
     * captcha value
     * @var     mixed   _value
     * @access  private
     * @default null
     */
    private $_value;
    /**
     * constructor
     */
    function __construct($args = []){
        if(isset($args) && is_array($args)){

        }
    }
    /**
     * initializing
     * @access  public
     * @param   array args
     * @return  object instance
     */
    public function init($args = []){

        $this->_initialized = TRUE;

        return $this;
    }
    /**
     * set the label
     * @access  public
     * @param   string  label
     * @return  object  instance
     */
    public function set_label($label = ''){
        $this->_label = $label;
        \sasak\Session::set_flash_data($this->_session_name, $label);
        return $this;
    }
    /**
     * set the value
     * @access  public
     * @param   mixed   value
     * @return  object  instance
     */
    public function set_value($value = NULL){
        $this->_value = $value;
        \sasak\Session::set_flash_data($this->_session_name, $value);
        return $this;
    }
    /**
     * get the captcha label
     * @access  public
     * @param   null
     * @return  string  label
     */
    public function get_label(){
        $label = \sasak\Session::get_flash_data($this->_session_name, FALSE);
        if($label) {
            return $label;
        }
        return 0;
    }
    /**
     * the label value
     * @access  public
     * @param   null
     * @return  mixed   value
     */
    public function get_value(){
        $value = \sasak\Session::get_flash_data($this->_session_name, FALSE);
        if($value) {
            eval('$total = '. $value .';');
            return $total;
        }
        return 0;
    }
    /**
     * generator calculator captcha
     * 
     * @access  public
     * @param   null
     * @return  object instance
     */
    public function generate_calculator(){
        $number = [1,2,3,4,5,6,7,8,9];
        shuffle($number);
        $operator = [' * ', ' - ',' + '];
        shuffle($operator);
        $val1 = $number[mt_rand(1, 8)];
        $val2 = $number[mt_rand(1, 3)];
        $label = $val1 . $operator[0] . $val2;
        eval('$total = '. $val1. trim($operator[0]) .$val2 .';');
        $this->set_label($label);
        $this->set_value($total);

        $this->_initialized = TRUE;
        return $this;
    }
    /**
     * verify captcha value
     * @access  public
     * @param   mixed input
     * @return  bool
     */
    public function verify($input = NULL){
        $value = $this->get_value();
        eval('$total = '. $value .';');
        return ($total == $input);
    }
}