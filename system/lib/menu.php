<?php
/**
 * Admin Menu Librari
 *
 * Dibuat Oleh 				: Muh Sukrillah
 * Url 						: http://www.sukrillah.xyz
 * Versi 					: 1.0
 */
namespace sasak\lib;

class Menu {
	public $menu;
	public $child;
	var $attr;
	var $active_menu;
	var $active_child_menu;
	
	function __construct(){
		
		$this->reset();
		$this->attr = " ";
	}
	
	function set_active_menu($menu_id = ""){
		$this->active_menu = $menu_id;
		return $this;
	}
	
	function set_active_child_menu($menu_id = ""){
		$this->active_child_menu = $menu_id;
		return $this;
	}
	
	function reset(){
		$this->menu = array();
		$this->child = array();
		return $this;
	}
	
	function add_attr($attr = '', $value = ''){
		if(is_array($attr)){
			foreach($attr as $key => $val){
				$this->attr .= $key .'="'. $val .'" ';
			}
		}else{
			if(!empty($attr) && !empty($value)){
				$this->attr .= $attr .'="'. $value .'"';
			}
		}
		return $this;
	}

	function add(){
		$args = func_get_args();
		$ob =& $this;
		return @call_user_method_array('add_menu', $ob, $args);
	}
	
	function add_menu($id = "", $anchor = "", $icon = "", $attr = array(), $position = 1){
		$this->menu[] = array(
			"id" 		=> $id, 
			"anchor"	=> $anchor, 
			"attr" 		=> $attr, 
			"icon" 		=> $icon
		);
		return $this;
	}
	
	function add_child($id = "", $anchor="", $icon = "", $attr = array(), $parent = ""){
		$this->child[] = array(
			"id" 		=> $id, 
			"anchor" 	=> $anchor, 
			"attr" 		=> $attr,  
			"icon"		=> $icon,
			"parent"	=> $parent
		);
		return $this;
	}
	
	function have_child($id = ''){
		$return = FALSE;
		foreach($this->child as $key => $child){
			if(isset($child['parent']) && $child['parent'] == $id){
				$return = TRUE;
				break;
			}
		}
		return $return;
	}
	
	function fetch_icon($icon=array()){
		if(empty($icon) or !$icon) return;
		
		$output = "";
		if( is_string($icon) ){
			$icon_name = $icon;
			unset($icon);
			$icon = array(
				'icon_type' => 'font',
				'icon_name' => $icon_name
			);
		}
		
		if(is_array($icon)){
			if(isset($icon['icon_type'])){
				switch($icon['icon_type']){
					case "image":
						$output.='<i class="sm-icon" style="background-image:'. (isset($icon['src']) ? $icon['src'] : '') .'">&nbsp;</i>';
					break;
					case "font":
						$output.='<i class="'. ( isset($icon['icon_name']) ? $icon['icon_name'] : 'no-icon') .'"></i>';
					break;
					default:
						$output.='<i class="'. (isset($icon['icon_name']) ? $icon['icon_name'] : 'no-icon') .'"></i>';
				}
			}
			return $output;
		}
		return;
	}
	
	function is_active_menu($menu_id = ""){
		if($menu_id == $this->active_menu)
			return TRUE;
		else
			return FALSE;
	}
	
	function is_active_child_menu($menu_id = ""){
		if($menu_id == $this->active_child_menu)
			return TRUE;
		else
			return FALSE;
	}
	
	function get_lists(){
		$out = '';
		for($i= 0, $j=sizeof($this->menu); $i<$j; $i++){
			//if(empty($this->menu[$i]['parent'])){
			$menu_id = $this->menu[$i]['id'];

			if(isset($this->menu[$i]['id']) && $this->menu[$i]['id'] == 'custom'){
				$out.='<li>';
				$out.= $this->menu[$i]['anchor'];
				$out.='</li>';
			}else{
				$out.='<li id="menu_'.strtolower($this->menu[$i]['id']).'" ';
				if($this->is_active_menu($this->menu[$i]['id'])){
					$out.='style="display:block;" class="active ';					
				}else{
					$out.='class="';
				}
				if($this->have_child($this->menu[$i]['id'])){
					$out.= 'treeview"';
				}else{
					$out.= 'treeview"';
				}
				$out.='>';
				
				//$out.= '<div id="menu-arraow">&nbsp;</div>';
				$out.= '<a ';
				if($this->have_child($this->menu[$i]['id'])){
					for($x = 0, $y = count($this->child); $x<$y; $x++){
						if(
							isset($this->child[$x]) && 
							($this->child[$x]['parent'] == $this->menu[$i]['id'])
						){
							if(is_array($this->menu[$i]['attr'])){						
								foreach($this->menu[$i]['attr'] as $key => $value){
									$out.=' '.$key.'="'.$value.'" ';
								}
							}
							
							$out.='>'. $this->fetch_icon($this->menu[$i]['icon']) .' ';
							$out.='<span class="title">'.$this->menu[$i]['anchor']." </span>\n";							
							break;	
						}else{
							if(is_array($this->menu[$i]['attr'])){						
								foreach($this->menu[$i]['attr'] as $key => $value){
									$out.=' '.$key.'="'.$value.'" ';
								}
							}
							
							$out.='>'. $this->fetch_icon($this->menu[$i]['icon']) .' ';
							$out.='<span class="title">'.$this->menu[$i]['anchor']."</span>\n";
							break;
						}
					}
					$out.='<i class="fa fa-angle-left pull-right"></i>';
				}else{
					if(is_array($this->menu[$i]['attr'])){
						foreach($this->menu[$i]['attr'] as $key => $value){
							$out.=' '.$key.'="'.$value.'" ';
						}
					}
						
					$out.='>'. $this->fetch_icon($this->menu[$i]['icon']) .' ';
					$out.='<span class="title">'.$this->menu[$i]['anchor']."</span>\n";
				}
				$out.='</a>';
				if(($i+1) <= $j){
					
					for($x = 0, $y = count($this->child); $x<$y; $x++){
						if(isset($this->child[$x]) && $this->child[$x]['parent'] == $this->menu[$i]['id']){
							$out.= '<ul id="menu_'. $menu_id . '-child" class="child treeview-menu">';
							break;
						}
					}
					
					for($a = 0, $b = sizeof($this->child); $a < $b; $a++){
						if($this->child[$a]['parent'] == $menu_id){
							$out.= '<li id="'. strtolower($this->child[$a]['id']) .'" ';
							if($this->is_active_child_menu($this->child[$a]['id'])){
								$out.= 'class="active"';
							}							
							$out.= '><a ';
							if(is_array($this->child[$a]['attr'])){
								foreach($this->child[$a]['attr'] as $k => $v){
									$out.= $k .'="'.$v.'"';
								}
							}
							$out.= '><i class="fa fa-angle-right"></i> '.$this->child[$a]['anchor'].'</a></li>';
						}
					}
					
					for($x = 0, $y = count($this->child); $x<$y; $x++){
						if(isset($this->child[$x]) && $this->child[$x]['parent'] == $menu_id){
							$out.= '</ul>';
							break;
						}
					}
				}
				
				$out.='</li>'."\n";
			}
		}
		return $out;
	}

	function get_menu( $attr = '', $value = '', $wrapp = TRUE){
		
		$this->add_attr($attr, $value);
		
		//exit(print_r($this->menu, TRUE));
		$out = '';
		if($wrapp){
			$out .= '<ul '.$this->attr.'>';
		}

		$out.= $this->get_lists();

		if($wrapp){
			$out.= "</ul>\n";
		}
		//exit($out);
		return $out;
	}
}
/**
 * Berkas 			: admin_menu.php
 * Lokasi 			: /lib/admin_menu.php
 *
 * Dibuat Oleh 		: Muh. Sukrillah
 * Url 				: http://www.sukrillah.xyz
 */