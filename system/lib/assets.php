<?php
/**
 * Assets Manajement class
 * 
 * Dibuat oleh              : muh. sukrillah
 * Url                      : http://www.sukrillah.xyz
 * Versi                    : 1.0
 */
namespace sasak\lib;
class Assets {
    /**
     * asset path
     */
    var $path;
    /**
     * asset url
     */
    var $base_url;
    /**
     * Javascript resources queue
     * @type        Array
     * @access      private
     */
    private $jsQueue = [];
    /**
     * Javascript resources
     * @type        Array
     * @access      private
     */
    private $js = [];
    /**
     * CSS resources queue
     * @type        Array
     * @access      private
     */
    private $cssQueue = [];
    /**
     * CSS Resources
     * @type        Array
     * @access      private
     */
    private $css = [];
    /**
     * construcor
     */

    function __construct($configs = []){
        extract($configs);

        if(isset($path)){
            $this->setPath($path);
        }

        if(isset($base_url)){
            $this->setBaseUrl($base_url);
        }
    }
    /**
     * reset all js queue
     * 
     * @access      public
     * @param       null
     * @return      object
     */
    public function resetJs(){
        $this->jsQueue = [];
        $this->js = [];
        return $this;
    }
    /**
     * reset all css queue
     *
     * @access      public
     * @param       null
     * @return      object
     */
    public function resetCss(){
        $this->cssQueue = [];
        $this->css = [];
        return $this;
    }
    /**
     * set assets path
     *
     * @access      public
     * @param       string path
     * @return      object
     */
    public function setPath($path = ''){
        if(isset($path) && !empty($path)){
            $this->path = $path;
        }
        return $this;
    }
    /**
     * set assets base_url
     * 
     * @access      public
     * @param       string base_url
     * @return      object
     */
    public function setBaseUrl($base = ''){
        if(isset($base) && !empty($base)){
            $this->base_url = $base;
        }
        return $this;
    }
    /**
     * add js script queue
     *
     * @access      public
     * @param       array 
     * @return      object
     *
     * @example     Assets::addJs([
     *                  'path'  => 'js/jquery.js', // mengambil source dari path
     *                  'url'   => 'js/jquery.js', // mengambil source dari url
     *                  'external' => 'http://code.jquery.com/1.7.2/jquery.min.js',
     *                  'compress' => 'jquery.min.js', // compress menjadi
     *                  'id'    => 'jquery', 
     *                  'attr'  => ['type' => 'text/javascript', 'language' => 'jscript']
     *              ], [, next_js]);
     */
    public function addJs(){
        // get all js by parameters
        $args = func_get_args();
        // requried properties
        $required_prop = ['path', 'url', 'external'];
        // fetch parameters
        foreach($args as $key => $js){
            // ignore if not an array
            if(!is_array($js)){
                continue;
            }
            // get prop
            $prop = array_keys($js);
            // check if the resources have requred properties
            // ignore if not
            $have_prop = false;
            foreach($prop as $p){
                // ignore if property is empty like ['path' => '']
                if(empty($js[$p])){
                    continue;
                }
                if(in_array($p, $required_prop)){
                    $have_prop = true;
                    break;
                }
            }
            if(!$have_prop){
                continue;
            }
            // if all fine, push to queue
            $this->jsQueue[] = $js;
            $this->processJs($js);
        }
        // return object
        return $this;
    }
    /**
     * add css queue
     *
     * @access      public
     * @param       array 
     * @return      object
     *
     * @example     Assets::addJs([
     *                  'path'  => 'css/bootstrap.css', // mengambil source dari path
     *                  'url'   => 'css/bootstrap.css', // mengambil source dari url
     *                  'external' => 'http://cdn.bootstrap.com/css/bootstrap.min.css',
     *                  'compress' => 'bootstrap.min.css', // compress menjadi
     *                  'id'    => 'bootstrap', 
     *                  'attr'  => ['type' => 'text/css']
     *              ], [, next_css]);
     */
    public function addCss(){
        // get all css by parameters
        $args = func_get_args();
        // requried properties
        $required_prop = ['path', 'url', 'external'];
        // fetch parameters
        foreach($args as $key => $css){
            // ignore if not an array
            if(!is_array($css)){
                continue;
            }
            // get prop
            $prop = array_keys($css);
            // check if the resources have requred properties
            // ignore if not
            $have_prop = false;
            foreach($prop as $p){
                // ignore if property is empty like ['path' => '']
                if(empty($css[$p])){
                    continue;
                }
                if(in_array($p, $required_prop)){
                    $have_prop = true;
                    break;
                }
            }
            if(!$have_prop){
                continue;
            }
            // if all fine, push to queue
            $this->cssQueue[] = $css;
            $this->processCss($css);
        }
        // return object
        return $this;
    }
    /**
     * untuk memproses data queue js
     *
     * @access      private
     * @param       array js queue
     * @return      null
     */
    private function processJs($js = []){
        $id = '';
        $valid = FALSE;
        $path_mode = FALSE;
        $js_url = '';
        if(isset($js['path']) && is_string($js['path']) && !empty($js['path'])){
            $file = ASSETS_PATH . $js['path'];
            if(!is_file($file)){
                return;
            }
            if(!file_exists($file)){
                return;
            }

            $url = str_replce(DIRECTORY_SEPARATOR, '/', $js['path']);
            $js_url = $this->base_url . $url;

            if(empty($js['id'])){
                if(strpos($js['path'], DIRECTORY_SEPARATOR)){
                    $path = explode(DIRECTORY_SEPARATOR, $js['path']);
                }else{
                    $path = explode('/', $js['path']);
                }
                $js['id'] = end($path);
                $path = NULL;
            }
        }

        if(isset($js['url']) && is_string($js['url']) && !empty($js['url'])){
            if(empty($js['id'])){
                $url = explode('/', $js['url']);
                $js['id'] = end($url);
                $url = NULL;
            }
            $js_url = $this->base_url . $js['url'];
        }

        if(isset($js['external']) && is_string($js['external']) && !empty($js['external'])){
            if(empty($js['id'])){
                $ex         = explode('/', $js['external']);
                $js['id']   = end($ex);
                $ex         = NULL;
            }
            $js_url = $js['external'];
        }
        // attribute
        $attr = [];
        if(isset($js['attr']) && is_array($js['attr']) && !empty($js['attr'])){
            $attr = $js['attr'];
        }

        $this->js[] = array(
            'id'    => $js['id'],
            'src'   => $js_url,
            'attr'  => $attr
        );
    }
    /**
     * untuk memproses data queue css
     *
     * @access      private
     * @param       array css queue
     * @return      null
     */
    private function processCss($css = []){
        $id = '';
        $valid = FALSE;
        $path_mode = FALSE;
        $css_url = '';
        if(isset($css['path']) && is_string($css['path']) && !empty($css['path'])){
            $file = ASSETS_PATH . $css['path'];
            if(!is_file($file)){
                return;
            }
            if(!file_exists($file)){
                return;
            }

            $url = str_replce(DIRECTORY_SEPARATOR, '/', $css['path']);
            $css_url = $this->base_url . $url;

            if(empty($css['id'])){
                if(strpos($css['path'], DIRECTORY_SEPARATOR)){
                    $path = explode(DIRECTORY_SEPARATOR, $css['path']);
                }else{
                    $path = explode('/', $css['path']);
                }
                $css['id'] = end($path);
                $path = NULL;
            }
        }

        if(isset($css['url']) && is_string($css['url']) && !empty($css['url'])){
            if(empty($css['id'])){
                $url = explode('/', $css['url']);
                $css['id'] = end($url);
                $url = NULL;
            }
            $css_url = $this->base_url . $css['url'];
        }

        if(isset($css['external']) && is_string($css['external']) && !empty($css['external'])){
            if(empty($css['id'])){
                $ex         = explode('/', $css['external']);
                $css['id']   = end($ex);
                $ex         = NULL;
            }
            $css_url = $css['external'];
        }
        // attribute
        $attr = [];
        if(isset($css['attr']) && is_array($css['attr']) && !empty($css['attr'])){
            $attr = $css['attr'];
        }

        $this->css[] = array(
            'id'    => $css['id'],
            'src'   => $css_url,
            'attr'  => $attr
        );
    }
    /**
     * untuk mengambil js dalam queue
     */
    public function getJs(){
        $output = '';
        foreach($this->js as $key => $js){
            $output .= '<script id="'. $js['id'] .'" src="'. $js['src'] .'"';
            $unallowed_attr = ['id', 'src'];
            if(!empty($js['attr'])){
                foreach($js['attr'] as $k => $v){
                    if(in_array($k, $unallowed_attr)){
                        continue;
                    }
                    $output .= ' '. $k .'="'. $v .'"';
                }
            }
            $output .= '></script>' . "\n";
        }
        return $output;
    }
    /**
     * untuk mengambil css dalam queue
     */
    public function getCss(){
        $output = '';
        foreach($this->css as $key => $css){
            $output .= '<link rel="stylesheet" type="text/css" id="'. $css['id'] .'" href="'. $css['src'] .'"';
            $unallowed_attr = ['id', 'src', 'rel', 'type'];
            if(!empty($css['attr'])){
                foreach($css['attr'] as $k => $v){
                    if(in_array($k, $unallowed_attr)){
                        continue;
                    }
                    $output .= ' '. $k .'="'. $v .'"';
                }
            }
            $output .= '/>' . "\n";
        }
        return $output;
    }
}