<?php
/**
 * User object model
 * 
 * @ Dibuat Oleh 		: Muh. Sukrillah
 * @ Url 				: http://www.sukrillah.xyz
 * @ Versi 				: 1.0
 */
namespace sasak\object_model;

class User extends \sasak\Bokah {
	/**
	 * model name
	 * @var 	string _model_name
	 * @access 	public
	 */
	protected $_model_name = '\sasak\model\User_Model';
	/**
	 * model instance
	 * @var 	object __model
	 * @access 	public
	 */
	protected $__model;
	/**
	 * table name
	 * @var 	string _table_name
	 * @access 	public
	 */
	protected $_table_name = 'user';
	/**
	 * primary key
	 * @var 	int id_user
	 * @access 	public
	 */
	protected $_primary_key = 'id_user';

	private $_data = [];

	function __construct($id_user = 0){
		parent::__construct();
		if(in_array($id_user, ['unknown'])){
			return;
		}
		//$this->_model = new \sasak\model\User_Model;
		if($id_user){
			\sasak\Log::add('Set user data => '. $id_user);
			$this->__set_data($id_user);
		}
		\sasak\Log::add('Class ['. __CLASS__ .'] Initialized..');
	}

	public function __set_data($id_user = 0){
		$field = [];
		if(is_string($id_user)){
			$field = ['username' => $id_user];
			\sasak\Log::add('get userdata by username => '. $id_user);
		}
		if(filter_var($id_user, FILTER_VALIDATE_INT)){
			$field = ['id_user' => $id_user];
			\sasak\Log::add('get userdata by id_user => '. $id_user);
		}

		if(filter_var($id_user, FILTER_VALIDATE_EMAIL)){
			$field = ['email' => $id_user];
			\sasak\Log::add('Get userdata by email => '. $id_user);
		}
		
		if(!$id_user) return;		
		$data = $this->__model->get_by($field);
		if(!is_array($data) || !isset($data['id_user'])){
			return FALSE;
		}
		foreach($data as $key => $value){
			$this->$key = $value;
		}
	}
}