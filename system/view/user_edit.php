<?php include 'header.php'; ?>
<!-- Left side column. contains the logo and sidebar -->
<?php include 'menu.php'; ?>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <i class="fa fa-pencil"></i> Ubah Profil
            <small>Ubah Data Profil</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class=""><a href="<?php echo base_url('user'); ?>">Profil</a></li>
            <li class="active">Ubah Data Profil</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-sm-12">
              <div class="box box-primary">
              <form name="form-pegawai" id="form-pegawai" method="POST" action="<?php echo base_url('user/edit'); ?>" enctype="multipart/form-data">
              	<input type="hidden" name="last_nip" value='<?php echo (isset($nip) ? $nip : 0); ?>'/>
                <div class="box-body">
                	<div class="row">                    
                    	<div class="col-md-12">
                    		<?php 
                    		if(isset($pesan)){
                    			if(isset($jenis_pesan) && $jenis_pesan == 'sukses'){
                    				echo '<div class="alert alert-info">'. $pesan .'</div>';
                    			}else{
	                    			echo '<div class="alert alert-danger">'. $pesan .'</div>';
	                    		}
                    		}
                    		if(isset($found) && $found){
                    		?>
                    		<fieldset><legend class="text-info">Data Pribadi</legend></fieldset>
                    	</div>
                    	<div class="col-md-6">
	                    	<div class="form-group">
	                    		<label for="nip">Nomor Induk Pegawai <b class="text-danger">*</b></label>
	                    		<div class="input-group">
	                    			<input type="text" name="nip" id="nip" class="form-control" maxlength="7" step="3" min="0" placeholder="0000000" autofocus required value='<?php echo (isset($nip) ? $nip : ''); ?>'<?php echo (!\app\User::$role->can('update_pegawai') ? ' disabled' : ''); ?>/>
	                    		</div>
	                    	</div>
	                    	<div class="form-group">
	                    		<label for="nama">Nama Pegawai <b class="text-danger">*</b></label>
	                    		<div class="input-group col-md-8">
	                    			<input type="text" name="nama" id="nama" class="form-control col-md-12" maxlength="60" placeholder="Nama Lengkap" value='<?php echo (isset($nama) ? $nama : ''); ?>'/>
	                    		</div>
	                    	</div>
	                    	<div class="form-group">
	                    		<label for="tanggal_lahir">Tanggal Lahir <b class="text-danger">*</b></label>
	                    		<div class="input-group col-md-8">
	                    			<input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control" placeholder="" value='<?php echo ((isset($tanggal_lahir) && !empty($tanggal_lahir)) ? $tanggal_lahir : ''); ?>'>			
	                    		</div>
	                    	</div>
	                    	<div class="form-group">
	                    		<label for="jenis_kelamin">Jenis Kelamin <b class="text-danger">*</b></label>
	                    		<div class="input-group col-md-8">
	                    			<label class="col-md-5"><input type="radio" name="jenis_kelamin" id="jenis_kelamin" value="L"<?php echo (isset($jenis_kelamin) && $jenis_kelamin == 'L' ? ' checked' : ''); ?>> Laki-Laki</label>
	                    			<label class="col-md-5"><input type="radio" name="jenis_kelamin" id="jenis_kelamin" value="P"<?php echo (isset($jenis_kelamin) && $jenis_kelamin == 'P' ? ' checked' : ''); ?>> Perempuan</label>
	                    		</div>
	                    	</div>
	                    	<div class="form-group">
	                    		<label for="status_menikah">Status Menikah <b class="text-danger">*</b></label>
	                    		<div class="input-group col-md-8">
		                    		<select name="status_nikah" id="status_nikah" class="form-control">
	                    				<option value="0"<?php echo (isset($status_nikah) && $status_nikah == '0' ? ' selected':''); ?>>Belum Menikah</option>
	                    				<option value="1"<?php echo (isset($status_nikah) && $status_nikah == '1' ? ' selected':''); ?>>Menikah</option>
		                    		</select>
	                    		</div>
	                    	</div>
	                    	<div class="form-group clearfix">
	                    		<div class="col-md-4" style="padding:0">
	                    			<label for="jumlah_istri">Jumlah Istri <b class="text-danger">*</b></label>
	                    			<input type="text" name="jumlah_istri" id="jumlah_istri" class="form-control col-md-6" maxlength="2" placeholder="0" value='<?php echo (isset($jumlah_istri) ? $jumlah_istri : 0); ?>'/>
	                    		</div>
                    			<div class="col-md-4" style="padding-left:0">
                    				<label for="jumlah_anak" class="input-label">Jumlah Anak <b class="text-danger">*</b></label>
                    				<input type="text" name="jumlah_anak" id="jumlah_anak" class="form-control col-md-6" maxlength="2" placeholder="0" value='<?php echo (isset($jumlah_anak) ? $jumlah_anak : 0); ?>'/>                    				
                    			</div>	                    		
	                    	</div>
                    	</div>
                    	<div class="col-md-6">
                    		<div class="form-group">
	                    		<label for="alamat">Alamat <b class="text-danger">*</b></label>
	                    		<div class="input-group col-md-12">
	                    			<textarea name="alamat" id="alamat" rows="2" class="form-control" maxlength="150"><?php echo (isset($alamat) ? clean($alamat) : ''); ?></textarea>
	                    		</div>
	                    	</div>
	                    	<div class="form-group">
	                    		<label for="hp">No. HP <b class="text-danger">*</b></label>
	                    		<div class="input-group col-md-8">
	                    			<input type="text" name="hp" id="hp" class="form-control col-md-6" maxlength="15" placeholder="081xxx" value='<?php echo (isset($hp) ? $hp : ''); ?>'/>
	                    		</div>
	                    	</div>
	                    	<div class="form-group">
	                    		<label for="email">Email <b class="text-danger">*</b></label>
	                    		<div class="input-group col-md-8">
	                    			<input type="email" name="email" id="email" class="form-control col-md-12" maxlength="60" placeholder="" value='<?php echo (isset($email) ? $email : ''); ?>'/>
	                    		</div>
	                    	</div>
	                    	<div class="form-group">
	                    		<label for="no_ktp">Nomor KTP <b class="text-danger">*</b></label>
	                    		<div class="input-group col-md-8">
	                    			<input type="number" name="no_ktp" id="no_ktp" class="form-control col-md-12" maxlength="30" placeholder="" value='<?php echo (isset($no_ktp) ? $no_ktp : ''); ?>'/>
	                    		</div>
	                    	</div>
	                    	<div class="form-group">
	                    		<label for="no_rekening">Nomor Rekening <b class="text-danger">*</b></label>
	                    		<div class="input-group col-md-8">
	                    			<input type="number" name="no_rekening" id="no_rekening" class="form-control" maxlength="20" placeholder="" value='<?php echo (isset($no_rekening) ? $no_rekening : ''); ?>'/>
	                    		</div>
	                    	</div>
	                    	<div class="form-group">
	                    		<label for="nama_pemilik_rekening">Nama Pemilik Rekening <b class="text-danger">*</b></label>
	                    		<div class="input-group col-md-8">
	                    			<input type="text" name="nama_pemilik_rekening" id="nama_pemilik_rekening" class="form-control" maxlength="50" placeholder="" value='<?php echo (isset($nama_pemilik_rekening) ? $nama_pemilik_rekening : ''); ?>'/>
	                    		</div>
	                    	</div>
	                    	<div class="form-group">
	                    		<label for="nama_bank_rekening">Nama Bank Rekening <b class="text-danger">*</b></label>
	                    		<div class="input-group col-md-8">
	                    			<input type="text" name="nama_bank_rekening" id="nama_bank_rekening" class="form-control" maxlength="50" placeholder="" value='<?php echo (isset($nama_bank_rekening) ? $nama_bank_rekening : ''); ?>'/>
	                    		</div>
	                    	</div>
                    		<div class="form-group">
	                    		<label for="foto">Foto <b class="text-danger">*</b></label>
	                    		<div class="input-group">
	                    			<input type="file" name="foto" id="foto" class="form-control" accesskey="u" accept="image/*" />
	                    		</div>
	                    	</div>
                    	</div>
                    	<div class="col-md-12">
                    		<fieldset>
                    			<legend class="text-info">Data Akun</legend>
                    			<input type="hidden" name="id_user" value='<?php echo (isset($id_user) ? $id_user : ''); ?>'/>
                    		</fieldset>
                    		<div class="form-group">
                    			<div class="col-md-4" style="padding:0">
	                    			<div class="input-group">
	                    				<label for="username"><i class="fa fa-user"></i> Nama Pengguna <b class="text-danger">*</b></label>
	                    				<input type="text" name="username" id="username" class="form-control" value='<?php echo (isset($username) ? $username : ''); ?>' placeholder="nama.pengguna">
	                    			</div>
	                    		</div>
	                    		<div class="col-md-4">
	                    			<div class="input-group">
	                    				<label for="password"><i class="fa fa-key"></i> Kata Sandi <b class="text-danger">*</b></label>
	                    				<input type="password" name="password" id="password" class="form-control" placeholder="*****">
	                    			</div>
	                    		</div>
	                    		<?php if(\app\User::$role->can('update_pegawai')){ ?>
	                    		<div class="col-md-4">
	                    			<div class="input-group col-md-12">
	                    				<label for="level"><i class="fa fa-user"></i> Level <b class="text-danger">*</b></label>
	                    				<select name="level" id="level" class="form-control">
	                    					<option value="4"<?php echo (isset($level) && $level == '4' ? ' selected' : ''); ?>>Pegawai</option>
	                    					<option value="3"<?php echo (isset($level) && $level == '3' ? ' selected' : ''); ?>>Administrasi</option>
	                    					<option value="2" <?php echo (isset($level) && $level == '2' ? ' selected' : ''); ?>>HRD Manajer</option>
	                    					<option value="1"<?php echo (isset($level) && $level == '1' ? ' selected' : ''); ?>>Administrator</option>
	                    				</select>
	                    			</div>
	                    		</div>
	                    		<?php } ?>
                    		</div>
                    	<?php } ?>
                    	</div>                    
                    </div>
                </div><!-- /.box-body -->
                <?php if(isset($found) && $found){ ?>
                <div class="box-footer">
                	<div class="btn-group pull-right">
                		<a class="btn btn-flat" href="<?php echo base_url('user'); ?>">Batal</a>
                		<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> Simpan</button>
                	</div>
                </div>
                <?php } ?>
            </form>
            </div>
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
    <?php include VIEW_PATH . 'footer.php'; ?>
<!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('assets/js/jQuery-2.1.4.min.js'); ?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <!--
    <script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
    <!-- FastClick --
    <script src="<?php echo base_url('assets/plugins/fastclick/fastclick.min.js'); ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('assets/dist/js/app.min.js'); ?>"></script>
    <!-- datetime picker -->    
    <script src="<?php echo asset_url('plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo asset_url('plugins/datepicker/locales/bootstrap-datepicker.id.js'); ?>"></script>
    <script src="<?php echo asset_url('plugins/input-mask/jquery.inputmask.js'); ?>"></script>
    <script src="<?php echo asset_url('plugins/input-mask/jquery.inputmask.date.extensions.js'); ?>"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--<script src="assets/dist/js/pages/dashboard.js"></script>-->
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('assets/dist/js/demo.js'); ?>"></script>
    <script>
    <?php 
    $time = date('d-m') .'-'. ((int)date('Y') - 15);
    $maxDate = (int) (strtotime($time));
    echo '/* time: '. $time .'; maxDate :'. $maxDate .' */'."\n";
    $startY = $maxDate; 
    echo 'var startDate = "'. date('Y/m/d', $startY) .'";'."\n";
    ?>
    $(function(){
    	$('#tanggal_lahir').datepicker({
    		autoclose:true,
    		language:'id',
    		format:'yyyy-mm-dd',
    		endDate: startDate
    	});
    	$('#tanggal_lahir').inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
    });
    </script>
  </body>
</html>