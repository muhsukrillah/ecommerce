<!DOCTYPE html>
<html>
<head>
    <title>Sasak Framework</title>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto|Open+Sans'/>
</head>
<body style='text-align:center;padding-top:200px;font-family:Roboto'>
    <h1><span style="font-family:'Open Sans'">Hello</span>, <span style='color:#43a047;'>Sasak</span> Framework!</h1>
    <p><?php echo (isset($data) ? $data : ''); ?></p>
    <div style="color:#ccc;">
        <p>Versi 1.0 - Under Development</p>
        <p>&copy; <b>Muh. Sukrillah</b> 2015 - All right reserved</p>
        <small>waktu proses : {elapsed_time} detik</small><br/>
        <small>penggunaan memory: {memory_usage}</small>
    </div>
</body>
</html>