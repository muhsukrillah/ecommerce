<?php
/**
 * Role Model
 *
 * Dibuat Oleh				: Muh. Sukrillah
 * Url 						: http://www.sukrillah.xyz 	
 * Version					: 1.0
 *
 */
namespace sasak\model;

use \sasak\Model;
use \PDO;

class Role_Model extends Model {
 	/*
 	 * private variable
 	 * 
 	 * string 	table_name
 	 */
 	protected $table_name;
 	/*
 	 * private capability_table_name
 	 */
 	private $capability_table_name;
 	/*
 	 * Constructor
 	 */
 	function __construct(){
 		parent::__construct();
 		$this->table_name = 'user_role';
 		$this->capability_table_name = 'user_role_capabilities';
        \sasak\Log::add('Class ['. __CLASS__ .'] Initialized..');
 	}
 	/*
 	 * Get All role
 	 */
 	function get_all_role(){
 		$query = $this->db->query("SELECT * FROM `". $this->table_name ."`;");
 		$results = $query->fetch_all(\sasak\Database::FETCH_ARRAY);
 		return $results;
 	}
 	/*
 	 * Get role by fields
 	 *
 	 * @param 	array(field => value)
 	 * @return 	array row
 	 */
 	function get_role_by($data = array()){
 		$fields = array('id_role', 'role_name');
 		$where = '';
        $this->db->select('*')->from($this->table_name);
 		foreach($data as $key => $value){
 			if(!in_array($key, $fields)){
 				unset($data[$key]);
 				continue;
 			}

			if($key == 'id_role'){
                $value = (int) $value;
				$data['id_role'] = $value;
			}
			if($key == 'role_name'){
				$data['role_name'] = trim($value);
			}

			$where .= "`". $key. "` = ? AND ";
            $this->db->where([$key => $value]);
 		}

        $data = $this->db->get()->fetch_array();
        return $data;
        /**
 		if(empty($data)){
 			return [];
 		}

 		$where = rtrim($where, 'AND ');

 		$sql = "SELECT * FROM `". $this->table_name ."` WHERE $where;";
 		$query = $this->db->prepare($sql);
 		$query->execute(array_values($data));
 		$data = $query->fetch(\PDO::FETCH_ASSOC);
 		return $data;*/
 	}
 	/*
 	 * Get role by id
 	 *
 	 * @param 	int id_role
 	 * @return 	array row
 	 */
 	function get_role_by_id($id_role = 0){
 		return $this->get_by(compact('id_role'));
 	}
 	/*
 	 * get role by role_name
 	 *
 	 * @param 	string role_name
 	 * @return 	array row
 	 */
 	function get_role_by_name($role_name = ''){
 		return $this->get_by(compact('role_name'));
 	}
 	/*
 	 * get role capabilities
 	 *
 	 * @param 	int id_role
 	 * @return 	array result
 	 */
 	function get_role_capabilities($id_role = 0){
 		$table_name = $this->capability_table_name;
 		return $this->get_capability_by(compact('id_role'));
 	}
 	/*
 	 * get capabilities
 	 * 
 	 * @param 	array (field => value)
 	 * @return 	array row | result
 	 */
 	function get_capability_by($data = array()){
 		$fields = ['id', 'id_role', 'capability', 'value'];
 		if(!is_array($data) || empty($data)){
 			return array();
 		}
 		$where = '';
        $this->db->select('*')->from($this->capability_table_name);
        if(count($data) > 1){
     		foreach($data as $key => $value){
     			if(!in_array($key, $fields)){
     				unset($data[$key]);
     			}else{
     				if($key == 'id' || $key == 'id_role'){
     					$data[$key] = (int) $value;
                        $value = (int) $value;
     				}
     				$where = "`". $key ."` = ? AND ";
     			}
                $this->db->where([$key => $value]);
     		}
        }else{
            $fk = array_keys($data);
            if(in_array($fk[0], ['id', 'id_role'])){
                $v = (int) $data[$fk[0]];
                $data[$fk[0]] = $v;
            }
            $this->db->where($data);
        }
        $row = $this->db->get()->fetch_all();
        return $row;
        /*
 		if(empty($data)){
 			return array();
 		}
 		$where = rtrim($where, "AND ");

 		$query = $this->db->prepare("SELECT * FROM `". $this->capability_table_name ."` WHERE $where;");
 		$query->execute(array_values($data));
 		$result = $query->fetchAll(\PDO::FETCH_ASSOC);
 		return $result;*/
 	}
 	/*
 	 * add capability
 	 * 
 	 * @param 	int id_role
 	 * @param 	array (capability => value) || string capability
 	 * @param 	value
     * @param   description
 	 * @return 	boolean || int insert_id
 	 */
 	function add_capability($id_role = 0, $capability = '', $value = 0, $description = ''){
 		if($this->has_capability($id_role, $capability)){
 			return $this->update_capability(compact('id_role', 'capability', 'value', 'description'));
 		}
 		$data = array(
 			'id_role' => (int) $id_role,
 			'capability' => $capability,
 			'value' => $value,
            'description' => $description
 		);
 		$insert = $this->db->insert($this->capability_table_name, $data);
 		if($insert){
 			$insert_id = $this->db->insert_id();
 			return $insert_id;
 		}else{
 			return FALSE;
 		}
 	}
 	/*
 	 * update capability
 	 *
 	 * @param 	int id_role
 	 * @param 	string capability
 	 * @param 	int value
 	 * 
 	 * @return 	boolean
 	 */
 	function update_capability($id_role = 0, $capability = '', $value = 0){
 		$data = compact('value');
 		$where = compact('id_role', 'capability');
 		$update = $this->db->update($this->capability_table_name, $data, $where);
 		return $update;
 	}
 	/*
 	 * has capability ?
 	 * 
 	 * @param 	int id_role
 	 * @param 	string capability_name
 	 * @return 	boolean
 	 */
 	function has_capability($id_role = 0, $capability = ''){
 		$data = $this->get_capability_by(compact('id_role', 'capability'));
 		if(isset($data['id'])){
 			return TRUE;
 		}else{
 			return FALSE;
 		}
 	}
 	/*
 	 * Add role
 	 *
 	 * @param 	array data
 	 *
 	 * @return 	int (id_role) || bool
 	 */
 	function add_role($data = array()){
 		$fields = array('id_role', 'role_name');
 		if(!is_array($data)){
 			return FALSE;
 		}
 		foreach($data as $key => $value){
 			if(!in_array($key, $fields)){
 				unset($data[$key]);
 			}
 		}
 		$insert = $this->db->insert($this->table_name, $data);
 		if($insert){
 			$id_role = $this->db->insert_id;
 			return $id_role;
 		}else{
 			return FALSE;
 		}
 	}
 	/*
 	 * update role
 	 *
 	 * @param 	array data
 	 * @param 	array where clause
 	 *
 	 * @return 	bool
 	 */
 	function update_role($data = array(), $where = array()){
 		$fields = array('id_role', 'role_name');
 		$update_data = array();
 		$where_data = array();
 		if(!is_array($data) || empty($where)){
 			return FALSE;
 		}
 		foreach($data as $key => $value){
 			if(!in_array($key, $fields)){
 				unset($data[$key]);
 				continue;
 			}
 			$update_data[$key] = $value;
 		}
 		foreach($where as $key => $value){
 			if(!in_array($key, $fields)){
 				unset($where[$key]);
 				continue;
 			}
 			$where_data[$key] = $value;
 		}
 		if(empty($update_data) || empty($where_data)){
 			return FALSE;
 		}
 		$update = $this->db->update($this->table_name, $update_data, $where_data);
 		return $update;
 	}
 	/*
 	 * delete role
 	 *
 	 * @param 	array|int where
 	 *
 	 * @return 	boolean
 	 */
 	function delete_role($where = array()){
 		$fields = array('id_role', 'role_name');
 		if(is_int($where)){
 			$id_role = (int) $where;
 			$where = array('id_role' => $id_role);
 		}
 		if(!is_array($where)){
 			return FALSE;
 		}
 		foreach($where as $key => $value){
 			if(!in_array($key, $fields)){
 				unset($where[$key]);
 			}
 		}
 		if(empty($where)){
 			return FALSE;
 		}
 		$delete = $this->db->delete($this->table_name, $where);
 		return $delete;
 	}
 }