<?php
/**
 * Model User
 *
 * Dibuat Oleh 		: Muh Sukrillah
 * Url 				: http://www.sukrillah.xyz
 * Version 			: 1.0
 */
namespace sasak\model;

use \sasak\Model;
use \PDO;
use \sasak\Log;

class User_Model extends Model {
	/**
	 * default level / role
	 * @var 	int default_level
	 * @access 	public
	 */
	var $default_level = 4;
	/**
	 * table name
	 * @var 	string table_name
	 * @access 	public
	 */
	protected $table_name;
	/**
	 * fields name
	 * @var 	array _fields
	 * @access 	public
	 */
	protected $_fields = ['id_user', 'username', 'password', 'email', 'display_name', 'first_name', 'last_name', 'avatar', 'token', 'level', 'status', 'date_added', 'date_updated'];

	function __construct(){
		parent::__construct();
		$this->set_table_name('user');
	}
	/**
	 * save new user data
	 * @param 	array $data [username,password,level]
	 * @param 	string $return 'bool'|'data'
	 * 
	 * @return  mixed
	 */
	function save($data = array(), $return = 'bool'){
		$fields = ['username','password','level'];
		foreach($data as $key => $value){
			if(!in_array($key, $fields) || empty($data[$key])){
				unset($data[$key]);
			}
		}
		if(empty($data)){
			// tidak ada data
			return FALSE;
		}
		if(!isset($data['level'])){
			// set default level jika tidak diinputkan
			$data['level'] = $this->default_level;
		}
		$ada = $this->get_by(['username'=>$data['username']]);
		if(is_array($ada) && isset($ada['id_user'])){
			$update = $this->update($data, ['id_user' => $ada['id_user']]);
			if($update){
				$data = $this->get_by(['id_user' => $ada['id_user']]);
				return $data;
			}else{
				return $update;
			}
		}

		$sql = "INSERT INTO `user`(`username`,`password`,`level`) VALUES(:username,:password,:level);";
		$query = $this->db->prepare($sql);

		$data['password'] = md5($data['password']);

		$query->bindParam(':username', $data['username'], PDO::PARAM_STR);
		$query->bindParam(':password', $data['password'], PDO::PARAM_STR);
		$query->bindParam(':level', $data['level'], PDO::PARAM_STR);

		$saved = $query->execute();
		if($saved){
			if($return == 'data'){
				$data = $this->get_by(['username'=> $data['username']]);
				return $data;
			}else{
				return TRUE;
			}
		}
	}

	function update($data = array(), $where = array()){
		$fields = ['username', 'password','level'];
		$where_fields = ['id_user', 'username'];

		if(!is_array($data)){
			return FALSE;
		}
		foreach($data as $key => $value){
			$key = (string) $key;
			if(!in_array($key, $fields)){
				unset($data[$key]);
			}
			if($key == 'password'){
				$data['password'] = md5($data['password']);
			}
		}
		if(empty($data)){
			Log::add('Data tidak ada.. [user_model::update_user]');
			return FALSE;
		}
		$updated_field = [];
		foreach($data as $key => $value){
			$updated_field[] = "`$key` = :$key";
		}
		//$updated_field = rtrim($updated_field, ", ");
		$where_str = '';
		foreach ($where as $key => $value) {
			$where_str .= "`$key` = :where_".$key." AND ";
		}
		$where_str = rtrim($where_str, "AND ");

		$sql = "UPDATE `user` SET ". implode(', ', $updated_field) ." WHERE $where_str;";
		Log::add($sql);
		
		$query = $this->db->prepare($sql);

		foreach($data as $key => $value){
			$query->bindParam(':'. $key, $value);
		}

		foreach($where as $key => $value){
			$query->bindParam(":where_". $key, $value);
		}
		$update = $query->execute();
		if($update){
			Log::add('berhasil update data.. [user_model::update_user]');
			return TRUE;
		}else{
			Log::add('Gagal update data.. [user_model::update_user]');
			return FALSE;
		}
	}

	function getAll(){
		return $this->all_users();
	}

	function all_users(){
		$query = $this->db->query("SELECT * FROM `user`");
		return $query->fetchObject();
	}
}
/**
 * user_model.php
 *
 * Dibuat Oleh 		: Muh Sukrillah
 * Url 				: http://www.sukrillah.xyz
 * Version 			: 1.0
 */