<?php
/**
 * Routes
 *
 * Dibuat Oleh 			: Muh Sukrillah
 * Url 					: http://www.sukrillah.xyz
 * Versi 				: 1.0
 */
namespace sasak;

Router()->get('/about', function(){
	ob_start();
	include SYSTEM_PATH . 'view/about.php';
	$v = ob_get_clean();
	return $v;
});