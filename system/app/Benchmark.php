<?php
/**
 * MS Framework
 *
 * Sebuah aplikasi framework open source berbasis PHP 5.1.6 atau lebih baru
 *
 * @package		MS Framework
 * @author		Muh Sukrillah
 * @copyright	Copyright (c) 2015, MS Inc.
 * @license		http://www.sukrillah.xyz/msframework/license.html
 * @link		http://www.sukrillah.xyz/msframework/
 * @since		Versi 1.0
 * @filesource
 */
// ------------------------------------------------------------------------
/**
 * Benchmark Librari
 * Untuk mengetahui lama waktu sebuah proses
 * 
 * Dibuat Oleh 			: Muh Sukrillah
 * Url 					: http://www.sukrillah.xyz
 * Versi 				: 1.0
 */
// ------------------------------------------------------------------------
/**
 * CodeIgniter Benchmark Class
 *
 * This class enables you to mark points and calculate the time difference
 * between them.  Memory consumption can also be displayed.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://www.sukrillah.xyz/msframework/user_guide/libraries/benchmark.html
 */
namespace sasak;

class Benchmark {

	/**
	 * List of all benchmark markers and when they were added
	 *
	 * @var array
	 */
	static $marker = array();
	/**
	 * instance
	 */
	private static $instance;

	function __construct(){
		self::$instance =& $this;
		
		Log::add('Class: '. __CLASS__ .' Inititialized..');
	}

	// --------------------------------------------------------------------

	/**
	 * Set a benchmark marker
	 *
	 * Multiple calls to this function can be made so that several
	 * execution points can be timed
	 *
	 * @access	public
	 * @param	string	$name	name of the marker
	 * @return	void
	 */
	public static function mark($name, $manual = NULL)
	{
		self::$marker[$name] = isset($manual) ? $manual : microtime(true);
	}

	// --------------------------------------------------------------------

	/**
	 * Calculates the time difference between two marked points.
	 *
	 * If the first parameter is empty this function instead returns the
	 * {elapsed_time} pseudo-variable. This permits the full system
	 * execution time to be shown in a template. The output class will
	 * swap the real value for this variable.
	 *
	 * @access	public
	 * @param	string	a particular marked point
	 * @param	string	a particular marked point
	 * @param	integer	the number of decimal places
	 * @return	mixed
	 */
	public static function elapsed_time($point1 = NULL, $point2 = NULL, $decimals = 4)
	{
		if (! isset($point1) || !isset($point2) || $point1 == '')
		{
			return '{elapsed_time}';
		}

		if ( ! isset(self::$marker[$point1]))
		{
			return '';
		}

		if ( ! isset(self::$marker[$point2]))
		{
			self::$marker[$point2] = microtime(true);
		}
		return number_format(self::$marker[$point2] - self::$marker[$point1], $decimals);

		//print_r(self::$marker);
		//exit;
		list($sm, $ss) = self::$marker[$point1];//explode(' ', self::$marker[$point1]);
		list($em, $es) = self::$marker[$point2];//explode(' ', self::$marker[$point2]);

		return number_format(($em + $es) - ($sm + $ss), $decimals);
	}

	// --------------------------------------------------------------------

	/**
	 * Memory Usage
	 *
	 * This function returns the {memory_usage} pseudo-variable.
	 * This permits it to be put it anywhere in a template
	 * without the memory being calculated until the end.
	 * The output class will swap the real value for this variable.
	 *
	 * @access	public
	 * @return	string
	 */
	public static function memory_usage()
	{
		return '{memory_usage}';
	}

}