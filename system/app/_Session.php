<?php
/**
 * Session Library
 *
 * Dibuat Oleh 		: Muh Sukrillah
 * Url				: http://www.sukrillah.xyz
 * Versi 			: 1.0
 */
namespace app;

class Session {
	private static $instance;
	/**
	 * Constant for session name
	 * 
	 */
	const SESSION_NAME = "ms_session";
	/**
	 * session id
	 * @type 	integer
	 * @access 	private
	 * @var 	$_session_id
	 */
	private $_session_id = 0;
	/**
	 * public property
	 * session data
	 * @var 	session_data
	 * @type 	Array
	 */
	protected $_session_data = [];
	/**
	 * session_expire
	 * session berakhir
	 * @access 	private
	 * @var 	session_expire
	 * @type 	string
	 * 
	 */
	private $_session_expire = 3600 * 3; /* 3 Jam */

	function __construct(){
		session_start();
		$this->_session_id = session_id();
		$this->set_expire(time());
		
		$input =& Input();
		$cookie_serial = $input->cookie(self::SESSION_NAME);
		\app\Log::add('Unserializing cookie data..');
		\app\Log::add($cookie_serial);
		$cookie_data = @unserialize($cookie_serial);
		\app\Log::add('Unserialized data..');
		\app\Log::add(print_r($cookie_data,1));
		if(is_array($cookie_data) && !empty($cookie_data)){
			if(!isset($cookie_data['session_id'])){
				$cookie_data['session_id'] = $this->_session_id;
			}
			if(!isset($cookie_data['session_expire'])){
				$cookie_data['session_expire'] = $this->_session_expire;
			}
			$this->_session_data = $cookie_data;
		}
		self::$instance = $this;
	}

	function __destruct(){
		session_destroy();
	}

	public static function &instance(){
		return self::$instance;
	}

	public function get_session_data($key = ''){
		if(isset($this->_session_data[$key])){
			return $this->_session_data[$key];
		}
		return $this->_session_data;
	}
	/**
	 * set session expire
	 *
	 * @access 		public
	 * @param 		$expire 	int timestamp [3600 * 3] / 3 Jam
	 * @return 		null
	 */
	public function set_expire($expire = 3600 * 3, $auto_generate = FALSE){
		if($auto_generate){
			$expire = $expire * 3600;
		}

		$expire = $expire + time();
		$this->_session_expire = $expire;
	}
	/**
	 * remember session
	 * 
	 * @param 		$remember_session 	boolean
	 * @access 		public
	 * @return 		null
	 */
	public function set_remember($remember_session = FALSE){
		if($remember_session){
			$ex = 24 * 30 * 12; // 1 tahun
			$this->set_expire($ex, TRUE);
			\app\Log::add('Session Remembered : '. $ex);
		}else{
			$this->set_expire(3600);
			\app\Log::add('Session Not Remembered..');
		}
	}

	public function set_userdata($key = '', $value = NULL){
		if(!isset($this->_session_data['userdata'])){
			$this->_session_data['userdata'] = [];
		}

		if(is_array($key)){
			$sess_data = $this->_session_data;
			if(!empty($key)){
				$this->_session_data = array_merge($sess_data, $key);
			}else{
				$this->_session_data = $key;
			}
			Log::add('storing session data => '. print_r($this->_session_data, 1));
			return $this->__store($this->_session_data);
		}

		$this->_session_data['userdata'][$key] = $value;
		return $this->__store($this->_session_data);
	}

	public function userdata($data = ''){
		if(isset($this->_session_data['userdata'])){
			if(empty($data) || !$data || is_null($data)){
				return $this->_session_data['userdata'];
			}else{
				if(isset($this->_session_data['userdata'][$data])){
					return $this->_session_data['userdata'][$data];
				}
			}
		}else{
			return [];
		}
	}

	private function __store($data = []){
		\app\Log::add('data will be stored => '. print_r($data,1));
		$c_data = serialize($data);
		\app\Log::add('preparing session data => '. $c_data);
		$input =& Input();
		\app\Log::add('Session Expired : '. date('d-m-Y', $this->_session_expire));
		$stored = $input->set_cookie(self::SESSION_NAME, $c_data, $this->_session_expire);
		//$stored = $input->set_cookie(self::SESSION_NAME, 'a:2:{s:8:"userdata";s:117:"a:3:{s:8:"username";s:5:"warid";s:8:"password";s:32:"d9b1d7db4cd6e70935368a1efb10e377";s:9:"timestamp";i:1454029355;}";s:5:"token";s:56:"MmE0MDlkYzliNDdjODc1NzZlYTBjYWFkYWRjNjFkZmQ1MWZmMzYyNA==";}', $this->_session_expire);
		if($stored){
			\app\Log::add('[Session::__store] : storing session data success..');
			\app\Log::add($stored);
			\app\Log::add($input->cookie(self::SESSION_NAME));
		}else{
			\app\Log::add('Cookie not stored..');
			\app\Log::add($stored);
		}
		return $this;
	}

	public function truncate($callback = NULL){
		\app\Log::add('Truncating user data..');
		$this->_session_data = [];
		Input()->set_cookie(self::SESSION_NAME, '');
		$this->set_userdata([]);
	}

	public function encrypt_session($data = []){
 		// extract username
 		$username = isset($data['username']) ? $data['username'] : 'unknown';
 		$password = isset($data['password']) ? md5($data['password']) : '';
 		$timestamp = isset($data['timestamp']) ? $data['timestamp'] : time(); 		
		$token = $this->generate_token(compact('username', 'password', 'timestamp'));
		$userdata = serialize(compact('username', 'password', 'timestamp'));
		$session_data = compact('userdata', 'token');
		$this->set_userdata($session_data);
 	}

 	public function extract_session(){
 		$session_data = $this->get_session_data();
 		\app\Log::add('Extracting session data..');
 		\app\Log::add(print_r($session_data,1));
 		$data_serialized = isset($session_data['userdata']) ? $session_data['userdata'] : 'a:0:{}';
 		if(is_string($data_serialized)){
 			$userdata = unserialize($data_serialized);
 		}
 		if(is_array($data_serialized)){
 			$userdata = $data_serialized;
 		}
 		return (is_array($userdata) ? $userdata : []);
 	}

 	public function generate_token($data = []){
 		if(!is_array($data) || empty($data)) return 0;

 		$username = isset($data['username']) ? $data['username'] : 'unknown';
 		$password = isset($data['password']) ? $data['password'] : '';
 		$timestamp = isset($data['timestamp']) ? $data['timestamp'] : time();
 		// timestamp / password_hash / username
 		$token_data = $timestamp .'/'. $password .'/'. $username;
 		Log::add('Preparing Token Data: '. $token_data);
 		$token_sha = sha1($token_data);
 		Log::add('Encrypted Token Data: '. $token_sha);
 		$new_token = base64_encode($token_sha);
 		Log::add('New Token: '. $new_token);
 		return $new_token;
 	}

 	public function get_token(){
 		$data = $this->get_session_data();
 		return (isset($data['token']) ? $data['token'] : '0');
 	}

 	public function validate_token($token = ''){
 		if(empty($token)) return FALSE;

 		// get session data
 		$data = $this->extract_session();
 		// check data
 		if(!is_array($data) || empty($data)) return FALSE;
 		Log::add('token data : '. print_r($data,1));
 		$token_ori = $this->generate_token($data);
 		Log::add('validating token: '. $token_ori .' <> '. $token);
 		if($token_ori == $token){
 			$valid = TRUE;
 		}else{
 			$valid = FALSE;
 		}
 		Log::add('token valid : '. (($valid) ? 'TRUE' : 'FALSE'));
 		return $valid;
 	}
}
/**
 * Berkas 			: session.php
 * Lokasi			: /lib/session.php
 *
 * Dibuat Oleh 		: Muh Sukrillah
 * Url				: http://www.sukrillah.xyz
 * Versi 			: 1.0
 */