<?php
/**
 * Driver abstract class
 * 
 * Dibuat Oleh              : Muh. Sukrillah
 * Url                      : http://www.sukrillah.xyz
 * Versi                    : 1.0
 */
namespace sasak\database\driver;

abstract class Drivers {
    public abstract function select($fields);
    public abstract function from($table_name);
    public abstract function where($field, $operator, $value);
    public abstract function where_and($field, $operator, $value);
    public abstract function where_or($field, $operator, $value);
    public abstract function get($table_name, $fields);
    public abstract function fetch($fetch_type);
    public abstract function fetch_object();
    public abstract function fetch_array();
    public abstract function fetch_all();
    public abstract function query($query);
    public abstract function insert($table_name, $data);
    public abstract function update($table_name, $data, $where);
    public abstract function delete($table_name, $where);
    public abstract function limit($limit, $offset); // db::limit(0, 5)
}