<?php
/**
 * Database Driver Class
 * Dibuat Oleh              : Muh. Sukrillah
 * Url                      : http://www.sukrillah.xyz
 * Versi                    : 1.0
 */
namespace sasak\database\driver;
use \sasak\Log;
class PDO_Driver extends Drivers{
    /**
     * Database connection
     * @var     object $db
     * @access  private
     */
    private $db;
    /**
     * query string
     *
     * @var     string _query
     * @access  private
     */
    private $_query = '';
    /**
     * last query string
     * @var     string _last_query
     * @access  private
     */
    private $_last_query = '';
    /**
     * resources
     * @var     object $_resource
     * @access  private
     */
    private $_resource;
    /**
     * selected fields
     * @var     string $_selected_fields
     * @access  private
     */
    private $_selected_fields = '';
    /**
     * is selected the fields ?
     * @var     bool $_is_select
     * @access  private
     * @default false   
     */
    private $_is_select = FALSE;
    /**
     * table name
     * @var     string $_table_name
     * @access  private
     */
    private $_table_name;
    /**
     * where clause
     * @var     array $_where
     * @access  private
     */
    private $_where = [];
    /**
     * where clause check
     * @var     bool $_is_where
     * @access  private
     * @default false
     */
    private $_is_where = FALSE;
    /**
     * where operator
     * @var     array $_where_operator
     * @access  private
     * @default array ['and']
     */
    private $_where_operator = [];
    /**
     * is ordered ?
     *
     * @var     bool _is_ordered 
     * @access  private
     * @default false
     */
    private $_is_ordered = FALSE;
    /**
     * order by
     *
     * @var     string  _order
     * @access  private
     * @default null
     */
    private $_order;
    /**
     * is limit
     * @var     bool _is_limit
     * @access  private
     * @default false
     */
    private $_is_limit = FALSE;
    /**
     * limit data
     * @var     array _limit
     * @access  private
     * @default false
     */
    private $_limit = [];
    /** 
     * Constructor
     * @access  public
     * @param   resource $conn => database connection
     * @return  null
     */
    function __construct($conn = NULL){
        $this->db = $conn;
    }
    /**
     * reset query
     * @access  public
     * @param   null
     * @return  object instance
     */
    public function reset_query(){
        $this->_where_operator = [];
        $this->_where = [];
        $this->_is_where = FALSE;
        $this->_is_select = FALSE;
        $this->_resource = NULL;
        $this->_selected_fields = NULL;
        $this->_limit = [];
        $this->_is_limit = FALSE;
        return $this;
    }
    /**
     * select method for => select query
     * @access  public
     * @param   string|array $fields
     * @return  object instance
     */
    public function select($fields = '*'){
        $selected = '*';
        if(is_array($fields) && !empty($fields)){
            $selected = implode('`,`', $fields);
            $selected = '`'. $selected .'`';
        }
        if(is_string($fields) && !empty($fields)){
            $selected = $fields;
        }
        $this->_selected_fields = $selected;
        $this->_is_select = TRUE;
        return $this;
    }
    /**
     * from method for => select * from
     * @access  public
     * @param   string $table_name
     * @return  object instance
     */
    public function from($table_name = ''){
        if(isset($table_name) && !empty($table_name)){
            if($this->_is_select){
                $this->_table_name = $table_name;
            }
        }
        return $this;
    }
    /**
     * where method
     * @example:
     * db::where('field1', '=', 'value1'); //-> (where field1 = 'value1')
     * db::where(['field1' => 'value1']); //-> (where field1 = 'value')
     * db::where(['field1', '=', 'value1']); //-> (where field1 = 'value1')
     * db::where('field', '<>', 'value'); //-> (where field <> 'value')
     * db::where('field', 'in', ['1', '2','3']); //-> (where field in ('1', '2','3'))
     * db::where('field', 'not in', ['1', '2', '3']); //-> (where field not in (1,2,3))
     * db::where('field', '<', 'value'); //-> (where field < value)
     * db::where('field', 'like', 'value', TRUE)
     * -------------
     * @access  public
     * @param   string|array selected_fields
     * @return  object instance
     */
    public function where($field = NULL, $operator = '=', $value = NULL){
        $operators = ['=', '<>', '!=', 'in', 'not in', 'like', '<', '>', '<=', '>='];
        if(isset($field) && !empty($field)){
            if(is_array($field)){
                // db::where(['field' => 'value'])
                Log::add('where field : '. print_r($field,1));
                if(sizeof($field) == 1){
                    Log::add('where count == 1');
                    $ka = array_keys($field);
                    $key = $ka[0];
                    // db::where([field => [1,2]]) // conver to in operator
                    // db::where('field', 'in', [1,2])
                    if(is_array($field[$key])){
                        return $this->where($key, 'in', $field[$key]);
                    }else{
                        return $this->where($key, '=', $field[$key]);
                    }
                }
                // # query 1
                // db::where(['field', '!=', 'value'])
                // # query 2
                /* db::where([
                 *      'field1' => 'value1', 
                 *      'field2' => 'value2',
                 *      'field3' => 'value3'
                 * ]);
                 */
                if(count($field) > 1){
                    Log::add('where count > 1');
                    $fk = array_keys($field); // [0=>field1, 1=>field2, 2=>field3]
                    if(isset($fk[1]) && in_array($fk[1], $operators)){
                        // # query 1
                        Log::add('query 1');
                        $key = $field[0];
                        $op = $field[1];
                        $v = $field[2];
                        return $this->where($key, $op, $v);
                    }else{
                        // # query 2
                        Log::add('query 2');
                        foreach($field as $f => $fv){
                            // [field1 => value 1]
                            // dst..
                            $this->where($f, '=', $fv);
                        }
                        Log::add('# Query 2');
                        return $this;
                    }                    
                }
            }
            $this->_where[] = compact('field', 'operator', 'value');
            $this->_is_where = TRUE;
        }        
        return $this;
    }
    public function where_and($field = NULL, $operator = '=', $value = NULL){
        $this->_where_operator[] = 'and';
        return $this->where($field, $operator, $value);
    }
    public function where_or($field = NULL, $operator = '=', $value = NULL){
        $this->_where_operator[] = 'or';
        return $this->where($field, $operator, $value);
    }
    /**
     * order data
     * @example :
     * db::order_by(['id' => 'asc']); //-> (select ... ORDER BY `id` ASC)
     * db::order_by(['id' => 'asc', 'date' => 'desc']); //-> (SELECT ... ORDER BY `id` ASC, `date` DESC)
     * db::order_by(['id', 'name']); //-> (SELECT ... ORDER BY `id` ASC, `name` ASC)
     * 
     */
    public function order_by(Array $args = []) {
        $orders = ['ASC', 'DESC'];
        $order = '';
        foreach($args as $key => $value) {
            $field = $key;
            $order_value = $value;

            if(is_int($key) && !empty($value)){
                $field = $value;
                $order_value = 'ASC';

                $order .= "`$field` $order_value, ";
                continue;
            }

            if(is_string($key) && !empty($value)) {
                $order_value = strtoupper($value);
                if(!in_array($order_value, $orders)){
                    $order_value = "ASC";
                }
                $field = $key;

                $order .= "`$field` $order_value, ";
                continue;
            }
        }
        $order = rtrim($order, ", ");
        $this->_order = $order;
        $this->_is_ordered = TRUE;

        return $this;
    }
    /**
     * method untuk set limit query
     * @example :
     * db::limit(1, 0); //-> (SELECT * FROM table LIMIT 0, 1)
     * db::limit(1); // (SELECT * FROM table LIMIT 1)
     * db::limit(2, 0); //-> (SELECT * FROM table LIMIT 0, 2);
     * db::limit(1, 1); //-> (SELECT * FROM table LIMIT 1, 1);
     * @access  public
     * @param   int         offset
     * @param   int|array   limit|[limit,offset]
     * @return  object instance
     */
    public function limit($offset = 0, $limit = 0){
        if(!$offset){
            return $this;
        }
        if(is_array($limit)){
            $this->_limit = $limit;
        }else{
            $this->_limit = [$limit, $offset];
        }
        $this->_is_limit = TRUE;
        return $this;
    }
    /**
     * insert method
     * fungsi ini digunakan untuk menginput data ke table
     * example:
     * db::insert('table_name', ['field1' => 'value1', 'field2' => 'value2'])
     * 
     * @access  public
     * @param   string table_name
     * @param   array  data
     * @return  int|bool    last_insert_id|false
     */
    public function insert($table_name = '', $data = []){
        if(!isset($table_name) || empty($table_name)) return FALSE;
        if(!isset($data) || empty($data)) return FALSE;

        $sql  = "INSERT INTO `". $table_name ."`(`";
        $keys = array_keys($data);
        $sql .= implode("`,`", $keys);
        $sql .= "`) VALUES(";
        foreach($data as $key => $value){
            $sql .= "?, ";
        }
        $sql = rtrim($sql, ", ");
        $sql .= ")";
        Log::add('Running Query: '. $sql, TRUE, TRUE);
        $query = $this->db->prepare($sql);
        $insert = FALSE;
        $insert_id = 0;
        $inserted_data = array_values($data);
        Log::add(print_r($inserted_data,1), TRUE, TRUE);
        
        $insert = $query->execute($inserted_data);;
        $insert_id = $this->db->lastInsertId();
        Log::add('insert => '. ($insert ? 'TRUE' : 'FALSE'), TRUE, TRUE);
        Log::add('insert_id => '. $insert_id, TRUE, TRUE);
        /*try{
            $this->db->beginTransaction();
            $insert = $query->execute(array_values($data));
            $this->db->commit();
            $insert_id = $this->db->lastInsertId();
        }catch(PDOException $e){
            $this->db->rollback();
            Log::add('[Error Database Insert] '. $e->getMessage());
            Error($e->getMessage());
        }*/
        if($insert){
            if($insert_id)
                return $insert_id;

            return $insert;
        }
        return FALSE;
    }
    /**
     * update method
     * fungsi ini digunakan untuk merubah data record
     * example:
     * db::update('table_name', ['field1' => 'value1', 'field2' => 'value2'], ['where1' => 'value1']);
     *
     * @access  public
     * @param   string  table_name
     * @param   array   data
     * @param   array   where
     * @return  bool
     */
    public function update($table_name = '', $data = [], $where = []){
        if(!isset($table_name) || empty($table_name)){
            return FALSE;
        }
        if(!isset($data) || empty($data) || !is_array($data)){
            return FALSE;
        }
        if(!isset($where) || empty($where) || !is_array($where)){
            return FALSE;
        }
        $sql = "UPDATE `". $table_name ."` SET `";
        foreach($data as $key => $value){
            $field = $key;
            $sql.= $field ."` = ?, `";
        }
        $sql = rtrim($sql, ", `");
        $sql.= " WHERE `";
       /* $whr = [];
        $whv = [];
        foreach($where as $key => $value){
            $whf = $key;
            $whr[] = $key;
            $sql.= $whf ."` = ?, `";
        }*/
        $sql.= implode('` = ? AND `', array_keys($where)) . '` = ?';
        //$sql = rtrim($sql, ' AND `');
        // preparing data
        $bind = array_merge(array_values($data), array_values($where));
        Log::add('Running Query : '. $sql, TRUE, TRUE);
        Log::add(print_r($bind, 1), TRUE, TRUE);
        // preparing query
        $query = $this->db->prepare($sql);
        // execute query
        $updated = $query->execute($bind);
        if($updated){
            return TRUE;
        }
        return FALSE;
    }
    /**
     * delete method
     * fungsi ini digunakan untuk menghapus record
     * example:
     * db::delete('user', ['id_user' => 1])
     * db::delete('table_name', ['field' => 'value']);
     * db::where(['id_user' => 10]);
     * db::delete('user');
     *
     * @access  public
     * @param   required    string table_name
     * @param   optional    array where
     * @return  bool
     */
    public function delete($table_name = '', $where = []){
        if(!isset($table_name) || empty($table_name)){
            return FALSE;
        }
        if(!isset($where) || empty($where)){
            return FALSE;
        }
        $sql = "DELETE FROM `". $table_name ."`";
        $sql.= " WHERE ";
        foreach($where as $key => $value){
            $sql.= '`'. $key .'` = ?, ';
        }
        $sql = rtrim($sql, ', ');
        $query = $this->db->prepare($sql);
        return $query->execute(array_values($where));
    }
    /**
     * get method
     * fungsi ini digunakan untuk mengambil data dari table
     * example:
     * db::get('table_name', ['field1', 'field2', 'dst..']); //-> get by spesific field(s)
     * db::get('table_name'); //-> get all
     * db::get(); //-> jika ini digunakan, harus terpanggil db:select() terlebih dahulu
     * 
     * @access  public
     * @param   string table_name
     * @param   string|array selectedfield
     * @return  object instance
     */
    public function get($table_name = NULL, $fields = NULL){
        if(isset($table_name) && empty($this->_table_name)){
            $this->_table_name = $table_name;
        }
        if(!$this->_is_select){
            $this->select($fields);
        }
        $sql = '';
        if($this->_is_select){
            $sql .= "SELECT ";
            $sql .= trim($this->_selected_fields) . " ";
            $sql .= "FROM `". $this->_table_name ."` ";
            if($this->_is_where){
                $sql .= "WHERE ";
                $where_operator = $this->_where_operator;
                if(empty($where_operator) || !is_array($where_operator)){
                    foreach($this->_where as $k){
                        $where_operator[] = ' AND ';
                    }
                }
                Log::add('where : '. print_r($where_operator,1));
                Log::add('where operator : '. print_r($where_operator, 1));
                if(count($where_operator) >= count($this->_where)){
                    //unset($where_operator[count($this->_where_operator)-1]);
                    $where_operator = array_slice($where_operator, 0, count($this->_where)-1);
                }
                Log::add('where operator sliced: '. print_r($where_operator, 1));
                $where_values = [];

                foreach($this->_where as $k => $v){
                    //$value = $v['value'];
                    
                    $operator = strtolower($v['operator']);

                    if(in_array($operator, ['in', 'not in', 'like'])){
                        if(is_array($v['value']) && $operator != 'like'){
                            //$value = "('". implode("','", $value) ."') ";
                            $value = '';
                            foreach($v['value'] as $in_val){
                                $value .= '?, ';
                                $where_values[] = $in_val;
                            }
                            $value = "(". rtrim($value, ', ') .")";
                        }else{
                            $where_values[] = $v['value'];
                            $value .= ' ?, ';
                        }/*elseif(is_string($value)){
                            $value = "(". $value .") ";
                        }*/
                        $operator = " ". strtoupper($operator);
                    }else{
                        /* if(is_int($value)){
                            $value = " $value, ";
                        }else{
                            $value = " '$value', ";
                        }*/
                        $value = ' ?, ';
                        $where_values[] = $v['value'];
                    }
                    $sql .= "`". $v['field'] ."` ". $operator . $value;
                    if(!empty($where_operator) && isset($where_operator[$k])){
                        $sql = rtrim($sql, ', ');
                        $sql .= $where_operator[$k];
                    }
                }
                $sql = rtrim($sql, ', ');
            }
            // order
            if($this->_is_ordered){
                $sql .= " ORDER BY ". $this->_order;
            }
            // limit
            if($this->_is_limit){
                $sql .= " LIMIT ";
                $limit = (int) $this->_limit[0];
                $offset = (int) $this->_limit[1];
                if(!$limit){
                    $sql.= $offset;
                }
                if($limit){
                    $sql.= $limit .', '. $offset;
                }
            }
        }
        Log::add('Running Query : '. $sql, TRUE, TRUE);
        $this->_last_query = $sql;
        $query = $this->db->prepare($sql);
        if(isset($where_values) && !empty($where_values)){
            Log::add(print_r($where_values,1), TRUE, TRUE);
            $query->execute($where_values);
        }else{
            $query->execute();
        }
        $this->_resource = $query;
        return $this;
        //return $this->query($sql);
    }
    /**
     * Buat method baru salinan dari \PDO::query()
     *
     * @access  public
     * @param   string query
     * @return  object instance
     */
    public function query($sql = ''){
        // PDO::query()
        Log::add('Running Query : '. $sql, TRUE, TRUE);
        $this->_resource = $this->db->query($sql);
        $this->_last_query = $sql;
        return $this;
    }
    /**
     * Buat method baru untuk menjalankan query yang difilter
     * contoh:
     * db::filter_query("SELECT * FROM table WHERE id = ?", [1]);
     * db::filter_query("SELECT * FROM user WHERE `email` = :email", [':email' => $email])
     *
     * @access  public
     * @param   required    string  query
     * @param   required    array   data
     * @return  object instance
     */
    public function filter_query($sql = '', $data = []){
        Log::add("Running Query : ". $sql, TRUE, TRUE);
        Log::add(print_r($data,1), TRUE, TRUE);
        $query = $this->db->prepare($sql);
        if(!empty($data)){
            $query->execute($data);
        }else{
            $query->execute($data);
        }
        $this->_resource = $query;
        return $this;
    }
    /**
     * fetch method
     */
    public function fetch($fetch_all = FALSE, $fetch_type = NULL){
        $resource = $this->_resource->fetchAll(\PDO::FETCH_ASSOC);
        $data = [];
        if(!$fetch_all && count($resource) == 1 && isset($resource[0])){
            $data = $resource[0];
            $resource = $data;
        }elseif(count($resource) == 1 && isset($resource[$fetch_type])){
            $resource = $resource[$fetch_type];
        }
        $this->reset_query();
        return $resource;
    }
    /**
     * num rows
     */
    public function num_rows(){
        $resource = $this->_resource->fetchAll(\PDO::FETCH_ASSOC);
        return count($resource);
    }
    /**
     * Buat method baru salinan dari \PDO::fetchObject()
     * 
     * @access  public
     * @param   null
     * @return  object resources
     */
    public function fetch_object(){
        $resources = $this->_resource->fetchObject();
        $this->reset_query();
        return $resources;
    }
    /**
     * Buat method baru salinan dari \PDO::fetchArray()
     *
     * @access  public
     * @param   null
     * @return  array resources
     */
    public function fetch_array(){
        $resources = $this->_resource->fetch(\PDO::FETCH_ASSOC);
        $this->reset_query();
        return $resources;
    }
    /**
     * Buat method baru salinan dari \PDO::fetchAll()
     *
     * @access  public
     * @param   null
     * @return  mixed [array,object] resources
     */
    public function fetch_all($type = NULL){
        if(!isset($type)){
            $type = \PDO::FETCH_ASSOC;
        }
        $resources = $this->_resource->fetchAll($type);
        $this->reset_query();
        return $resources;
    }
    /**
     * method untuk mengambail nama column
     */
}