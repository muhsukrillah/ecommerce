<?php
/**
 * Model Class
 * Dibuat Oleh 			: Muh. Sukrillah
 * Url 					: http://www.sukrillah.xyz
 * Versi 				: 1.0
 */
namespace sasak;
use \PDO;
use \sasak\Database;
class Model extends Database{
    /**
     * class instance
     * @var     object
     * @access  private
     */
    private static $instance;
	/**
	 * Database connection instance
	 * @var 	object $db
	 * @access 	public
	 */
	protected $db = NULL;
	/**
	 * Query
	 * @var 	string $query
	 * @access 	private
	 */
	private $query;
	/**
	 * table name
	 * @var 	string table_name
	 * @access 	public
	 */
	protected $table_name;
	/**
	 * construcor
	 */
	function __construct(){
        self::$instance =& $this;
		parent::__construct();

		$this->db = $this->get_connection();
		Log::add('Class ['. __CLASS__ .'] Initialized..');
	}
    /**
     * method untuk mengambil instanc object
     * @access  private
     * @param   null
     * @return  object instance
     */
    public static function &get_instance(){
        return self::$instance;
    }
	/**
     * fungsi untuk memberikan nama table
     * @access  public
     * @param   string table_name
     * @return  object instance
     */
    public function set_table_name($table_name = ''){
        $this->table_name = $table_name;
        return $this;
    }
    /**
     * method to insert new record
     * 
     * @access  public
     * @param   array $data
     * @return  bool
     */
    public function insert($data = []){
        // filtering and ignore while incompleted
        if(!isset($data) || empty($data)){
            return FALSE;
        }
        // filtering and ignore while incompleted
        if(empty($this->table_name)){
            return FALSE;
        }
        if(method_exists($this, '__filter_insert')){
            $data = $this->__filter_insert($data);
        }
        // buat secara default data untuk [date_added, date_updated]
        // $data['date_added'] = date('Y-m-d h:i:s');
        // $data['date_updated'] = $data['date_added'];
        // running..
        return $this->db->insert($this->table_name, $data);
    }
    /**
     * method untuk mengubah data
     *
     * @access  public
     * @param   array data  [updated data]
     * @param   array where
     * @return  bool
     */
    public function update($data = [], $where = []){
        // running..
        if(method_exists($this, '__filter_update')){
            $data = $this->__filter_insert($data);
        }
        return $this->db->update($this->table_name, $data, $where);
    }
    /**
     * fungsi untuk menghapus data
     *
     * @access 	public
     * @param 	required 	array 	where
     * @return 	bool
     */
    public function delete($where = []){
    	return $this->db->delete($this->table_name, $where);
    }
	/**
     * fungsi untuk mengambil data / field tertentu berdasarkan kriteria tertentu
     *
     * @access  public
     * @param   array data  [where]
     * @param   array field [field needle]
     * @return  array field data
     */
    public function get_by($data = [], $field = [], $fetch_all = FALSE){
        if(empty($data)) return [];
        $this->db->where($data);
        if(isset($field['limit'])){
            $limit_arg = $field['limit'];
            unset($field['limit']);
            if(isset($limit_arg[0])){
                $offset = (int) $limit_arg[0];
            }
            $limit = 0;
            if(isset($limit_arg[1])){
                $limit = (int) $limit_arg[1];
            }
            if(isset($limit) && isset($offset)){
                $this->db->limit($offset, $limit);
            }
        }
        $all = $this->db->get($this->table_name, $field)->fetch($fetch_all);//\PDO::FETCH_COLUMN);
        return $all;
    }
    /**
     * fungsi untuk mengambil semua data
     *
     * @access 	public
     * @param 	null
     * @return 	array data
     */
    public function get_all(){
        $args = func_get_args();
        foreach($args as $key => $value){
            // where
            if(isset($value['where']) && !empty($value['where'])){
                $this->db->where($value['where']);
            }
            // order
            // model::get_all(['order_by' => ['field' => 'asc']])
            if(isset($value['order_by']) && !empty($value['order_by'])) {
                $this->db->order_by($value['order_by']);
            }
            // limit
            // model::get_all(['limit' => [0, 10]])
            if(isset($value['limit']) && !empty($value['limit'])){
                $offset = isset($value['limit'][0]) ? $value['limit'][0] : 0;
                $limit = isset($value['limit'][1]) ? $value['limit'][1] : 10;
                $this->db->limit($limit, $offset);
            }
        }
    	return $this->db->get($this->table_name)->fetch_all();
    }
    /**
     * get count all data
     *
     * @access  public
     * @param   null
     * @return  int total
     */
    public function count_all(){
        $query = $this->db->filter_query("SELECT COUNT(*) `total` FROM `". $this->table_name ."`");
        $data = $query->fetch();
        return $data['total'];
    }
	/**
     * get column names
     * @access  public
     * @param   null
     * @return  array columns
     */
    public function get_columns(){
        $query = $this->db->limit(1)->get($this->table_name);//query("SELECT * FROM `". $this->table_name ."` LIMIT 1");
        $data = $query->fetch();
        return array_keys($data);
    }
}