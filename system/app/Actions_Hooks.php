<?php
namespace sasak;
/*
 * Class Name		: Actions_hooks
 * Author			: Muh. Sukrillah
 * Version			: 1.0
 * Desctiption		: action handler
 */
class Actions_Hooks {
	private $point = array();
	private $canceled_actions = array();
	private $only_actions = array();
	private static $instance;
	
	function __construct(){
		self::$instance =& $this;
		Log::add('Class : '. __CLASS__ .' Initialized..');
	}

	public static function &instance(){
		return self::$instance;
	}
	/*
	 * add_action('admin_head', 'register_the_script');
	 */
	function _add_action($point_name = "", $action = "", $priority = 10, $accepted_args = 1){
		if(! isset($action) ) return;
		if(! isset($point_name) ) return;
		
		$idx = $this->_unique_hook_id($point_name, $action, $priority);
		
		if(isset($point_name) && !empty($point_name)){
			$this->point[$point_name][$priority][$idx] = array(
				'action' => $action, 
				'accepted_args' => $accepted_args
			);
		}
		return TRUE;
	}
	
	function _unique_hook_id($point_name = '', $action = '', $priority = 0){
		if(empty($action)) return $point_name .'_'. $priority;
		
		if(is_array($action)){
			if(isset($action[0]) && is_object($action[0]) && isset($action[1]) && is_string($action[1]) && !empty($action[1])){
				return $action[1];
			}
		}else{
			if(is_object($action)){
				return $point_name .'_'. $priority;
			}
			return $action;
		}
	}
	
	function _remove_action($point_name = ""){
		if(isset($this->point[$point_name]))
			unset($this->point[$point_name]);
	}
	
	function _cancel_action($point_name = ""){
		if(! isset($point_name) ) return;
		if( empty($point_name) ) return;
		
		array_push($this->canceled_actions, $point_name);
	}
	/*
	 * Do the actions on available point name
	 *
	 * if the point name not available
	 * return NULL
	 */
	function _do_action($point_name = ""){
		if(! isset($this->point[$point_name])){
			return;
		}
		
		if(in_array($point_name, $this->canceled_actions) ){
			return;
		}			
			
		$args = func_get_args();
		$num_args = func_num_args();
		if($num_args > 1){
			$me =& $this;
			if(! empty($args)){
				@call_user_method_array('_run_hook', $me, $args);
			}else{
				@call_user_method('_run_hook', $me);
			}
		}else{
			$this->_run_hook($point_name);
		}
		//$this->_run_hook($point_name, $param);
		return TRUE;			
		
	}
	/*
	 * Run / Do actions
	 */
	function _run_hook($point_name = "", $arg = ''){
		error_reporting(E_ALL);
		if(empty($point_name)) return;
		
		$args = array();
		if ( is_array($arg) && 2 == count($arg)){
			if(isset($arg[0]) && is_object($arg[0]) ){
				$args[] =& $arg[0];
			}
		}else if(is_object($arg)){
			$args[] =& $arg;
		}else{
			$args[] = $arg;
		}
		
		for ( $a = 2; $a < func_num_args(); $a++ )
			$args[] = func_get_arg($a);
		
		
		ksort($this->point[$point_name]);
		reset($this->point[$point_name]);
		//exit();
		do {
			foreach((array) current($this->point[$point_name]) as $the_){
				if(! is_null($the_['action']) ){
					//exit(print_r($the_['action'][1], TRUE));
					
					if( is_array($the_['action']) ){						
						if(isset($the_['action'][0]) && is_object($the_['action'][0])){
							$object =& $the_['action'][0];
						}
						if(isset($the_['action'][1]) && is_string($the_['action'][1])){
							$method = $the_['action'][1];
						}
						
						if(isset($object) && isset($method)){
							//exit($method);
							if(method_exists($object, $method)){
								@call_user_method_array($method, $object, array_slice($args, 0, (int) $the_['accepted_args']));
							}
						}
					}else{
						if(is_object($the_['action'])){
							if(!empty($args)){
								call_user_func_array($the_['action'], array_slice($args, 0, (int) $the_['accepted_args']));
							}else{
								@call_user_func($the_['action']);
							}
						}
						if(is_string($the_['action']) && function_exists($the_['action'])){
							if(!empty($args)){
								@call_user_func_array($the_['action'], array_slice($args, 0, (int) $the_['accepted_args']));
							}else{
								@call_user_func($the_['action']);
							}
						}
					}
				}else{
					//exit(print_r($the_, TRUE));
				}
			}
		} while(next($this->point[$point_name]) !== FALSE);
		
	}
}