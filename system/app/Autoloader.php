<?php
/**
 * Autoloader Class
 *
 * Dibuat Oleh          : Muh. Sukrillah
 * Url                  : http://www.sukrillah.xyz
 * Versi                : 1.0
 */
namespace sasak;

Class Autoloader {

    function __construct(){

        Log::add('Class: '. __CLASS__ .' Inititialized..');
    }
    /**
     * fungsi untuk mendeteksi sebuah class merupakan system class
     *
     * @access      private
     * @param       string $class_name
     * @return      bool
     */
    private function __is_system($class_name = ''){
        if(strpos($class_name, 'sasak\\') === 0){
            return TRUE;
        }
        return FALSE;
    }
    /**
     * fungsi untuk mendeteksi sebuah class merupakan user app class
     *
     * @access      private
     * @param       string $class_name
     * @return      bool
     */
    private function __is_app($class_name = ''){
        if(strpos($class_name, 'app\\') === 0){
            return TRUE;
        }
        return FALSE;
    }
    /**
     * fungsi untuk regsitrasi auoload sebuah class pada system
     *
     * @access      private
     * @param       string $class_name
     * @return      bool
     */
    private function __load_system($class = ''){
        if(class_exists($class)){
            return FALSE;
        }

        if(strpos($class, 'sasak\lib\\') === 0){
            $file_name = str_replace('sasak\lib\\', '', $class);
            $file_path = SYSTEM_PATH . 'lib/'. $file_name .'.php';
            $about = 'Library';
        }elseif(strpos($class, 'sasak\object_model\\') === 0){
            $file_name = str_replace('sasak\object_model\\', '', $class);
            $file_path = SYSTEM_PATH . 'object/'. $file_name .'.php';
            $about = 'Object_Model';
        }elseif(strpos($class, 'sasak\model\\') === 0){
            $file_name = str_replace('sasak\model\\', '', $class);
            $file_path = SYSTEM_PATH . 'models/'. $file_name .'.php';
            $about = 'Model';
        }elseif(strpos($class, 'sasak\database\driver\\') === 0){
            $file_name = str_replace('sasak\database\driver\\', '', $class);
            $file_name = strtolower($file_name);
            $file_path = SYSTEM_PATH . 'app/database_drivers/'. $file_name .'.php';
            $about = 'Database Driver';
        }else{
            $file_name = str_replace('sasak\\', '', $class);
            $file_path = SYSTEM_PATH . 'app/'. $file_name .'.php';
            $about = 'Object';
        }

        $file_path = str_replace(['\\'], '/', $file_path);

        if(is_file($file_path) && file_exists($file_path)){
            include $file_path;
            Log::add('Auto load ['. $class .'] : '. $file_path);
            if(!class_exists($class)){
                Log::add('Error: ['. $about .' Class] not found => ['. $class .']');
                Error('Error: ['. $about .' Class] not found => ['. $class .']');
            }
            return true;
        }else{
            Log::add('Autoload Error file not found : ['. $class .'] => '. $file_path);
            return false;
        }
    }
    /**
     * fungsi untuk regsitrasi auoload sebuah class pada aplikasi
     *
     * @access      private
     * @param       string $class_name
     * @return      bool
     */
    private function __load_app($class = ''){
        if(class_exists($class)){
            Log::add('Autoload ['. $class .'] : exists and ignored..');
            return FALSE;
        }

        if(strpos($class, 'app\lib\\') === 0){
            $about = 'Library';
            $file_name = str_replace('app\lib\\', '', $class);
            $path = '';
            if(strpos($file_name, '/')){
                $file_ex = explode('/', $file_name);
                if(!empty($file_ex)){
                    $file_name = end($file_ex);
                    $file_ex = array_slice($file_ex, 0, count($file_ex) - 1);
                    $path = implode(DIRECTORY_SEPARATOR, $file_ex) . DIRECTORY_SEPARATOR;
                }
            }
            $file_path = LIB_PATH . $path . $file_name . '.php';
        }elseif(strpos($class, 'app\model\\') === 0){
            $about = 'Model';
            $file_name = str_replace('app\model\\', '', $class);
            $path = '';
            if(strpos($file_name, '/')){
                $file_ex = explode('/', $file_name);
                if(!empty($file_ex)){
                    $file_name = end($file_ex);
                    $file_ex = array_slice($file_ex, 0, count($file_ex) - 1);
                    $path = implode(DIRECTORY_SEPARATOR, $file_ex) . DIRECTORY_SEPARATOR;
                }
            }
            $file_path = MODEL_PATH . $path . $file_name . '.php';
        }elseif(strpos($class, 'app\object_model\\') === 0){
            $about = 'Object_Model';
            $file_name = str_replace('app\object_model\\', '', $class);
            $path = '';
            if(strpos($file_name, '/')){
                $file_ex = explode('/', $file_name);
                if(!empty($file_ex)){
                    $file_name = end($file_ex);
                    $file_ex = array_slice($file_ex, 0, count($file_ex) - 1);
                    $path = implode(DIRECTORY_SEPARATOR, $file_ex) . DIRECTORY_SEPARATOR;
                }
            }
            $file_path = OBJECT_PATH . $path . $file_name . '.php';
        }else{
            $about = 'AppObject';
            $file_name = str_replace('app\\', '', $class);
            $file_path = APP_PATH . 'core/'. $file_name .'.php';
        }

        if(is_file($file_path) && file_exists($file_path)){
            include $file_path;
            Log::add('Auto load ['. $class .'] : '. $file_path);
            if(!class_exists($class)){
                Log::add('Autoload Library: ['. $about .'] Class not found => ['. $class.']');
                Error('Error: ['. $about .'] Class ['. $class.'] tidak tersedia..');
            }
            return true;
        }else{
            Log::add('Autoload Error file not found : '. $file_path);
            return false;
        }
    }

    function load($class = ''){
        if($this->__is_system($class)){
            return $this->__load_system($class);
        }
        if($this->__is_app($class)){
            return $this->__load_app($class);
        }
    }

    public function register(){
        spl_autoload_register(array($this, 'load'));
    }
}