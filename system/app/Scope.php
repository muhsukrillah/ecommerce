<?php
/**
 * Scope object class
 *
 * @package         Sasak Framework
 * @author          Muh. Sukrillah
 * @version         1.0
 */
namespace sasak;

class Scope {
    private static $instance;
    function __construct(){
        self::$instance = $this;
    }
    public static function &get_instance(){
        return self::$instance;
    }
};