<?php
/**
 * Input Library
 *
 * Dibuat Oleh 		: Muh Sukrillah
 * Url 				: http://muh.sukrillah.xyz
 * Version 			: 1.0
 */
namespace app;

class Input{
	/**
	 * property for instance
	 */
	private static $instance;
	/**
	 * property for store url data
	 * @var 		$get
	 * @type 		string
	 * @access 		private
	 */ 
	protected $_get = [];
	/**
	 * property for store post data from form submit
	 *
	 * @var 		$post
	 * @type 		mixed
	 * @access 		private
	 */
	protected $_post = [];
	/**
	 * property for store server information data
	 * 
	 * @var 		$server
	 * @type 		mixed
	 * @access 		private
	 */
	protected $_server = [];
	/**
	 * property for store cookie information data
	 *
	 * @var 		$cookie
	 * @type 		mixed
	 * @access 		private
	 */
	protected $_cookie = [];

	function __construct(){		
        /**
         * Mengambil data $_POST
         */
        $this->_post = isset($_POST) ? $_POST : [];
        //unset($_POST);
        /**
         * Mengambil data $_SERVER
         */
        $this->_server = isset($_SERVER) ? $_SERVER : [];
        //unset($_SERVER);
        /**
         * Mengambil data $_COOKIE
         */
        $this->_cookie = isset($_COOKIE) ? $_COOKIE : [];
        //unset($_COOKIE);
        /**
		 * mengambil data $_GET dari url
		 */
		$request = $this->server('REQUEST_URI');
        $pos = strpos($request, '?');
        if($pos){
        	$str = substr($request, $pos + 1, strlen($request) - $pos);
        	parse_str($str, $args);
        	$this->_get = $args;
        }
        self::$instance =& $this;
	}
	/**
	 * get the instance
	 */
	public static function &instance(){
		return self::$instance;
	}
	/**
	 *
	 * method untuk mengambil server data
	 */
	public function server($key = ''){
		if(isset($this->_server[$key])){
			return $this->_server[$key];
		}
		return FALSE;
	}
	/**
	 * method untuk mengambil data url / $_GET
	 * @access 		public
	 * @param 		string nama variable
	 * @return 		mixed
	 */
	public function get($get = ''){

		if(empty($get)){
			return $this->_get;
		}	
		if(isset($this->_get[$get])){
			return $this->_get[$get];
		}
		return FALSE;
	}
	/**
	 * method untuk mengambil data post / $_POST
	 *
	 * @access 		public
	 * @param 		string nama variable
	 * @return 		mixed
	 */
	public function post($post_name = ''){
		if(empty($post_name)){
			return $this->_post;
		}
		if(isset($this->_post[$post_name])){
			return $this->_post[$post_name];
		}
		return FALSE;
	}
	/**
	 * method untuk mengmbil data cokie / $_COOKIE
	 *
	 * @access 		public
	 * @param 		string nama variable
	 * @return 		mixed
	 **/
	public function cookie($cookie_name = ''){
		if(empty($cookie_name)){
			\app\Log::add('Getting all cookie data..');
			return $this->_cookie;
		}
		if(isset($this->_cookie[$cookie_name])){
			\app\Log::add('Getting cookie with name => '. $cookie_name);
			$data = $this->_cookie[$cookie_name];
			\app\Log::add('Cookie['. $cookie_name .'] => '. print_r($data,1));
			return $data;
		}
		return NULL;
	}
	/**
	 * method untuk membuat data cookie
	 *
	 * @access 		public
	 * @param 		string 	cookie_name
	 * @param 		string 	value
	 * @param 		int 	expire
	 * @param 		string 	path
	 * @param 		boolean	secure
	 * @return 		mixed
	 */
	public function set_cookie($cookie_name = '', $value = NULL, $expire = 0, $path = '/', $domain = NULL, $secure = FALSE){
		$base_url = base_url();
		$cpath = str_replace('http://', '', $base_url);
		$cpath = explode('/', trim($cpath, '/'));
		$_domain = $cpath[0];
		if(isset($domain)){
			$_domain = $domain;
		}
		unset($cpath[0]);
		if(empty($cpath)){
			$cookie_path = '/';
		}else{
			$cookie_path = implode('/', $cpath);
		}
		$trimpath = trim($path, '/');
		$path = '/'. $cookie_path .'/'. (!empty($trimpath) ? $trimpath .'/' : '');
		\app\Log::add('set cookie ['. $cookie_name .']..');
		\app\Log::add($value);
		\app\Log::add('Cookie Data Length : '. strlen($value));
		\app\Log::add('Cookie Expired at : '. date_id($expire));
		ob_start();
		//setcookie('tes', $value);
		$set = setcookie($cookie_name, $value, $expire, $path, $_domain, $secure);
		ob_end_flush();
		if($set){
			\app\Log::add('Stored Cookie : '. $this->cookie($cookie_name));
			return $set;
		}else{
			\app\Log::add('Cookie not stored successfully..');
			return FALSE;
		}
	}
	/**
	 * method untuk menghapus data cookie
	 *
	 * @access 		public
	 * @param 		string nama cookie
	 * @return 		object
	 */
	public function remove_cookie($cookie_name = ''){
		return $this->set_cookie($cookie_name, '');
	}
	/**
	 * Is ajax Request?
	 *
	 * Test to see if a request contains the HTTP_X_REQUESTED_WITH header
	 *
	 * @return 	boolean
	 */
	public function is_ajax_request()
	{
		return ($this->server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest');
	}

	// --------------------------------------------------------------------

	/**
	 * Is cli Request?
	 *
	 * Test to see if a request was made from the command line
	 *
	 * @return 	bool
	 */
	public function is_cli_request()
	{
		return (php_sapi_name() === 'cli' OR defined('STDIN'));
	}
}
/**
 * Berkas 			: input.php
 * Lokasi 			: /lib/input.php
 *
 * Dibuat Oleh 		: Muh Sukrillah
 * Url 				: http://wwww.sukrillah.xyz
 *
 */