<?php
namespace sasak;
/*
 * Class Name		: Filter_Hooks
 * Author			: Muh. Sukrillah
 * Version			: 1.0
 * Description		: filter handler
 */
class Filter_Hooks {
	private $_filter = array();
	private $_filtered = array();
	private static $instance;

	function __construct(){
		self::$instance =& $this;
	}

	public static function &instance(){
		return self::$instance;
	}
	/*
	 * add_filter('admin_post_form_action_url', 'http://localhost/admin/post/new');
	 */
	function _add_filter($tag = '', $func_to_add = '', $priority = 10, $accepted_args = 1){
		if(!isset($tag)) return FALSE;
		if(!isset($func_to_add)) return FALSE;
		if(empty($tag)) return FALSE;
		if(empty($func_to_add)) return FALSE;
		
		$idx = $this->_get_unique_filter_id($tag, $func_to_add);
		$this->_filter[$tag][$priority][$idx] = array(
			'function'		=> $func_to_add,
			'accepted_args' => $accepted_args
		);
		return TRUE;
	}
	
	function _get_unique_filter_id($tag = '', $func = ''){
		if(is_string($func) && !empty($func))
			return $func;
		if(is_array($func)){
			if(isset($func[0]) && is_object($func[0]))
				$obj =& $func[0];
			if(isset($func[1]) && is_string($func[1]) && !empty($func[1]))
				$mtd = $func[1];
			
			if(method_exists($obj, $mtd)){
				$obj_name = get_class($obj);
				return $obj_name .'::'. $mtd;
			}
		}
	}
	
	function _remove_filter($tag = '', $func_to_remove = '', $priority = 10){
		$idx = $this->_get_unique_filter_id($tag, $func_to_remove);
		
		$isset = isset($this->_filter[$tag][$priority][$idx]);
		if(TRUE === $isset){
			unset($this->_filter[$tag][$priority][$idx]);
			if(empty($this->_filter[$tag][$priority]))
				unset($this->_filter[$tag][$priority]);
			unset($this->_filter[$tag]);
		}
		return $isset;
	}
	
	function _has_filtered($tag = '', $func_to_check = FALSE){
		
		$has = !empty($this->_filter[$tag]);
		if(FALSE === $func_to_check || FALSE == $has){
			return $has;
		}
		
		if(!$idx = $this->_get_unique_filter_id($tag, $func_to_check))
			return FALSE;
		
		foreach((array) array_keys($this->_filter[$tag]) as $priority){
			if(isset($this->_filter[$tag][$priority][$idx]))
				return $priority;
		}
		
		return FALSE;
	}
	
	function _apply_filter($tag = '', $value = ''){	
		$args = array();
		
		if(! isset($this->_filter[$tag])){
			return $value;
		}
		
		// sorting
		ksort($this->_filter[$tag]);
		
		reset($this->_filter[$tag]);
		
		if(empty($args))
			$args = func_get_args();
		
		do {
			foreach((array) current($this->_filter[$tag]) as $the_){
				if(! is_null($the_['function']) ){
					$args[1] = $value;
					if(is_array($the_['function'])){
						if(isset($the_['function'][0]) && is_object($the_['function'][0])){
							$obj =& $the_['function'][0];
						}
						
						if(isset($the_['function'][1]) && is_string($the_['function'][1])){
							$mtd = $the_['function'][1];
						}
						
						if(isset($obj) && isset($mtd)){
							if(method_exists($obj, $mtd)){
								$value = call_user_method_array($mtd, $obj, array_slice($args, 1, (int)$the_['accepted_args']));
							}
						}
					}	

					if(is_object($the_['function'])){
						$value = call_user_func_array($the_['function'], array_slice($args, 0, $accepted_args));
					}				
					
					if(is_string($the_['function'])){						
						if(function_exists($the_['function'])){
							$value = call_user_func_array($the_['function'], array_slice($args, 1, (int)$the_['accepted_args']));
						}else{
							if(!empty($the_['function'])){
								$value = $the_['function'];
							}else{
								$value = $args[1];
							}
						}
					}
				}
			}
		} while( FALSE !== next($this->_filter[$tag]));
		
		return $value;
	}
}