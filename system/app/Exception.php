<?php
/**
 * Error Exception Handler Class
 *
 * Dibuat Oleh              : Muh. Sukrillah
 * Url                      : http://www.sukrillah.xyz
 * Versi                    : 1.0
 */
namespace sasak;

class SasakException {

    function __construct(){}

    function database_handler($ex){
        Error('[Database Error] : '. $ex->getMessage());
    }

    function handler($ex){
        Log::add('Error : '. $ex->getMessage());
        Error('[Error] : '. $ex->getMessage());
    }
}