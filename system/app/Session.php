<?php
/**
 * Session Library
 *
 * Dibuat Oleh 		: Muh Sukrillah
 * Url				: http://www.sukrillah.xyz
 * Versi 			: 1.0
 */
namespace sasak;

use \sasak\Log;

class Session {
	/**
	 * static member for instace
	 *
	 * @var 	object $instance
	 * @access 	private
	 */
	private static $instance;
	/**
	 * Constant for session name
	 * 
	 * @var 	string $_session_name
	 * @access 	private
	 */
	private static $_session_name = "ms_session";
	/**
	 * session id
	 * @type 	integer
	 * @access 	private
	 * @var 	int $_session_id
	 */
	private $_session_id = 0;
	/**
	 * public property
	 * session data
	 * @var 	array session_data
	 * @type 	Array
	 */
	protected $_session_data = [];
	/**
	 * session_expire
	 * session berakhir
	 * @access 	private
	 * @var 	int session_expire
	 * @type 	string
	 * 
	 */
	private $_session_expire = 3600 * 3; /* 3 Jam */

	function __construct(){
		// start the session
		session_start();

		$this->_session_id = session_id();
		$this->set_expire(time());
		
		$input =& Input();
		$cookie_serial = $input->cookie(self::$_session_name);
		$cookie_data = @unserialize($cookie_serial);
		
		if(is_array($cookie_data) && !empty($cookie_data)){
			if(!isset($cookie_data['session_id'])){
				$cookie_data['session_id'] = $this->_session_id;
			}
			if(!isset($cookie_data['session_expire'])){
				$cookie_data['session_expire'] = $this->_session_expire;
			}
			$this->_session_data = $cookie_data;
		}
		self::$instance = $this;
		Log::add('Class ['. __CLASS__ .'] Initialized..');
	}

	function __destruct(){
		if(session_id()){
			@session_destroy();
		}
	}

	public static function &instance(){
		return self::$instance;
	}

	public function get_session_data($key = ''){
		if(isset($this->_session_data[$key])){
			return $this->_session_data[$key];
		}
		return $this->_session_data;
	}
	/**
	 * set session expire
	 *
	 * @access 		public
	 * @param 		$expire 	int timestamp [3600 * 3] / 3 Jam
	 * @return 		null
	 */
	public function set_expire($expire = 3600 * 3, $auto_generate = FALSE){
		if($auto_generate){
			$expire = $expire * 3600;
		}

		$expire = $expire + time();
		$this->_session_expire = $expire;
	}
	/**
	 * remember session
	 * 
	 * @param 		$remember_session 	boolean
	 * @access 		public
	 * @return 		null
	 */
	public function set_remember($remember_session = FALSE){
		if($remember_session){
			$ex = 24 * 30 * 12;
			$this->set_expire($ex, TRUE);
		}else{
			$this->set_expire(3600);
		}
	}
	/**
	 * fungsi ini digunakan untuk membuat session data sementara / flash data
	 *
	 * @contoh : 
	 * Session::set_flash_data('message', 'this is a message..');
	 *
	 * @access 	public
	 * @param 	required 	string 	name
	 * @param 	required 	string 	value
	 * @return 	object instance
	 */
	public static function set_flash_data($name = '', $value = NULL){
		if(empty($name)) return FALSE;
		if(!isset($value)) return FALSE;
		if(!is_string($value)) return FALSE;
		// ambil data sebelumnya
		$before = self::get_flash_data();
		if(FALSE !== $before || !empty($before)){
			if(is_array($before)){
				$before[$name] = $value;
			}
			$data = serialize($before);
		}else{
			$data = serialize([$name => $value]);
		}
		Input()->set_cookie('flash_data', $data);
	}
	/**
	 * fungsi ini digunakan untuk mengambil flast data
	 *
	 * @example:
	 * Session::get_flash_data('message'); // return message data. the data will auto cleaned
	 * Session::get_flash_data('message', FALSE); // return message data. The data found next time
	 * Session::get_flash_data(); // return array all flash data
	 *
	 * @access 	public
	 * @param 	optional 	string 	name
	 * @param 	optional 	bool 	autoclean 	=> auto clean the data if it's exists
	 * @return 	string data
	 */
	public static function get_flash_data($name = '', $autoclean = TRUE){
		if(isset($name) && !empty($name)){
			$data = self::get_flash_data();
			if(is_array($data) && isset($data[$name])){
				if($autoclean){
					self::clean_flash_data($name);
				}
				return $data[$name];
			}else{
				return FALSE;
			}
		}else{
			$data = Input()->cookie('flash_data');
			if(empty($data)){
				return FALSE;
			}
			$unserialized = @unserialize($data);
			return $unserialized;
		}
	}
	/**
	 * fungsi ini untuk menghapus data flash data
	 *
	 * @example:
	 * Session::clean_flash_data('message'); // delete spesific data
	 * Session::clean_flash_data(); // delete all data
	 *
	 * @access 	public
	 * @param 	optional 	string 	name
	 * @return 	bool
	 */
	public static function clean_flash_data($name = ''){
		if(isset($name) && !empty($name)){
			$data = self::get_flash_data();
			if(FALSE !== $data || !empty($data)){
				if(isset($data[$name])){
					unset($data[$name]);
				}
			}
			$data = serialize($data);
			Input()->set_cookie('flash_data', $data);
		}else{
			Input()->set_cookie('flash_data', '');
		}
	}

	public function set_userdata($key = '', $value = NULL){
		if(!isset($this->_session_data['userdata'])){
			$this->_session_data['userdata'] = [];
		}

		if(is_array($key)){
			$sess_data = $this->_session_data;
			if(!empty($key)){
				$this->_session_data = array_merge($sess_data, $key);
			}else{
				$this->_session_data = $key;
			}
			Log::add('storing session data => '. print_r($this->_session_data, 1));
			return $this->__store($this->_session_data);
		}

		$this->_session_data['userdata'][$key] = $value;
		return $this->__store($this->_session_data);
	}

	public function userdata($data = ''){
		if(isset($this->_session_data['userdata'])){
			if(empty($data) || !$data || is_null($data)){
				return $this->_session_data['userdata'];
			}else{
				if(isset($this->_session_data['userdata'][$data])){
					return $this->_session_data['userdata'][$data];
				}
			}
		}else{
			return [];
		}
	}

	private function __store($data = []){
		$c_data = serialize($data);
		$input =& Input();
		$input->set_cookie(self::$_session_name, $c_data, $this->_session_expire);
		return $this;
	}

	public function truncate(){
		$this->set_userdata([]);
	}

	public function encrypt_session($data = [], $encrypt_password = TRUE){
 		// extract username
 		$username = isset($data['username']) ? $data['username'] : 'unknown';
 		if($encrypt_password){
 			$password = isset($data['password']) ? md5($data['password']) : '';
 		}else{
 			$password = isset($data['password']) ? $data['password'] : '';
 		}
 		$timestamp = isset($data['timestamp']) ? $data['timestamp'] : time(); 		
		$token = $this->generate_token(compact('username', 'password', 'timestamp'));
		$userdata = serialize(compact('username', 'password', 'timestamp'));
		$session_data = compact('userdata', 'token');
		$this->set_userdata($session_data);
 	}

 	public function extract_session(){
 		$session_data = $this->get_session_data();
 		
 		$data_serialized = isset($session_data['userdata']) ? $session_data['userdata'] : 'a:0:{}';
 		if(is_string($data_serialized)){
 			$userdata = unserialize($data_serialized);
 		}
 		if(is_array($data_serialized)){
 			$userdata = $data_serialized;
 		}
 		return (is_array($userdata) ? $userdata : []);
 	}

 	public function generate_token($data = []){
 		if(!is_array($data) || empty($data)) return 0;

 		$username = isset($data['username']) ? $data['username'] : 'unknown';
 		$password = isset($data['password']) ? $data['password'] : '';
 		$timestamp = isset($data['timestamp']) ? $data['timestamp'] : time();
 		// timestamp / password_hash / username
 		$token_data = $timestamp .'/'. $password .'/'. $username;
 		Log::add('Preparing Token Data: '. $token_data);
 		$token_sha = sha1($token_data);
 		Log::add('Encrypted Token Data: '. $token_sha);
 		$new_token = base64_encode($token_sha);
 		Log::add('New Token: '. $new_token);
 		return $new_token;
 	}

 	public function get_token(){
 		$data = $this->get_session_data();
 		return (isset($data['token']) ? $data['token'] : '0');
 	}

 	public function validate_token($token = ''){
 		if(empty($token)) return FALSE;

 		// get session data
 		$data = $this->extract_session();
 		// check data
 		if(!is_array($data) || empty($data)) return FALSE;
 		Log::add('token data : '. print_r($data,1));
 		$token_ori = $this->generate_token($data);
 		Log::add('validating token: '. $token_ori .' <> '. $token);
 		if($token_ori == $token){
 			$valid = TRUE;
 		}else{
 			$valid = FALSE;
 		}
 		Log::add('token valid : '. (($valid) ? 'TRUE' : 'FALSE'));
 		return $valid;
 	}
}
/**
 * Berkas 			: session.php
 * Lokasi			: /system/app/session.php
 *
 * Dibuat Oleh 		: Muh Sukrillah
 * Url				: http://www.sukrillah.xyz
 * Versi 			: 1.0
 */