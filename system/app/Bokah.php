<?php
/**
 * Class Bokah
 *
 * Dibuat Oleh          : Muh. Sukrillah
 * Url                  : http://www.sukrillah.xyz
 * Versi                : 1.0
 */
namespace sasak;

abstract class Bokah{
    /**
     * primary key
     * @var     mixed primary_key
     * @access  public
     */
    protected $_primary_key;
    /**
     * @var     string tale_name
     * @access  private
     */
    protected $_table_name;
    /**
     * @var     string model_name
     * @access  private
     * required
     */
    protected $_model_name;
    /**
     * @var     object model
     * @access  public
     */
    protected $__model;
    /**
     * protected properties
     * @var     array _prop
     * @access  private
     */
    protected $_prop = ['_table_name', '_model_name', '__model', '_primary_key'];

    public function __construct(){
        if(isset($this->_model_name) && class_exists($this->_model_name)){
            $this->__model = new $this->_model_name($this->_table_name);
        }
        \sasak\Log::add('Class ['. __CLASS__ .'] Initialized..');
    }
    //public abstract function 
    /**
     * method to save the current table record
     * @access  public
     * @param   null
     * @return  bool
     */
    public function save(){
        if(empty($this->_table_name)){
            \sasak\Log::add('[Bokah] can not save data on ['.__CLASS__.']. because table_name is empty..');
            return FALSE;
        }
        
        $data = $this->__fetch();
        if(method_exists($this, '__filter_insert')){
            $data = $this->__filter_insert($data);
        }
        return $this->__model->insert($data);
    }
    public function insert(){
        return $this->save();
    }
    /**
     * method to update data to table
     * 
     * @access  public
     * @param   null
     * @return  bool
     */
    public function update($where = []){
        if(empty($this->_table_name)){
            \sasak\Log::add('[Bokah] can not update data on ['. __CLASS__.']. Because table_name is empty..');
            return FALSE;
        }
        $pk = $this->_primary_key;
        $primary_key = $this->$pk;
        $wh = [$pk => $primary_key];
        if(isset($where[$pk]) && !empty($where[$pk])){
            $wh[$pk] = $where[$pk];
            unset($where[$pk]);
        }
        if(!empty($where)){
            $wh = array_merge($wh, $where);
        }
        // don not update primary key
        $this->_prop[] = $this->_primary_key;
        // se date updated as now
        $this->date_updated = date('Y-m-d H:i:s');
        // fetch data
        $data = $this->__fetch();
        if(method_exists($this, '__filter_update')){
            $data = $this->__filter_update($data);
        }
        return $this->__model->update($data, $wh);
    }
    /**
     * method to delete record
     * @access  public
     * @param   null
     * @return  bool
     */
    public function delete($where = []){
        $fk = $this->_primary_key;
        if(is_int($where)){
            $this->$fk = $where;
        }
        $where[$fk] = $this->$fk;
        if(empty($this->_primary_key)){
            $where = $where;
        }
        return $this->__model->delete($where);
    }

    private function __fetch($callback = NULL){
        $data = [];
        foreach($this as $key => $value){
            if(in_array($key, $this->_prop)){
                continue;
            }
            if(is_string($value) || is_int($value)){
                $data[$key] = $value;
            }
        }
        
        if(is_object($callback)){
            return $callback($data);
        }else{
            return $data;
        }
    }

    public function get_all(){
        $args = func_get_args();
        if(!empty($args)){
            $ob =& $this->__model;
            return @call_user_method_array('get_all', $ob, $args);
        }
        return $this->__model->get_all();
    }

    public function get_by($where = [], $data = [], $fetch_all = FALSE){
        return $this->__model->get_by($where, $data, $fetch_all);
    }

    public function count_all(){
        return $this->__model->count_all();
    }

    public function get_columns(){
        $data = $this->__model->get_columns();
        return $data;
    }

}