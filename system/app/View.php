<?php
/**
 * View Class
 * Dibuat Oleh 			: Muh Sukrillah
 * Url 					: http://www.sukrillah.xyz
 * Versi 				: 1.0
 */
namespace sasak;
use Scope;

class View {
	/**
	 * obect instance
	 * @var 	object instance
	 * @access 	private
	 */
	private static $instance;
	/**
	 * variable untuk lokasi file view
	 *
	 * @var 		_source_path
	 * @type 		string
	 * @access 		private
	 */
	private static $_source_path;

	private static $_initialized = FALSE;

	function __construct(){
		self::$instance =& $this;
		if(!defined('VIEW_PATH')){
			Log::add('VIEW_PATH belum dibuat..');
			Error("VIEW_PATH belum dibuat..");
		}
		// reset to default
		self::reset_to_default();
		// log
		Log::add('Class ['. __CLASS__.'] Initialized..');
	}

	public static function &get_instance(){
		return self::$instance;
	}

	public static function set_source_path($path = ''){
		if(!empty($path)){
			self::$_source_path = $path;
		}else{
			self::$_source_path = VIEW_PATH;
		}
	}

	public static function reset_to_default(){
		self::set_source_path(VIEW_PATH);
		self::$_initialized = TRUE;
	}

	public static function load(){
		if(!self::$_initialized){
			self::reset_to_default();
		}
		$args = func_get_args();

		if(empty($args)) return FALSE;

		if(isset($args[0])){
			$file_name = $args[0] .".php";
			$file = self::$_source_path . $file_name;
			if(!file_exists($file)){
				$file = SYSTEM_PATH .'view/'. $file_name;
			}
		}
		$data = [];
		$scope = \sasak\Scope::get_instance();
		if(isset($scope)){
			foreach((array)$scope as $key => $value){
				$data[$key] = $value;
			}
		}
		if(isset($args[1]) && !empty($args[1])){
				$data = $args[1];
		}
		if(isset($file) && file_exists($file) && is_file($file)){
			ob_start();
			Log::add('Loading view..');
			if(isset($data) && !empty($data)){
				extract($data);
			}
			include_once $file;			
			$response = ob_get_clean();
			return $response;
		}else{
			\sasak\Log::add('View file not found..');
			Error('[View] file tidak tersedia.. ['. $file .']');
			return self::load('404', [], 404);
		}
	}
}