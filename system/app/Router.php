<?php
/**
 * Router
 *
 * Dibuat Oleh          : Muh Sukrillah
 * Url                  : http://www.sukrillah.xyz
 * Versi                : 1.0
 */
namespace sasak;

class Router extends URI{
    private static $instance;
    private $_routes = [];
    private $controller_type = 'class';

    var $vars = '';
    var $controller_file;
    var $controller_name;
    var $method_name;
    var $params  = array();
    var $is_manual_routing = FALSE;

    function __construct(){
        self::$instance =& $this;
        parent::__construct();
        $this->fetch_url();
    }

    public static function &instance(){
        return self::$instance;
    }

    function fetch_url(){
        $url = $this->url;

        if(isset($url[0]) && !empty($url[0])){
            $url[0] = $this->trim_extention($url[0]);
            $this->controller_name = $url[0];
        }else{
            if(defined("DEFAULT_CONTROLLER")){
                $this->controller_name = DEFAULT_CONTROLLER;
            }
        }
        if(isset($url[1]) && !empty($url[1])){
            $url[1] = $this->trim_extention($url[1]);
            $this->method_name = $url[1];
        }else{
            $this->method_name = 'index';
        }
        if(sizeof($url) > 2){
            $params = array_slice($url, 2);
            $params = array_map(function($a){
                return $this->trim_extention($a);
            }, $params);
            $this->params = $params;
        }
        return $this;
    }

    function fetch_controller(){
        if($this->is_manual_routing){
            return;
        }
        $file_url = ltrim($this->uri, '/');
        if(strpos($file_url, '?')){
            $file_url = substr($file_url, 0, strpos($file_url, '?'));
        }
        $file_name = $file_url;
        $file_name_arg = explode('/', $file_name);
        $arg_backup = $file_name_arg;
        $path = '';
        $args = [];
        foreach($file_name_arg as $key => $value){
            if(file_exists($f = CONTROLLER_PATH . $path . $value .'.php')){
                $method = 'index';
                if(isset($file_name_arg[$key + 1]) && !empty($file_name_arg[$key + 1])){
                    $method = $file_name_arg[$key + 1];
                }
                $params = [];
                if(isset($file_name_arg[$key + 2])){
                    $params = array_slice($arg_backup, $key + 2, 2);
                }
                $args = [
                    'controller_file' => $f,
                    'controller_name' => ucfirst($value),
                    'method_name' => $method,
                    'params' => $params
                ];
                foreach($args as $k => $v){
                    $this->$k = $v;
                }
                break;
            }
            $path .= $value . '/';
        }
        return $args;
    }

    function regex(){
        $pattern = 'lorem/ipsum/index/(:any)';
        $pattern = str_replace('(:any)', '(.*)', $pattern);
        if(preg_match('#'. $pattern .'#', 'lorem/ipsum/index/1/')){
            exit('match');
        }else{
            exit('mismatch');
        }
    }

    function get_controller($str = NULL){
        if(isset($str) && is_string($str) && !empty($str)){
            $c = explode('@', $str);
            if(isset($c[0]) && !empty($c[0])){
                $this->controller_name = $c[0];
            }
        }
        return $this->controller_name;
    }

    function get_method($str = NULL){
        if(isset($str) && is_string($str) && !empty($str)){
            $m = explode('@', $str);
            if(isset($m[1]) && !empty($m[1])){
                $this->method_name = $m[1];
            }
        }
        return $this->method_name;
    }

    function get_params($router_pattern = NULL, $url = NULL){
        if(isset($router_pattern) && isset($url)){
            $params = [];
            @preg_match($router_pattern, $url, $params);
            if(!empty($params)){
                $params = array_map(function($value){
                    if(preg_match('#^([0-9]+)$#', $value)){
                        $value = (int) $value;
                    }
                    return $value;
                }, $params);
                $params = array_slice($params, 1);
                $this->params = $params;
            }
        }
        return $this->params;
    }

    function get($router = '/', $controller = NULL){
        if(empty($router)){
            $router = '/';
        }
        if(!isset($controller) || empty($controller)){
            return $this;
        }
        if(Input()->server('REQUEST_METHOD') == 'GET'){
            $this->_routes['get'][] = ['router' => $router, 'controller' => $controller];
        }
        return $this;
    }

    function post($router = '', $controller = ''){
        if(empty($router)){
            $router = '/';
        }
        if(is_string($controller) && empty($controller)){
            return;
        }

        if(Input()->server('REQUEST_METHOD') == 'POST'){
            $this->_routes['post'][] = ['router' => $router, 'controller' => $controller];
        }
    }

    function delete($router = '/', $controller = ''){
        if(empty($router)){
            $router = '/';
        }
        if(is_string($controller) && empty($controller)){
            return;
        }
        if(Input()->server('REQUEST_METHOD') == 'DELETE'){
            $this->_routes['delete'][] = ['router' => $router, 'controller' => $controller];
        }
    }

    function all($router = '/', $controller = ''){
        if(empty($router)){
            $router = '/';
        }
        if(is_string($controller) && empty($controller)){
            return;
        }
        $routes = ['router' => $router, 'controller' => $controller];
        $this->_routes['all'][] = $routes;
        if(isset($this->_routes['get']) && is_array($this->_routes['get']) && !empty($this->_routes['get'])){
            $this->_routes['get'][] = array_merge($this->_routes['get'], $routes);
        }
        if(isset($this->_routes['post']) && is_array($this->_routes['post']) && !empty($this->_routes['post'])){
            $this->_routes['post'][] = array_merge($this->_routes['post'], $routes);
        }
        if(isset($this->_routes['delete']) && is_array($this->_routes['delete']) && !empty($this->_routes['delete'])){
            $this->_routes['delete'][] = array_merge($this->_routes['delete'], $routes);
        }
    }

    function error($code = 404, $controller = ''){
        $this->_routes['error_'. $code] = $controller;
    }

    private function filter_router($router = ''){
        if(preg_match('#\{(id|number|angka|int)\}#', $router)){
            $router = preg_replace('#\{(id|number|angka|int)\}#', '([0-9]+)', $router);
        }
        if(preg_match('#\{(any)\}#', $router)){
            $router = preg_replace('#\{(any)\}#', '([^*]+)', $router);
        }
        if(preg_match('#\{([a-z]+)\}#', $router)){
            $router = preg_replace('#\{([a-z]+)\}#', '([a-z]+)', $router);
        }
        return $router;        
    }

    private function do_route($method = 'all'){
        $url = '/'. ltrim($this->uri, '/');
        if(strpos($url, '?')){
            $url = substr($url, 0, strpos($url, '?'));
        }
        Log::add('Url routing: '. $url);
        if(isset($this->_routes[$method]) && !empty($this->_routes[$method])) {
            Log::add('Routing method '. $method .' found..');
            //Log::add('Routing data: '. print_r($this->_routes[$method], 1));
            $routes = '';
            $match = 0;
            foreach($this->_routes[$method] as $key => $value){
                $router = '/'. ltrim($value['router'], '/');
                $router = $this->filter_router($router);
                $regex = '#^'. $router .'$#';
                if(preg_match($regex, $url)){
                    Log::add('Router match..');
                    Log::add('URL Mask : '. $regex .', '. $url);
                    $params = $this->get_params($regex, $url);
                    if (is_array($params) && !empty($params) && sizeof($params) > 1) {
                        $params = array_slice($params, 1);
                    }
                    if (is_array($value['controller'])) {
                        $controller = end($value['controller']);
                        $opt = array_slice($value['controller'], 0, count($value['controller']) - 1);
                        $params_bc = end($params);
                        foreach($opt as $key => $option){
                            $option = strtolower($option);

                            if($option == 'param'){
                                $params[$key] = $params_bc;
                                continue;
                            }
                            switch($option){
                                case 'scope':
                                    $c = new \sasak\Scope();
                                    $params[$key] = $c;
                                break;
                                case 'loader':
                                    $c = new \stdClass();
                                    $loader = new Loader();
                                    $c->load = $loader;
                                    $params[$key] =& $c;
                                    unset($c);
                                    unset($loader);
                                break;
                            }
                            if(strtolower($option) == 'controller'){
                                $c = new Controller();
                                $params[$key] =& Controller();
                                unset($c);
                            }
                            if(strtolower($option) == 'view'){
                                $v = new \sasak\View();
                                $params[$key] =& View::get_instance();
                                unset($v);
                                unset($opt[$Key]);
                                continue;
                            }
                            if(strtolower($option) == 'response'){
                                $r = new \sasak\Response();
                                $params[$key] =& $r;//\sasak\Response::get_instance();
                                unset($r);
                                unset($opt[$key]);
                                continue;
                            } 
                        }
                        if(is_object($controller)){
                            $value['controller'] = $controller;
                        }
                        if(is_string($controller) && strpos($controller, '@')){
                            $value['controller'] = $controller;
                        }
                    }
                    if (is_object($value['controller'])) {
                        $this->controller_type = 'function';
                        $this->controller_name = $value['controller'];
                        $this->params = $params;
                    }
                    if (is_string($value['controller'])) {
                        if(strpos($value['controller'], '@')){
                            $c = explode('@', $value['controller']);
                        }else{
                            $c[0] = $value['controller'];
                            $c[1] = 'index';
                        }
                        $this->controller_type = 'class';
                        $file_name = 'index';
                        if(strpos($c[0], '\app\controller\\') === 0){
                            $file_name = substr($c[0], 16);
                        }
                        if(strpos($c[0], '/')){
                            $fn = explode('/', $c[0]);
                            $file_name = $c[0];
                            $c[0] = end($fn);
                        }
                        $this->controller_file = CONTROLLER_PATH . $file_name .'.php';
                        $this->controller_name = $c[0];
                        $this->method_name = $c[1];
                        $this->params = $params;
                        $this->is_manual_routing = TRUE;
                    }
                    Log::add('Router handler found..');
                    Log::add('Param : '. print_r($params,1));
                    $match++;
                    break;
                }
            }
            if(!$match){
                Log::add('No router match with url: '. $url);
            }
        }else{
            Log::add('No router handler found..');
        }
    }
    /**
     * method to get error from routers
     *
     * @access  public
     * @param   int $code status code
     * @return  string resonse
     */
    public function get_error($code = 404){
        $error = '';
        if(isset($this->_routes['error_'. $code])){
            $router = $this->_routes['error_'. $code];
            if(is_object($router)){
                $error = $router();
            }
            if(is_string($router)){
                if(strpos($router, '@')){
                    $r_ex = explode('@', $router);
                    $controller = $r_ex[0];
                    $method = $r_ex[1];
                    if(class_exists($controller)){
                        if(method_exists($controller, $method)){
                            $error = @call_user_method($method, $controller);
                        }
                    }
                }
            }
        }
        Log::add('Error code '. $code .' found..');
        return $error;
    }

    private function route(){
        Log::add("Routing started..");
        $method = Input()->server('REQUEST_METHOD');
        $method = strtolower($method);
        Log::add('Routing method : '. $method);
        $this->do_route($method);
        return;
        /*

        if(isset($this->_routes['get']) && !empty($this->_routes['get'])){
            $routes = '';
            foreach($this->_routes['get'] as $key => $value){
                //$routes .= '['. $value['router'] .'] = '. gettype($value['controller']) ."\n";
                $router = '/'. ltrim($value['router'], '/');
                $router = $this->filter_router($router);
                $regex = '#^'. $router .'$#';
                if(preg_match($regex, $url)){
                    $params = $this->get_params($regex, $url);
                    if(is_array($params) && !empty($params) && sizeof($params) > 1){
                        $params = array_slice($params, 1);
                    }
                    if(is_object($value['controller'])){
                        $this->controller_type = 'function';
                        $this->controller_name = $value['controller'];
                        $this->params = $params;
                    }
                    if(is_string($value['controller'])){
                        $c = explode('@', $value['controller']);
                        $this->controller_type = 'class';
                        $this->controller_name = $c[0];
                        $this->method_name = $c[1];
                        $this->params = $params;
                    }

                    //$routes .= ($url .' == '. $regex) ."\n";
                   // $routes .= print_r($params, 1);
                    break;
                }else{
                    $routes.= ($url .' <> '. $regex) ."\n";
                }
            }            
            //echo '<pre>';
            //print_r($this->_routes['get']);
            //echo $routes;
            //echo '</pre>';
            //exit;
        }

        if(isset($this->_routes['post']) && !empty($this->_routes['post'])){
            foreach($this->_routes['post'] as $key => $value){
                $router = '/'. ltrim($value['router'], '/');
                $router = $this->filter_router($router);
                $regex = '#^'. $router .'$#';
                if(preg_match($regex, $url)){
                    $params = $this->get_params($regex, $url);
                    if(is_array($params) && !empty($params) && sizeof($params) > 1){
                        $params = array_slice($params, 1);
                    }
                    if(is_object($value['controller'])){
                        $this->controller_type = 'function';
                        $this->controller_name = $value['controller'];
                        $this->params = $params;
                    }
                    if(is_string($value['controller'])){
                        $c = explode('@', $value['controller']);
                        $this->controller_type = 'class';
                        $this->controller_name = $c[0];
                        $this->method_name = $c[1];
                        $this->params = $params;
                    }

                    //$routes .= ($url .' == '. $regex) ."\n";
                    //$routes .= print_r($params, 1);
                    break;
                }
            }
            //echo '<pre>';
            //print_r($this->_routes['post']);
            //echo $routes;
            //echo '</pre>';
            //exit;
        }*/
    }

    function process(){
        // run the router
        $this->route();
        Log::add('Starting Controller..');
        Log::add('Controller Type: '. $this->controller_type);
        if($this->controller_type == 'function'){
            $controller = $this->controller_name;
            Log::add('Controller Type Callback Detected..');
            if(is_array($this->params) && !empty($this->params)){
                $responses = call_user_func_array($controller, $this->params);
            }else{
                $responses = $controller($view);
            }
            Log::add('Sending Response from callback controller..');
            Response::send($responses);    
        }

        $this->fetch_controller();
        /*$file_url = ltrim($this->uri, '/');
        if(strpos($file_url, '?')){
            $file_url = substr($url, 0, strpos($url, '?'));
        }
        $file_name = $file_url;
        $file_name_arg = explode('/', $file_name);
        $class_name = end($file_name_arg);
        $method_name = 'index';
        if(!file_exists(CONTROLLER_PATH . $file_name .'.php')){
            $class_name = $this->get_controller();
            $method_name = $this->get_method();
            $params = $this->get_params();
            $fn = explode('\\', $class_name);
            $file_name = end($fn);
        } */
        $class_file = $this->controller_file;//CONTROLLER_PATH . $file_name .".php";
        $class_name = $this->controller_name;
        $method_name = $this->method_name;
        $params = $this->params;
        if(file_exists($class_file)){        
            include_once $class_file;
            Log::add('Controller File: '. $class_file);
            $cnpos = strpos($class_name, '\app\controller\\');
            
            if($cnpos === 0){
                $class_name = substr($class_name, 16);//'\app\controller\\'); 
            }

            $controller = '\app\controller\\'. ucwords($class_name);
            if(class_exists($controller)){              
                Log::add('Controller class found : '. $controller);
                $class = new $controller();
            }else{
                Log::add('Object tidak tersedia : '. $controller .', class: '. $class_name);
                Response::send($this->get_error('404'), [
                    'status_code' => 404,
                    'status_text' => 'Halaman Tidak Tersedia'
                ]);
            }
        }else{
            Log::add('File tidak tersedia : '. $class_file);
            Response::send($this->get_error('404'), [
                'status_code' => 404, 
                'status_text' => 'Halaman Tidak Tersedia'
            ]);
        }
        if(isset($class) && method_exists($class, $method_name)){
            if(!empty($params)){
                $response = @call_user_method_array($method_name, $class, $params);
            }else{
            	$response = $class->$method_name();
//                $response = call_user_method($method_name, $class);
            }
            //echo $controller .'::'. $method_name;
            Response::send($response);
            Log::add('Response send..');
        }else{
            Log::add($class_name .'::'. $method_name .' tidak tersedia..');
            Response::send($this->get_error('404'), [
                'status_code' => 404, 
                'status_text' =>  'Halaman Tidak Tersedia'
            ]);
        }  
    }
    /**
     * listen the request
     * @access  public
     * @param   null
     * @return  null||response html
     */
    public function listen(){
        $this->process();
    }
} 