<?php
/**
 * Password Hash
 *
 * Dibuat Oleh          : Muh. Sukrillah
 * Url                  : http://www.sukrillah.xyz
 * Versi                : 1.0
 */
namespace sasak;

class Hash {

    public static function uniqid(){
        $args = func_get_args();
        $npt = [];
        if(!empty($args)){
            $npt = array_map(function($arg){
                $r = '%';
                if(!is_int($arg)){
                    $r.= 3;
                }else{
                    $r.= (int) $arg;
                }
            }, $args);
        }
        $m = microtime(true);
        //$pt = ["%8X","%2x"];
        //shuffle($pt);
        //$s = implode('', $pt);
        $s = '%8X';
        $uid = sprintf($s,floor($m),($m-floor($m))*1000000);
        return $uid;
    }

    public static function make($password = NULL){
        return password_hash($password, PASSWORD_BCRYPT, ['cost' => 10]);
    }

    public static function verify($password = NULL, $hash = NULL){
        return password_verify($password, $hash);
    }
}