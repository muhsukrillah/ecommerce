<?php
/**
 * Database Connection Driver Class
 * 
 * Dibuat Oleh 			: Muh. Sukrillah
 * Url 					: http://www.sukrillah.xyz
 * Versi 				: 1.0
 */
namespace sasak;
use \PDO;

class Database {
	/**
	 * database constant for fetch type object
	 * @const 	int FETCH_OBJECT
	 * @access 	public
	 */
	const FETCH_OBJECT = 1;
	/**
	 * database constant for fetch type array
	 * @const 	int FETCH_ARRAY
	 * @access 	public
	 */
	const FETCH_ARRAY = 2;
	/**
	 * Database constant for fetch type all [object,array]
	 * @const 	int FETCH_ALL
	 * @access 	public
	 */
	const FETCH_ALL = 3;
	/**
	 * Connection instance
	 *
	 * @var 	resource $db
	 * @access 	private
	 */
	private $_db;
	/**
	 * Connection status
	 *
	 * @var 	bool $_connected
	 * @access 	private
	 */
	private $_connected = FALSE;
	/**
	 * instance
	 */
	private static $instance;
	/**
	 * Constructor
	 */
	function __construct(){
		$this->_connected = FALSE;
		self::$instance =& $this;
		Log::add('Class ['. __CLASS__ .'] Initializsed..');
	}

	public static function &instance(){
		return self::$instance;
	}

	public function conn(){
		return $this->get_connection();
	}

	public function get_connection(){
		if(!defined('DB_DRIVER')){
			Error('[Database Error] : Driver database belum didefinisikan..');
		}
		switch(strtolower(DB_DRIVER)){
			case 'pdo':
				$db = $this->__pdo_connect();
				$this->_db = new \sasak\database\driver\PDO_Driver($db);
			break;
			case 'mysqli':
				$db = $this->__mysqli_connect();
				$this->_db = new \sasak\database\driver\MySQLi_Driver($db);
			break;
		}
		return $this->_db;
	}

	function __exception($ex){
		Error('[Database Error] : '. $ex->getMessage());
	}

	private function __pdo_connect(){
		if(!class_exists('PDO')){
			Error('[Database Error] : PDO Library Not Exists!');
		}
		$conn = NULL;
		//set_exception_handler(array($this, '__exception'));
		try{
//			exit(DB_HOST . DB_USER . DB_NAME . DB_USER . DB_PASS);
			$conn = new PDO('mysql:host='. DB_HOST .';dbname='. DB_NAME, DB_USER, DB_PASS);
			$this->_connected = TRUE;
			Log::add('Database Connection using PDO [connected]');
		}catch(PDOException $e){
			Log::add('Database PDO Connection Error..');
			Error('[Database Error] ' . $e->getMessage());
		}
		//restore_exception_handler();
		return $conn;
	}

	private function __mysqli_connect(){
		$conn = NULL;
		try{
			$conn = $this->__mysqli_sql_connect();
		}catch(Exception $e){
			Error('[Database Error] : '. $e->getMessage());
		}
		return $conn;
	}

	private function __mysqli_sql_connect(){
		$mysqli = NULL;
		try{
			$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);// || die("Koneksi Database Gagal!");
			$this->_connected = TRUE;
			Log::add('Database Connection using MySQLi [connected]');
		}catch(mysqli_sql_exception $e){
			Log::add('Database MySQLi Connection Error..');
			throw $e;
		}
		return $mysqli;
	}

	public function close_connection(){
		switch(strtolower(DB_DRIVER)){
			case 'pdo':
				$this->_db = NULL;
			break;
			case 'mysqli':
				$this->_db->close();
			break;
		}
	}

	public function close_db(){
		$this->close_connection();
	}

	public function close(){
		$this->close_db();
	}

	function __descruct(){
		$this->close_db();
	}

	function escape_str($str, $like = FALSE)
	{
		if (is_array($str))
		{
			foreach ($str as $key => $val)
			{
				$str[$key] = $this->escape_str($val, $like);
			}

			return $str;
		}
		
		//Escape the string
		$str = $this->quote($str);
		
		//If there are duplicated quotes, trim them away
		if (strpos($str, "'") === 0)
		{
			$str = substr($str, 1, -1);
		}
		
		// escape LIKE condition wildcards
		if ($like === TRUE)
		{
			$str = str_replace(['%', '_'],['%', '_', $this->_like_escape_chr], $str);
		}

		return $str;
	}
}