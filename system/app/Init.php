<?php
namespace sasak;
/**
 * environment setting
 */
if(!defined('ENVIRONMENT')){
	define("ENVIRONMENT", "development");
}
if(in_array(ENVIRONMENT, ['testing','production'])){
	error_reporting(0);
}else{
	ini_set('display_errors', 1);
	error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
}
/**
 * timezone set
 * secara default jika tidak di set
 * maka akan kita gunakan timezone asia/jakarta
 */
if(!defined('TIME_ZONE')){
	define('TIME_ZONE', "Asia/Jakarta");
}
$tz = TIME_ZONE;
if(empty($tz)){
	$tz = "Asia/Jakarta";
}
date_default_timezone_set($tz);
/**
 * Tanggal Sekarang
 */
function now($format = 'Y-m-d h:i:s'){
	return date($format, time() + 3600);
}
// buat alias fungsi dari now()
// agar mudah diingat
function system_date($format = 'Y-m-d h:i:s'){
	return date($format);
}
if(!defined("SYSTEM_PATH")){
	die("System direktori belum didefinisikan");
}
include_once SYSTEM_PATH . "app/Log.php";
Log::add("\n-----------------------------------------------------------\n", FALSE);
Log::add('system started..');
/**
 * Load Autoloader Class
 */
include_once SYSTEM_PATH . 'app/Autoloader.php';
$Autoloader = new Autoloader();
$Autoloader->register();
/**
 * Exception Class
 */
include_once SYSTEM_PATH . 'app/Exception.php';
$Exception = new SasakException();
// set default exception handler
set_exception_handler(array($Exception, 'handler'));
/*set_error_handler(function($e, $m){
	echo ('<pre>'. print_r(func_get_args(),1) .'</pre>');
});*/
/**
 * load bench mark class
 */
//include_once SYSTEM_PATH . "app/Benchmark.php";
Benchmark::mark('sasak_start', SASAK_START);
function &_log($message = ''){
	return Log::add($message);
}
//include_once SYSTEM_PATH . "app/Input.php";
$Input = new Input();
function &Input(){
	return Input::instance();
}
//include_once SYSTEM_PATH . "app/Model.php";
//include_once SYSTEM_PATH . "app/URI.php";
//include_once SYSTEM_PATH . "app/Router.php";
//include_once SYSTEM_PATH . "app/Actions_Hooks.php";
$Actions_Hooks = new Actions_Hooks();
function &Actions(){
	return Actions_Hooks::instance();
}
//include_once SYSTEM_PATH . "app/Filter_Hooks.php";
$Filter_Hooks = new Filter_Hooks();
function &Filter(){
	return Filter_Hooks::instance();
}
/**
 * uri Class
 */
$URI = new URI();
/**
 * helpers
 */
include_once SYSTEM_PATH . 'helpers/general_helpers.php';
/**
 * Hooks
 */
include_once CONFIG_PATH . 'Hooks.php';
/**
 * View Factor
 */
include_once SYSTEM_PATH . 'app/View.php';
/**
 * response library
 */
//include_once SYSTEM_PATH . 'app/Response.php';
/**
 * load controller library
 */
//include_once SYSTEM_PATH . "app/Controller.php";
/**
 * Session Class
 */
//include_once SYSTEM_PATH . "app/Session.php";
$session = new Session();
function &session(){
	return Session::instance();
}
/**
 * User Class
 */
//include_once SYSTEM_PATH . "app/User.php";
function &User(){
	return User::instance();
}

$Router = new Router();
function &Router(){
	return Router::instance();
}
if(file_exists(SYSTEM_PATH . 'routes/Routes.php')){
	include SYSTEM_PATH . 'routes/Routes.php';
}
if(file_exists(CONFIG_PATH . 'Router.php')){
	include_once CONFIG_PATH . 'Router.php';
}
$Response = new Response();
Log::add('Response Class Started..');

function &Response(){
	return Response::instance();
}

function &Controller(){
	return Controller::instance();
}

return Router();