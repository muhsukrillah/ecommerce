<?php
/*
 * Core Controller Class
 * Version : 1.0
 */
namespace sasak;

class Controller {
	private static $instance;
	/*
	 * public property for Loader class
	 */
	public $load;
	/**
	 * public property for input class
	 */
	public $input;
	/**
	 * public property for URI class
	 */
	public $uri;
	/**
	 * public property for Session class
	 */
	public $session;
	
	function __construct(){	
		self::$instance =& $this;
		Log::add('Class: '. __CLASS__ .' Inititialized..');

		$this->security = new Security();

		$this->input =& Input::instance();

		$this->uri =& uri();

		$this->session =& Session::instance();
		
		//include SYSTEM_PATH .'app/Loader.php';
		$this->load = new Loader();
		// user auth
		User::auth_check();
		// auto load libraries
		$this->load->library(['lib_title','breadcrumb'], [['namespace'=>'title']]);
		$this->data['title'] =& $this->title;
		$this->data['breadcrumb'] =& $this->breadcrumb;
		$this->load->library('Menu', ['namespace' => 'menu']);
//		$this->data['admin_menu'] =& $this->admin_menu;
//		$this->admin_menu();

	}

	public function ajax($method = ''){
		if($this->input->is_ajax_request()){
			$methods = get_class_methods($this);
			$ajax = [];
			$ajax_method = '';
			foreach($methods as $key => $m){
				if(preg_match('#^ajax_([a-zA-Z0-9\_]+)$#', $m)){
					$ajax[] = $m;
				}
			}
			$method = 'ajax_'. $method;
			if(!empty($method) && in_array($method, $ajax)){
				$args = func_get_args();
				$instance =& $this;			
				if(count($args) > 1){
					$params = array_slice($args, 1);
					return call_user_method_array($method, $instance, $params);
				}
				return call_user_method($method, $instance);
			}
			return 'yes you\'re ajax!' . print_r($ajax,1);
		}else{
			return 'no, you\'re not ajax!';
		}
	}

	function admin_init(){

	}

	function frontend_init(){

	}

	function __destruct(){

	}

	public static function &instance(){
		return self::$instance;
	}
}