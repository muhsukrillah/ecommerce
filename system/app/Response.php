<?php
/**
 * Respon Library
 *
 * Dibuat Oleh 			: Muh Sukrillah
 * Url 					: http://www.sukrillah.xyz
 * Versi 				: 1.0
 */
namespace sasak;

class Response {

	public static $responses = '';
	private static $instance;
	private static $status_code = 200;
	private static $status_text = 'Tersedia';
	private static $headers = [];

	function __construct(){
		self::$instance =& $this;
	}

	public static function &instance(){
		return self::$instance;
	}

	public static function &get_instance(){
		return self::$instance;
	}

	private static function __get_momory_usage(){
		$usage = (memory_get_usage() / 1024);
		return number_format(intval($usage) / 1024, 2) .' MB';
	}

	private static function __get_elapsed_time(){
		$time = Benchmark::elapsed_time('sasak_start', 'sasak_end', 3);
		return $time;//number_format($time, 3);
	}

	public static function set_header($header = NULL){
		if(isset($header) && is_string($header) && !empty($header)){
			self::$headers[] = $header;
		}
	}

	private static function extract_headers(){
		if(!empty(self::$headers)){
			foreach(self::$headers as $header){
				@header($header);
			}
		}
	}

	public static function set_status($code = 200, $text = 'Tersedia'){
		self::set_status_code($code);
		self::set_status_text($text);
	}

	public static function set_status_code($code = 200){
		Log::add('Set header status code to => '. $code);
		self::$status_code = $code;
	}

	public static function set_status_text($text = 'Tersedia'){
		Log::add('Set header status text to => '. $text);
		self::$status_text = $text;
	}

	public static function get_status_code(){
		return self::$status_code;
	}

	public static function get_status_text(){
		return self::$status_text;
	}

	public static function send($data = '', $options = []){ //$status_code = NULL, $status = NULL){
		error_reporting(E_ALL);
		if(is_array($options) && !empty($options)){
			extract($options);
		}

		if(isset($status_code)){
			self::set_status_code($status_code);
		}
		if(isset($status_text)){
			self::set_status_text($status_text);
		}
		// get status code
		// get status text
		$code = self::get_status_code();
		$text = self::get_status_text();
		// start ob
		ob_start();
		@header("HTTP/1.1 $code $text");
		self::extract_headers();
		Log::add('Sending responses..');
		$response = $data;
		if(preg_match('#{memory_usage}#', $response)){
			$response = preg_replace('#{memory_usage}#', self::__get_momory_usage(), $response);
		}
		if(preg_match('#{elapsed_time}#', $response)){
			$response = preg_replace("#{elapsed_time}#", self::__get_elapsed_time(), $response);
		}
		echo $response;
		Log::add('System end..');
		Log::add("\n==================================================================", FALSE);
		exit;
	}
}