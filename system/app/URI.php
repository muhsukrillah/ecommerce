<?php
/**
 * Class URI
 * Dibuat Oleh          : Muh. Sukrillah
 * Url                  : http://www.sukrillah.xyz
 * Versi                : 1.0
 */
namespace sasak;
//use app\Input;

class URI {
    /**
     * Class instance
     * @var object|null $_instance
     * @access  private
     */
    private static $_instance;

    public $url = array();
    public $uri = '';
    function __construct(){
        $this->uri = '';

        // $request_uri = $_GET['route'];

        $request_uri = Input()->server('REQUEST_URI');//$_SERVER['REQUEST_URI'];
        $uri = preg_replace('#(^'. $this->__get_base_path() .')#', '', $request_uri);
        //exit($this->__get_base_path() . ' => '. $uri);
        if(!empty($uri)){
            $this->uri = $uri;
            if(strpos($uri, '?')){
                $uri = substr($uri, 0, strpos($uri, '?'));
            }

            //$this->url = explode('/', $uri);
            $i = 1;
            foreach(explode('/', $uri) as $seg){
                $this->url[$i++] = $seg;
            }
            //print_r($uri);
            //exit;
        }else{
            $this->uri = '/';
        }
        self::$_instance =& $this;
        Log::add('Class ['. __CLASS__ .'] Initialized..');
    }

    public static function &instance(){
        return self::$_instance;
    }

    public function query_string(){
        $get = Input()->get();
        $url = $this->array_to_url($get);
        if(!empty($url)){
            return '?'. $url;
        }else{
            return $url;
        }
    }

    public function request(){
        $uri = $this->uri;
        if($this->uri == '/') $uri = '';
        return '/'. ltrim($uri, '/');
    }

    public function compare($data = []){
        $get = Input()->get();
        $data = array_merge($get, $data);
        $url = $this->array_to_url($data);
        if(!empty($url)){
            return '?'. $url;
        }else{
            return $url;
        }
    }

    private function __get_base_path(){
        $path = str_replace(['index.php/', 'index.php'], '', $_SERVER['SCRIPT_NAME']);
        return $path;
    }

    public function base_url($segment = ''){
        $base_url = '';
        if(!defined("BASE_URL") || empty(BASE_URL)){
            $scheme = isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http';
            $domain = $_SERVER['SERVER_NAME'];
            $request = $this->__get_base_path();
            $port = $_SERVER['SERVER_PORT'];
            if($port == 80){
                $port = '';
            }else{
                $port = ':'. $port;
            }
            /*echo '<pre>';
            print_r($_SERVER);
            exit($scheme); */
            $base_url = $scheme .'://'. $domain . $port . $request;
        }else{
            $base_url = BASE_URL;
        }
        /**echo $_SERVER['PHP_SELF'] .'<br>';
        echo $this->__get_base_path() .'<br>';
        echo $request .'<br>';
        echo $base_url;
        exit;*/
        return $base_url . (!empty($segment) ? ltrim($segment, '/') : '');
    }

    public function current_url($segment = ''){
        $query_string = $this->query_string();

        if(empty($this->url)){
            return $this->base_url($segment . $query_string);
        }else{
            $url = implode('/', $this->url) . (!empty($segment) ? ltrim($segment, '/') : '');
            return base_url($url . $query_string);
        }
    }

    public function trim_extention($str = ''){
    	if(!defined("URL_SUBFIX")){
    		return $str;
    	}
        if(strpos($str, URL_SUBFIX)){
            $str = str_replace(URL_SUBFIX, '', $str);
        }
        return $str;
    }

    public function segment($seg = NULL){
    	if(isset($this->url[$seg])){
    		$data = $this->url[$seg];
    		$data = $this->trim_extention($data);
    		return $data;
    	}
        if(!isset($seg)){
            return $this->url;
        }
    	return FALSE;
    }

    public function array_to_url($data = []){
        if(!is_array($data)) return FALSE;

        $u = '';
        foreach($data as $key => $value){
            $u.= $key .'='. $value .'&';
        }
        $u = rtrim($u, '&');
        return $u;
    }
}