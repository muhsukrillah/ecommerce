<?php
/**
 * User Class
 *
 * Dibuat Oleh 		: Muh Sukrillah
 * Url 				: http://www.sukrillah.xyz
 * Versi 			: 1.0
 */
namespace sasak;

class User {
	/**
	 * user instance
	 *
	 * @var 		$instance
	 * @access 		private
	 * @type 		object
	 */
	private static $instance;
	/**
	 * user login status
	 *
	 * @var 		$is_loggedin
	 * @access 		private
	 * @type 		boolean
	 */
	private static $is_loggedin = FALSE;
	/**
	 * data instead
	 *
	 * @var 		$data
	 * @access 		public
	 * @type 		object
	 */
	public static $data;
	/**
	 * User_Role instance
	 *
	 * @var 		$role
	 * @access 		public
	 * @type 		object
	 */
	public static $role;
	/**
	 * user class constructor
	 *
	 * @access 		public
	 * @param 		null
	 * @return 		null
	 */
	function __construct(){
		self::$instance =& $this;
	}

	public static function auth_check(){
		self::$is_loggedin = FALSE;

		//require OBJECT_PATH . 'user.php';

		$controller =& Controller::instance();
		$session =& Session::instance();

		$userdata = $session->extract_session();
		$username = isset($userdata['username']) ? $userdata['username'] : 'unknown';
		$password = isset($userdata['password']) ? $userdata['password'] : '';
		$timestamp = isset($userdata['timestamp']) ? $userdata['timestamp'] : time();
		include_once "User_Role.php";
		if(!class_exists("\sasak\User_Role")){
			Error("User: User_Role class tidak tersedia..");
		}

		$controller->load->model('role');
		$role_model =& $controller->role_model;
		//$controller->load->object('user');
		//$controller->load->model('user');
		$user = new \sasak\object_model\User($username);
		if(isset($user->id_user)){
			if(md5($user->password) == $password){
				self::$is_loggedin = TRUE;

				if(!file_exists(SYSTEM_PATH . "app/User_Role.php") || !is_file(SYSTEM_PATH . "app/User_Role.php")){
					Error("User: User_Role class tidak tersedia.. [file]");
				}

				$role = new \sasak\User_Role($user->level, $role_model);
				self::$role =& $role;

				self::$data =& $user;
				return;
			}else{
				Log::add('Password missmatch. ');//. md5($user->password) .' <> '. $password);
			}
		}else{
			Log::add('User not found with username => '. $username);
			Log::add('userdata => '. print_r($userdata, 1));
		}
		$role = new \sasak\User_Role(0, $role_model);
		self::$role =& $role;
	}

	public static function get_data($meta = ''){
		if(self::$is_loggedin){
			if(isset(self::$data->$meta)){
				return self::$data->$meta;
			}else{
				return FALSE;
			}
		}
		return FALSE;
	}

	public static function get_role(){
		if(self::$is_loggedin){
			return self::$role->get_role_title();
		}else{
			return 'Unknown';
		}
	}

	public static function get_profile_url($data = ''){
		return admin_url('profile/'. ltrim($data, '/'));
	}

	public static function profile_url($data = ''){
		return self::get_profile_url($data);
	}

	public static function logout_url($data = ''){
		return logout_url($data);
	}

	public static function get_logout_url($data = ''){
		return self::logout_url($data);
	}

	public static function is_logedin(){
		//self::auth_check();

		return self::$is_loggedin;
	}

	public static function auth($redirect_to = ''){
		if(!self::is_logedin()){
			if(empty($redirect_to)){
				$redirect_to = uri()->current_url();
			}
			$session =& \sasak\Session::instance();
			$session->set_userdata([]);
			header("location: ". login_url() . (!empty($redirect_to) ? '?next='. urlencode($redirect_to) : ''));
			exit;
		}
	}

	public static function instance(){
		return self::$instance;
	}
}
/*
 * Berkas 			: user.php
 * Lokasi 			: /lib/user.php
 *
 * Dibuat Oleh 		: Muh Sukrillah
 * Url 				: http://www.sukrillah.xyz
 */