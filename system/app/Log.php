<?php
/**
 * Log library
 *
 * Dibuat Oleh 		: Muh Sukrillah
 * Url 				: http://www.sukrillah.xyz
 * Versi 			: 1.0
 */
namespace sasak;

class Log {
	private static $resource;
	private static $source_name;
	private static $data;

	function __construct(){
		
	}

	function __destruct(){
		if(self::$resource){
			fclose(self::$resource);
		}
	}

	public function create($log = '', $file_name = ''){
		$fp = fopen($file_name, 'r a');
		if($fp){
			fwrite($fp, $log);
			fclose($fp);
		}
	}

	private static function open_source(){
		if(!defined('LOG_ENABLED')){
			die('Konstanta [LOG_ENABLED] belum didefinisikan..');
		}
		
		if(!is_writeable(LOG_PATH)){
			die("[write protected] : Direktori untuk log [". LOG_PATH ."] harus dapat dimanipulasi..");
		}
		$file_name = 'log_'. date('Y_m_d') .'.txt';
		self::$source_name = LOG_PATH . $file_name;
		$source = fopen(self::$source_name, 'a+w+');
		if($source){
			self::$resource = $source;
			return $source;
		}
	}

	public static function add($log_message = '', $enable_time = TRUE, $force_log = FALSE){
		if(!LOG_ENABLED && !$force_log) return;

		self::open_source();
		$data = '';
		if(self::$resource){
			$data = fgets(self::$resource);
			if(!empty($data)){
				$data = "\n";
			}
		}
		if($enable_time){
			$data.= now() ."\t\t";
		}
		$data.= $log_message;
		if(self::$resource){
			fwrite(self::$resource, $data);
			fclose(self::$resource);
		}
	}
}