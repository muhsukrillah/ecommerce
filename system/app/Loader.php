<?php
/**
 * Loader Class
 *
 * Dibuat Oleh 			: Muh. Sukrillah
 * Url 					: http://www.sukrillah.xyz
 * Versi 				: 1.0
 * Paket 				: SasakFramework
 */
namespace sasak;

use \sasak\Log;

class Loader {
	public $data = array();
	public $loaded_models = array();

	function __construct(){

		Log::add('Loader class initialized..');
	}

	private function get_args($args = array()){
		return $args;
	}

	function object($object_name = '', $param = NULL){
		$file_name = $object_name;
		if(strpos($object_name, '/')){
			$ex = explode('/', $object_name);
			$file_name = end($ex);
		}
		$path = OBJECT_PATH . $object_name .'.php';
		if(is_file($path) && file_exists($path)){
			$class_name = 'app\object_model\\'. $file_name;
		}else{
			$class_name = 'sasak\object_model\\'. $file_name;
		}

		if(isset($param)){
			return new $class_name($param);
		}else{
			return new $class_name;
		}
	}

	function model($path_name = ''){
		error_reporting(E_ALL);
		if(is_array($path_name)){
			foreach((array)$path_name as $path){
				$this->model($path);
			}
			return;
		}
		$file_name = $path_name . '_model.php';
		$path = MODEL_PATH . $file_name;

		$path_args = explode('/', $path_name);
		$class_name = end($path_args) .'_model';
		$object_name = 'app\\model\\'. $class_name;

		if(in_array($class_name, $this->loaded_models)){
			Log::add('Model ['. $class_name .'] has loaded.. it was ignored..');
			return;
		}
		
		if(file_exists($path) && is_file($path)){
			//include_once $path;
			
			/*if(!class_exists($object_name)){
				exit('[model] class : '. $class_name .' tidak ada');
			}*/
			$model = new $object_name();			
		}else{
			//$path = SYSTEM_PATH . 'models/'. $file_name;
			$object_name = '\sasak\model\\'. $class_name;
			$model = new $object_name();
		}
		$c =& Controller::instance();
		$c->$class_name =& $model;
		unset($model);
		$this->loaded_models[] = $class_name;
	}

	function library($pathname = '', $args = array(), &$var = NULL){
		if(is_array($pathname) && !empty($pathname)){
			foreach($pathname as $key => $value){
				$params = [];
				if(isset($args[$key])){
					$params = $args[$key];
				}
				$this->library($value, $params);
				unset($value);
				unset($params);
			}
			return;
		}
		$path_args = explode('/', $pathname);
		$file_name = $pathname . '.php';
		$path = LIB_PATH . $file_name;

		$class_name = 'app\\lib\\'. end($path_args);
		$namespace = end($path_args);
		$lib_args = isset($args['param']) ? $args['param'] : FALSE;
		$lib_namespace = isset($args['namespace']) ? $args['namespace'] : $namespace;
		
		if(is_file($path) && file_exists($path)){			
			//if(class_exists($class_name)){
				if(FALSE != $lib_args){
					$lib = new $class_name($lib_args);
				}else{
					$lib = new $class_name;
				}				
			/*}else{
				exit('class '. $class_name .' not found');
			}*/
		}else{
			$path = SYSTEM_PATH . 'lib/'. $file_name;
			$class_name = 'sasak\lib\\'. end($path_args);
			if(!is_file($path) || !file_exists($path)){
				Log::add('Loader : Error loading library ['. $path .']');
				Error('Error: library ['. $class_name .'] tidak tersedia..');
			}
			if(FALSE != $lib_args){
				$lib = new $class_name($lib_args);
			}else{
				$lib = new $class_name;
			}
		}
		if(isset($args['return']) && $args['return']){
			return $lib;
		}
		if($class_name != $lib_namespace){
			$c =& Controller::instance();
			$c->$lib_namespace =& $lib;
			unset($lib);
		}
	}

	function view(){
		$sm =& Controller::instance();
		if(!empty($sm->data)){
			$data = $sm->data;
		}
		$args = func_get_args();
		$file = isset($args[0]) ? $args[0] : '404.php';
		$param = isset($args[1]) ? $args[1] : [];
		$status_code = isset($args[2]) ? $args[2] : 200;
		if(is_array($data) && !empty($data)){
			$data = array_merge($data, $param);
		}else{
			$data = $param;
		}
		
		return View::load($file, $data, $status_code);
	}
}
/**
 * End of file : ./Loader.php
 * Lokasi File : /system/app/Loader.php
 */