<?php
/**
 * User Role / Privilige
 * 
 * Dibuat Oleh 					: Muh Sukrillah
 * Url 							: http://www.sukrillah.xyz
 * Versi 						: 1.0
 */
namespace sasak;
use \sasak\model\Role_Model;

class User_Role {
	/**
	 * role id
	 * @var 		$role_id
	 * @access 		private
	 * @type 		int	
	 */
	private $role_id = 0;
	/**
	 * role model instance
	 * @var 		$_model
	 * @access 		private
	 * @type 		object
	 */
	protected $_model = NULL;
	/**
	 * capabilities
	 * @var 		$_capabilities
	 * @access 		private
	 * @type 		array
	 */
	private $_capabilities = [];
	/**
	 * role name
	 * @var 		$_role_name;
	 * @access 		public
	 * @type 		string
	 */
	private $_role_name;
	/**
	 * role title
	 * @var 		$_role_title
	 * @access 		private
	 * @type 		string
	 */
	private $_role_title;
	/**
	 * konstruktor
	 * 
	 * @access 		public
	 * @param 		$id_role 	int 	=> role id
	 * @param 		$model 		object 	=> Role_Model instead
	 * @return 		null
	 *
	 */
	function __construct($id_role = 0, Role_Model $model){
		$this->role_id = $id_role;
		$this->_model = $model;
		//Log::add('id_role = '. $id_role);
		$data = $this->_model->get_role_by_id($id_role);
		//Log::add(print_r($data, 1));
		// get role name
		$role_name = (isset($data['role_name']) ? $data['role_name'] : 'anonymouse');
		$this->_role_name = $role_name;
		// get role title
		$role_title = (isset($data['role_title']) ? $data['role_title'] : 'Anonymouse');
		$this->_role_title = $role_title;

		$this->__get_capabilities();
	}
	/**
	 * get the capabilities / priviliges
	 *
	 * @param 		null
	 * @access 		private
	 * @return 		object
	 */
	private function __get_capabilities(){
		if(is_null($this->_model)){
			return array();
		}
		$capabilities = $this->_model->get_role_capabilities($this->role_id);
		//print_r($capabilities);
		foreach($capabilities as $key => $value){
			$capability = $value['capability'];
			$cap_value = (int) $value['value'];
			$desc = $value['description'];

			$this->_capabilities[$capability] = ['value' => $cap_value, 'desc' => $desc];
		}
		return $this;
	}
	/**
	 * get the role name
	 *
	 * @param 			null
	 * @access 			public
	 * @return 			string
	 */
	public function get_role_name(){
		return $this->_role_name;
	}
	/**
	 * get role title
	 *
	 * @param 			null
	 * @access 			public
	 * @return 			string
	 */
	public function get_role_title(){
		return $this->_role_title;
	}

	public function can(){
		$args = func_get_args();
		$capability = isset($args[0]) ? $args[0] : '';
		$param = isset($args[1]) ? $args[1] : NULL;

		$can = FALSE;
		if(isset($this->_capabilities[$capability])){
			if($param == 'desc' || $param === TRUE){
				return (isset($this->_capabilities[$capability]['desc']) ? $this->__get_capabilities[$capability]['desc'] : $capability);
			}
			if($this->_capabilities[$capability]['value']){
				$can = TRUE;
			}
		}
		if($param == 'desc' || $param === TRUE){
			return (isset($this->_capabilities[$capability]['desc']) ? $this->__get_capabilities[$capability]['desc'] : $capability);
		}
		return $can; //apply_filter('capability_'. $capability, $can, $param);
	}
}