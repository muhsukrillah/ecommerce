<?php
/**
 * Bootstrap file
 *
 * Dibuat Oleh          Muh. Sukrillah
 * Url                  http://www.sukrillah.xyz
 * Package              SasakFramework
 * Versi                1.0
 ******************************************************/
define('SASAK_START', microtime(true));
/**
 * Define versi system
 * 
 * @const               VERSION
 * @type                string
 * @value               1.0
 */
define("VERSION", "1.0");
/**
 * define the root path of system
 * @const               ROOT_PATH
 * @type                string path
 * @value               current directory
 */
define("ROOT_PATH", str_replace(DIRECTORY_SEPARATOR, "/", dirname(__FILE__)) .'/');
/**
 * define the system path
 * @const               SYSTEM_PATH
 * @type                string path
 * @value               ROOT_PATH . 'system/' [default]
 */
define("SYSTEM_PATH", ROOT_PATH . 'system/');
/**
 * define the app path
 *
 * @const               APP_PATH
 * @type                string path
 * @value               ROOT_PATH . 'app/' [default]
 */
define("APP_PATH", ROOT_PATH . 'app/');
/**
 * define the app controller path
 *
 * @const               CONTROLLER_PATH
 * @type                string path
 * @value               APP_PATH . 'controllers/' [default];
 */
define("CONTROLLER_PATH", APP_PATH . 'controllers/');
/**
 * define config path of app
 *
 * @const               CONFIG_PATH
 * @type                string path
 * @value               APP_PATH . 'configs/' [default]
 */
define("CONFIG_PATH", APP_PATH . 'configs/');
/**
 * define app helpers path
 *
 * @const               HELPER_PATH
 * @type                string path
 * @value               APP_PATH . 'helpers/' [default]
 */
define("HELPERS_PATH", APP_PATH . 'helpers/');
/**
 * define language path of app
 *
 * @const               LANG_PATH
 * @type                string path
 * @value               APP_PATH . 'langs/' [default]
 */
define("LANG_PATH", APP_PATH . 'langs/');
/**
 * define libs path of app
 *
 * @const               LIB_PATH
 * @type                string path
 * @value               APP_PATH . 'libs/' [default]
 */
define("LIB_PATH", APP_PATH . 'libs/');
/**
 * define models path of app
 *
 * @const               MODEL_PATH
 * @type                string path
 * @value               APP_PATH . 'models/' [default]
 */
define("MODEL_PATH", APP_PATH . 'models/');
/**
 * define objects path of app
 *
 * @const               OBJECT_PATH
 * @type                string path
 * @value               APP_PATH . 'objects/' [default]
 */
define("OBJECT_PATH", APP_PATH . 'objects/');
/**
 * define view path of app
 *
 * @const               VIEW_PATH
 * @type                string path
 * @value               APP_PATH . 'view/' [default]
 */
define("VIEW_PATH", APP_PATH . 'views/');
//require '../system/settings.php';
//require './options.php';
if(!file_exists(CONFIG_PATH . 'Config.php')){
    // tampilkan pesan error
    Error('[Error] App config.php not found!');
}
/**
 * panggil file config aplikasi
 */
include_once CONFIG_PATH . 'Config.php';
if(defined('USE_DATABASE') && USE_DATABASE){
    if(!file_exists(CONFIG_PATH . 'Database.php')){
        //  tampilkan pesan error 
        Error('[System Error] App Database config not found..');
    }
    include_once CONFIG_PATH . 'Database.php';
}
/**
 * memanggil file utama sistem
 *
 */
return include SYSTEM_PATH . 'app/Init.php';